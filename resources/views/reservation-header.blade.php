<div class="jumbotron">
    <div class="container-fluid">
        {{--<h1 class="main-title h2 text-center text-uppercase">{!! trans('reservationHeaderNav.bookARoom') !!}</h1>--}}
    </div>
</div>

<div id="anchor"></div>

<div class="container-fluid booking-breadcrumbs">
    <div class="row">
        <div class="col-sm-12">
            <div class="container">
                <!-- Five columns -->
                <!-- https://gist.github.com/JayHoltslander/9881361 -->
                <div class="fivecolumns">

                    <?php
                    $session=\Session::get('new_reservation');
                    ?>


                    <ul class="booking-breadcrumbs-navbar text-uppercase">
                        <li class="booking-breadcrumb-nav nav-complex col-sm-2 @if(Request::is('reservation/all_complexes')) active @endif">
                            <a href="{!! url('reservation/all_complexes') !!}"><span>1</span> {!! trans('reservationHeaderNav.navStep1') !!}</a>
                            @if(Request::is('reservation/all_complexes'))
                                <script>
                                    $('.nav-complex .hoverbox').remove();
                                </script>
                            @endif
                            <div class="hoverbox">
                                <div class="line"><span class="title">Your choice:</span> <span class="content">{!! \App\Classes\GlobalFunctions::getStepsNavInfo(1) !!}</span></div>
                            </div>
                        </li>

                        {{--DATE VIEW--}}
                        @if(isset($session['complex_id']))
                            <li class="booking-breadcrumb-nav nav-date col-sm-2 @if(Request::is('reservation/complex/*')) active @endif ">
                                <a href="{!! url('reservation/complex/'.$session['complex_id']) !!}"><span>2</span> {!! trans('reservationHeaderNav.navStep2') !!}</a>
                                <div class="hoverbox">
                                    <div class="line"><span class="title">Check in:</span> <span class="content">{!! @\App\Classes\GlobalFunctions::getStepsNavInfo(2)['from'] !!}</span></div>
                                    <div class="line"><span class="title">Check out:</span> <span class="content">{!! @\App\Classes\GlobalFunctions::getStepsNavInfo(2)['to'] !!}</span></div>
                                </div>
                            </li>
                        @else
                            <li class="booking-breadcrumb-nav nav-date col-sm-2 "><a><span>2</span> {!! trans('reservationHeaderNav.navStep2') !!}</a></li>
                        @endif


                        {{--ROOMS TYPE LIST--}}
                        @if(isset($session['complex_id']) && isset($session['date_from']) && isset($session['date_to']))
                            <?php
                            $from=str_replace('/','-',$session['date_from']);
                            $to=str_replace('/','-',$session['date_to']);
                            ?>
                            <li class="booking-breadcrumb-nav nav-rooms col-sm-2 @if(Request::is('reservation/date/*')) active @endif">
                                <a href="{!! url('reservation/date/1/'.$session['complex_id']).'/'.$from.'/'.$to !!}"><span>3</span>{!! trans('reservationHeaderNav.navStep3') !!}</a>
                                <div class="hoverbox">
                                    <div class="line"><span class="title left">You have reserved </span> <span class="mid">{!! \App\Classes\GlobalFunctions::getStepsNavInfo(3) !!}</span> <span class="content right">rooms</span></div>
                                </div>
                            </li>
                        @else
                            <li class="booking-breadcrumb-nav nav-rooms col-sm-2"><a><span>3</span>{!! trans('reservationHeaderNav.navStep3') !!}</a></li>
                        @endif

                        {{--PERSONAL DATA FORM--}}
                        @if(isset($session['rooms_data']) && isset($session['complex_id']) && isset($session['date_from']))
                            <li class="booking-breadcrumb-nav nav-name col-sm-2 @if(Request::is('reservation/personal_data')) active @elseif(Request::is('reservation/personal_data/organic')) active @endif"><a href="{!! url('reservation/personal_data') !!}"><span>4</span>{!! trans('reservationHeaderNav.navStep4') !!}</a></li>
                        @else
                            <li class="booking-breadcrumb-nav nav-name col-sm-2"><a><span>4</span>{!! trans('reservationHeaderNav.navStep4') !!}</a></li>
                        @endif

                        {{--CONFIRMATION--}}
                        @if(isset($session['personalData']) && isset($session['rooms_data']) && isset($session['date_from']))
                            <li class="booking-breadcrumb-nav nav-confirm col-sm-2 @if(Request::is('reservation/confirm') && !Request::isMethod('post')) active @endif"><a href="{!! url('reservation/confirm') !!}"><span>5</span> {!! trans('reservationHeaderNav.navStep5') !!}</a></li>
                        @else
                            <li class="booking-breadcrumb-nav nav-confirm col-sm-2"><a><span>5</span> {!! trans('reservationHeaderNav.navStep5') !!}</a></li>
                        @endif



                    </ul>


                </div>
            </div>
        </div>
    </div>
</div>

@section('nav-scripts')

<!-- AUTO SCROLL -->

<script>
$(document).ready(function() {
  var target = $('#anchor');
  var new_position = target.offset();
  window.scrollTo(new_position.left, new_position.top);
  return false;
  //scrollToElement($('#anchor'));
});
</script>

<!-- HOVERBOXES -->

<script>
$(document).ready(function(){

    $.each($('.booking-breadcrumb-nav'),function(){
        // find hoverbox
        var hoverbox = $(this).find('.hoverbox');
        // hide it initially
        hoverbox.hide();
        // show on hover only
        $(this).hover(
          function() {
            hoverbox.fadeIn( 300 );
          }, function() {
            hoverbox.fadeOut( 100 );
          }
        );
    });

});
</script>

@if(Request::is('reservation/all_complexes'))
    <script>
        $('.nav-complex .hoverbox').remove();
    </script>
@elseif(Request::is('reservation/complex/*'))
    <script>
        $('.nav-date .hoverbox').remove();
    </script>


@elseif(Request::is('reservation/date/*'))
    <script>
        $('.nav-rooms .hoverbox').remove();
    </script>


@endif

@endsection