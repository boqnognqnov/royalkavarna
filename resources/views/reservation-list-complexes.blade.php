@extends( 'master' )

@section( 'id' )booking @endsection

@section( 'content' )

	@include('reservation-header')

<div class="container complexes-menu">
	<div class="row complexes-menu-items">
		@foreach($complexes as $complex)
		<div class="col-sm-4 text-center">
			<h3 class="complex-title h2">{!! $complex['name_'.App::getLocale()] !!}</h3>
			<a href="{!! url('reservation/complex/'.$complex->id) !!}"><img src="{!! url(\App\Complex::$imagePath.$complex->image) !!}" alt="" class="img-responsive center-block"></a>
			<a href="{!! url('reservation/complex/'.$complex->id) !!}" class="btn btn-lg btn-custom btn-block btn-warning text-uppercase">{!! trans('reservationStep1.select') !!}</a>
		</div>
		@endforeach
	</div>
</div>

@endsection

@section( 'view-scripts' )

@yield('nav-scripts')

@endsection