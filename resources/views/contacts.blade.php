@extends( 'master' )

@section( 'id' )contacts @endsection

@section( 'content' )

	<div class="content-wrapper">
		@if(Session::has('successMessage'))
			<div class="alert alert-success">
				{!! Session::get('successMessage') !!}
			</div>
		@endif

		@if($errors -> any() )
			@foreach ($errors->all() as $error)
				<div class="alert alert-danger" style="width: 100%;">
					<button type="button" class="close" data-dismiss="alert">X</button>
					<p>{{ $error }}</p>
				</div>
			@endforeach
		@endif

<div class="jumbotron">
	<div id="gMap"></div>
</div>

<div class="container">

	<!-- full width on all screens -->
	<div class="row">
	  <div class="col-sm-12">
	    <h1 class="main-title h2 text-center text-uppercase">{!! trans('contacts.contactUs') !!}</h1>
	    <hr class="on-wide">
	  </div>
	</div>

	<div class="row core">
		<div class="left-side col-sm-8">

			{!! Form::open( array( 'action' => 'AboutController@contactSendMessage') ) !!}
				<div class="form-group form-group-lg col-sm-6">
					{!! Form::text('name', null, ['placeholder' => trans('contacts.name'), 'class'=>'form-control', 'required'=>'required']) !!}
				</div>

				<div class="form-group form-group-lg col-sm-6">
					{!! Form::email('email', null, ['placeholder' => 'E-mail', 'class'=>'form-control', 'required'=>'required']) !!}
				</div>

				<div class="form-group form-group-lg col-sm-12">
					{!! Form::textarea('text_message', null, ['placeholder' => trans('contacts.message'), 'class'=>'form-control', 'rows'=>'3', 'cols'=>'', 'required'=>'required']) !!}
				</div>

				<div class="form-group form-group-lg col-sm-12">
					{!! Form::submit(trans('contacts.sendMessButton'), ['class'=>'btn btn-lg btn-custom btn-block btn-primary']) !!}
				</div>
			{{ Form::close() }}
		</div>
		<div class="right-side col-sm-4">
			<address>
				<h4 class="address-title">{!! trans('contacts.mainAddress') !!}</h4>
				<p><strong>{{$about['contact_address_'.App::getLocale()]}}</strong></p>
				<p><strong>{!! trans('contacts.phone_fax') !!}</strong> <a href="tel:{{$about->contact_telephone}}">{{$about->contact_telephone}}</a></p>
				<p><strong>{!! trans('contacts.mobile') !!}</strong> <a href="tel:{{$about->contact_mobile}}">{{$about->contact_mobile}}</a></p>
				<p><strong>E-mail:</strong>
					<?php $emails = array_filter(explode(" ",$about->contact_emails ));?>
					@if(sizeof($emails) > 0)
						@foreach($emails as $email)
							<a href="mailto:{{$email}}">{{$email}}  </a><br>
						@endforeach
					@endif
			</address>
			<address>
				<h4 class="address-title">{!! trans('contacts.generalManager') !!}</h4>
				<p><strong>E-mail:</strong> <a href="mailto:{{$about->contact_general_email }}">{{$about->contact_general_email }}</a></p>
			</address>
		</div>
	</div>
</div>

@endsection

@section( 'view-scripts' )

<input type="hidden" id="coordinates-royal-bay" value="43.403783,28.244613">
<input type="hidden" id="coordinates-royal-cove" value="43.415101,28.354180">
<input type="hidden" id="coordinates-villa-vallenti" value="43.414142,28.355988">

<script>

	function initialize() {

    coordsFieldBay = document.getElementById('coordinates-royal-bay').value;
    coordsFieldCove = document.getElementById('coordinates-royal-cove').value;
    coordsFieldVilla = document.getElementById('coordinates-villa-vallenti').value;

    coordsStrArrayBay = coordsFieldBay.split(',');
    coordsStrArrayCove = coordsFieldCove.split(',');
    coordsStrArrayVilla = coordsFieldVilla.split(',');

    coordsIntArrayBay = [];
    coordsIntArrayCove = [];
    coordsIntArrayVilla = [];

    locations = [];

    coordsStrArrayBay.forEach(function(entry) {
      // console.log(parseFloat(entry));
      coordsIntArrayBay.push(parseFloat(entry));
    });
    locations.push(coordsIntArrayBay);

    coordsStrArrayCove.forEach(function(entry) {
      // console.log(parseFloat(entry));
      coordsIntArrayCove.push(parseFloat(entry));
    });
    locations.push(coordsIntArrayCove);

    coordsStrArrayVilla.forEach(function(entry) {
      // console.log(parseFloat(entry));
      coordsIntArrayVilla.push(parseFloat(entry));
    });
    locations.push(coordsIntArrayVilla);

    // console.log(locations);

	  var mapProp  = {
			center: new google.maps.LatLng(43.4109091,28.3020296),
			zoom: 14,
			scrollwheel: true,
			draggable: true,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};

		map  = new google.maps.Map(document.getElementById("gMap"), mapProp);

    var marker, i;

    for (i = 0; i < locations.length; i++) {
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][0], locations[i][1]),
        icon: "img/royal_pin.png",
        map: map
      });

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          // infowindow.setContent(locations[i][0]);
          // infowindow.open(map, marker);
          map.setCenter(this.getPosition());
          map.setZoom(17);
        }
      })(marker, i));
    }

  }
  google.maps.event.addDomListener(window, 'load', initialize);
</script>

@endsection
