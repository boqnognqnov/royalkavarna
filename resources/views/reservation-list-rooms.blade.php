@extends('master')

@section( 'id' )booking rooms @endsection

@section('content')

    @include('reservation-header')

    <div class="container booking-rooms room-list">
        <div class="row">
            <div class="col-sm-12">
                {!! trans('reservationStep3.pleaseChooseInfo') !!}
                <hr>
            </div>
        </div>
    </div>

    <div class="container">
      <div class="row room-list">
        <?php
          switch (count($roomTypes)) {
            // can't do the same as on pricelist because guest rows don't fit
            case '4':
              $colclass = 'col-sm-6 col-md-3';
              break;

            case '1':
              $colclass = 'col-sm-12';
              break;

            default:
              $colclass = 'col-sm-6';
              break;
          }
        ?>
        @foreach($roomTypes as $count=>$roomType)
          <div class="<?php echo $colclass; ?> booking-room-group" id="selected_room_type{!! $roomType['id'] !!}">
            <div class="flexbox room-item">
              <div class="flexitem room-image">
                <a href="{!! url('reservation/step_3/add_row_event/'.$roomType['id']) !!}" class="">
                  <img src="{!! asset(\App\ComplexRooms::$imagePath.$roomType['main_image']) !!}"class="img-responsive">
                </a>
              </div>
              <div class="flexitem room-title matchHeights" data-mh="room-titles">
                <h2 class="room-title text-uppercase">
                  {!! $roomType['type_'.App::getLocale()] !!}
                </h2>
                <h3 class="room-subtitle">
                  {!! $roomType['type_sub_'.App::getLocale()] !!}
                </h3>
              </div>
              <div class="flexitem room-desc matchHeights" data-mh="room-descriptions">
                {!! $roomType['description_'.App::getLocale()] !!}
              </div>
              <div class="flexitem room-cta">
                <a href="{!! url('room/'.$roomType['id']) !!}" class="room-more-link text-right">
                  <em>{!! trans('reservationStep3.viewDetails') !!}</em>
                </a>
                <hr>
                <div class="row">
                  <div class="col-xs-12">
                    <p class="selectedCTA text-danger pull-left">
                      <strong>Please choose the number of guests</strong>
                    </p>
                  </div>
                  <div class="col-xs-12">
                    <div class="container-fluid room-rows" id="reservation_inputs_{!! $roomType['id'] !!}">
                      @if($reservationData['is_inseparable']==1)
                        <div id="room1" class="row inputs-row equal-cols-2">
                          <div class="col-xs-3 padded">
                            <h4 class="room-res-inputs-title">
                              {!! trans('reservationStep3.room') !!} <span class="span_temp_counter"></span>
                            </h4>
                          </div>
                          <div class="col-xs-4">
                            <label for="label-adult">
                              {!! trans('reservationStep3.people') !!}
                            </label>
                            <select id="label-adult" class="form-control blue adult-selector people-selector">
                              @for($i=1; $i<=$roomType['beds'];$i++)
                                <option value="{!! $i !!}">
                                  {!! $i !!}
                                </option>
                              @endfor
                            </select>
                          </div>
                          <select class="child-selector" style="display: none"></select>
                        </div>
                      @endif
                    </div>
                  </div>
                  <div class="col-xs-12 text-right">
                    @if($reservationData['is_inseparable']!=1)
                        <button class="btn btn-warning btn-custom reserveRoom pull-right" name="reserveRoom" data-room-id="{!! $roomType['id'] !!}" data-rooms-available="{!! $availableRooms[$roomType['id']] !!}">
                          <span class="plus">+</span> {!! trans('reservationStep3.addRoom') !!}
                        </button>
                    @endif
                  </div>
                </div>
              </div>
              <div class="flexitem room-meta">
                <p class="fine-print hidden">
                  {!! trans('reservationStep3.extraBedPriceInfo') !!}
                </p>
                <p class="room-price">
                  <strong>{!! \App\Classes\GlobalFunctions::calculateCurrency($roomType['price']) !!} {!! trans('reservationStep3.currency') !!}</strong> <span class="small">{!! trans('reservationStep3.perNight') !!}</span>
                </p>
              </div>
            </div>
          </div>
        @endforeach
      </div>
    </div>

    <div class="container reservation-table">
        <div class="row" id="total_data_table">
            <div class="col-md-12 text-right">
                <table class="table">
                    <tbody>
                    </tbody>
                </table>
                <p class="final_price_with_booking text-primary"></p>
                <a href="#" class="sendReservationApi btn-next">{!! trans('reservationStep3.next') !!}
                    <span class="arrow">&rarr;</span></a>
            </div>
        </div>
    </div>

    <!-- reservation input lines prototype -->
    <div class="reservation_inputs_template" style="display:none;">
        <div class="flexbox row equal-cols-2">
            <div class="flexitem padded">
                <h4 class="room-res-inputs-title">{!! trans('reservationStep3.roomJS') !!} <span class="span_temp_counter"></span></h4>
            </div>
            <div class="flexitem">
                <label for="label-adult">{!! trans('reservationStep3.adultsJS') !!}</label>
                <select id="label-adult" class="form-control blue adult-selector">
                    <option value="0">0</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                </select>
            </div>
            <div class="flexitem">
                <label for="label-child">{!! trans('reservationStep3.childsJS') !!}</label>
                <select id="label-child" class="form-control blue child-selector">
                    <option value="0">0</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                </select>
            </div>
            <div class="flexitem padded">
                <button type="button" class="close remove_room_row" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
            </div>
        </div>
    </div>

    <!-- reservation data table prototype -->
    <div class="reservation_data_template" style="display:none;">
        <table>
            <tbody>
            <tr class="row-first">
                <td class="td_rowspan row-label">{!! trans('reservationStep3.youAreBooking') !!}</td>
                <td class="row-label">{!! trans('reservationStep3.roomType') !!}</td>
                <td class="row-label">{!! trans('reservationStep3.adult') !!}</td>
                <td class="row-label">{!! trans('reservationStep3.child') !!}</td>
                <td class="row-label">{!! trans('reservationStep3.days') !!}</td>
                <td class="row-label">{!! trans('reservationStep3.extraBed') !!}</td>
                <td class="row-label">{!! trans('reservationStep3.price') !!}</td>
            </tr>
            </tbody>
        </table>
    </div>

    <input type="hidden" name="is_inseparable_js" value="{!! $reservationData['is_inseparable'] !!}">

    <input type="hidden" name="currencyName" value="{!! trans('reservationStep3.currency') !!}">
    <input type="hidden" name="currencyCoef" value="{!! \App\Classes\GlobalFunctions::getCoef() !!}">

    <input type="hidden" name="tokenJs" value="{{ csrf_token() }}">

    @if(\Session::get('selected_room'))
        <input type="hidden" name="selected_room" value="{!! \Session::get('selected_room') !!}">
    @endif

    @if(\Session::get('reservation_inputs_ankers'))
        <input type="hidden" name="reservation_inputs_ankers" value="{!! \Session::get('reservation_inputs_ankers') !!}">
    @endif

@endsection

@section('view-scripts')

    @yield('nav-scripts')
    @if ($errors -> any())
        @foreach($errors -> all() as $error)
            <input type="hidden" id="error" value="{!! $error !!}">
            <script>
                $(document).ready(function () {
                    var errormsg = $('#error').val();
                    $.notify({
                        // options
                        icon: 'glyphicon glyphicon-warning-sign',
                        message: errormsg
                    }, {
                        // settings
                        type: 'warning',
                        timer: 0,
                        allow_dismiss: true
                    });
                });
            </script>
        @endforeach
    @endif
    <script src="{!! asset('js/reservationList.js') !!}"></script>
    <script>
    function scrollToElement(ele) {
        $(window).scrollTop(ele.offset().top).scrollLeft(ele.offset().left);
    }

    $(document).ready(function () {
        var roomAnkersId = $('input[name="reservation_inputs_ankers"]').val();
        scrollToElement($('#'+roomAnkersId));
    });

    $(function() {
  		$('.room-titles.matchHeights').matchHeight();
      $('.room-desc.matchHeights').matchHeight(
        {
            byRow: true
        }
      );
  	});
</script>

@endsection
