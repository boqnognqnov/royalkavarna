<?php
$complexes= \App\Complex::all();
?>

@extends( 'master' )

@section( 'id' )notfound @endsection

@section( 'content' )

<div class="container core text-center">
	<div class="row">
		<div class="col-sm-12">
			<h1 class="main-title text-uppercase">404 Page</h1>
			<p>Sorry, we couldn't find what you are looking for!</p>
			<a href="/" title="Back to home page" class="text-uppercase">Back to homepage</a>
		</div>
	</div>
</div>

@endsection

@section( 'view-scripts' )



@endsection