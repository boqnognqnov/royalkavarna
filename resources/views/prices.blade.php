@extends( 'master' )

@section( 'id' )complex pricelist @endsection

@section( 'content' )

    <div class="complex-subnav container-fluid hidden-lg">
        <div class="row">
            <div class="col-sm-12 col-md-8 col-md-offset-2">
                <!-- current subpage name should be here dyniamically -->
                <button class="btn btn-lg btn-block btn-primary text-uppercase inner-pages-nav-trigger collapsed"
                        type="button" data-toggle="collapse" data-target="#inner-pages-nav" aria-expanded="false"
                        aria-controls="inner-pages-nav">
                    {!! trans('NavPromotionPricesGuestBook.priceList') !!} <span class="caret"></span>
                </button>
                <!-- the rest of the subpages should be in the UL -->
                <ul id="inner-pages-nav" class="inner-pages-nav nav nav-pills nav-justified text-uppercase collapse">
                    <li role="presentation"><a
                                href="{!! url('complex/'. $complex->id ) !!}">{!! trans('NavPromotionPricesGuestBook.theComplex') !!}</a>
                    </li>
                    <li role="presentation"><a
                                href="{!! url('complex/'. $complex->id ) . '/promotions'!!}">{!! trans('NavPromotionPricesGuestBook.promotions') !!}</a>
                    </li>
                    <li role="presentation"><a
                                href="{!! url('complex/'. $complex->id ) . '/guestbook'!!}">{!! trans('NavPromotionPricesGuestBook.guestBook') !!}</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container complex-nav">
        <div class="row">
            <div class="col-sm-12">
                <div class="breadcrumbs">
                    <ol class="breadcrumb">
                        <li><a href="{!! url('/' ) !!}">{!! trans('NavPromotionPricesGuestBook.home') !!}</a></li>
                        <li>
                            <a href="{!! url('complex/'. $complex->id ) !!}">{{ $complex['name_'.App::getLocale()] }}</a>
                        </li>
                        <li class="active">{!! trans('NavPromotionPricesGuestBook.priceList') !!}</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="row visible-lg-block">
            <div class="col-sm-12 col-md-8 col-md-offset-2">
                <ul class="nav nav-pills nav-justified text-uppercase">
                    <li role="presentation"><a
                                href="{!! url('complex/'. $complex->id ) !!}">{!! trans('NavPromotionPricesGuestBook.theComplex') !!}</a>
                    </li>
                    <li role="presentation" class="active"><a
                                href="{!! url('complex/'. $complex->id .'/prices') !!}">{!! trans('NavPromotionPricesGuestBook.priceList') !!}</a>
                    </li>
                    <li role="presentation"><a
                                href="{!! url('complex/'. $complex->id ) . '/promotions'!!}">{!! trans('NavPromotionPricesGuestBook.promotions') !!}</a>
                    </li>
                    <li role="presentation"><a
                                href="{!! url('complex/'. $complex->id ) . '/guestbook'!!}">{!! trans('NavPromotionPricesGuestBook.guestBook') !!}</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-8 col-md-offset-2">
                <h1 class="main-title text-center text-uppercase">{!! trans('priceList.pricePageTitle') !!}</h1>
                <hr class="on-wide">
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row room-list">
            <?php
            switch (count($roomTypes)) {
                case '4':
                    $colclass = 'col-sm-6 col-md-3';
                    break;

                case '1':
                    $colclass = 'col-sm-12';
                    break;

                default:
                    $colclass = 'col-sm-6';
                    break;
            }
            ?>
            @foreach($roomTypes as $count=>$roomType)
                <div class="<?php echo $colclass; ?> room-list-item">
                    <div class="flexbox room-item">
                        <div class="flexitem room-image">
                            <a href="{!! url('room/'.$roomType['id']) !!}"
                               title="{!! $roomType['type_'.\App::getLocale()] !!}">
                                <img src="{!! asset(\App\ComplexRooms::$imagePath.$roomType['main_image']) !!}"
                                     class="img-responsive">
                                <p class="room-price">
                                    <span class="top">{!! trans('priceList.startingFrom') !!}</span> <span
                                            class="text-uppercase"><strong>{{ $roomType['price'] }} BGN</strong></span>
                                    <span class="small">{!! trans('priceList.perNight') !!}</span>
                                </p>
                            </a>
                        </div>
                        <div class="flexitem room-title matchHeights" data-mh="room-titles">
                            <h2 class="room-title text-uppercase">
                                {!! $roomType['type_'.\App::getLocale()] !!}
                            </h2>
                            <h3 class="room-subtitle">
                                {!! $roomType['type_sub_'.\App::getLocale()] !!}
                            </h3>
                        </div>
                        <div class="flexitem room-desc matchHeights" data-mh="room-descriptions">
                            {!! $roomType['description_'.\App::getLocale()] !!}
                        </div>
                        <div class="flexitem room-meta">
                            <hr>
                            <div class="row">
                                <div class="col-xs-6 col-sm-6 col-md-4 col-lg-6">
                                    <a href="{!! url('room/'.$roomType['id']) !!}" class="room-more-link text-left">
                                        <em>{!! trans('priceList.viewDetails') !!}</em>
                                    </a>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-8 col-lg-6 text-right">
                                    <a href="{!! url('reservation/complex/'.$complex->id.'/'.$roomType['id']) !!}"
                                       class="btn btn-custom btn-block btn-warning">
                                        {!! trans('priceList.bookNowButton') !!}
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>


    </div>

    <div class="container fine-print">
        <div class="row">
            <div class="col-sm-12">
                <p>{!! trans('priceList.priceInfo') !!}</p>
            </div>
        </div>
    </div>

@endsection

@section( 'view-scripts' )

    @include('internal-slider-script')

    <script>
        $(function () {
            $('.room-title.matchHeights').matchHeight();
            $('.room-desc.matchHeights').matchHeight(
                    {
                        byRow: true
                    }
            );
        });
    </script>

@endsection
