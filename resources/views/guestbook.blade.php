@extends( 'master' )

@section( 'id' )complex guestbook @endsection

@section( 'content' )

<style>body {background-image:url('{!! asset(\App\Complex::$bgPath.$complex->guest_book_background) !!}') !important;}</style>

<div class="complex-subnav container-fluid hidden-lg">
	<div class="row">
		<div class="col-sm-12 col-md-8 col-md-offset-2">
			<!-- current subpage name should be here dyniamically -->
			<button class="btn btn-lg btn-block btn-primary text-uppercase inner-pages-nav-trigger collapsed" type="button" data-toggle="collapse" data-target="#inner-pages-nav" aria-expanded="false" aria-controls="inner-pages-nav">
			  {!! trans('NavPromotionPricesGuestBook.guestBook') !!} <span class="caret"></span>
			</button>
			<!-- the rest of the subpages should be in the UL -->
			<ul id="inner-pages-nav" class="inner-pages-nav nav nav-pills nav-justified text-uppercase collapse">
				<li role="presentation"><a href="{!! url('complex/'. $complex->id ) !!}">{!! trans('NavPromotionPricesGuestBook.theComplex') !!}</a></li>
				<li role="presentation"><a href="{!! url('complex/'. $complex->id .'/prices') !!}">{!! trans('NavPromotionPricesGuestBook.priceList') !!}</a></li>
				<li role="presentation"><a href="{!! url('complex/'. $complex->id ) . '/promotions'!!}">{!! trans('NavPromotionPricesGuestBook.promotions') !!}</a></li>
			</ul>
		</div>
	</div>
</div>

<div class="container complex-nav">
	{{--<div class="row">
		<div class="col-sm-12">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
					<li><a href="{!! url('/' ) !!}">Home</a></li>
					<li><a href="{!! url('complex/'. $complex->id ) !!}">{{ $complex['name_'.App::getLocale()] }}</a></li>
					<li class="active">Guestbook</li>
				</ol>
			</div>
		</div>
	</div>--}}
	<div class="row visible-lg-block">
		<div class="col-sm-12 col-md-8 col-md-offset-2">
			<ul class="nav nav-pills nav-justified text-uppercase">
				<li role="presentation"><a href="{!! url('complex/'. $complex->id ) !!}">{!! trans('NavPromotionPricesGuestBook.theComplex') !!}</a></li>
				<li role="presentation"><a href="{!! url('complex/'. $complex->id .'/prices') !!}">{!! trans('NavPromotionPricesGuestBook.priceList') !!}</a></li>
				<li role="presentation"><a href="{!! url('complex/'. $complex->id ) . '/promotions'!!}">{!! trans('NavPromotionPricesGuestBook.promotions') !!}</a></li>
				<li role="presentation" class="active"><a>{!! trans('NavPromotionPricesGuestBook.guestBook') !!}</a></li>
			</ul>
		</div>
	</div>
</div>

<div class="container-fluid book hidden-xs hidden-sm">
	<div class="row">
		<div class="col-sm-12">

			<!-- FLIPBOOK -->
			<div id="flipbook">
				<div class="hard"><img src="{!! asset(\App\Complex::$bookPath.$complex->guest_book_color) !!}" alt=""></div>
				@foreach($posts as $onePost)

				<div class="hard text-center"><img src="{!! asset('img/logo-element.png') !!}" alt="" class="centerY"></div>
				<div class="text-center">
					<div class="padded">
						<div class="avatar"><img src="{!! asset(\App\ComplexGuestBook::$path.$onePost->image) !!}" alt="" class="img-circle"></div>
						{{--<blockquote>Your bones don't break, mine do. That's clear. Your cells react to bacteria and viruses differently than mine. You don't get sick, I do. That's also clear. But for some reason, you and I react the exact same way to water.</blockquote>--}}
						{{--<p>We swallow it too fast, we choke. We get some in our lungs, we drown. However unreal it may seem, we are connected, you and I. We're on the same curve, just on opposite ends.</p>--}}
						<blockquote>
							{!! $onePost->message !!}
						</blockquote>
						<p class="author">{!! $onePost->name !!}</p>
						<p class="jobtitle">	{!! $onePost->position !!}</p>
					</div>
				</div>

				@endforeach

				<div class="hard"></div>
				<div class="hard"></div>
			</div>

		</div>
	</div>
</div>

<div class="container hidden-md hidden-lg">
	<div class="row">
		<div class="col-sm-12">
			@foreach($posts as $onePost)
			<div class="well text-center">
				<div class="padded">
					<div class="avatar"><img src="{!! asset(\App\ComplexGuestBook::$path.$onePost->image) !!}" alt="" class="img-circle"></div>
					{{--<blockquote>Your bones don't break, mine do. That's clear. Your cells react to bacteria and viruses differently than mine. You don't get sick, I do. That's also clear. But for some reason, you and I react the exact same way to water.</blockquote>--}}
					{{--<p>We swallow it too fast, we choke. We get some in our lungs, we drown. However unreal it may seem, we are connected, you and I. We're on the same curve, just on opposite ends.</p>--}}
					<blockquote>
						{!! $onePost->message !!}
					</blockquote>
					<p class="author">{!! $onePost->name !!}</p>
					<p class="jobtitle">	{!! $onePost->position !!}</p>
				</div>
			</div>
			@endforeach
		</div>
	</div>
</div>

<div class="container form">
	<div class="row">
		<div class="col-sm-12 col-md-10 col-md-offset-1">
			{!! Form::open( array( 'action'=>'GuestBooksController@addPosts','id' => 'guestbook-form', 'files' => true ) ) !!}
			{!! Form::hidden('complex_id',$complex->id) !!}
					<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
						    {!! Form::text( 'name', null, array( 'class' => 'form-control', 'id' => 'gbform-name', 'placeholder' => trans('guestBook.ph_name') ) ) !!}
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							{!! Form::text( 'email', null, array( 'class' => 'form-control', 'id' => 'gbform-email', 'placeholder' => trans('guestBook.ph_email') ) ) !!}
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							{!! Form::text( 'position', null, array( 'class' => 'form-control', 'id' => 'gbform-job', 'placeholder' => trans('guestBook.ph_profession') ) ) !!}
						</div>
					</div>
					<div class="col-sm-6 file-upload">
						<div class="form-group">
							{!! Form::text('photo-name', null, array('class'=>'form-control feedback', 'placeholder'=>trans('guestBook.ph_photo'), 'readonly' => 'readonly')) !!}
							{!! Form::file('image', array('class'=>'form-control uploader')) !!}
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="form-group">
							{!! Form::textarea( 'message', null, array( 'class' => 'form-control', 'id' => 'gbform-message', 'placeholder' => trans('guestBook.ph_message') ) ) !!}
							<span class="help-block" style="color: red">{!! trans('guestBook.maxCharLenght') !!} <span class="char-lenght"></span> {!! trans('guestBook.characters') !!} <span class="char-left"></span><span class="spanChLeft"> {!! $validations['chars_left'] !!}</span></span>

						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="form-group">
							{!! Form::submit(trans('guestBook.sendOpinion'), array('class' => 'btn btn-block btn-custom btn-primary text-uppercase') ) !!}
						</div>
					</div>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>

<input type="hidden" name="tooManyChars" value="{!! $validations['tooManyChars'] !!}">
@endsection

@section( 'view-scripts' )


	@if(Session::has('successMessage'))
		<input type="hidden" id="error" value="{!! Session::get('successMessage') !!}">
		<script>
			$(document).ready(function () {
				var errormsg = $('#error').val();
				$.notify({
					// options
					icon: 'glyphicon glyphicon-warning-sign',
					message: errormsg
				}, {
					// settings
					type: 'success',
					timer: 0,
					allow_dismiss: true
				});
			});
		</script>
	@endif

			<!-- ERROR HANDLING -->
		<!-- storing error sources and messages in hidden inputs for use in jQuery -->
		@if($errors -> any() )
				<!-- collect the names of elements which threw errors -->
		<!-- holy grail of Laravel error catching - improve if you can -->
		@foreach ($errors->getBag('default')->keys() as $elementName)
			<input type="hidden" class="error-src" value="{{ $elementName }}">
			@endforeach
					<!-- collect the error messages -->
			@foreach ($errors->all() as $error)
				<input type="hidden" class="error-message" value="{{ $error }}">
			@endforeach
			@endif

			<script>
				$(document).ready(function () {
					// loop through hidden fields with error messages
					$('.error-message').each(function () {
						// show an alert with the message
						$.notify({
							// options
							icon: 'glyphicon glyphicon-remove',
							message: $(this).attr('value')
						}, {
							// settings
							type: 'danger',
							timer: 0,
							allow_dismiss: true
						});
					});
					// loop through hidden fields with element names
					$('.error-src').each(function () {
						// construct the selector to target faulty inputs, eg. [name="firstname"]
						var selector = '[name="' + $(this).attr('value') + '"]';
						// find its 'form-group' parent - that's where the validation classes must be applied
						$(selector).parent('.form-group').addClass('has-error');
					});
				});
			</script>
			<!-- END OF ERROR HANDLING -->

	<script>
		var lenght = 500;

		$(document).ready(function () {
			$('.char-lenght').text(lenght);
			$('.char-left').text(lenght);
			calcLeftChars();
			textAreaKeyUpEvent();
		});
		function textAreaKeyUpEvent() {
			$('#gbform-message').on('change, keyup', function (event) {
				calcLeftChars();
			});
		}

		function calcLeftChars() {
			var tempLenght = $('#gbform-message').val().length;
			var result=lenght - tempLenght;
			if(result<0){
				$('.char-left').text($('input[name="tooManyChars"]').val());
				$('.spanChLeft').empty();
			}else{
				$('.char-left').text(result);
			}

		}


	</script>

<!-- TURN.JS -->
<script src="{!! asset('turnjs4/lib/turn.min.js') !!}"></script>

<script type="text/javascript">
	$("#flipbook").turn({
		width: 960,
		height: 600,
		//autoCenter: true
	});
</script>

<script>
/* Detect file upload name */
$('.uploader').on('change',function(){
	var that = $(this).val();
	var icon = '<span class="glyphicon glyphicon-ok form-control-feedback"></span>';
	$('.feedback').val(that).parent('.form-group').addClass('has-feedback').append(icon);
});
</script>

@endsection