@extends( 'master' )

@section( 'id' )about @endsection

@section( 'content' )

<div class="jumbotron">
	<div class="container-fluid">
		
	</div>
</div>

<div class="container">

	<!-- full width on all screens -->
	<div class="row">
	  <div class="col-sm-12">
	    <h1 class="main-title h2 text-center text-uppercase">{!! trans('contacts.aboutUs') !!}</h1>
	    <hr class="on-wide">
	  </div>
	</div>

	<div class="row core">
		<div class="left-side col-sm-6">
			<img src="{{ asset(\App\About::$imagePath . $about->about_image) }}" alt="the three hotels" class="img-responsive center-block">
		</div>
		<div class="right-side col-sm-6">
			<img src="{{ asset(\App\About::$logoPath . $about->about_logo) }}" alt="company logo" class="center-block">
			<hr class="on-wide">
			{!! $about['about_text_'.App::getLocale()] !!}
		</div>
	</div>
</div>

@endsection

@section( 'view-scripts' )



@endsection