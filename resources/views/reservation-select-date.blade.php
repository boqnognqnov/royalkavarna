@extends( 'master' )

@section( 'id' )booking @endsection

@section( 'content' )

    @include('reservation-header')

    <div class="container datepickers">
        <div class="row datepickers-inputs">
            <div class="col-sm-10 col-md-offset-1">
                {!! Form::open( array( 'action' => 'ReservationsEntityController@setDateRange','id'=>'submitDateForm','class'=>'form-inline' ) ) !!}
                {!! Form::hidden('complex_id',$complex_id) !!}
                {!! Form::hidden('date_from') !!}
                {!! Form::hidden('date_to') !!}
                <div class="form-group">
                    {{ Form::label('datepicker-from', trans('reservationStep2.from'), array('class' => 'text-uppercase')) }}
                    <div class="input-group date">
                        {!! Form::text('datefrom', null, ['id' => 'datepicker-from', 'class'=>'form-control input-lg', 'required'=>'required', 'placeholder'=>'D-MM-YYYY', 'data-jumpto'=>'inputs-target']) !!}
                        <span class="input-group-addon"><img src="{!! asset('img/icon-calendar.png') !!}" alt=""></span>
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('datepicker-to', trans('reservationStep2.to'), array('class' => 'text-uppercase')) }}
                    <div class="input-group date">
                        {!! Form::text('dateto', null, ['id' => 'datepicker-to', 'class'=>'form-control input-lg', 'required'=>'required', 'placeholder'=>'D-MM-YYYY', 'data-jumpto'=>'inputs-target']) !!}
                        <span class="input-group-addon"><img src="{!! asset('img/icon-calendar.png') !!}" alt=""></span>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
        <div class="row datepickers-inline">
            <div class="col-sm-12">
                <div id="datepicker-target" class="text-center">
                    <div class="datepicker"></div>
                </div>
            </div>
            <div class="col-sm-10 col-md-offset-1 text-right">
                <a href="#" class="submitDateLink btn-next">{!! trans('reservationStep2.next') !!} <span
                            class="arrow">&rarr;</span></a>
            </div>
        </div>
    </div>


    <input type="hidden" name="old_date_from" value="{!! @$oldDateData['date_from'] !!}">
    <input type="hidden" name="old_date_to" value="{!! @$oldDateData['date_to'] !!}">

    <input type="hidden" name="availableFrom" value="{!! @$startDate !!}">
    <input type="hidden" name="availableTo" value="{!! @$endDate !!}">

    <input type="hidden" name="notavailable" value="no rooms available">


    @endsection

    @section( 'view-scripts' )

    @yield('nav-scripts')

            <!-- ERROR HANDLING -->

    @if($errors -> any() )
        @foreach ($errors->all() as $error)
            <input type="hidden" id="error" value="{!! $error !!}">
            <script>
                $(document).ready(function () {
                    var errormsg = $('#error').val();
                    $.notify({
                        // options
                        icon: 'glyphicon glyphicon-warning-sign',
                        message: errormsg
                    }, {
                        // settings
                        type: 'danger',
                        timer: 0,
                        allow_dismiss: true
                    });
                });
            </script>
        @endforeach

    @endif


    <script src="{!! asset('js/reservationDateStep2.js') !!}"></script>

@endsection