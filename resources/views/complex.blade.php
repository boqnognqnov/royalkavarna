@extends( 'master' )

@section( 'id' )complex main @endsection

@section( 'content' )

    <div class="complex-subnav container-fluid hidden-lg">
        <div class="row">
            <div class="col-sm-12 col-md-8 col-md-offset-2">
                <!-- current subpage name should be here dyniamically -->
                <button class="btn btn-lg btn-block btn-primary text-uppercase inner-pages-nav-trigger collapsed"
                        type="button" data-toggle="collapse" data-target="#inner-pages-nav" aria-expanded="false"
                        aria-controls="inner-pages-nav">
                    {!! trans('NavPromotionPricesGuestBook.theComplex') !!} <span class="caret"></span>
                </button>
                <!-- the rest of the subpages should be in the UL -->
                <ul id="inner-pages-nav" class="inner-pages-nav nav nav-pills nav-justified text-uppercase collapse">
                    <li role="presentation"><a
                                href="{!! url('complex/'. $complex->id .'/prices') !!}">{!! trans('NavPromotionPricesGuestBook.priceList') !!}</a>
                    </li>
                    <li role="presentation"><a
                                href="{!! url('complex/'. $complex->id ) . '/promotions'!!}">{!! trans('NavPromotionPricesGuestBook.promotions') !!}</a>
                    </li>
                    <li role="presentation"><a
                                href="{!! url('complex/'. $complex->id ) . '/guestbook'!!}">{!! trans('NavPromotionPricesGuestBook.guestBook') !!}</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    @include('internal-slider-jumbotron')

    <div class="container complex-nav">
        <div class="row visible-lg-block">
            <div class="col-sm-12 col-md-8 col-md-offset-2">
                <ul class="nav nav-pills nav-justified text-uppercase">
                    <li role="presentation" class="active"><a
                                href="{!! url('complex/'. $complex->id ) !!}">{!! trans('NavPromotionPricesGuestBook.theComplex') !!}</a>
                    </li>
                    <li role="presentation"><a
                                href="{!! url('complex/'. $complex->id .'/prices') !!}">{!! trans('NavPromotionPricesGuestBook.priceList') !!}</a>
                    </li>
                    <li role="presentation"><a
                                href="{!! url('complex/'. $complex->id ) . '/promotions'!!}">{!! trans('NavPromotionPricesGuestBook.promotions') !!}</a>
                    </li>
                    <li role="presentation"><a
                                href="{!! url('complex/'. $complex->id ) . '/guestbook'!!}">{!! trans('NavPromotionPricesGuestBook.guestBook') !!}</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="book-now-inflow"><button type="button" class="btn btn-lg btn-gold btn-free text-uppercase" onClick="location.href='{!! url('reservation/all_complexes') !!}'"><img src="{!! asset('img/icon-calendar.png') !!}" alt="calendar icon"> <strong>{!! trans('headerAndFooter.bookNowGoldButton') !!}</strong></button></div>

    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-8 col-md-offset-2">
                <h2 class="main-subtitle h3 text-center text-uppercase">{!! trans('complex.welcomeTo') !!}</h2>

                <h1 class="main-title text-center text-uppercase">{{ $complex['name_'.App::getLocale()] }}</h1>
                <hr class="on-wide">
                <div class="complex-desc">
                    <!-- start slipsum code -->
                    {!! $complex['about_'.App::getLocale()] !!}
                            <!-- please do not remove this line -->

                    <div style="display:none;">
                        <a href="http://slipsum.com">lorem ipsum</a></div>

                    <!-- end slipsum code -->
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid core">
        <div class="row">
            <div class="col-sm-12">
                <div class="container">
                    @if(sizeof($facilities) > 0)
                        <div class="row">
                            <div class="col-sm-12 col-md-10 col-md-offset-1">
                                <h2 class="title h3 text-center text-uppercase">{!! trans('complex.facilities') !!}</h2>
                                <hr class="on-wide">
                            </div>
                        </div>
                        <div class="row tabs-wrapper">
                            <div class="col-sm-12 col-md-10 col-md-offset-1">
                                <ul class="nav nav-tabs" role="tablist">
                                    <?php $first = true; ?>
                                    @foreach($facilities as $facility)
                                        <li @if($first)
                                            <?php $first = false; ?>
                                            class="active"
                                            @endif
                                            role="presentation">
                                            {{--<a href="#{{ str_replace(" ","",$facility->title_en) }}" aria-controls="hotel" role="tab" data-toggle="tab">--}}
                                            <a href="#{{ preg_replace("/[^A-Za-z0-9]/", '', $facility->title_en) }}"
                                               aria-controls="hotel" role="tab" data-toggle="tab">
                                                <img src="{!! asset( \App\Facilities::$logoPath . $facility->logo) !!}"
                                                     alt="{{ $facility['title_'.App::getLocale()] }}">
                                                {{ $facility['title_'.App::getLocale()] }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        <div class="row tab-content-wrapper tab-content hidden-sm hidden-xs">
                            <?php $first = true; ?>
                            @foreach($facilities as $facility)
                                {{--<div id="{{ str_replace(" ","",$facility->title_en) }}" class="tab-pane--}}
                                <div id="{{ preg_replace("/[^A-Za-z0-9]/", '', $facility->title_en) }}" class="tab-pane


								@if($first)
                                <?php $first = false; ?>
                                        active
                                @endif" role="tabpanel">
                                    <div class="col-sm-6">
                                        <img src="{!! asset( \App\Facilities::$imagePath . $facility->image) !!}"
                                             alt="{{ $facility['title_'.App::getLocale()] }}" class="img-responsive">
                                    </div>
                                    <div class="col-sm-6">
                                        {!! $facility['text_'.App::getLocale()] !!}
                                    </div>
                                </div>
                            @endforeach
                        </div>
                </div>
                @endif
            </div>
        </div>
    </div>


    <div class="container complex-location-section">
        <div class="row">
            <div class="col-sm-12 col-md-8 col-md-offset-2 text-center">
                <h2 class="title h3 text-center text-uppercase">{!! trans('complex.location') !!}</h2>
                <hr class="on-wide">
                <address>
                    <strong>{{ @$complex['address_'.App::getLocale()] }}</strong><br>
                    <strong>{!! trans('complex.phone') !!}</strong> <a
                            href="tel:{{ @$complex->phone }}">{{ @$complex->phone }}</a><br>
                    <strong>{!! trans('complex.mobilePhone') !!}</strong> <a
                            href="tel:{{ @$complex->mobile_phone }}">{{ @$complex->mobile_phone }}</a><br>
                    <strong>{!! trans('complex.reception') !!}</strong> <a
                            href="tel:{{ @$complex->reception_phone }}">{{ @$complex->reception_phone }}</a><br>
                    <strong>{!! trans('complex.email') !!}</strong> <a
                            href="mailto:{{ @$complex->email }}">{{ @$complex->email }}</a>
                </address>
            </div>
        </div>
    </div>

    <div class="container-fluid complex-map">
        <div class="row">
            <div id="map-canvas" class="embed-responsive"></div>
            <div class="infobox">
                <h3 class="title">{!! trans('complex.locationNearBy') !!}</h3>
                <hr>

                @if(\App::getLocale()=='bg')
                    {!! @$complex->location_information_bg !!}
                @elseif(\App::getLocale()=='en')
                    {!! @$complex->location_information_en !!}
                @elseif(\App::getLocale()=='ru')
                    {!! @$complex->location_information_ru !!}
                @else
                    {!! @$complex->location_information_ro !!}
                @endif
                {{--<p>{!! trans('complex.nearAirport') !!}</p>--}}

                {{--<p>{!! trans('complex.nearVarnaCity') !!}</p>--}}

                {{--<p>{!! trans('complex.nearBalckik') !!}</p>--}}

                {{--<p>{!! trans('complex.neatKavarna') !!}</p>--}}
            </div>
        </div>
    </div>

    <input type="hidden" name="mapPoinName" value="{{ $complex['name_'.App::getLocale()] }}">

    @endsection

    @section( 'view-scripts' )

            <!-- GOOGLE MAPS API -->
    <script src="https://maps.googleapis.com/maps/api/js"></script>

    <script type="text/javascript">
        function init_map() {
            var myOptions = {
                zoom: 17,
                scrollwheel: false,
                draggable: false,
                center: new google.maps.LatLng({!! @$complex->coordinates !!}, 17),
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };


            map = new google.maps.Map(document.getElementById("map-canvas"), myOptions);
            marker = new google.maps.Marker({
                map: map,
                icon: "img/map-marker.png",
                position: new google.maps.LatLng({!! @$complex->coordinates !!}, 17)
            });
            infowindow = new google.maps.InfoWindow({content: "<font style='color: #000;'><b>" + $('input[name="mapPoinName"]').val() + "</b></font>"});
            google.maps.event.addListener(marker, "click", function () {
                infowindow.open(map, marker);
            });
            infowindow.open(map, marker);
        }
        google.maps.event.addDomListener(window, 'load', init_map);
    </script>
    <!-- Google Maps API end -->

    <!-- CAROUSEL -->
    @include('internal-slider-script')

            <!-- RESPONSIVE TABS -->
    <script src="{!! asset('js/bootstrap-tabcollapse.js') !!}"></script>

    <script>
        $('.nav-tabs').tabCollapse({
            tabsClass: 'hidden-sm hidden-xs',
            accordionClass: 'visible-sm visible-xs tabs-collapsed'
        });
    </script>

@endsection
