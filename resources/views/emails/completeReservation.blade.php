От:  {!! $personalData['firstname'] !!} {!! $personalData['middle'] !!} {!! $personalData['lastname'] !!}<br>
Адрес: {!! $personalData['country'] !!} ,{!! $personalData['city'] !!}, {!! $personalData['address'] !!} <br>
Телефон: {!! $personalData['telephonecode'] !!}  <br>
{!! $personalData['telephone1'] !!}  <br>
{!! $personalData['telephone2'] !!}  <br>
Email:  {!! $personalData['email'] !!}  <br>

Комплекс: {!! $complex_name !!}<br>
Дата на резервацията: {!! $date_from !!} - {!! $date_to !!}<br>

Стаи и данни за настаняване:<br>
@foreach($rooms_data as $oneRoomType)
    @foreach($oneRoomType as $oneRoom)
        Стая: {!! $oneRoom['room_type'] !!} Възрастни: {!! $oneRoom['adult'] !!}  Деца: {!! $oneRoom['child'] !!}<br>
    @endforeach
@endforeach

<hr>
Обща цена: {!! $total !!} лв.<br>


Съобщение:  <br>
{!! $personalData['requirements'] !!}  <br>
