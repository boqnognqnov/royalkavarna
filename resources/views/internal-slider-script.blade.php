<script>
$(document).ready(function() {

  // Synchronized carousels
  var carousel = $("#carousel");
  var filmstrip = $("#filmstrip");

  carousel.owlCarousel({
      singleItem : true,
      slideSpeed : 1000,
      navigation: false,
      pagination:false,
      mouseDrag : false,
      touchDrag : false,
      autoHeight: true,
      afterAction : syncPosition,
      responsiveRefreshRate : 200,
  });

  filmstrip.owlCarousel({
      items                       : 10,
      //itemsDesktop      : [1199,{!! sizeof($gallery) !!}],
      itemsDesktop      : [1199,8],
      itemsDesktopSmall : [979,6],
      itemsTablet       : [768,4],
      itemsMobile       : [479,2],
      pagination:false,
      responsiveRefreshRate : 100,
      afterInit : function(el){
          el.find(".owl-item").eq(0).addClass("synced");
      }
  });

  function syncPosition(el){
      var current = this.currentItem;
      $("#filmstrip")
              .find(".owl-item")
              .removeClass("synced")
              .eq(current)
              .addClass("synced")
      if($("#filmstrip").data("owlCarousel") !== undefined){
          center(current)
      }
  }

  $("#filmstrip").on("click", ".owl-item", function(e){
      e.preventDefault();
      var number = $(this).data("owlItem");
      carousel.trigger("owl.goTo",number);
  });

  function center(number){
      var filmstripvisible = filmstrip.data("owlCarousel").owl.visibleItems;
      var num = number;
      var found = false;
      for(var i in filmstripvisible){
          if(num === filmstripvisible[i]){
              var found = true;
          }
      }

      if(found===false){
          if(num>filmstripvisible[filmstripvisible.length-1]){
              filmstrip.trigger("owl.goTo", num - filmstripvisible.length+2)
          }else{
              if(num - 1 === -1){
                  num = 0;
              }
              filmstrip.trigger("owl.goTo", num);
          }
      } else if(num === filmstripvisible[filmstripvisible.length-1]){
          filmstrip.trigger("owl.goTo", filmstripvisible[1])
      } else if(num === filmstripvisible[0]){
          filmstrip.trigger("owl.goTo", num-1)
      }

  }

  // Custom Navigation Events
  $(".carousel-custom-nav .next").click(function(e){
      e.preventDefault();
      $("#carousel").trigger('owl.next');
  });
  $(".carousel-custom-nav .prev").click(function(e){
      e.preventDefault();
      $("#carousel").trigger('owl.prev');
  });
  $(".filmstrip-custom-nav .next").click(function(e){
      e.preventDefault();
      $("#filmstrip").trigger('owl.next');
  });
  $(".owl-custom-nav .prev").click(function(e){
      e.preventDefault();
      $("#filmstrip").trigger('owl.prev');
  });

});
</script>

<script>
  function scrollToElement(ele) {
    $(window).scrollTop(ele.offset().top).scrollLeft(ele.offset().left);
  }

  /*$(document).ready(function() {
    scrollToElement($('#anchor'));
  });*/
</script>
