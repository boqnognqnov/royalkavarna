@extends('master')

@section( 'id' )booking success @endsection

@section('content')

    @include('reservation-header')

    <div class="container booking-success text-center">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="h3 text-center main-subtitle">{!! trans('reservationStep5.successSubtitle') !!}</h2>
                <p>{!! trans('reservationStep5.successMessage') !!}<a href="{!! url('/contacts') !!}">{!! trans('reservationStep5.successMessagePart2') !!}</a>
                </p>
            </div>
        </div>
    </div>

@endsection

@section('view-scripts')

@yield('nav-scripts')

@endsection