<div class="jumbotron clearfix">
  <div class="container-fluid">
    <div class="row carousel-wrapper">
      @if(sizeof($gallery) > 1)
      <div class="owl-custom-nav carousel-custom-nav">
        <a href="#" class="prev pull-left"><img src="{!! asset('img/arrow-left-blue.png') !!}" alt=""></a>
        <a href="#" class="next pull-right"><img src="{!! asset('img/arrow-right-blue.png') !!}" alt=""></a>
      </div>
      @endif
      <div id="carousel" class="owl-carousel">
        @if(sizeof($gallery) > 0)
        @foreach($gallery as $id=>$image)
        @if(!isset($isRoomSlider))
          <img src="{!! asset(\App\ComplexGalery::$path . $image->image) !!}" alt="" class="img-responsive">
        {{-- <div class="carousel-img" style="background-image: url('{!! asset(\App\ComplexGalery::$path . $image->image) !!}');"> --}}
          {{--<h1 class="carousel-slide-title">Royal Bay promotions</h1>--}}
        {{-- </div> --}}
        @else
          <img src="{!! asset(\App\RoomGalery::$path . $image->image) !!}" alt="" class="img-responsive">
        {{-- <div class="carousel-img" style="background-image: url('{!! asset(\App\RoomGalery::$path . $image->image) !!}');"> --}}
          {{--<h1 class="carousel-slide-title">Royal Bay promotions</h1>--}}
          {{-- </div> --}}
        @endif
        @endforeach
        @endif
      </div>
    </div>
    <div class="row filmstrip-wrapper">
      <div class="filmstrip-custom-nav">
        <a href="#" class="prev"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span></a>
        <a href="#" class="next"><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a>
      </div>
      <div id="filmstrip" class="filmstrip">
        @if(sizeof($gallery) > 0)
        @foreach($gallery as $image)
        @if(!isset($isRoomSlider))
        <a href="#" class="owl-carousel-control"><img src="{!! asset(\App\ComplexGalery::$pathSmall . $image->image) !!}" alt="carousel control"></a>
        @else
        <a href="#" class="owl-carousel-control"><img src="{!! asset(\App\RoomGalery::$pathSmall . $image->image) !!}" alt="carousel control"></a>
        @endif
        @endforeach
        @endif
      </div>
    </div>
  </div>
</div>
