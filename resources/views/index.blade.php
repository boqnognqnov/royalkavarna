@extends( 'master' )

@section( 'id' )index @endsection

@section( 'content' )

    <div class="jumbotron">
        <div class="container-fluid full-vertical">
            <div class="row">
                @if(sizeof($sliders) > 1)
                    <div class="owl-custom-nav">
                        <a href="#" class="prev pull-left"><img src="{!! asset('img/arrow-left-blue.png') !!}"
                                                                alt=""></a>
                        <a href="#" class="next pull-right"><img src="{!! asset('img/arrow-right-blue.png') !!}" alt=""></a>
                    </div>
                @endif
                <div id="owl-index" class="owl-carousel">
                    @if(sizeof($sliders) > 0)
                        @foreach($sliders as $slide)
                            <img src="{!! asset(\App\Slider::$imagePathEsc . $slide['image']) !!}" alt=""
                                 class="img-responsive">
                        @endforeach
                    @endif
                </div>
                {{-- <div class="overlay"></div> --}}
            </div>
        </div>
        <div class="container content">
            <div class="row">
                <div class="col-sm-12">
                    <!-- <hr class="on-wide"> -->
                    <p class="h2 card-nav-heading text-center"><em>{!! trans('index.chooseComplexes') !!}</em></p>
                </div>
            </div>
            <div class="row">
                @if(isset($complexes))
                    @foreach($complexes as $complex)
                        <div class="col-md-4 text-center complex-tab complex-id-{!! $complex->id !!}">
                            <div class="perimeter clearfix">
                                <h1 class="card-title text-uppercase"><a class="btn btn-lg btn-gold"
                                                                         href="{!! url('complex/'. $complex->id ) !!}">{{ $complex['name_'.App::getLocale()] }}</a>
                                </h1>
                                <a class="hover-card" href="{!! url('complex/'. $complex->id ) !!}"><img
                                            src="{!! asset( App\Complex::$imagePath.$complex->image ) !!}" alt=""></a>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>

@endsection

@section( 'view-scripts' )

    <script type="text/javascript" src="{!! asset('js/jquery.simpleWeather.min.js') !!}"></script>

    <script>

        $(document).ready(function () {

            $("#owl-index").owlCarousel({
                singleItem: true,
                pagination: false,
                mouseDrag: false,
                touchDrag: true,
                autoHeight: true,
                paginationSpeed: 300,
                rewindSpeed: 300
            });

            // Custom Navigation Events
            $(".owl-custom-nav .next").click(function (e) {
                e.preventDefault();
                $("#owl-index").trigger('owl.next');
            });
            $(".owl-custom-nav .prev").click(function (e) {
                e.preventDefault();
                $("#owl-index").trigger('owl.prev');
            });
        });

        // http://simpleweatherjs.com
        $(document).ready(function () {
            $("#weather").hide();
            $.simpleWeather({
                location: 'Kavarna',
                //location: 'Kavarna',
                //woeid: '834770', // Balchik
                woeid: '837023', // Kavarna
                unit: 'c',
                success: function (weather) {
                    html = '<button class="btn btn-lg btn-gold btn-free" onClick="window.open(\'https://weather.yahoo.com/bulgaria/dobrich/kavarna-837023/\'); return false;">'
                            + '<span class="text-primary"><strong>Kavarna:</strong></span>'
                            + ' '
                            + '<span class="text-primary"><strong> ' + weather.temp + '&deg;' + weather.units.temp + '</strong></span>'
                            + '</button>';
                    $("#weather").html(html).show('slide', {direction: 'left'}, 350);
                },
                error: function (error) {
                    //$("#weather").html('<p>'+error+'</p>');
                    console.log(error);
                    $("#weather").remove();
                }
            });
        });

    </script>

@endsection
