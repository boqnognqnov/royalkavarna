@extends('admin.master')
@section('content')

    <div class="content-wrapper">
        <section class="content">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Всички удобства</h3>
                    <div class="box-tools">
                        <a href="{!! url('admin/facilities/create') !!}" class="btn btn-block btn-success btn-flat">Добавяне на удобство</a>
                    </div>
                </div>
                <div class="box-body no-padding">
                    <table class="table table-striped">
                        <tr>
                            <th>#</th>
                            <th>Име</th>
                            <th>Иконка</th>
                            <th>Снимка</th>
                            <th>BG текст</th>
                            <th>Редакция</th>
                            <th>Изтрий</th>
                        </tr>
                        <?php $help_array = []?>
                        @foreach( $facilitiesData as $key => $facility )
                            @if( ! isset($help_array[$facility->complex_id]))
                                <?php $help_array[$facility->complex_id] = true ?>
                                <tr>
                                    <th colspan="7"><h2>{{ $facility->complex_name }}</h2></th>
                                </tr>
                            @endif
                            <tr>
                                <td>{!! $facility -> id !!}</td>
                                <td>{!! $facility -> title_bg !!}</td>
                                <td><img src="{!! asset( \App\Facilities::$logoPath . $facility->logo) !!}" alt="{{ $facility['title_'.App::getLocale()] }}"></td>
                                <td><img src="{!! asset( \App\Facilities::$imagePath . $facility->image) !!}" alt="{{ $facility['title_'.App::getLocale()] }}" class="img-responsive" width="200px"></td>
                                <td>{!! $facility -> text_bg !!}</td>
                                <td><a href="{!! url('admin/facilities/edit/' . $facility -> id ) !!}" class="btn btn-primary">Редакция</a></td>
                                <td><a href="{!! url('admin/facilities/delete/' . $facility -> id  ) !!}" class="btn btn-danger">Изтрий</a></td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </section>
    </div>

@endsection