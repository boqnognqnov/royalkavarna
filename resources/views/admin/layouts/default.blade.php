<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

        <title>RoyalsKavarna Administration</title>

        <link rel="stylesheet" href="{!! asset('panel/bootstrap/css/bootstrap.min.css') !!}">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <link rel="stylesheet" href="{!! asset('panel/dist/css/AdminLTE.min.css') !!}">
        <link rel="stylesheet" href="{!! asset('panel/dist/css/skins/skin-blue.min.css') !!}">
        <link rel="stylesheet" href="{!! asset('panel/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') !!}">

        <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>



        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>

    <body class="hold-transition
        @if( Request::is('admin') || Request::is('admin/*') )
                skin-blue sidebar-mini
        @elseif( Request::is('login') )
                login-page
        @elseif( Request::is('register') )
                register-page
        @endif">

        @yield('body')


        <script src="{!! asset('panel/bootstrap/js/bootstrap.min.js') !!}"></script>
        <script src="{!! asset('panel/dist/js/app.min.js') !!}"></script>
        <script src="{!! asset('panel/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.js') !!}"></script>


        <script>

//            $(function () {
//                $(".html5area").wysihtml5(
//                );
//            });




        </script>

        <script>
            $(function () {
                $(".html5area").wysihtml5({
                    useLineBreaks: true
                });
            });
        </script>


        @yield('after-js')

    </body>
</html>