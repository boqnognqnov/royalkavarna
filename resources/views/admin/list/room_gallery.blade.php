@extends('admin.master')
@section('content')

    <div class="content-wrapper">
        <section class="content">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Галерия на стая:</h3>
                </div>
                <div class="box-body no-padding">
                    <div>
                        {!! Form::open(array('action' => 'RoomsEntityController@setImageToGallery','class'=>'form-horizontal','files'=>true)) !!}
                        {!! Form::hidden('room_id',$roomId) !!}
                        {!! Form::file('image') !!}
                        {!! Form::submit('Upload',['class'=>'btn btn-success']) !!}
                        {!! Form::close() !!}
                    </div>

                    <div class="box-body no-padding">
                        @if(Session::has('successMessage'))
                            <div class="alert alert-success">
                                {!! Session::get('successMessage') !!}
                            </div>
                        @endif

                        @if($errors -> any() )
                            @foreach ($errors->all() as $error)
                                <div class="alert alert-danger" style="width: 100%;">
                                    <button type="button" class="close" data-dismiss="alert">?</button>
                                    <p>{{ $error }}</p>
                                </div>
                            @endforeach
                        @endif
                        {{--<table class="table table-striped">--}}

                        {{--@foreach( $galleryData as $image )--}}
                        {{--<tr class="extra-list-propery">--}}
                        {{--<td>{!! $image->id !!}</td>--}}
                        {{--<td><img src="{!! asset(\App\ComplexGalery::$path.$image->image) !!}" width="80px">--}}
                        {{--</td>--}}
                        {{--<td>{!! $image->image !!}</td>--}}

                        {{--<td>--}}
                        {{--{!! Form::button('Изтриване', ['class'=>'btn btn-danger imageDel','image_id_tag'=>$image -> id]) !!}--}}
                        {{--</td>--}}
                        {{--</tr>--}}
                        {{--@endforeach--}}
                        {{--</table>--}}
                        <div class="row">
                            @foreach( $gallery as $oneElement )

                                <div class="col-md-4 portfolio-item">
                                    <a href="#">
                                        <img class="img-responsive"
                                             src="{!! asset(\App\RoomGalery::$pathSmall.$oneElement['image']) !!}"
                                             alt="">
                                    </a>

                                    <br>
                                    <a href="{!! url('admin/complexes/room/gallery/'.$roomId.'/left/'.$oneElement['id']) !!}"
                                       class="glyphicon glyphicon-hand-left"></a>
                                    <button class="glyphicon glyphicon-remove-circle imageDel"
                                            image_id_tag="{!! $oneElement['id'] !!}"></button>
                                    <a href="{!! url('admin/complexes/room/gallery/'.$roomId.'/right/'.$oneElement['id']) !!}"
                                       class="glyphicon glyphicon-hand-right"></a>
                                    </br>

                                </div>
                            @endforeach
                        </div>
                    </div>

                    <br>

                    <div class="text-center">
                        <a href="{!! url('admin/complexes/'.$complexId.'/rooms') !!}"
                           class="btn btn-primary">Назад</a>
                    </div>
                </div>
            </div>

        </section>
    </div>

    <div style="display: none">
        {!! Form::open(array('action' => 'RoomsEntityController@destroyImage', 'id' => 'roomGalleryFormDel')) !!}
        {!! Form::submit('Изтриване', ['class'=>'btn btn-danger']) !!}
        {!! Form::hidden( 'image_id' ) !!}
        {!! Form::close() !!}
    </div>

    <script>

        $(document).ready(function () {

            $('.imageDel').on('click', function (event) {
                var $this = $(this);
                var id = $this.attr('image_id_tag');
                $('input[name="image_id"]').val(id);
                var r = confirm("Наистина ли желаете да изтриете записа");
                if (r == true) {
                    $('#roomGalleryFormDel').submit();
                }
            });
        });
    </script>

@endsection