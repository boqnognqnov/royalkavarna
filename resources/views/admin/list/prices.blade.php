@extends('admin.master')
@section('content')

    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <link rel="stylesheet" href="/resources/demos/style.css">
    <div class="content-wrapper">
        <section class="content">

            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Всички цени</h3>

                    <img src="{!! asset(\App\ComplexRooms::$imagePath.$room->main_image) !!}"
                             width="100%" height="350px">
                    <a href="{!! url('/admin/complexes/'.$room->complex_id.'/rooms' ) !!}" class="btn btn-primary">&laquo; Към стаите на комплекса</a>
                    <button type="button" id="add_room_price_button" class="btn btn-info"
                                                                 data-toggle="modal"
                                                                 data-target="#addPrice" > Добавете цена
                    </button>
                </div>
                <div class="box-body no-padding">
                    <table class="table table-striped">
                        <tr>
                            <th>#</th>
                            <th>Цена</th>
                            <th>От</th>
                            <th>До</th>
                            <th>Редакция</th>
                            <th>Изтрий</th>
                        </tr>
                        @foreach( $pricesData as $key => $prices )
                            <tr>
                                <td>{!! $prices -> id !!}</td>

                                <td>{!! number_format($prices -> price, 2, '.', '') !!}</td>
                                <td>{!! date('d / m / Y', strtotime($prices -> from_date)) !!}</td>
                                <td>{!! date('d / m / Y', strtotime($prices -> to_date)) !!}</td>
                                <td><a href="{!! url('admin/prices/edit/' . $prices -> id ) !!}" class="btn btn-primary">Редакция</a></td>
                                <td><a href="{!! url('admin/prices/delete/' . $prices -> id  ) !!}" class="btn btn-danger">Изтрий</a></td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>

        </section>
    </div>
    <div id="addPrice" class="modal fade cat1">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Резервиране на стая "<strong id="roomName"></strong>"</h4>
                </div>
                <div class="col-sm-12">
                    <div class="modal-body" id="add_price_modal_body">
                        {!! Form::open( array( 'action' => 'RoomsPricesEntityController@store' ) ) !!}

                        {!! Form::hidden('room_id', $room->id) !!}
                            <div class="form-group {!! $errors -> has('price') ? 'has-error' : '' !!}">
                                {!! Form::label( 'Цена за периода:', 'Цена за периода',['class'=>'col-sm-4 control-label'] ) !!}
                                <div class="col-sm-8">
                                {!! Form::text( 'price', null, array( 'class' => 'form-control' ) ) !!}
                                </div>
                                {!! $errors -> first( 'price', '<span class="help-block">:message</span>' ) !!}
                            </div>
                            <div class="form-group {!! $errors -> has('from_date') ? 'has-error' : '' !!}">
                                {!! Form::label('От дата','От дата',['class'=>'col-sm-4 control-label']) !!}
                                <div class="col-sm-8">
                                {!! Form::text( 'from_date',null,['class'=>'datepicker form-control'] ) !!}
                                </div>
                                {!! $errors -> first( 'from_date', '<span class="help-block">:message</span>' ) !!}
                            </div>

                            <div class="form-group {!! $errors -> has('to_date') ? 'has-error' : '' !!}">
                                {!! Form::label('До дата','До дата',['class'=>'col-sm-4 control-label']) !!}
                                <div class="col-sm-8">
                                {!! Form::text( 'to_date',null,['class'=>'datepicker form-control'] ) !!}
                                </div>
                                {!! $errors -> first( 'to_date', '<span class="help-block">:message</span>' ) !!}
                            </div>


                        <div class="form-group col-sm-5">
                        </div>
                        <div class="form-group col-sm-7">
                            {!! Form::submit('Запази', ['class'=>'btn btn-success']) !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Затвори</button>
                </div>
            </div>

        </div>
    </div>

    <script>
        $(function () {
            $(".datepicker").datepicker();
        });

    </script>
@endsection