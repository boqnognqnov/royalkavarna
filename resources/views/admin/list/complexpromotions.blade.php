@extends('admin.master')
@section('content')


    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script>
        $(function () {
            $(".datepicker").datepicker();
        });

        //        $( ".selector" ).datepicker({
        //            dateFormat: "yy-mm-dd"
        //        });
    </script>
    <style>
        .middletd{
            vertical-align: middle !important;
        }
    </style>


    <div class="content-wrapper">
        <section class="content">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Промоции за комплекса</h3>
                </div>
                <div class="box-body no-padding">

                    <a href='{{ url("/admin/complexes/$complex->id/promotions/create") }}' class="btn btn-primary">Нова промоция</a>


                    <table class="table table-striped">
                        <thead>
                        <th class="middletd">ID:</th>
                        <th class="middletd">Статус</th>
                        <th class="middletd">Промоция:</th>
                        <th class="middletd">Заглавие:</th>
                        <th class="middletd">Валидна от:</th>
                        <th class="middletd">Валидна до:</th>
                        <th class="middletd">Промяна</th>
                        <th class="middletd">Изтриване:</th>

                        </thead>
                        <tbody>
                        @foreach( $promotions as $promotion )
                            <tr>
                                <td class="middletd">{{$promotion->id}}</td>
                                <td class="middletd">
                                    @if(date('d-m-Y',strtotime($promotion->date_from)) <= date('d-m-Y',strtotime('now'))
                                        && date('d-m-Y',strtotime($promotion->date_to)) >= date('d-m-Y',strtotime('now')))
                                        <div class="label label-success">Active</div>
                                    @else
                                        <div class="label label-danger">Inactive</div>
                                    @endif
                                </td>
                                <td class="middletd"><img src="{!! asset(\App\ComplexPromotions::$imagePath.$promotion->image) !!}"
                                         width="100px"></td>
                                <td  class="middletd">{{$promotion->title_bg}}</td>
                                <td  class="middletd">{{date('d-m-Y', strtotime($promotion->date_from))}}</td>
                                <td class="middletd">{{date('d-m-Y', strtotime($promotion->date_to))}}</td>
                                <td class="middletd"><a class='btn btn-primary' href='{{ url("/admin/complexes/$complex->id/promotions/$promotion->id/update") }}' >Промяна</a></td>
                                <td class="middletd">
                                    {!! Form::open(array('url' => '/admin/complexes/'.$complex->id.'/promotions/'.$promotion->id.'/destroy')) !!}
                                    {!! Form::hidden('promotion_id',$promotion->id) !!}
                                    {!! Form::submit('Изтриване', ['class'=>'btn btn-danger']) !!}
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <br>

                    <div class="text-center">
                        <a href="{!! url('admin/complexes') !!}"
                           class="btn btn-primary">Назад</a>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection