@extends('admin.master')
@section('content')



    <div class="content-wrapper">
        <section class="content">
            <div class="box">
                @if(Session::has('successMessage'))
                    <div class="alert alert-success">
                        {!! Session::get('successMessage') !!}
                    </div>
                @endif

                @if($errors -> any() )
                    @foreach ($errors->all() as $error)
                        <div class="alert alert-danger" style="width: 100%;">
                            <button type="button" class="close" data-dismiss="alert">?</button>
                            <p>{{ $error }}</p>
                        </div>
                    @endforeach
                @endif

                <div class="box-header">
                    <h3 class="box-title">Всички екстри</h3>

                    <div class="box-tools">
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#createExtras">
                            Добавяне на екстра
                        </button>
                    </div>
                </div>
                <div class="box-body no-padding">


                    <table class="table table-striped">

                        <tr>
                            <th>№</th>
                            <th>Icon</th>
                            <th>Name</th>
                            <th></th>
                            <th></th>
                        </tr>
                        @foreach( $extras as $extra )
                            <tr class="extra-list-propery">
                                <td>{!! $extra->id !!}</td>
                                <td><img src="{!! asset(\App\Extras::$path.$extra->icon) !!}" width="80px"></td>
                                <td>{!! $extra->name_bg !!}</td>

                                <td>
                                    <button type="button" id="edit_extra_button" class="btn btn-success"
                                            data-toggle="modal"
                                            data-target="#editExtra" tag_extra_id="{!! $extra->id  !!}"
                                            tag_extra_icon="{!! $extra->icon  !!}"
                                            tag_extra_name_bg="{!! $extra->name_bg  !!}"
                                            tag_extra_name_en="{!! $extra->name_en  !!}"
                                            tag_extra_name_ru="{!! $extra->name_ru  !!}"
                                            tag_extra_name_ro="{!! $extra->name_ro  !!}"> Промени
                                    </button>
                                </td>
                                <td>
                                    {!! Form::button('Изтриване', ['name' => 'action[del]','class'=>'btn btn-danger extraDel','extra_id_tag'=>$extra -> id]) !!}
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </section>
    </div>

    <div class="modal fade" id="createExtras" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"> Добавяне на екстра</h4>
                </div>
                <div class="col-sm-12">
                    <div class="modal-body">
                        {!! Form::open(array('action' => 'ExtrasEntityController@setExtras','class'=>'form-horizontal','files'=>true)) !!}

                        <div class="form-group">
                            {!! Form::label('Икона','Икона',['class'=>'col-sm-2 control-label']) !!}
                            <div class="col-sm-10">
                                {!! Form::file( 'icon',null,['class'=>'form-control']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('Заглавие BG','Заглавие BG',['class'=>'col-sm-2 control-label']) !!}
                            <div class="col-sm-10">
                                {!! Form::text( 'name_bg',null,['class'=>'form-control'] ) !!}
                                {{--                                {!! Form::text('title',null,['class'=>'form-control']) !!}--}}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('Заглавие EN','Заглавие EN',['class'=>'col-sm-2 control-label']) !!}
                            <div class="col-sm-10">
                                {!! Form::text( 'name_en',null,['class'=>'form-control'] ) !!}
                                {{--                                {!! Form::text('title',null,['class'=>'form-control']) !!}--}}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('Заглавие RU','Заглавие RU',['class'=>'col-sm-2 control-label']) !!}
                            <div class="col-sm-10">
                                {!! Form::text( 'name_ru',null,['class'=>'form-control'] ) !!}
                                {{--                                {!! Form::text('title',null,['class'=>'form-control']) !!}--}}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('Заглавие RO','Заглавие RO',['class'=>'col-sm-2 control-label']) !!}
                            <div class="col-sm-10">
                                {!! Form::text( 'name_ro',null,['class'=>'form-control'] ) !!}
                                {{--                                {!! Form::text('title',null,['class'=>'form-control']) !!}--}}
                            </div>
                        </div>


                        <div class="form-group col-sm-6">
                            {!! Form::submit('Нова екстра', ['name' => 'action[del]','class'=>'btn btn-success']) !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Затвори</button>
                </div>
            </div>

        </div>
    </div>

    <div style="display: none">
        {!! Form::open(array('action' => 'ExtrasEntityController@destroyExtras', 'id' => 'extraFormDel')) !!}
        {!! Form::submit('Изтриване', ['class'=>'btn btn-danger']) !!}
        {!! Form::hidden( 'extra_id' ) !!}
        {!! Form::close() !!}
    </div>

    <div id="editExtra" class="modal fade cat1">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"> Добавяне на екстра</h4>
                </div>
                <div class="col-sm-12">
                    <div class="modal-body" id="edit_extra_modal_body">
                        {!! Form::open(array('action' => 'ExtrasEntityController@updateExtra','class'=>'form-horizontal','files'=>true)) !!}

                        {!! Form::hidden('extra_id') !!}

                        <div class="form-group">
                            {!! Form::label('Икона','Икона',['class'=>'col-sm-2 control-label']) !!}
                            <div class="col-sm-10">
                                {!! Form::file( 'icon',null,['class'=>'form-control']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('Заглавие BG','Заглавие BG',['class'=>'col-sm-2 control-label']) !!}
                            <div class="col-sm-10">
                                {!! Form::text( 'name_bg',null,['class'=>'form-control'] ) !!}
                                {{--                                {!! Form::text('title',null,['class'=>'form-control']) !!}--}}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('Заглавие EN','Заглавие EN',['class'=>'col-sm-2 control-label']) !!}
                            <div class="col-sm-10">
                                {!! Form::text( 'name_en',null,['class'=>'form-control'] ) !!}
                                {{--                                {!! Form::text('title',null,['class'=>'form-control']) !!}--}}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('Заглавие RU','Заглавие RU',['class'=>'col-sm-2 control-label']) !!}
                            <div class="col-sm-10">
                                {!! Form::text( 'name_ru',null,['class'=>'form-control'] ) !!}
                                {{--                                {!! Form::text('title',null,['class'=>'form-control']) !!}--}}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('Заглавие RO','Заглавие RO',['class'=>'col-sm-2 control-label']) !!}
                            <div class="col-sm-10">
                                {!! Form::text( 'name_ro',null,['class'=>'form-control'] ) !!}
                                {{--                                {!! Form::text('title',null,['class'=>'form-control']) !!}--}}
                            </div>
                        </div>


                        <div class="form-group col-sm-6">
                            {!! Form::submit('Промени', ['name' => 'action[del]','class'=>'btn btn-success']) !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Затвори</button>
                </div>
            </div>

        </div>
    </div>

    <script>

        $(document).ready(function () {

            $('.extraDel').on('click', function (event) {
                var $this = $(this);
                var id = $this.attr('extra_id_tag');
                $('input[name="extra_id"]').val(id);
                var r = confirm("Наистина ли желаете да изтриете записа");
                if (r == true) {
                    $('#extraFormDel').submit();
                }
            });
        });
    </script>

    <script>

        $(document).ready(function () {
            $('button[id="edit_extra_button"]').on('click', function (event) {

                var $this = $(this);

                $('#edit_extra_modal_body input[name="extra_id"]').val($this.attr('tag_extra_id'));

                $('#edit_extra_modal_body input[name="name_bg"]').val($this.attr('tag_extra_name_bg'));
                $('#edit_extra_modal_body input[name="name_en"]').val($this.attr('tag_extra_name_en'));
                $('#edit_extra_modal_body input[name="name_ru"]').val($this.attr('tag_extra_name_ru'));
                $('#edit_extra_modal_body input[name="name_ro"]').val($this.attr('tag_extra_name_ro'));

//                $('#edit_extra_modal_body img[id="icon"]').attr('src', $this.attr('tag_extra_icon'));
            });
        });

    </script>





@stop