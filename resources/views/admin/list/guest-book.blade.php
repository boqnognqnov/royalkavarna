@extends('admin.master')
@section('content')

    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    {{--<script src="//code.jquery.com/jquery-1.10.2.js"></script>--}}
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script>
        $(function () {
            $("#accordion").accordion({
                heightStyle: "content"
            });
        });

    </script>

    <div class="content-wrapper">
        <section class="content">

            @if(Session::has('successMessage'))
                <div class="alert alert-success">
                    {!! Session::get('successMessage') !!}
                </div>
            @endif

            @if($errors -> any() )
                @foreach ($errors->all() as $error)
                    <div class="alert alert-danger" style="width: 100%;">
                        <button type="button" class="close" data-dismiss="alert">?</button>
                        <p>{{ $error }}</p>
                    </div>
                @endforeach
            @endif
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Мнения</h3>


                </div>
                <div class="box-body no-padding">


                    <div id="accordion">
                        @foreach($posts as $complexId=>$complexPost)
                            <h2>{!! $complexPost['name'] !!}</h2>

                            <div>
                                <table class="table table-striped">
                                    @foreach($complexPost['posts'] as $post)
                                        <tr class="extra-list-propery">
                                            <td><img src="{!! asset(\App\ComplexGuestBook::$path.$post['image']) !!}"
                                                     width="80px">
                                            </td>

                                            <td>{!! $post['name'] !!}</td>
                                            <td>{!! $post['email'] !!}</td>

                                            <td>
                                                {!! Form::button('Текст', ['class'=>'btn btn-primary','id'=>'postInfoButton','data-toggle'=>'modal' ,'data-target'=>'#postInfoModal',
                                                'tag_post_id'=>$post['id'],'tag_post_name'=>$post['name'], 'tag_post_position'=>$post['position'],'tag_post_message'=>$post['message']]) !!}

                                            </td>

                                            <td>
                                                @if($post['is_approved']==true)
                                                    {!! Form::button('Одобрен', ['style'=>'min-width: 120px;','class'=>'btn btn-success postApprove','tag_post_id'=>$post['id']]) !!}
                                                @else
                                                    {!! Form::button('Неодобрен', ['style'=>'min-width: 120px;','class'=>'btn btn-danger postApprove','tag_post_id'=>$post['id']]) !!}
                                                @endif
                                            </td>
                                            <td>
                                                {!! Form::button('Изтриване', ['class'=>'btn btn-danger postDel','tag_post_id'=>$post['id']]) !!}
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>

                        @endforeach
                    </div>
                    <style>
                        .ui-accordion .ui-accordion-header {
                            font-size: 26px;
                        }
                    </style>

                </div>
            </div>
        </section>
    </div>


    <div id="postInfoModal" class="modal fade cat1">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content post-info-modal">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"> Мнение от <span id="modal_client_name"></span></h4>
                </div>
                <div class="col-sm-12">
                    <div class="modal-body">
                        Позиция:
                        <p id="modal_client_position"></p>
                        Мнение:
                        <p id="modal_client_message"></p>


                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Затвори</button>
                </div>
            </div>

        </div>
    </div>



    <div style="display: none">
        {!! Form::open(array('action' => 'GuestBooksController@deletePost', 'id' => 'postFormDel')) !!}
        {!! Form::hidden( 'post_id' ) !!}
        {!! Form::close() !!}
    </div>

    <div style="display: none">
        {!! Form::open(array('action' => 'GuestBooksController@approvePost', 'id' => 'postFormApprove')) !!}
        {!! Form::hidden( 'post_id' ) !!}
        {!! Form::close() !!}
    </div>



    <script>

        $(document).ready(function () {

            $('.postDel').on('click', function (event) {
                var $this = $(this);
                var id = $this.attr('tag_post_id');
                $('#postFormDel input[name="post_id"]').val(id);
                var r = confirm("Наистина ли желаете да изтриете записа");
                if (r == true) {
                    $('#postFormDel').submit();
                }
            });
        });
    </script>

    <script>

        $(document).ready(function () {

            $('.postApprove').on('click', function (event) {
                var $this = $(this);
                var id = $this.attr('tag_post_id');
                $('#postFormApprove input[name="post_id"]').val(id);
                $('#postFormApprove').submit();
            });
        });
    </script>

    <script>

        $(document).ready(function () {
            $('button[id="postInfoButton"]').on('click', function (event) {


                var $this = $(this);


                $('.post-info-modal span[id="modal_client_name"]').text($this.attr('tag_post_name'));

                $('.post-info-modal p[id="modal_client_position"]').text($this.attr('tag_post_position'));

                $('.post-info-modal p[id="modal_client_message"]').text($this.attr('tag_post_message'));

            });
        });

    </script>

    <script>

        $('.postApprove.btn-danger').hover(function () {
            $(this).attr('class', 'activation btn btn-success');
            $(this).text('Одобри');
        }, function () {
            $(this).attr('class', 'activation btn btn-danger');
            $(this).text('Неодобрен');
        });

        $('.postApprove.btn-success').hover(function () {
            $(this).attr('class', 'activation btn btn-danger');
            $(this).text('Отхвърли');
        }, function () {
            $(this).attr('class', 'activation btn btn-success');
            $(this).text('Одобрен');
        });
    </script>



@stop