@extends('admin.master')
@section('content')

    <div class="content-wrapper">
        <section class="content-header">
            @if(Session::has('successMessage'))
                <div class="alert alert-success">
                    {!! Session::get('successMessage') !!}
                </div>
            @endif

            @if($errors -> any() )
                @foreach ($errors->all() as $error)
                    <div class="alert alert-danger" style="width: 100%;">
                        <button type="button" class="close" data-dismiss="alert">?</button>
                        <p>{{ $error }}</p>
                    </div>
                @endforeach
            @endif
            <h1>
                @if($isEditable)
                    Направени резервации
                    @if(isset($complex) && $complex != null)
                        <small> <a href="/admin/reservations/complex/{{$complex->id}}/archive">изтекли резервации</a></small>
                    @endif
                @else
                    АРХИВ с резервации за {{ $complex->name_bg }}
                    <small> <a href="/admin/reservations/complex/{{$complex->id}}">назад</a> към актуалните резервации</small>
                @endif
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
                <li class="active">Here</li>
            </ol>
        </section>

        <section class="content">
            <table class="table">
                <tbody>
                <tr>
                    <th>№</th>
                    <th>Име</th>
                    <th>Фамилия</th>
                    <th>Телефон</th>
                    <th>Дата на настаняване</th>
                    <th>Брой нощувки</th>
                    <th>Цена</th>
                    @if($isEditable)
                    <th></th>
                    <th></th>
                    <th></th>
                    @endif
                </tr>
                @if(sizeof($reservations) > 0)
                @foreach($reservations as $reservation)
                    <tr>
                        <td>{!! $reservation->id !!}</td>
                        <td>{!! $reservation->firstname !!}</td>
                        <td>{!! $reservation->lastname !!}</td>
                        <td>{!! $reservation->telephone1 !!}</td>
                        <td>{!! date('d / M / Y', strtotime($reservation->date_from)) !!}</td>
                        <td>{{ ceil(abs(strtotime($reservation->date_to) - strtotime($reservation->date_from)) / 86400) }}</td>
                        <td>{!! $reservation->price !!}&nbsp;лв.</td>

                        @if($isEditable)
                            <td>
                                <button class="btn btn-primary change_status"
                                        data-toggle="modal" data-target="#editStatus"
                                        tag_reservation_id="{!! $reservation->id !!}"
                                        tag_res_status_id="{!! $reservation->getStatus->id !!}">
                                    {!! $reservation->getStatus->status !!}</button>
                            </td>

                            <td><a href="{!! asset('admin/reservations/update/'.$reservation->id) !!}"
                                   class="btn btn-success">Промени</a></td>
                            <td>
                                <button class="btn btn-danger delete_reservation_button"
                                        tag_reservation_id="{!! $reservation->id !!}">Изтрий
                                </button>
                            </td>
                        @endif
                    </tr>


                @endforeach
                @else
                    <tr>
                        <td colspan="10">Няма направени резервации</td>
                    </tr>
                @endif
                </tbody>
            </table>

            {{--{!! Form::select('statuses',$statuses,null,['class'=>'']) !!}--}}

        </section>
    </div>



    <div id="editStatus" class="modal fade cat1">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"> Редактиране на статус</h4>
                </div>
                <div class="col-sm-12">
                    <div class="modal-body" id="edit_status_body">
                        {!! Form::open(array('action' => 'ReservationsEntityController@statusAndMessage','class'=>'form-horizontal','files'=>true)) !!}
                        {!! Form::hidden('reservation_id') !!}
                        <div class="form-group">
                            {!! Form::label('Статус:','Статус:',['class'=>'col-sm-2 control-label']) !!}
                            <div class="col-sm-10">
                                {!! Form::select( 'status',$statuses,null,['class'=>'form-control'] ) !!}
                            </div>
                        </div>
                        <br>

                        <div class="form-group">
                            {!! Form::label('Изпрати съобщение','Изпрати съобщение',['class'=>'col-sm-2 control-label']) !!}
                            <div class="col-sm-10">
                                {!! Form::checkbox( 'send_true',true,false ) !!}
                            </div>
                        </div>
                        <br>

                        <div class="form-group">
                            {!! Form::label('Съобщение:','Съобщение:',['class'=>'col-sm-2 control-label']) !!}
                            <div class="col-sm-10">
                                {!! Form::textarea( 'text',null,['class'=>'form-control'] ) !!}
                            </div>
                        </div>

                        <div class="form-group col-sm-6">
                            {!! Form::submit('Промени', ['name' => 'action[del]','class'=>'btn btn-success']) !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Затвори</button>
                </div>
            </div>

        </div>
    </div>

    <div class="del_reservation_form" style="display: none">
        {!! Form::open(array('action' => 'ReservationsEntityController@deleteReservation','class'=>'form-horizontal','id'=>'reseravation_form_id')) !!}
        {!! Form::hidden('reservation_id') !!}

        {!! Form::close() !!}

    </div>


    <script>

        $(document).ready(function () {
            $('.change_status').on('click', function (event) {

                var $this = $(this);
                $('#edit_status_body input[name="reservation_id"]').val($this.attr('tag_reservation_id'));
                $('#edit_status_body select[name="status"]').val($this.attr('tag_res_status_id'));
            });

            $('.delete_reservation_button').on('click', function (event) {
                var $this = $(this);
                var reservation_id = $this.attr('tag_reservation_id');
                $('.del_reservation_form input[name="reservation_id"]').val(reservation_id);
                var r = confirm("Наистина ли желаете да изтриете записа");
                if (r == true) {
                    $('#reseravation_form_id').submit();
                }
            });


        });


    </script>

@endsection