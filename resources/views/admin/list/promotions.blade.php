@extends('admin.master')
@section('content')


    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script>
        $(function () {
            $(".datepicker").datepicker();
        });

        //        $( ".selector" ).datepicker({
        //            dateFormat: "yy-mm-dd"
        //        });
    </script>



    <div class="content-wrapper">
        <section class="content">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Промоционални оферти на комплекса</h3>
                </div>
                <div class="box-body no-padding">

                    <button class="btn btn-primary" data-toggle="modal" data-target="#addPromotion">Нова оферта</button>


                    <table class="table table-striped">
                        <thead>
                        <th>ID:</th>
                        <th>Проценти:</th>
                        <th>Валидна до:</th>
                        <th>Дата на създаване:</th>
                        <th>Промяна</th>
                        <th>Изтриване:</th>

                        </thead>
                        <tbody>
                        @foreach( $promotions as $key => $promotion )
                            <tr>
                                <td>{!! $promotion->id !!}</td>
                                <td>{!! $promotion->percentage !!}</td>

                                <td> {!!  date("d/m/y",strtotime($promotion->until_date)) !!}</td>
{{--
                                <td>
                                    {!! Form::open(array('action' => 'ComplexPromotionsController@changeActivityPromotion')) !!}
                                    {!! Form::hidden('promotion_id',$promotion->id) !!}

                                    @if($promotion->is_active==true)
                                        {!! Form::submit('Активна', ['style'=>'min-width: 120px;','class'=>'activation btn btn-success']) !!}
                                    @else
                                        {!! Form::submit('Неактивна', ['style'=>'min-width: 120px;','class'=>'activation btn btn-danger']) !!}
                                    @endif
                                    {!! Form::close() !!}

                                </td>--}}

                                <td>{!! $promotion->created_at !!}</td>

                                <td>

                                    <button class="edit_promotion btn btn-primary"
                                            tag_promotion_id="{!! $promotion->id !!}"
                                            tag_promotion_date="{!! $promotion->until_date !!}"
                                            tag_promotion_percentage="{!! $promotion->percentage !!}"
                                            data-toggle="modal" data-target="#addPromotion">Промяна
                                    </button>

                                </td>

                                <td>
                                    {!! Form::open(array('action' => 'ComplexPromotionsController@deletePromotion')) !!}
                                    {!! Form::hidden('promotion_id',$promotion->id) !!}
                                    {!! Form::submit('Изтриване', ['class'=>'btn btn-danger']) !!}
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <br>

                    <div class="text-center">
                        <a href="{!! url('admin/complexes') !!}"
                           class="btn btn-primary">Назад</a>
                    </div>
                </div>
            </div>
            {{--<div class="form-group">--}}
            {{--{!! Form::button('Активиране',['class'=>'btn btn-primary']) !!}--}}
            {{--</div>--}}
        </section>
    </div>

    <div id="addPromotion" class="modal fade cat1">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"> Добавяне на оферта</h4>
                </div>
                <div class="col-sm-12">
                    <div class="edit_promo_modal modal-body">
                        {!! Form::open(array('action' => 'ComplexPromotionsController@setPromotion','class'=>'form-horizontal','files'=>true)) !!}
                        {!! Form::hidden('complex_id',$complex_id) !!}
                        {!! Form::hidden('promotion_id') !!}
                        <div class="form-group">
                            {!! Form::label('Коефициент в %','Коефициент в %',['class'=>'col-sm-2 control-label']) !!}
                            <div class="col-sm-10">
                                {!! Form::text( 'percentage',null,['class'=>'form-control'] ) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('Date:','Date:',['class'=>'col-sm-2 control-label']) !!}
                            <div class="col-sm-10">
                                {{--{!! Form::text( 'until_date',null,['class'=>'form-control','id'=>'datepicker'] ) !!}--}}
                                {!! Form::text( 'until_date',null,['class'=>'datepicker form-control'] ) !!}
                                {{--{!! Form::text( 'until_date',null,['class'=>'datepicker form-control'] ) !!}--}}
                            </div>
                        </div>

                        <div class="form-group col-sm-6">
                            {!! Form::submit('Запис', ['name' => 'action[del]','class'=>'btn btn-success']) !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Изход</button>
                </div>
            </div>

        </div>
    </div>


    <script>

        $('.activation.btn-danger').hover(function () {
            $(this).attr('class', 'activation btn btn-success');
            $(this).val('Активиране');
        }, function () {
            $(this).attr('class', 'activation btn btn-danger');
            $(this).val('Неактивна');
        });

        $('.activation.btn-success').hover(function () {
            $(this).attr('class', 'activation btn btn-danger');
            $(this).val('Деактивиране');
        }, function () {
            $(this).attr('class', 'activation btn btn-success');
            $(this).val('Активна');
        });
    </script>

    <script>

        $(document).ready(function () {
            $('.edit_promotion').on('click', function (event) {
                var $this = $(this);
                $('.edit_promo_modal input[name="promotion_id"]').val($this.attr('tag_promotion_id'));
                $('.edit_promo_modal input[name="percentage"]').val($this.attr('tag_promotion_percentage'));
                var result = $this.attr('tag_promotion_date').split('-');
                var strToInput = result[1] + '/' + result[2].split(' ')[0] + '/' + result[0];
                $('.edit_promo_modal input[name="until_date"]').val(strToInput);
            });
        });


    </script>

@endsection