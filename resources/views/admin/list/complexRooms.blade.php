@extends('admin.master')
@section('content')

    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script>
        $(function () {
            $(".datepicker").datepicker();
        });

        //        $( ".selector" ).datepicker({
        //            dateFormat: "yy-mm-dd"
        //        });
    </script>

    <div class="content-wrapper">
        @if(Session::has('successMessage'))
            <div class="alert alert-success">
                {!! Session::get('successMessage') !!}
            </div>
        @endif

        @if($errors -> any() )
            @foreach ($errors->all() as $error)
                <div class="alert alert-danger" style="width: 100%;">
                    <button type="button" class="close" data-dismiss="alert">X</button>
                    <p>{{ $error }}</p>
                </div>
            @endforeach
        @endif
        <section class="content">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Всички типове стаи за комплекс : <strong>{{ $complex -> name_bg }}</strong>
                    </h3>
                </div>

                @if(sizeof($complexRooms) > 0)
                <div class="container-fluid">
                    @foreach($complexRooms as $complexRoom)
                    <div class="row room-row">
                        <div class="col-md-12 col-lg-6">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-md-4">
                                        <img src="{!! asset(\App\ComplexRooms::$imagePath.$complexRoom->main_image) !!}" width="80px" style="display: block;">
                                        {!! $complexRoom->type_bg !!}
                                    </div>
                                    <div class="col-md-4">
                                        <em style="font-size: 1.5em">{{ $complexRoom->count_rooms }}</em> : стаи в хотела
                                    </div>
                                    <div class="col-md-4">
                                        <em style="font-size: 1.5em">{!! $reserved_rooms[$complexRoom->id] !!}</em> : заети стаи днес
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-6 text-right">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="inline-block">
                                            <a href="/admin/complexes/{{$complexRoom->complex_id}}/rooms/{{$complexRoom->id}}/prices" class="btn btn-success"> Цени </a>
                                        </div>
                                        <div class="inline-block">
                                            <button class="btn btn-primary view-extras" data-room-id="{!! $complexRoom->id !!}" data-toggle="modal" data-target="#getExtrasModal">Екстри
                                            </button>
                                        </div>
                                        <div class="inline-block">
                                            <a  href="{!! url('admin/complexes/room/'.$complexRoom->id.'/gallery') !!}" class="btn btn-primary" >Галерия</a>
                                        </div>
                                        <div class="inline-block">
                                            <button type="button" id="add_occupancy_button" class="btn btn-info" data-toggle="modal" data-target="#addOccupancy" room-name="{!! $complexRoom->type_bg !!}" room-id="{!! $complexRoom->id !!}">
                                            Резервирайте стая
                                            </button>
                                        </div>
                                        <div class="inline-block">
                                            <a href="/admin/complexes/{{$complexRoom->complex_id}}/rooms/{{$complexRoom->id}}/occupancies" class="btn btn-primary">
                                            Резервирани стаи
                                            </a>
                                        </div>
                                        <div class="inline-block">
                                            <a href="/admin/complexes/{{$complexRoom->complex_id}}/rooms/{{$complexRoom->id}}/edit"class="btn btn-warning">
                                            Промени
                                            </a>
                                        </div>
                                        <style>.room-row { margin-bottom: 18px;} .inline-block {display: inline-block; margin-bottom: 4px;}</style>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    <div class="row" style="margin-top: 20px; margin-bottom: 20px;">
                        <div class="col-md-12">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-md-6">
                                        <a href="{!! url('/admin/complexes' ) !!}" class="btn btn-primary">&laquo; Назад към комплекси</a>
                                        {{--<a href="{!! url('admin/complexes') !!}" class="btn btn-default">Назад</a>--}}
                                    </div>
                                    <div class="col-md-6 text-right">
                                        @if($complex->is_inseparable == false || sizeof($complexRooms) == 0)
                                            <a href="/admin/complexes/{{$complex->id}}/rooms/create" class="btn btn-success"> Добави тип стая </a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
            </div>
        </section>
    </div>
    <div id="addOccupancy" class="modal fade cat1">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Резервиране на стая "<strong id="roomName"></strong>"</h4>
                </div>
                <div class="col-sm-12">
                    <div class="modal-body" id="add_occupancy_modal_body">
                        {!! Form::open(array('action' => 'RoomOccupancyController@store','class'=>'form-horizontal','files'=>true)) !!}

                        {!! Form::hidden('room_id') !!}

                        <div class="form-group">
                            {!! Form::label('Брой заети стаи','Брой заети стаи',['class'=>'col-sm-4 control-label']) !!}
                            <div class="col-sm-8">
                                {!! Form::number( 'count_occupancy_rooms',null,['class'=>'form-control']) !!}
                            </div>
                        </div>

                        {{--<div class="form-group">--}}
                        {{--{!! Form::label('От дата','От дата',['class'=>'col-sm-4 control-label']) !!}--}}
                        {{--<div class="col-sm-8">--}}
                        {{--{!! Form::date( 'date_from',null,['class'=>'form-control'] ) !!}--}}
                        {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group">--}}
                        {{--{!! Form::label('До дата','До дата',['class'=>'col-sm-4 control-label']) !!}--}}
                        {{--<div class="col-sm-8">--}}
                        {{--{!! Form::date( 'date_to',null,['class'=>'form-control'] ) !!}--}}
                        {{--</div>--}}
                        {{--</div>--}}

                        <div class="form-group">
                            {!! Form::label('От дата','От дата',['class'=>'col-sm-4 control-label']) !!}
                            <div class="col-sm-8">
                                {!! Form::text( 'date_from',null,['class'=>'datepicker form-control'] ) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('До дата','До дата',['class'=>'col-sm-4 control-label']) !!}
                            <div class="col-sm-8">
                                {!! Form::text( 'date_to',null,['class'=>'datepicker form-control'] ) !!}
                            </div>
                        </div>

                        <div class="form-group col-sm-5">
                        </div>
                        <div class="form-group col-sm-7">
                            {!! Form::submit('Запази', ['class'=>'btn btn-success']) !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Затвори</button>
                </div>
            </div>

        </div>
    </div>
    <input type="hidden" name="temp_room_id" value="{!! Session::get('successSetExtras') !!}">

    <script>

        $(document).ready(function () {
            $('button[id="add_occupancy_button"]').on('click', function (event) {
                $('#roomName').text($(this).attr('room-name'));
                $('#add_occupancy_modal_body input[name="room_id"]').val($(this).attr('room-id'));
            });
        });

        $(document).ready(function () {
            viewExtrasEvent();

                    @if(Session::has('successSetExtras'))

                       var data = {};
                        data['id'] = $('input[name="temp_room_id"]').val();
//                        alert(data['id']);
                         getExtrasFromRoom(data);
                        $('#getExtrasModal').modal('show');

                    @endif

        });


        function viewExtrasEvent() {
            $('.view-extras').on('click', function (event) {
                var roomId = $(this).attr('data-room-id');
                var data = {};
                data['id'] = roomId;
                getExtrasFromRoom(data);
            });
        }
        function getExtrasFromRoom(data) {
            $.ajaxSetup({headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'}});
            $.ajax({
                url: "/api/getRoomExtras",
                type: "POST",
                dataType: "json",
                data: data
            }).done(function (result) {
//                alert('success');
                console.log(JSON.stringify(result['data']));
                var extras = result['data'];
                printExtras(data['id'], extras);
            }).fail(function (result) {
                alert('error');
            })
        }

        function printExtras(roomId, extras) {
            $('#modal-checkboxes .check-boxes-div').empty();
            $('#modal-checkboxes input[name="room_id"]').val(roomId);

            var checkBoxes = '';
            $.each(extras, function (checkBoxId, checkData) {
                console.log(JSON.stringify(checkData));

                if (checkData['checked'] == true) {
                    checkBoxes += '<label class="checkbox-inline"><input type="checkbox" name="check_boxes[' + checkBoxId + ']" value="' + checkBoxId + '" checked>' + checkData['label'] + '</label>';
                } else {
                    checkBoxes += '<label class="checkbox-inline"><input type="checkbox" name="check_boxes[' + checkBoxId + ']" value="' + checkBoxId + '" >' + checkData['label'] + '</label>';
                }


            });
            $('#modal-checkboxes .check-boxes-div').append(checkBoxes);
        }


    </script>
    <div id="getExtrasModal" class="modal fade cat1">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"> Добавяне на екстра</h4>
                </div>
                <div class="col-sm-12">
                    <div class="modal-body" id="modal-checkboxes">
                        {!! Form::open(array('action' => 'ComplexRoomController@updateRoomExtras','class'=>'form-horizontal')) !!}

                        {!! Form::hidden('room_id') !!}

                        <div class="form-group check-boxes-div"></div>
                        <div class="form-group col-sm-6">
                            {!! Form::submit('Промени', ['class'=>'btn btn-success']) !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Затвори</button>
                </div>
            </div>

        </div>
    </div>

@endsection