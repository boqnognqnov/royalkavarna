@extends('admin.master')
@section('content')

    <div class="content-wrapper">
        @if(Session::has('successMessage'))
            <div class="alert alert-success">
                {!! Session::get('successMessage') !!}
            </div>
        @endif

        @if($errors -> any() )
            @foreach ($errors->all() as $error)
                <div class="alert alert-danger" style="width: 100%;">
                    <button type="button" class="close" data-dismiss="alert">X</button>
                    <p>{{ $error }}</p>
                </div>
            @endforeach
        @endif
        <section class="content">
            <div class="box box-default">
                <div class="box-header with-border">
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        {!! Form::open( array( 'url' => '/admin/about/','files' => true ) ) !!}
                        <h1>About PAGE</h1>
                        <div class="col-md-12">
                            <img src="{{ asset(\App\About::$sliderImagePath . $about->about_slide_image) }}"
                                 style="width: 100%; max-height: 500px;">
                        </div>
                        <div class="col-md-12">
                            <div class="form-group {!! $errors -> has('about_slide_image') ? 'has-error' : '' !!}">
                                {!! Form::label( 'Header Снимка *:' ) !!}
                                {!! Form::file( 'about_slide_image',array( 'class' => 'form-control' ) ) !!}
                                {!! $errors -> first( 'about_slide_image', '<span class="help-block">:message</span>' ) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <img src="{{ asset(\App\About::$imagePath . $about->about_image) }}"
                                 style="width: 100%; max-height: 500px;">
                        </div>
                        <div class="col-md-6">
                            <img src="{{ asset(\App\About::$logoPath . $about->about_logo) }}"
                                 style="width: 100%; max-height: 500px;">
                        </div>
                        <div class="col-md-6">
                            <div class="form-group {!! $errors -> has('about_image') ? 'has-error' : '' !!}">
                                {!! Form::label( 'Снимка *:' ) !!}
                                {!! Form::file( 'about_image',array( 'class' => 'form-control' ) ) !!}
                                {!! $errors -> first( 'about_image', '<span class="help-block">:message</span>' ) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group {!! $errors -> has('about_logo') ? 'has-error' : '' !!}">
                                {!! Form::label( 'Лого *:' ) !!}
                                {!! Form::file( 'about_logo',array( 'class' => 'form-control' ) ) !!}
                                {!! $errors -> first( 'about_logo', '<span class="help-block">:message</span>' ) !!}
                            </div>
                        </div>


                        {{------------------------------------About Information---------------------------------------------}}
                        <h2>About Information *</h2>
                        <div class="col-md-6">
                            <div class="form-group {!! $errors -> has('about_text_bg') ? 'has-error' : '' !!}">
                                {!! Form::label( '(BG) Описание за компанията*:' ) !!}
                                {!! Form::textarea( 'about_text_bg', $about->about_text_bg, array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                {!! $errors -> first( 'about_text_bg', '<span class="help-block">:message</span>' ) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group {!! $errors -> has('about_text_en') ? 'has-error' : '' !!}">
                                {!! Form::label( '(EN) Описание за компанията*:' ) !!}
                                {!! Form::textarea( 'about_text_en', $about->about_text_en, array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                {!! $errors -> first( 'about_text_en', '<span class="help-block">:message</span>' ) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group {!! $errors -> has('about_text_ru') ? 'has-error' : '' !!}">
                                {!! Form::label( '(RU) Описание за компанията*:' ) !!}
                                {!! Form::textarea( 'about_text_ru', $about->about_text_ru, array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                {!! $errors -> first( 'about_text_ru', '<span class="help-block">:message</span>' ) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group {!! $errors -> has('about_text_ro') ? 'has-error' : '' !!}">
                                {!! Form::label( '(RO) Описание за компанията*:' ) !!}
                                {!! Form::textarea( 'about_text_ro', $about->about_text_ro, array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                {!! $errors -> first( 'about_text_ro', '<span class="help-block">:message</span>' ) !!}
                            </div>
                        </div>
                        {{------------------------------------CONTACT INFO---------------------------------------------}}
                        <h1>Contact PAGE</h1>

                        <div class="col-md-6">
                            <div class="form-group {!! $errors -> has('contact_address_bg') ? 'has-error' : '' !!}">
                                {!! Form::label( '(BG) Адрес на фирмата*:' ) !!}
                                {!! Form::text( 'contact_address_bg', $about->contact_address_bg, array( 'class' => 'form-control' ) ) !!}
                                {!! $errors -> first( 'contact_address_bg', '<span class="help-block">:message</span>' ) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group {!! $errors -> has('contact_address_en') ? 'has-error' : '' !!}">
                                {!! Form::label( '(EN) Адрес на фирмата*:' ) !!}
                                {!! Form::text( 'contact_address_en', $about->contact_address_en, array( 'class' => 'form-control' ) ) !!}
                                {!! $errors -> first( 'contact_address_en', '<span class="help-block">:message</span>' ) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group {!! $errors -> has('contact_address_ru') ? 'has-error' : '' !!}">
                                {!! Form::label( '(RU) Адрес на фирмата*:' ) !!}
                                {!! Form::text( 'contact_address_ru', $about->contact_address_ru, array( 'class' => 'form-control' ) ) !!}
                                {!! $errors -> first( 'contact_address_ru', '<span class="help-block">:message</span>' ) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group {!! $errors -> has('contact_address_ro') ? 'has-error' : '' !!}">
                                {!! Form::label( '(RO) Адрес на фирмата*:' ) !!}
                                {!! Form::text( 'contact_address_ro', $about->contact_address_ro, array( 'class' => 'form-control' ) ) !!}
                                {!! $errors -> first( 'contact_address_ro', '<span class="help-block">:message</span>' ) !!}
                            </div>
                        </div>

                        <h2>Phones</h2>
                        <div class="col-md-6">
                            <div class="form-group {!! $errors -> has('contact_telephone') ? 'has-error' : '' !!}">
                                {!! Form::label( 'Телефон*:' ) !!}
                                {!! Form::text( 'contact_telephone', $about->contact_telephone, array( 'class' => 'form-control' ) ) !!}
                                {!! $errors -> first( 'contact_telephone', '<span class="help-block">:message</span>' ) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group {!! $errors -> has('contact_mobile') ? 'has-error' : '' !!}">
                                {!! Form::label( 'Мобилен Телефон*:' ) !!}
                                {!! Form::text( 'contact_mobile', $about->contact_mobile, array( 'class' => 'form-control' ) ) !!}
                                {!! $errors -> first( 'contact_mobile', '<span class="help-block">:message</span>' ) !!}
                            </div>
                        </div>

                        <h2>EMAILS</h2>
                        <div class="col-md-6">
                            {{--<div class="form-group {!! $errors -> has('contact_emails') ? 'has-error' : '' !!}">--}}
                            {{--{!! Form::label( 'Emails*:' ) !!}--}}
                            {{--{!! Form::text( 'contact_emails', $about->contact_emails, array( 'class' => 'form-control' ) ) !!}--}}
                            {{--e.g.: email_one@mysite.bg email_two@mysite.bg email_three@mysite.bg--}}
                            {{--{!! $errors -> first( 'contact_emails', '<span class="help-block">:message</span>' ) !!}--}}
                            {{--</div>--}}
                        </div>
                        <div class="col-md-6">
                            <div class="form-group {!! $errors -> has('contact_general_email') ? 'has-error' : '' !!}">
                                {!! Form::label( 'Генерален емайл*:' ) !!}
                                {!! Form::text( 'contact_general_email', $about->contact_general_email, array( 'class' => 'form-control' ) ) !!}
                                {!! $errors -> first( 'contact_general_email', '<span class="help-block">:message</span>' ) !!}
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                {!! Form::submit( 'Промени стаята', array( 'class' => 'btn btn-primary btn-block' ) ) !!}
                            </div>
                        </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>

        </section>
    </div>

@endsection