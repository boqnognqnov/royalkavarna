@extends('admin.master')
@section('content')

    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                Page Header
                <small>Optional description</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
                <li class="active">Here</li>
            </ol>
        </section>

        <section class="content">

            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Слайдъри</h3>

                    <div class="box-tools">
                        <a class="btn btn-primary" href="{!! url('admin/sliders/create') !!}">Добавяне на слайдър</a>
                    </div>
                </div>
                <div class="box-body no-padding">
                    @if(Session::has('successMessage'))
                        <div class="alert alert-success">
                            {!! Session::get('successMessage') !!}
                        </div>
                    @endif

                    @if($errors -> any() )
                        @foreach ($errors->all() as $error)
                            <div class="alert alert-danger" style="width: 100%;">
                                <button type="button" class="close" data-dismiss="alert">?</button>
                                <p>{{ $error }}</p>
                            </div>
                        @endforeach
                    @endif
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th width="20%">Изображение</th>
                            <th width="40%">Текст BG</th>
                            <th width="20%">Активност</th>
                            <th width="10%">Редакция</th>
                            <th width="10%">Изтрий</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach( $sliders as $key => $slider )
                            <tr>
                                <td><img src="{!! asset(\App\Slider::$imagePath.$slider -> image) !!}" width="150px">
                                </td>
                                <td>{!! substr($slider->text_bg,0,20) !!}</td>


                                <td>
                                    @if($slider->is_active==true)
                                        {!! Form::button('Активен', ['style'=>'min-width: 120px;','class'=>'btn btn-success sliderActivate','data-slider-id'=>$slider -> id]) !!}
                                    @else
                                        {!! Form::button('Неактивен', ['style'=>'min-width: 120px;','class'=>'btn btn-danger sliderActivate','data-slider-id'=>$slider -> id]) !!}
                                    @endif
                                </td>


                                <td><a href="{!! url('admin/sliders/edit/' . $slider -> id ) !!}"
                                       class="btn btn-primary">Редакция</a></td>
                                <td>
                                    <button class="btn btn-danger sliderDel" data-slider-id="{!! $slider -> id !!}">
                                        Изтрий
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        </section>
    </div>

    <div style="display: none">
        {!! Form::open(array('action' => 'SlidersEntityController@destroy','id'=>'form-del-slider')) !!}
        {!! Form::hidden('slider_id') !!}

        {!! Form::close() !!}
    </div>

    <div style="display: none">
        {!! Form::open(array('action' => 'SlidersEntityController@setActivity','id'=>'form-activate-slider')) !!}
        {!! Form::hidden('slider_id') !!}
        {!! Form::close() !!}
    </div>


    <script>

        $(document).ready(function () {

            $('.sliderDel').on('click', function (event) {

                var id = $(this).attr('data-slider-id');

                $('#form-del-slider input[name="slider_id"]').val(id);
                var r = confirm("Наистина ли желаете да изтриете записа");
                if (r == true) {
                    $('#form-del-slider').submit();
                }
            });
        });
    </script>

    <script>

        $(document).ready(function () {

            $('.sliderActivate').on('click', function (event) {
                var id = $(this).attr('data-slider-id');
                $('#form-activate-slider input[name="slider_id"]').val(id);
                $('#form-activate-slider').submit();
            });
        });

        $('.sliderActivate.btn-danger').hover(function () {
            $(this).attr('class', 'activation btn btn-success');
            $(this).text('Активирай');
        }, function () {
            $(this).attr('class', 'activation btn btn-danger');
            $(this).text('Неактивен');
        });

        $('.sliderActivate.btn-success').hover(function () {
            $(this).attr('class', 'activation btn btn-danger');
            $(this).text('Деактивирай');
        }, function () {
            $(this).attr('class', 'activation btn btn-success');
            $(this).text('Активен');
        });
    </script>



@endsection