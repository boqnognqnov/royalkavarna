@extends('admin.master')
@section('content')

    <style>
        .complexImage:hover{
            background: goldenrod;
        }
        .complexImage{
            padding: 40px;
            background: rgba(218, 165, 32, 0.52);
            border-radius: 6px;
        }
    </style>
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                Page Header
                <small>Optional description</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
                <li class="active">Here</li>
            </ol>
        </section>

        <section class="content">

            @foreach($complexes as $complex)

                <div class="col-lg-4">
                    <div class="text-center">
                        <a href="{!! url('admin/reservations/complex/'.$complex->id) !!}">
                            <img src="{!! asset(\App\Complex::$imagePath.$complex->image) !!}" class="complexImage" width="300px">
                        </a>
                    </div>
                </div>

            @endforeach

        </section>
    </div>

@endsection