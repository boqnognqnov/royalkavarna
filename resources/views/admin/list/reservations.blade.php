@extends('admin.master')
@section('content')

    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script>
        $(function () {
            $(".datepicker").datepicker();
        });

    </script>



    <div class="content-wrapper">
        @if(Session::has('successMessage'))
            <div class="alert alert-success">
                {!! Session::get('successMessage') !!}
            </div>
        @endif

        @if($errors -> any() )
            @foreach ($errors->all() as $error)
                <div class="alert alert-danger" style="width: 100%;">
                    <button type="button" class="close" data-dismiss="alert">X</button>
                    <p>{{ $error }}</p>
                </div>
            @endforeach
        @endif
        <section class="content">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Резервации за стая "  <strong>{{ $complexRoom -> type_bg  }}</strong>  " в комплекс " <strong>{{ $complex -> name_bg }}</strong> "</h3>

                    @if($isEditable)
                        <h4><a href="{!! url('/admin/complexes/'.$complex -> id.'/rooms/'.$complexRoom -> id.'/occupancies/archive' ) !!}">АРХИВ</a></h4>
                    @else
                        <h4><a href="{!! url('/admin/complexes/'.$complex -> id.'/rooms/'.$complexRoom -> id.'/occupancies') !!}">Назад</a> към предстоящите резервации</h4>
                    @endif
                </div>
                <div class="box-header">
                    <h4><a href="{!! url('admin/complexes/'.$complex -> id.'/rooms'  ) !!}">Назад към стаите на комплекса</a></h4>
                </div>

                <div class="box-body no-padding">
                    <table class="table table-striped">
                        <tr>
                            <th>№</th>
                            <th>Брой Стаи</th>
                            <th>Дата на настаняване</th>
                            <th>Брой нощувки</th>
                            @if($isEditable)
                                <th></th>
                                <th></th>
                            @endif
                        </tr>
                        @if(sizeof($occupancies) > 0)
                            @foreach($occupancies as $occupancy)
                            <tr class="extra-list-propery">
                                <td>{!! $occupancy->id !!}</td>
                                <td>{!! $occupancy->count_occupancy_rooms !!}</td>
                                <td>{!! date('d / M / Y', strtotime($occupancy->date_from)) !!}</td>
                                <td>{{ ceil(abs(strtotime($occupancy->date_to) - strtotime($occupancy->date_from)) / 86400) }}</td>
                                @if($isEditable)
                                <td>
                                    <button type="button" id="update_occupancy_button" class="btn btn-info" data-toggle="modal"
                                            data-target="#updateOccupancy"
                                            room_occupancy_id="{!! $occupancy->id !!}"
                                            count_occupancy_rooms="{!! $occupancy->count_occupancy_rooms !!}"
                                            date_from="{!! $occupancy->date_from !!}"
                                            date_to="{!! $occupancy->date_to !!}"> Редактирайте резервацията
                                    </button>
                                </td>
                                <td>
                                    <a href="#" occupancy_id="{{$occupancy->id}}" class="btn btn-danger deleteOccupancyButton"> Изтрии </a>
                                </td>
                                @endif
                            </tr>
                            @endforeach
                        @endif
                    </table>
                </div>
            </div>
        </section>
    </div>

    {!! Form::open(array('action' => 'RoomOccupancyController@destroy', 'id' => 'deleteOccupancyForm')) !!}
    {!! Form::hidden('occupancy_room_id') !!}
    {!! Form::close() !!}

    <div id="updateOccupancy" class="modal fade cat1">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Резервиране на стая "<strong id="roomName">{{ $complexRoom -> type_bg }}</strong>"</h4>
                </div>
                <div class="col-sm-12">
                    <div class="modal-body" id="update_occupancy_modal_body">
                        {!! Form::open(array('action' => 'RoomOccupancyController@update','class'=>'form-horizontal','files'=>true)) !!}

                        {!! Form::hidden('room_occupancy_id') !!}
                        {!! Form::hidden('room_id', $complexRoom -> id ) !!}

                        <div class="form-group">
                            {!! Form::label('Брой заети стаи','Брой заети стаи',['class'=>'col-sm-4 control-label']) !!}
                            <div class="col-sm-8">
                                {!! Form::number( 'count_occupancy_rooms',null,['class'=>'form-control']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('От дата','От дата',['class'=>'col-sm-4 control-label']) !!}
                            <div class="col-sm-8">
                                {!! Form::text( 'date_from',null,['class'=>'datepicker form-control'] ) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('До дата','До дата',['class'=>'col-sm-4 control-label']) !!}
                            <div class="col-sm-8">
                                {!! Form::text( 'date_to',null,['class'=>'datepicker form-control'] ) !!}
                            </div>
                        </div>


                        <div class="form-group col-sm-5">
                        </div>
                        <div class="form-group col-sm-7">
                            {!! Form::submit('Запази', ['class'=>'btn btn-success']) !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Затвори</button>
                </div>
            </div>

        </div>
    </div>

    <script>

        $(document).ready(function () {
            $('button[id="update_occupancy_button"]').on('click', function (event) {
                $('#update_occupancy_modal_body input[name="room_occupancy_id"]').val($(this).attr('room_occupancy_id'));
                $('#update_occupancy_modal_body input[name="count_occupancy_rooms"]').val($(this).attr('count_occupancy_rooms'));

                var $this = $(this);

                var result = $this.attr('date_from').split('-');
                var strToInput = result[1] + '/' + result[2].split(' ')[0] + '/' + result[0];
                $('#update_occupancy_modal_body input[name="date_from"]').val(strToInput);

//                $('#update_occupancy_modal_body input[name="date_from"]').val($(this).attr('date_from'));

                var result = $this.attr('date_to').split('-');
                var strToInput = result[1] + '/' + result[2].split(' ')[0] + '/' + result[0];
                $('#update_occupancy_modal_body input[name="date_to"]').val(strToInput);

//                $('#update_occupancy_modal_body input[name="date_to"]').val($(this).attr('date_to'));
            });


            $('.deleteOccupancyButton').on('click', function (event) {
                var id = $(this).attr('occupancy_id');
                $('#deleteOccupancyForm input[name="occupancy_room_id"]').val(id);
                var r = confirm("Наистина ли желаете да изтриете записа");
                if (r == true) {
                    $('#deleteOccupancyForm').submit();
                }
            });
        });

    </script>

@endsection