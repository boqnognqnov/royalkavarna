@extends('admin.master')
@section('content')

    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <link rel="stylesheet" href="/resources/demos/style.css">
    <style>
        .activation {
            min-width: 115px;
        }
    </style>
    <div class="content-wrapper">
        @if(Session::has('successMessage'))
            <div class="alert alert-success">
                {!! Session::get('successMessage') !!}
            </div>
        @endif

        @if($errors -> any() )
            @foreach ($errors->all() as $error)
                <div class="alert alert-danger" style="width: 100%;">
                    <button type="button" class="close" data-dismiss="alert">X</button>
                    <p>{{ $error }}</p>
                </div>
            @endforeach
        @endif
        <section class="content">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><strong>Всички презентации</strong>
                    </h3>

                    <div class="box-tools">
                        <button data-toggle="modal"
                           data-target="#addPresentation" class="btn btn-success"> Добави Презентация</button>
                    </div>

                </div>
                <div class="box-body no-padding">
                    <table class="table table-striped">
                        <tr>
                            <th>ID</th>
                            <th>Type</th>
                            <th>Action</th>
                        </tr>
                        @if(sizeof($presentations) > 0)
                            @foreach($presentations as $presentation)
                                <tr class="extra-list-propery">
                                    <td>{!! $presentation->id !!}</td>
                                    <td>
                                        @if(\App\Presentation::youtube_type == $presentation->type)
                                            Youtube Video
                                        @elseif(\App\Presentation::video_file_type == $presentation->type)
                                            Video File
                                        @elseif(\App\Presentation::pdf_file_type == $presentation->type)
                                            PDF File
                                        @endif
                                       </td>
                                    <td>
                                        @if(!$presentation->is_active)
                                            <a href="#" class="activation btn btn-danger" id="{!! $presentation->id !!}" >NOT Active</a>
                                        @else
                                            <a href="#" class="activation btn btn-success" id="{!! $presentation->id !!}" >Active</a>
                                        @endif
                                    </td>
                                    <td >
                                        <a href="/admin/presentations/{{$presentation->id}}/destroy" id="dellPresentation"
                                           class="btn btn-danger">Изтрии</a>
                                    </td>
                                </tr>

                                {!! Form::open(array('url' => '/admin/presentations/active/'.$presentation->id, 'id' => 'form'.$presentation->id)) !!}

                                {!! Form::close() !!}
                            @endforeach
                        @endif
                    </table>
                </div>
            </div>
        </section>
    </div>
    <div id="addPresentation" class="modal fade cat1">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><strong >Добавяне на презентация</strong></h4>
                </div>
                <div class="col-sm-12">
                    <div class="modal-body">
                        {!! Form::open(array('action' => 'PresentationController@addPresentation','class'=>'form-horizontal','files'=>true)) !!}

                        <div class="form-group">
                            {!! Form::label('Тип на презентацията','Тип на презентацията',['class'=>'col-sm-4 control-label']) !!}
                            <div class="col-sm-8">
                                {!! Form::select( 'type', [
                                    \App\Presentation::youtube_type => 'Youtube video',
                                    \App\Presentation::video_file_type => 'Video file',
                                    \App\Presentation::pdf_file_type => 'PDF file',
                                    ],['class'=>'form-control']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('Файл (само когато типа е Video file или PDF file)','Файл (само когато типа е Video file или PDF file)',['class'=>'col-sm-4 control-label']) !!}
                            <div class="col-sm-8">
                                {!! Form::file( 'file',null,['class'=>'form-control']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('YouTube линк  (само когато типа е Youtube video)','YouTube линк  (само когато типа е Youtube video)',['class'=>'col-sm-4 control-label']) !!}
                            <div class="col-sm-8">
                                {!! Form::text( 'youtube_video',null,['class'=>'form-control']) !!}
                            </div>
                        </div>



                        <div class="form-group col-sm-7">
                            {!! Form::submit('Запази', ['class'=>'btn btn-success']) !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Затвори</button>
                </div>
            </div>

        </div>
    </div>

@endsection
@section('after-js')
    <script>
        $('.activation').click(function(){
            var isConfirmed = confirm("Really you want to change presentation status");
            if( isConfirmed == true ){
                $('#form' + $(this).attr('id')).submit();
            }});


        $('.activation.btn-danger').hover(function(){
            $(this).attr('class', 'activation btn btn-success');
            $(this).text('ACTIVATE');
        }, function(){
            $(this).attr('class', 'activation btn btn-danger');
            $(this).text('NOT Active');
        });
        $('.activation.btn-success').hover(function(){
            $(this).attr('class', 'activation btn btn-danger');
            $(this).text('DEACTIVATE');
        }, function(){
            $(this).attr('class', 'activation btn btn-success');
            $(this).text('Active');
        });
    </script>
@endsection