@extends('admin.layouts.default')
@section('body')

    <div class="wrapper">

        <header class="main-header">
            <a href="{!! url('admin/sliders') !!}" class="logo">
                <span class="logo-mini"><b>R</b>K</span>
                <span class="logo-lg"><b>Royals</b>Kavarna</span>
            </a>
            <nav class="navbar navbar-static-top" role="navigation">
                <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>
            </nav>
        </header>

        <aside class="main-sidebar">

            <section class="sidebar">

                <ul class="sidebar-menu">
                    <li class="header">НАВИГАЦИЯ</li>
                    {{--<li class="active"><a href="{!! url('admin/home') !!}"><i class="fa fa-link"></i><span>Начало</span></a></li>--}}
                    <li class=""><a href="{!! url('admin/sliders') !!}"><i class="fa fa-link"></i> <span>Слайдъри</span></a></li>
                    <li class=""><a href="{!! url('admin/complexes') !!}"><i class="fa fa-link"></i> <span>Комплекси</span></a></li>
                    <li class=""><a href="{!! url('admin/facilities') !!}"><i class="fa fa-link"></i> <span>Удобства</span></a></li>
                    {{--<li class="active"><a href="{!! url('admin/rooms') !!}"><i class="fa fa-link"></i> <span>Стаи</span></a></li>--}}
                    <li class=""><a href="{!! url('admin/reservations') !!}"><i class="fa fa-link"></i> <span>Резервации</span></a></li>
                    <li class=""><a href="{!! url('admin/statuses') !!}"><i class="fa fa-link"></i> <span>Статуси</span></a></li>
                    <li class=""><a href="{!! url('admin/extras') !!}"><i class="fa fa-link"></i> <span>Екстри за стаите</span></a></li>
                    <li class=""><a href="{!! url('admin/guest_books') !!}"><i class="fa fa-link"></i> <span>Мнения</span></a></li>
                    <li class=""><a href="{!! url('admin/about') !!}"><i class="fa fa-link"></i> <span>about/contact страници</span></a></li>
                    <li class=""><a href="{!! url('admin/presentations') !!}"><i class="fa fa-link"></i> <span>Презентации</span></a></li>
                    <li class=""><a href="{!! url('logout') !!}"><i class="fa fa-link"></i> <span>Изход</span></a></li>
                </ul>
            </section>
        </aside>

        @yield('content')

        <footer class="main-footer">
            <div class="pull-right hidden-xs">
                RoyalsKavarna Administration
            </div>
            <strong>Copyright &copy; 2015 <a href="#">RoyalsKavarna</a>.</strong> All rights reserved.
        </footer>
    </div>

@endsection