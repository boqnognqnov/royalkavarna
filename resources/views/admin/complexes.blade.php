@extends('admin.master')
@section('content')

    <div class="content-wrapper">
        <section class="content">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Всички комплекси</h3>
                </div>

                <div class="box-tools">
                    <a href="/admin/complexes/create" class="btn btn-success"> Добави комплекс </a>
                </div>
                <div class="box-body no-padding">
                    <table class="table table-striped">
                        <tr>

                            <th width="20%" class="text-center">Комплекс</th>
                            {{--<th>Лого</th>--}}

                            <th width="10%">Галерия</th>
                            <th width="10%">Стаи</th>
                            <th width="10%">Промоции</th>
                            <th width="10%">Early Booking</th>
                            <th width="10%">Редакция</th>
                            <th width="30%">Изтрий</th>
                        </tr>
                        @foreach( $complexData as $key => $complex )
                            <tr>

                                <td width="20%" class="text-center"><img
                                            src="{!!  url(\App\Complex::$imagePath.$complex->image) !!}"
                                            height="100px"/>
                                    <br>
                                    {!! $complex -> name_bg !!}
                                </td>
                                {{--<td style="background: goldenrod;"><img--}}
                                {{--src="{!!  url(\App\Complex::$logoPath.$complex->logo) !!}" height="100px"/>--}}
                                {{--</td>--}}
                            
                                <td width="10%"><a href="{!! url('admin/complexes/gallery/' . $complex -> id  ) !!}"
                                                   class="btn btn-primary">Галерия</a></td>
                                <td width="10%"><a href="{!! url('admin/complexes/'.$complex -> id.'/rooms'  ) !!}"
                                                   class="btn btn-primary">Стаи</a></td>
                                <td width="10%"><a
                                            href="{!! url('/admin/complexes/' . $complex -> id .'/promotions') !!}"
                                            class="btn btn-primary">Промоции</a></td>
                                <td width="10%"><a href="{!! url('admin/complexes/promotions/' . $complex -> id ) !!}"
                                                   class="btn btn-primary">Early Booking</a></td>
                                <td width="10%"><a href="{!! url('admin/complexes/edit/' . $complex -> id ) !!}"
                                                   class="btn btn-primary">Редакция</a></td>
                                <td width="30%">
                                    <button type="button" class="complexButtonDel btn btn-danger"
                                            tag_complex_id="{!! $complex -> id !!}" disabled>Изтрий
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>

        </section>
    </div>


    <div id="editExtra" class="modal fade cat1">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"> Добавяне на екстра</h4>
                </div>
                <div class="col-sm-12">
                    <div class="modal-body" id="edit_extra_modal_body">
                        {!! Form::open(array('action' => 'ExtrasEntityController@updateExtra','class'=>'form-horizontal','files'=>true)) !!}

                        {!! Form::hidden('extra_id') !!}

                        <div class="form-group">
                            {!! Form::label('Икона','Икона',['class'=>'col-sm-2 control-label']) !!}
                            <div class="col-sm-10">
                                {!! Form::file( 'icon',null,['class'=>'form-control']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('Заглавие BG','Заглавие BG',['class'=>'col-sm-2 control-label']) !!}
                            <div class="col-sm-10">
                                {!! Form::text( 'name_bg',null,['class'=>'form-control'] ) !!}
                                {{--                                {!! Form::text('title',null,['class'=>'form-control']) !!}--}}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('Заглавие EN','Заглавие EN',['class'=>'col-sm-2 control-label']) !!}
                            <div class="col-sm-10">
                                {!! Form::text( 'name_en',null,['class'=>'form-control'] ) !!}
                                {{--                                {!! Form::text('title',null,['class'=>'form-control']) !!}--}}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('Заглавие RU','Заглавие RU',['class'=>'col-sm-2 control-label']) !!}
                            <div class="col-sm-10">
                                {!! Form::text( 'name_ru',null,['class'=>'form-control'] ) !!}
                                {{--                                {!! Form::text('title',null,['class'=>'form-control']) !!}--}}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('Заглавие RO','Заглавие RO',['class'=>'col-sm-2 control-label']) !!}
                            <div class="col-sm-10">
                                {!! Form::text( 'name_ro',null,['class'=>'form-control'] ) !!}
                                {{--                                {!! Form::text('title',null,['class'=>'form-control']) !!}--}}
                            </div>
                        </div>


                        <div class="form-group col-sm-6">
                            {!! Form::submit('Промени', ['name' => 'action[del]','class'=>'btn btn-success']) !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Затвори</button>
                </div>
            </div>

        </div>
    </div>

    <div style="display: none">
        {!! Form::open(array('action' => 'ComplexesEntityController@destroy', 'id' => 'complexFormDel')) !!}
        {!! Form::submit('Изтриване', ['class'=>'btn btn-danger']) !!}
        {!! Form::hidden( 'id' ) !!}
        {!! Form::close() !!}
    </div>




    <script>

        $(document).ready(function () {
            complexDelEvent();
        });


        function complexDelEvent() {
            $('.complexButtonDel').on('click', function (event) {
                var $this = $(this);
                var id = $this.attr('tag_complex_id');
                $('input[name="id"]').val(id);
                var r = confirm("Наистина ли желаете да изтриете записа");
                if (r == true) {
                    $('#complexFormDel').submit();
                }
            });
        }


    </script>

@endsection