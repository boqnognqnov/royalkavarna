@extends('admin.master')
@section('content')

    <div class="content-wrapper">

        <section class="content">
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Създаване на удобство</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">

                        {!! Form::open( array( 'action' => 'FacilitiesEntityController@store', 'files' => true ) ) !!}
                            <div class="col-md-12">
                                <div class="form-group {!! $errors -> has('complex_id') ? 'has-error' : '' !!}">
                                    {!! Form::label( 'Стая:' ) !!}
                                    {!! Form::select( 'complex_id', $complexesSelect, null, array( 'class' => 'form-control' ) ) !!}
                                    {!! $errors -> first( 'complex_id', '<span class="help-block">:message</span>' ) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group {!! $errors -> has('image') ? 'has-error' : '' !!}">
                                    {!! Form::label( 'Снимка:' ) !!}
                                    {!! Form::file( 'image', array( 'class' => 'form-control' ) ) !!}
                                    {!! $errors -> first( 'image', '<span class="help-block">:message</span>' ) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group {!! $errors -> has('logo') ? 'has-error' : '' !!}">
                                    {!! Form::label( 'Лого:' ) !!}
                                    {!! Form::file( 'logo', array( 'class' => 'form-control' ) ) !!}
                                    {!! $errors -> first( 'logo', '<span class="help-block">:message</span>' ) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group {!! $errors -> has('title_bg') ? 'has-error' : '' !!}">
                                    {!! Form::label( '(BG) Име на удобството:' ) !!}
                                    {!! Form::text( 'title_bg', null, array( 'class' => 'form-control' ) ) !!}
                                    {!! $errors -> first( 'title_bg', '<span class="help-block">:message</span>' ) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group {!! $errors -> has('title_en') ? 'has-error' : '' !!}">
                                    {!! Form::label( '(EN) Име на удобството:' ) !!}
                                    {!! Form::text( 'title_en', null, array( 'class' => 'form-control' ) ) !!}
                                    {!! $errors -> first( 'title_en', '<span class="help-block">:message</span>' ) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group {!! $errors -> has('title_ru') ? 'has-error' : '' !!}">
                                    {!! Form::label( '(RU) Име на удобството:' ) !!}
                                    {!! Form::text( 'title_ru', null, array( 'class' => 'form-control' ) ) !!}
                                    {!! $errors -> first( 'title_ru', '<span class="help-block">:message</span>' ) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group {!! $errors -> has('title_ro') ? 'has-error' : '' !!}">
                                    {!! Form::label( '(RO) Име на удобството:' ) !!}
                                    {!! Form::text( 'title_ro', null, array( 'class' => 'form-control' ) ) !!}
                                    {!! $errors -> first( 'title_ro', '<span class="help-block">:message</span>' ) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group {!! $errors -> has('text_bg') ? 'has-error' : '' !!}">
                                    {!! Form::label( '(BG) Информация за удобството:' ) !!}
                                    {!! Form::textarea( 'text_bg', null, array( 'class' => 'form-control html5area', 'rows' => '8' ) ) !!}
                                    {!! $errors -> first( 'text_bg', '<span class="help-block">:message</span>' ) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group {!! $errors -> has('text_en') ? 'has-error' : '' !!}">
                                    {!! Form::label( '(EN) Информация за удобството:' ) !!}
                                    {!! Form::textarea( 'text_en', null, array( 'class' => 'form-control html5area', 'rows' => '8' ) ) !!}
                                    {!! $errors -> first( 'text_en', '<span class="help-block">:message</span>' ) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group {!! $errors -> has('text_ru') ? 'has-error' : '' !!}">
                                    {!! Form::label( '(RU) Информация за удобството:' ) !!}
                                    {!! Form::textarea( 'text_ru', null, array( 'class' => 'form-control html5area', 'rows' => '8' ) ) !!}
                                    {!! $errors -> first( 'text_ru', '<span class="help-block">:message</span>' ) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group {!! $errors -> has('text_ro') ? 'has-error' : '' !!}">
                                    {!! Form::label( '(RO) Информация за удобството:' ) !!}
                                    {!! Form::textarea( 'text_ro', null, array( 'class' => 'form-control html5area', 'rows' => '8' ) ) !!}
                                    {!! $errors -> first( 'text_ro', '<span class="help-block">:message</span>' ) !!}
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    {!! Form::submit( 'Добави удобството', array( 'class' => 'btn btn-primary btn-block' ) ) !!}
                                </div>
                            </div>
                        {!! Form::close() !!}

                    </div>
                </div>
            </div>

        </section>
    </div>


@endsection