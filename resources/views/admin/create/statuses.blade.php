@extends('admin.master')
@section('content')

    <div class="content-wrapper">
        <section class="content">

            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Създаване на статус</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        {!! Form::open( array( 'action' => 'ReservationsStatusesEntityController@store' ) ) !!}
                            <div class="col-md-12">
                                <div class="form-group {!! $errors -> has('status') ? 'has-error' : '' !!}">
                                    {!! Form::label( 'Име на статуса:' ) !!}
                                    {!! Form::text( 'status', null, array( 'class' => 'form-control' ) ) !!}
                                    {!! $errors -> first( 'status', '<span class="help-block">:message</span>' ) !!}
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    {!! Form::submit( 'Добави статуса', array( 'class' => 'btn btn-primary btn-block' ) ) !!}
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>

        </section>
    </div>

@endsection