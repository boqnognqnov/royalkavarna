@extends('admin.master')
@section('content')

    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script>
        $(function () {
            $(".datepicker").datepicker();
        });

    </script>

    <div class="content-wrapper">

        <section class="content">
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Създаване на промоция за комплекс <strong>{{ $complex->name_bg }}</strong></h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        {!! Form::open( array( 'url' => '/admin/complexes/'.$complex->id.'/promotions/create','files' => true ) ) !!}

                            {!! Form::hidden('complex_id', $complex->id) !!}

                            <div class="col-md-4">
                                <div class="form-group {!! $errors -> has('image') ? 'has-error' : '' !!}">
                                    {!! Form::label( 'Снимка *:' ) !!}
                                    {!! Form::file( 'image', array( 'class' => 'form-control' ) ) !!}
                                    {!! $errors -> first( 'image', '<span class="help-block">:message</span>' ) !!}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group {!! $errors -> has('count_rooms') ? 'has-error' : '' !!}">
                                    {!! Form::label( 'Начало на промоцията *:' ) !!}
                                    {!! Form::text( 'date_from', null, array( 'class' => 'form-control datepicker' ) ) !!}
                                    {!! $errors -> first( 'count_rooms', '<span class="help-block">:message</span>' ) !!}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group {!! $errors -> has('count_rooms') ? 'has-error' : '' !!}">
                                    {!! Form::label( 'Край на промоцията *:' ) !!}
                                    {!! Form::text( 'date_to', null, array( 'class' => 'form-control datepicker' ) ) !!}
                                    {!! $errors -> first( 'count_rooms', '<span class="help-block">:message</span>' ) !!}
                                </div>
                            </div>
                        {{------------------------------------PROMOTION TITLE-----------------------------------------}}
                        <h2>Promotion title *</h2>
                            <div class="col-md-6">
                                <div class="form-group {!! $errors -> has('title_bg') ? 'has-error' : '' !!}">
                                    {!! Form::label( '(BG) Promotion title*:' ) !!}
                                    {!! Form::text( 'title_bg', null, array( 'class' => 'form-control' ) ) !!}
                                    {!! $errors -> first( 'title_bg', '<span class="help-block">:message</span>' ) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group {!! $errors -> has('title_en') ? 'has-error' : '' !!}">
                                    {!! Form::label( '(EN) Promotion title*:' ) !!}
                                    {!! Form::text( 'title_en', null, array( 'class' => 'form-control' ) ) !!}
                                    {!! $errors -> first( 'title_en', '<span class="help-block">:message</span>' ) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group {!! $errors -> has('title_ru') ? 'has-error' : '' !!}">
                                    {!! Form::label( '(RU) Promotion title*:' ) !!}
                                    {!! Form::text( 'title_ru', null, array( 'class' => 'form-control' ) ) !!}
                                    {!! $errors -> first( 'title_ru', '<span class="help-block">:message</span>' ) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group {!! $errors -> has('title_ro') ? 'has-error' : '' !!}">
                                    {!! Form::label( '(RO) Promotion title*:' ) !!}
                                    {!! Form::text( 'title_ro', null, array( 'class' => 'form-control' ) ) !!}
                                    {!! $errors -> first( 'title_ro', '<span class="help-block">:message</span>' ) !!}
                                </div>
                            </div>
                        {{------------------------------------DESCRIPTION---------------------------------------------}}
                        <h2>Description *</h2>
                        <div class="col-md-6">
                            <div class="form-group {!! $errors -> has('description_bg') ? 'has-error' : '' !!}">
                                {!! Form::label( '(BG) Описание*:' ) !!}
                                {!! Form::textarea( 'description_bg', null, array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                {!! $errors -> first( 'description_bg', '<span class="help-block">:message</span>' ) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group {!! $errors -> has('description_en') ? 'has-error' : '' !!}">
                                {!! Form::label( '(EN) Описание*:' ) !!}
                                {!! Form::textarea( 'description_en', null, array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                {!! $errors -> first( 'description_en', '<span class="help-block">:message</span>' ) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group {!! $errors -> has('description_ru') ? 'has-error' : '' !!}">
                                {!! Form::label( '(RU) Описание*:' ) !!}
                                {!! Form::textarea( 'description_ru', null, array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                {!! $errors -> first( 'description_ru', '<span class="help-block">:message</span>' ) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group {!! $errors -> has('description_ro') ? 'has-error' : '' !!}">
                                {!! Form::label( '(RO) Описание*:' ) !!}
                                {!! Form::textarea( 'description_ro', null, array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                {!! $errors -> first( 'description_ro', '<span class="help-block">:message</span>' ) !!}
                            </div>
                        </div>
                        {{-------------------------------PROMOTION MINI TITLE-----------------------------------------}}
                        <h2>Promotion mini title</h2>
                        <div class="col-md-6">
                            <div class="form-group {!! $errors -> has('mini_title_bg') ? 'has-error' : '' !!}">
                                {!! Form::label( '(BG) Promotion MINI title:' ) !!}
                                {!! Form::text( 'mini_title_bg', null, array( 'class' => 'form-control' ) ) !!}
                                {!! $errors -> first( 'mini_title_bg', '<span class="help-block">:message</span>' ) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group {!! $errors -> has('mini_title_en') ? 'has-error' : '' !!}">
                                {!! Form::label( '(EN) Promotion MINI title:' ) !!}
                                {!! Form::text( 'mini_title_en', null, array( 'class' => 'form-control' ) ) !!}
                                {!! $errors -> first( 'mini_title_en', '<span class="help-block">:message</span>' ) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group {!! $errors -> has('mini_title_ru') ? 'has-error' : '' !!}">
                                {!! Form::label( '(RU) Promotion MINI title:' ) !!}
                                {!! Form::text( 'mini_title_ru', null, array( 'class' => 'form-control' ) ) !!}
                                {!! $errors -> first( 'mini_title_ru', '<span class="help-block">:message</span>' ) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group {!! $errors -> has('mini_title_ro') ? 'has-error' : '' !!}">
                                {!! Form::label( '(RO) Promotion MINI title:' ) !!}
                                {!! Form::text( 'mini_title_ro', null, array( 'class' => 'form-control' ) ) !!}
                                {!! $errors -> first( 'mini_title_ro', '<span class="help-block">:message</span>' ) !!}
                            </div>
                        </div>
                        {{------------------------------------MINI DESCRIPTION----------------------------------------}}
                        <h2>MINI Description</h2>
                        <div class="col-md-6">
                            <div class="form-group {!! $errors -> has('mini_description_bg') ? 'has-error' : '' !!}">
                                {!! Form::label( '(BG) MINI Описание*:' ) !!}
                                {!! Form::textarea( 'mini_description_bg', null, array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                {!! $errors -> first( 'mini_description_bg', '<span class="help-block">:message</span>' ) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group {!! $errors -> has('mini_description_en') ? 'has-error' : '' !!}">
                                {!! Form::label( '(EN) MINI Описание*:' ) !!}
                                {!! Form::textarea( 'mini_description_en', null, array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                {!! $errors -> first( 'mini_description_en', '<span class="help-block">:message</span>' ) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group {!! $errors -> has('mini_description_ru') ? 'has-error' : '' !!}">
                                {!! Form::label( '(RU) MINI Описание*:' ) !!}
                                {!! Form::textarea( 'mini_description_ru', null, array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                {!! $errors -> first( 'mini_description_ru', '<span class="help-block">:message</span>' ) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group {!! $errors -> has('mini_description_ro') ? 'has-error' : '' !!}">
                                {!! Form::label( '(RO) MINI Описание*:' ) !!}
                                {!! Form::textarea( 'mini_description_ro', null, array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                {!! $errors -> first( 'mini_description_ro', '<span class="help-block">:message</span>' ) !!}
                            </div>
                        </div>
                        {{--------------------------------------Href--------------------------------------------}}
                        <h2>Link (href)</h2>
                        <div class="col-md-12">
                            <div class="form-group {!! $errors -> has('href') ? 'has-error' : '' !!}">
                                {!! Form::label( 'Href link:' ) !!}
                                {!! Form::text( 'href', null, array( 'class' => 'form-control' ) ) !!}
                                {!! $errors -> first( 'href', '<span class="help-block">:message</span>' ) !!}
                            </div>
                        </div>
                        {{--------------------------------------Href TEXT--------------------------------------------}}
                        <h2>Link text</h2>
                        <div class="col-md-6">
                            <div class="form-group {!! $errors -> has('href_text_bg') ? 'has-error' : '' !!}">
                                {!! Form::label( '(BG) Href link text' ) !!}
                                {!! Form::text( 'href_text_bg', null, array( 'class' => 'form-control' ) ) !!}
                                {!! $errors -> first( 'href_text_bg', '<span class="help-block">:message</span>' ) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group {!! $errors -> has('href_text_en') ? 'has-error' : '' !!}">
                                {!! Form::label( '(EN) Href link text' ) !!}
                                {!! Form::text( 'href_text_en', null, array( 'class' => 'form-control' ) ) !!}
                                {!! $errors -> first( 'href_text_en', '<span class="help-block">:message</span>' ) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group {!! $errors -> has('href_text_ru') ? 'has-error' : '' !!}">
                                {!! Form::label( '(RU) Href link text' ) !!}
                                {!! Form::text( 'href_text_ru', null, array( 'class' => 'form-control' ) ) !!}
                                {!! $errors -> first( 'href_text_ru', '<span class="help-block">:message</span>' ) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group {!! $errors -> has('href_text_ro') ? 'has-error' : '' !!}">
                                {!! Form::label( '(RO) Href link text' ) !!}
                                {!! Form::text( 'href_text_ro', null, array( 'class' => 'form-control' ) ) !!}
                                {!! $errors -> first( 'href_text_ro', '<span class="help-block">:message</span>' ) !!}
                            </div>
                        </div>

                        <div class="col-md-12">
                                <div class="form-group">
                                    {!! Form::submit( 'Добави промоцията', array( 'class' => 'btn btn-primary btn-block' ) ) !!}
                                </div>
                            </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>

        </section>
    </div>

@endsection