@extends('admin.master')
@section('content')

    <div class="content-wrapper">

        <section class="content">
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Създаване на тип стая за комплекс <strong>{{ $complex->name_bg }}</strong></h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        {!! Form::open( array( 'url' => '/admin/complexes/'.$complex->id.'/rooms/create','files' => true ) ) !!}

                            {!! Form::hidden('complex_id', $complex->id) !!}

                            <div class="col-md-3">
                                <div class="form-group {!! $errors -> has('main_image') ? 'has-error' : '' !!}">
                                    {!! Form::label( 'Снимка *:' ) !!}
                                    {!! Form::file( 'main_image', array( 'class' => 'form-control' ) ) !!}
                                    {!! $errors -> first( 'main_image', '<span class="help-block">:message</span>' ) !!}
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group {!! $errors -> has('count_rooms') ? 'has-error' : '' !!}">
                                    {!! Form::label( 'Броя стаи в хотела от този тип *:' ) !!}
                                    {!! Form::text( 'count_rooms', null, array( 'class' => 'form-control' ) ) !!}
                                    {!! $errors -> first( 'count_rooms', '<span class="help-block">:message</span>' ) !!}
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group {!! $errors -> has('beds') ? 'has-error' : '' !!}">
                                    {!! Form::label( 'Брой легла:' ) !!}
                                    {!! Form::select( 'beds', [0,1,2,3,4,5,6,7,8,9,10,11,12], null, array( 'class' => 'form-control' ) ) !!}
                                    {!! $errors -> first( 'beds', '<span class="help-block">:message</span>' ) !!}
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group {!! $errors -> has('extra_beds') ? 'has-error' : '' !!}">
                                    {!! Form::label( 'Брой допълнителни легла:' ) !!}
                                    {!! Form::select( 'extra_beds', [0,1,2,3,4,5,6],null, array( 'class' => 'form-control' ) ) !!}
                                    {!! $errors -> first( 'extra_beds', '<span class="help-block">:message</span>' ) !!}
                                </div>
                            </div>
                        {{------------------------------------TIPE ROOM---------------------------------------------}}
                        <h2>Room type *</h2>
                            <div class="col-md-6">
                                <div class="form-group {!! $errors -> has('type_bg') ? 'has-error' : '' !!}">
                                    {!! Form::label( '(BG) Тип стая*:' ) !!}
                                    {!! Form::text( 'type_bg', null, array( 'class' => 'form-control' ) ) !!}
                                    {!! $errors -> first( 'type_bg', '<span class="help-block">:message</span>' ) !!}
                                </div>
                            </div>
                        <div class="col-md-6">
                            <div class="form-group {!! $errors -> has('type_sub_bg') ? 'has-error' : '' !!}">
                                {!! Form::label( '(BG) Подзаглавие*:' ) !!}
                                {!! Form::text( 'type_sub_bg', null, array( 'class' => 'form-control' ) ) !!}
                                {!! $errors -> first( 'type_sub_bg', '<span class="help-block">:message</span>' ) !!}
                            </div>
                        </div>
                            <div class="col-md-6">
                                <div class="form-group {!! $errors -> has('type_en') ? 'has-error' : '' !!}">
                                    {!! Form::label( '(EN) Тип стая*:' ) !!}
                                    {!! Form::text( 'type_en', null, array( 'class' => 'form-control' ) ) !!}
                                    {!! $errors -> first( 'type_en', '<span class="help-block">:message</span>' ) !!}
                                </div>
                            </div>
                        <div class="col-md-6">
                            <div class="form-group {!! $errors -> has('type_sub_en') ? 'has-error' : '' !!}">
                                {!! Form::label( '(EN) Подзаглавие*:' ) !!}
                                {!! Form::text( 'type_sub_en', null, array( 'class' => 'form-control' ) ) !!}
                                {!! $errors -> first( 'type_sub_en', '<span class="help-block">:message</span>' ) !!}
                            </div>
                        </div>
                            <div class="col-md-6">
                                <div class="form-group {!! $errors -> has('type_ru') ? 'has-error' : '' !!}">
                                    {!! Form::label( '(RU) Тип стая*:' ) !!}
                                    {!! Form::text( 'type_ru', null, array( 'class' => 'form-control' ) ) !!}
                                    {!! $errors -> first( 'type_ru', '<span class="help-block">:message</span>' ) !!}
                                </div>
                            </div>
                        <div class="col-md-6">
                            <div class="form-group {!! $errors -> has('type_sub_ru') ? 'has-error' : '' !!}">
                                {!! Form::label( '(RU) Подзаглавие*:' ) !!}
                                {!! Form::text( 'type_sub_ru', null, array( 'class' => 'form-control' ) ) !!}
                                {!! $errors -> first( 'type_sub_ru', '<span class="help-block">:message</span>' ) !!}
                            </div>
                        </div>
                            <div class="col-md-6">
                                <div class="form-group {!! $errors -> has('type_ro') ? 'has-error' : '' !!}">
                                    {!! Form::label( '(RO) Тип стая*:' ) !!}
                                    {!! Form::text( 'type_ro', null, array( 'class' => 'form-control' ) ) !!}
                                    {!! $errors -> first( 'type_ro', '<span class="help-block">:message</span>' ) !!}
                                </div>
                            </div>
                        <div class="col-md-6">
                            <div class="form-group {!! $errors -> has('type_sub_ro') ? 'has-error' : '' !!}">
                                {!! Form::label( '(RO) Подзаглавие*:' ) !!}
                                {!! Form::text( 'type_sub_ro', null, array( 'class' => 'form-control' ) ) !!}
                                {!! $errors -> first( 'type_sub_ro', '<span class="help-block">:message</span>' ) !!}
                            </div>
                        </div>
                        {{------------------------------------DESCRIPTION---------------------------------------------}}
                        <h2>Description *</h2>
                        <div class="col-md-6">
                            <div class="form-group {!! $errors -> has('description_bg') ? 'has-error' : '' !!}">
                                {!! Form::label( '(BG) Описание*:' ) !!}
                                {!! Form::textarea( 'description_bg', null, array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                {!! $errors -> first( 'description_bg', '<span class="help-block">:message</span>' ) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group {!! $errors -> has('description_en') ? 'has-error' : '' !!}">
                                {!! Form::label( '(EN) Описание*:' ) !!}
                                {!! Form::textarea( 'description_en', null, array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                {!! $errors -> first( 'description_en', '<span class="help-block">:message</span>' ) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group {!! $errors -> has('description_ru') ? 'has-error' : '' !!}">
                                {!! Form::label( '(RU) Описание*:' ) !!}
                                {!! Form::textarea( 'description_ru', null, array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                {!! $errors -> first( 'description_ru', '<span class="help-block">:message</span>' ) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group {!! $errors -> has('description_ro') ? 'has-error' : '' !!}">
                                {!! Form::label( '(RO) Описание*:' ) !!}
                                {!! Form::textarea( 'description_ro', null, array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                {!! $errors -> first( 'description_ro', '<span class="help-block">:message</span>' ) !!}
                            </div>
                        </div>
                        {{------------------------------------OTHER INFO---------------------------------------------}}
                        <h2>Other info</h2>
                        <div class="col-md-6">
                            <div class="form-group {!! $errors -> has('other_info_bg') ? 'has-error' : '' !!}">
                                {!! Form::label( '(BG) Друга информация:' ) !!}
                                {!! Form::textarea( 'other_info_bg', null, array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                {!! $errors -> first( 'other_info_bg', '<span class="help-block">:message</span>' ) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group {!! $errors -> has('other_info_en') ? 'has-error' : '' !!}">
                                {!! Form::label( '(EN) Друга информация:' ) !!}
                                {!! Form::textarea( 'other_info_en', null, array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                {!! $errors -> first( 'other_info_en', '<span class="help-block">:message</span>' ) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group {!! $errors -> has('other_info_ru') ? 'has-error' : '' !!}">
                                {!! Form::label( '(RU) Друга информация:' ) !!}
                                {!! Form::textarea( 'other_info_ru', null, array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                {!! $errors -> first( 'other_info_ru', '<span class="help-block">:message</span>' ) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group {!! $errors -> has('other_info_ro') ? 'has-error' : '' !!}">
                                {!! Form::label( '(RO) Друга информация:' ) !!}
                                {!! Form::textarea( 'other_info_ro', null, array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                {!! $errors -> first( 'other_info_ro', '<span class="help-block">:message</span>' ) !!}
                            </div>
                        </div>
                        {{------------------------------------PRICE INFO---------------------------------------------}}
                        <h2>Price info</h2>
                        <div class="col-md-6">
                            <div class="form-group {!! $errors -> has('price_info_bg') ? 'has-error' : '' !!}">
                                {!! Form::label( '(BG) Ценова информация:' ) !!}
                                {!! Form::textarea( 'price_info_bg', null, array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                {!! $errors -> first( 'price_info_bg', '<span class="help-block">:message</span>' ) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group {!! $errors -> has('price_info_en') ? 'has-error' : '' !!}">
                                {!! Form::label( '(EN) Ценова информация:' ) !!}
                                {!! Form::textarea( 'price_info_en', null, array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                {!! $errors -> first( 'price_info_en', '<span class="help-block">:message</span>' ) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group {!! $errors -> has('price_info_ru') ? 'has-error' : '' !!}">
                                {!! Form::label( '(RU) Ценова информация:' ) !!}
                                {!! Form::textarea( 'price_info_ru', null, array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                {!! $errors -> first( 'price_info_ru', '<span class="help-block">:message</span>' ) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group {!! $errors -> has('price_info_ro') ? 'has-error' : '' !!}">
                                {!! Form::label( '(RO) Ценова информация:' ) !!}
                                {!! Form::textarea( 'price_info_ro', null, array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                {!! $errors -> first( 'price_info_ro', '<span class="help-block">:message</span>' ) !!}
                            </div>
                        </div>

                        <div class="col-md-12">
                                <div class="form-group">
                                    {!! Form::submit( 'Добави стаята', array( 'class' => 'btn btn-primary btn-block' ) ) !!}
                                </div>
                            </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>

        </section>
    </div>

@endsection