@extends('admin.master')
@section('content')

    <div class="content-wrapper">

        <section class="content">
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Създаване на комплекс</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        {!! Form::open( array( 'action' => 'ComplexesEntityController@store','files' => true ) ) !!}

                            <div class="col-md-3">
                                <div class="form-group {!! $errors -> has('image') ? 'has-error' : '' !!}">
                                    {!! Form::label( 'Снимка:' ) !!}
                                    {!! Form::file( 'image', array( 'class' => 'form-control' ) ) !!}
                                    {!! $errors -> first( 'image', '<span class="help-block">:message</span>' ) !!}
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group {!! $errors -> has('logo') ? 'has-error' : '' !!}">
                                    {!! Form::label( 'Лого:' ) !!}
                                    {!! Form::file( 'logo', array( 'class' => 'form-control' ) ) !!}
                                    {!! $errors -> first( 'logo', '<span class="help-block">:message</span>' ) !!}
                                </div>
                            </div>
                        <div class="col-md-3">
                            <div class="form-group {!! $errors -> has('guest_book_background') ? 'has-error' : '' !!}">
                                {!! Form::label( 'Guest book BackGround:' ) !!}
                                {!! Form::file( 'guest_book_background', array( 'class' => 'form-control' ) ) !!}
                                {!! $errors -> first( 'guest_book_background', '<span class="help-block">:message</span>' ) !!}
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group {!! $errors -> has('guest_book_color') ? 'has-error' : '' !!}">
                                {!! Form::label( 'Корица на книгата в guest book:' ) !!}
                                {!! Form::file( 'guest_book_color', array( 'class' => 'form-control' ) ) !!}
                                {!! $errors -> first( 'guest_book_color', '<span class="help-block">:message</span>' ) !!}
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group {!! $errors -> has('is_inseparable') ? 'has-error' : '' !!}">
                                {!! Form::label( 'Начин на предлагане на комплекса:' ) !!}
                                {!! Form::select( 'is_inseparable', ['no' => 'Choose...', 0=>'Разделен (на стаи)', 1 => 'Цялостно '], null, array( 'class' => 'form-control' ) ) !!}
                                {!! $errors -> first( 'is_inseparable', '<span class="help-block">:message</span>' ) !!}
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group {!! $errors -> has('is_percentage') ? 'has-error' : '' !!}">
                                {!! Form::label( 'Тип на калкулиране на доп. легла:' ) !!}
                                {!! Form::select( 'is_percentage', ['no' => 'Choose...', 0=>'Фиксирана сума', 1 => 'Процент'], null, array( 'class' => 'form-control' ) ) !!}
                                {!! $errors -> first( 'is_percentage', '<span class="help-block">:message</span>' ) !!}
                            </div>
                        </div>
                            <div class="col-md-4">
                                <div class="form-group {!! $errors -> has('extra_bed_price') ? 'has-error' : '' !!}">
                                    {!! Form::label( 'Сума/Процент за допълнително легло:' ) !!}
                                    {!! Form::text( 'extra_bed_price', null, array( 'class' => 'form-control' ) ) !!}
                                    {!! $errors -> first( 'extra_bed_price', '<span class="help-block">:message</span>' ) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group {!! $errors -> has('name_bg') ? 'has-error' : '' !!}">
                                    {!! Form::label( '(BG) Име на комплекс:' ) !!}
                                    {!! Form::text( 'name_bg', null, array( 'class' => 'form-control' ) ) !!}
                                    {!! $errors -> first( 'name_bg', '<span class="help-block">:message</span>' ) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group {!! $errors -> has('name_en') ? 'has-error' : '' !!}">
                                    {!! Form::label( '(EN) Име на комплекс:' ) !!}
                                    {!! Form::text( 'name_en', null, array( 'class' => 'form-control' ) ) !!}
                                    {!! $errors -> first( 'name_en', '<span class="help-block">:message</span>' ) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group {!! $errors -> has('name_ru') ? 'has-error' : '' !!}">
                                    {!! Form::label( '(RU) Име на комплекс:' ) !!}
                                    {!! Form::text( 'name_ru', null, array( 'class' => 'form-control' ) ) !!}
                                    {!! $errors -> first( 'name_ru', '<span class="help-block">:message</span>' ) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group {!! $errors -> has('name_ro') ? 'has-error' : '' !!}">
                                    {!! Form::label( '(RO) Име на комплекс:' ) !!}
                                    {!! Form::text( 'name_ro', null, array( 'class' => 'form-control' ) ) !!}
                                    {!! $errors -> first( 'name_ro', '<span class="help-block">:message</span>' ) !!}
                                </div>
                            </div>
                        <div class="col-md-6">
                            <div class="form-group {!! $errors -> has('mini_information_bg') ? 'has-error' : '' !!}">
                                {!! Form::label( '(BG) MINI Информация за HomePage:' ) !!}
                                {!! Form::textarea( 'mini_information_bg', null, array( 'class' => 'form-control html5area', 'rows' => '8' ) ) !!}
                                {!! $errors -> first( 'mini_information_bg', '<span class="help-block">:message</span>' ) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group {!! $errors -> has('mini_information_en') ? 'has-error' : '' !!}">
                                {!! Form::label( '(EN) MINI Информация за HomePage:' ) !!}
                                {!! Form::textarea( 'mini_information_en', null, array( 'class' => 'form-control html5area', 'rows' => '8' ) ) !!}
                                {!! $errors -> first( 'mini_information_en', '<span class="help-block">:message</span>' ) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group {!! $errors -> has('mini_information_ru') ? 'has-error' : '' !!}">
                                {!! Form::label( '(RU) MINI Информация за HomePage:' ) !!}
                                {!! Form::textarea( 'mini_information_ru', null, array( 'class' => 'form-control html5area', 'rows' => '8' ) ) !!}
                                {!! $errors -> first( 'mini_information_ru', '<span class="help-block">:message</span>' ) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group {!! $errors -> has('mini_information_ro') ? 'has-error' : '' !!}">
                                {!! Form::label( '(RO) MINI Информация за HomePage:' ) !!}
                                {!! Form::textarea( 'mini_information_ro', null, array( 'class' => 'form-control html5area', 'rows' => '8' ) ) !!}
                                {!! $errors -> first( 'mini_information_ro', '<span class="help-block">:message</span>' ) !!}
                            </div>
                        </div>



                        <div class="col-md-6">
                            <div class="form-group {!! $errors -> has('location_information_bg') ? 'has-error' : '' !!}">
                                {!! Form::label( '(BG) Location Информация за Картата на комплекса:' ) !!}
                                {!! Form::textarea( 'location_information_bg', null, array( 'class' => 'form-control html5area', 'rows' => '8' ) ) !!}
                                {!! $errors -> first( 'location_information_bg', '<span class="help-block">:message</span>' ) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group {!! $errors -> has('location_information_en') ? 'has-error' : '' !!}">
                                {!! Form::label( '(EN) Location Информация за Картата на комплекса:' ) !!}
                                {!! Form::textarea( 'location_information_en', null, array( 'class' => 'form-control html5area', 'rows' => '8' ) ) !!}
                                {!! $errors -> first( 'location_information_en', '<span class="help-block">:message</span>' ) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group {!! $errors -> has('location_information_ru') ? 'has-error' : '' !!}">
                                {!! Form::label( '(RU) Location Информация за Картата на комплекса:' ) !!}
                                {!! Form::textarea( 'location_information_ru', null, array( 'class' => 'form-control html5area', 'rows' => '8' ) ) !!}
                                {!! $errors -> first( 'location_information_ru', '<span class="help-block">:message</span>' ) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group {!! $errors -> has('location_information_ro') ? 'has-error' : '' !!}">
                                {!! Form::label( '(RO) Location Информация за Картата на комплекса:' ) !!}
                                {!! Form::textarea( 'location_information_ro', null, array( 'class' => 'form-control html5area', 'rows' => '8' ) ) !!}
                                {!! $errors -> first( 'location_information_ro', '<span class="help-block">:message</span>' ) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group {!! $errors -> has('about_bg') ? 'has-error' : '' !!}">
                                {!! Form::label( '(BG) Информация за комплекса:' ) !!}
                                {!! Form::textarea( 'about_bg', null, array( 'class' => 'form-control html5area', 'rows' => '8' ) ) !!}
                                {!! $errors -> first( 'about_bg', '<span class="help-block">:message</span>' ) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group {!! $errors -> has('about_en') ? 'has-error' : '' !!}">
                                {!! Form::label( '(EN) Информация за комплекса:' ) !!}
                                {!! Form::textarea( 'about_en', null, array( 'class' => 'form-control html5area', 'rows' => '8' ) ) !!}
                                {!! $errors -> first( 'about_en', '<span class="help-block">:message</span>' ) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group {!! $errors -> has('about_ru') ? 'has-error' : '' !!}">
                                {!! Form::label( '(RU) Информация за комплекса:' ) !!}
                                {!! Form::textarea( 'about_ru', null, array( 'class' => 'form-control html5area', 'rows' => '8' ) ) !!}
                                {!! $errors -> first( 'about_ru', '<span class="help-block">:message</span>' ) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group {!! $errors -> has('about_ro') ? 'has-error' : '' !!}">
                                {!! Form::label( '(RO) Информация за комплекса:' ) !!}
                                {!! Form::textarea( 'about_ro', null, array( 'class' => 'form-control html5area', 'rows' => '8' ) ) !!}
                                {!! $errors -> first( 'about_ro', '<span class="help-block">:message</span>' ) !!}
                            </div>
                        </div>
                            <div class="col-md-6">
                                <div class="form-group {!! $errors -> has('early_booking_text_bg') ? 'has-error' : '' !!}">
                                    {!! Form::label( '(BG) Промоционален текст:' ) !!}
                                    {!! Form::textarea( 'early_booking_text_bg', null, array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                    {!! $errors -> first( 'early_booking_text_bg', '<span class="help-block">:message</span>' ) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group {!! $errors -> has('early_booking_text_en') ? 'has-error' : '' !!}">
                                    {!! Form::label( '(EN) Промоционален текст:' ) !!}
                                    {!! Form::textarea( 'early_booking_text_en', null, array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                    {!! $errors -> first( 'early_booking_text_en', '<span class="help-block">:message</span>' ) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group {!! $errors -> has('early_booking_text_ru') ? 'has-error' : '' !!}">
                                    {!! Form::label( '(RU) Промоционален текст:' ) !!}
                                    {!! Form::textarea( 'early_booking_text_ru', null, array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                    {!! $errors -> first( 'early_booking_text_ru', '<span class="help-block">:message</span>' ) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group {!! $errors -> has('early_booking_text_ro') ? 'has-error' : '' !!}">
                                    {!! Form::label( '(RO) Промоционален текст:' ) !!}
                                    {!! Form::textarea( 'early_booking_text_ro', null, array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                    {!! $errors -> first( 'early_booking_text_ro', '<span class="help-block">:message</span>' ) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group {!! $errors -> has('address_bg') ? 'has-error' : '' !!}">
                                    {!! Form::label( '(BG) Адрес на комплекса:' ) !!}
                                    {!! Form::text( 'address_bg', null, array( 'class' => 'form-control' ) ) !!}
                                    {!! $errors -> first( 'address_bg', '<span class="help-block">:message</span>' ) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group {!! $errors -> has('address_en') ? 'has-error' : '' !!}">
                                    {!! Form::label( '(EN) Адрес на комплекса:' ) !!}
                                    {!! Form::text( 'address_en', null, array( 'class' => 'form-control' ) ) !!}
                                    {!! $errors -> first( 'address_en', '<span class="help-block">:message</span>' ) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group {!! $errors -> has('address_ru') ? 'has-error' : '' !!}">
                                    {!! Form::label( '(RU) Адрес на комплекса:' ) !!}
                                    {!! Form::text( 'address_ru', null, array( 'class' => 'form-control' ) ) !!}
                                    {!! $errors -> first( 'address_ru', '<span class="help-block">:message</span>' ) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group {!! $errors -> has('address_ro') ? 'has-error' : '' !!}">
                                    {!! Form::label( '(RO) Адрес на комплекса:' ) !!}
                                    {!! Form::text( 'address_ro', null, array( 'class' => 'form-control' ) ) !!}
                                    {!! $errors -> first( 'address_ro', '<span class="help-block">:message</span>' ) !!}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group {!! $errors -> has('phone') ? 'has-error' : '' !!}">
                                    {!! Form::label( 'Телефон:' ) !!}
                                    {!! Form::text( 'phone', null, array( 'class' => 'form-control' ) ) !!}
                                    {!! $errors -> first( 'phone', '<span class="help-block">:message</span>' ) !!}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group {!! $errors -> has('mobile_phone') ? 'has-error' : '' !!}">
                                    {!! Form::label( 'Мобилен телефон:' ) !!}
                                    {!! Form::text( 'mobile_phone', null, array( 'class' => 'form-control' ) ) !!}
                                    {!! $errors -> first( 'mobile_phone', '<span class="help-block">:message</span>' ) !!}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group {!! $errors -> has('reception_phone') ? 'has-error' : '' !!}">
                                    {!! Form::label( 'Телефон рецепция:' ) !!}
                                    {!! Form::text( 'reception_phone', null, array( 'class' => 'form-control' ) ) !!}
                                    {!! $errors -> first( 'reception_phone', '<span class="help-block">:message</span>' ) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group {!! $errors -> has('email') ? 'has-error' : '' !!}">
                                    {!! Form::label( 'E-mail адрес:' ) !!}
                                    {!! Form::text( 'email', null, array( 'class' => 'form-control' ) ) !!}
                                    {!! $errors -> first( 'email', '<span class="help-block">:message</span>' ) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group {!! $errors -> has('coordinates') ? 'has-error' : '' !!}">
                                    {!! Form::label( 'Кординати на комплекса:' ) !!}
                                    {!! Form::text( 'coordinates', null, array( 'class' => 'form-control' ) ) !!}
                                    {!! $errors -> first( 'coordinates', '<span class="help-block">:message</span>' ) !!}
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    {!! Form::submit( 'Добави комплекса', array( 'class' => 'btn btn-primary btn-block' ) ) !!}
                                </div>
                            </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>

        </section>
    </div>

@endsection