@extends('admin.master')
@section('content')

    <div class="content-wrapper">
        <section class="content">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Всички статуси</h3>
                    <div class="box-tools">
                        <a href="{!! url('admin/statuses/create') !!}" class="btn btn-block btn-success btn-flat">Добавяне на статус</a>
                    </div>
                </div>
                <div class="box-body no-padding">
                    @if(Session::has('successMessage'))
                        <div class="alert alert-success">
                            {!! Session::get('successMessage') !!}
                        </div>
                    @endif

                    @if($errors -> any() )
                        @foreach ($errors->all() as $error)
                            <div class="alert alert-danger" style="width: 100%;">
                                <button type="button" class="close" data-dismiss="alert">?</button>
                                <p>{{ $error }}</p>
                            </div>
                        @endforeach
                    @endif
                    <table class="table table-striped">
                        <tr>
                            <th width="10%">#</th>
                            <th width="60%">Име на статуса</th>
                            <th width="20%">Редакция</th>
                            <th width="20%">Изтрий</th>
                        </tr>
                        @foreach( $statusData as $key => $status )
                            <tr>
                                <td>{!! $status -> id !!}</td>
                                <td>{!! $status -> status !!}</td>
                                <td><a href="{!! url('admin/statuses/edit/' . $status -> id ) !!}" class="btn btn-primary">Редакция</a></td>
                                <td><a href="{!! url('admin/statuses/delete/' . $status -> id  ) !!}" class="btn btn-danger">Изтрий</a></td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </section>
    </div>

@endsection