@extends('admin.master')
@section('content')

    <div class="content-wrapper">
        <section class="content-header">
            {{--<h1>--}}
            {{--Page Header--}}
            {{--<small>Optional description</small>--}}
            {{--</h1>--}}
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
                <li class="active">Here</li>
            </ol>
        </section>

        <section class="content">

            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Слайдъри</h3>

                    <div class="box-tools">
                        {{--<a class="btn btn-primary" href="{!! url('admin/sliders/create') !!}" >Добавяне на слайдър</a>--}}
                    </div>
                </div>
                <div class="box-body no-padding">
                    @if(Session::has('successMessage'))
                        <div class="alert alert-success">
                            {!! Session::get('successMessage') !!}
                        </div>
                    @endif

                    @if($errors -> any() )
                        @foreach ($errors->all() as $error)
                            <div class="alert alert-danger" style="width: 100%;">
                                <button type="button" class="close" data-dismiss="alert">?</button>
                                <p>{{ $error }}</p>
                            </div>
                        @endforeach
                    @endif
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-12">

                                {!! Form::open(array('action' => 'SlidersEntityController@update','class'=>'form-horizontal','files'=>true)) !!}

                                {!! Form::hidden('id', $slider->id) !!}
                                <div class="col-md-6">
                                    <div class="form-group {!! $errors -> has('image') ? 'has-error' : '' !!}">
                                        {!! Form::label( 'Снимка:' ) !!}
                                        {!! Form::file( 'image', array( 'class' => 'form-control' ) ) !!}
                                        {!! $errors -> first( 'image', '<span class="help-block">:message</span>' ) !!}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <img src="{!! asset(\App\Slider::$imagePath.$slider->image) !!}" width="200px">
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group {!! $errors -> has('image') ? 'has-error' : '' !!}">
                                        {!! Form::label( 'Активност' ) !!}
                                        {!! Form::checkbox( 'is_active',true,$slider->is_active ) !!}
                                        {!! $errors -> first( 'image', '<span class="help-block">:message</span>' ) !!}
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group {!! $errors -> has('image') ? 'has-error' : '' !!}">
                                        {!! Form::label( 'Линк' ) !!}
                                        {!! Form::text( 'href',$slider->href,['class' => 'form-control'] ) !!}
                                        {!! $errors -> first( 'image', '<span class="help-block">:message</span>' ) !!}
                                    </div>
                                </div>

                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active"><a href="#area_en" aria-controls="area_en"
                                                                              role="tab"
                                                                              data-toggle="tab">Текс EN</a></li>
                                    <li role="presentation"><a href="#area_bg" aria-controls="area_bg" role="tab"
                                                               data-toggle="tab">Текс BG</a></li>
                                    <li role="presentation"><a href="#area_ru" aria-controls="area_ru" role="tab"
                                                               data-toggle="tab">Текс RU</a></li>
                                    <li role="presentation"><a href="#area_ro" aria-controls="area_ro" role="tab"
                                                               data-toggle="tab">Текс RO</a></li>
                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="area_en">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                {!! Form::textarea( 'text_en', $slider->text_en, array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="area_bg">
                                        <div class="col-md-12">
                                            <div class="form-group">

                                                {!! Form::textarea( 'text_bg', $slider->text_bg, array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}

                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="area_ru">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                {!! Form::textarea( 'text_ru', $slider->text_ru, array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="area_ro">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                {!! Form::textarea( 'text_ro', $slider->text_ro, array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>


                    <div class="col-md-12">
                        <div class="form-group">
                            {!! Form::submit( 'Промени', array( 'class' => 'btn btn-primary btn-block' ) ) !!}
                        </div>
                    </div>

                    {!! Form::close() !!}


                </div>

            </div>


        </section>
    </div>



@endsection