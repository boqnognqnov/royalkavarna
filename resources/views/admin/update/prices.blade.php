@extends('admin.master')
@section('content')

    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <link rel="stylesheet" href="/resources/demos/style.css">
    <div class="content-wrapper">
        <section class="content">

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Цена за стая "<strong id="roomName"></strong>"</h4>
                </div>
                <div class="col-sm-12">
                    <div class="modal-body" id="add_price_modal_body">
                        {!! Form::open( array( 'action' => 'RoomsPricesEntityController@update' ) ) !!}

                        {!! Form::hidden('id', $pricesData->id) !!}
                        {!! Form::hidden('room_id', $pricesData->room_id) !!}
                        <div class="form-group {!! $errors -> has('price') ? 'has-error' : '' !!}">
                            {!! Form::label( 'Цена за периода:', 'Цена за периода',['class'=>'col-sm-4 control-label'] ) !!}
                            <div class="col-sm-8">
                                {!! Form::text( 'price', $pricesData->price, array( 'class' => 'form-control' ) ) !!}
                            </div>
                            {!! $errors -> first( 'price', '<span class="help-block">:message</span>' ) !!}
                        </div>
                        <div class="form-group {!! $errors -> has('from_date') ? 'has-error' : '' !!}">
                            {!! Form::label('От дата','От дата',['class'=>'col-sm-4 control-label']) !!}
                            <div class="col-sm-8">
                                {!! Form::text( 'from_date', date('m/d/Y', strtotime($pricesData->from_date)),['class'=>'datepicker form-control'] ) !!}
                            </div>
                            {!! $errors -> first( 'from_date', '<span class="help-block">:message</span>' ) !!}
                        </div>

                        <div class="form-group {!! $errors -> has('to_date') ? 'has-error' : '' !!}">
                            {!! Form::label('До дата','До дата',['class'=>'col-sm-4 control-label']) !!}
                            <div class="col-sm-8">
                                {!! Form::text( 'to_date', date('m/d/Y', strtotime($pricesData->to_date)),['class'=>'datepicker form-control'] ) !!}
                            </div>
                            {!! $errors -> first( 'to_date', '<span class="help-block">:message</span>' ) !!}
                        </div>


                        <div class="form-group col-sm-5">
                        </div>
                        <div class="form-group col-sm-7">
                            {!! Form::submit('Запази', ['class'=>'btn btn-success']) !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <a href="{!! url('admin/complexes/'.$complexId.'/rooms/'.$pricesData->room_id.'/prices') !!}" class="btn btn-primary">Назад</a>
                </div>
            </div>

        </section>
    </div>
    <script>
        $(function () {
            $(".datepicker").datepicker();
        });

    </script>

@endsection