@extends('admin.master')
@section('content')

    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script>
        $(function () {
            $(".datepicker").datepicker();
        });

    </script>

    <div class="content-wrapper">
        <section class="content">
            @if(Session::has('successMessage'))
                <div class="alert alert-success">
                    {!! Session::get('successMessage') !!}
                </div>
            @endif

            @if($errors -> any() )
                @foreach ($errors->all() as $error)
                    <div class="alert alert-danger" style="width: 100%;">
                        <button type="button" class="close" data-dismiss="alert">?</button>
                        <p>{{ $error }}</p>
                    </div>
                @endforeach
            @endif
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Редакция на резервация</h3>
                </div>

                <div class="box-body">
                    <div class="container-fluid">

                        <h4 class="box-title">Данни за клиент:</h4>

                        {!! Form::open(array('action' => 'ReservationsEntityController@updatePersonalData','class'=>'form-horizontal')) !!}
                        {!! Form::hidden('id',$reservation->id) !!}
                        <div class="form-group">
                            {!! Form::label('Име:','Име:',['class'=>'col-sm-2 control-label']) !!}
                            <div class="col-sm-10">
                                {!! Form::text( 'firstname',$reservation->firstname,['class'=>'form-control'] ) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('Презиме:','Презиме:',['class'=>'col-sm-2 control-label']) !!}
                            <div class="col-sm-10">
                                {!! Form::text( 'middle',$reservation->middle,['class'=>'form-control'] ) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('Фамилия:','Фамилия:',['class'=>'col-sm-2 control-label']) !!}
                            <div class="col-sm-10">
                                {!! Form::text( 'lastname',$reservation->lastname,['class'=>'form-control'] ) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('Адрес:','Адрес:',['class'=>'col-sm-2 control-label']) !!}
                            <div class="col-sm-10">
                                {!! Form::text( 'address',$reservation->address,['class'=>'form-control'] ) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('Град:','Град:',['class'=>'col-sm-2 control-label']) !!}
                            <div class="col-sm-10">
                                {!! Form::text( 'city',$reservation->city,['class'=>'form-control'] ) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('Държава:','Държава:',['class'=>'col-sm-2 control-label']) !!}
                            <div class="col-sm-10">
                                {!! Form::text( 'country',$reservation->country,['class'=>'form-control'] ) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('Телефонен код:','Телефонен код:',['class'=>'col-sm-2 control-label']) !!}
                            <div class="col-sm-10">
                                {!! Form::text( 'telephonecode',$reservation->telephonecode,['class'=>'form-control'] ) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('Телефон:','Телефон:',['class'=>'col-sm-2 control-label']) !!}
                            <div class="col-sm-10">
                                {!! Form::text( 'telephone1',$reservation->telephone1,['class'=>'form-control'] ) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('Телефон 2:','Телефон 2:',['class'=>'col-sm-2 control-label']) !!}
                            <div class="col-sm-10">
                                {!! Form::text( 'telephone2',$reservation->telephone2,['class'=>'form-control'] ) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('Email:','Email:',['class'=>'col-sm-2 control-label']) !!}
                            <div class="col-sm-10">
                                {!! Form::text( 'email',$reservation->email,['class'=>'form-control'] ) !!}
                            </div>
                        </div>

                        <div class="form-group text-center">
                            {!! Form::submit('Промени', ['class'=>'btn btn-success']) !!}
                        </div>
                        {!! Form::close() !!}


                        <div class="row">
                            <h4>Данни за резервация:</h4>

                            {!! Form::open(array('action' => 'ReservationsEntityController@updateRoomsAndDate','class'=>'form-horizontal')) !!}
                            {!! Form::hidden('reservation_id',$reservation->id) !!}
                            {!! Form::hidden('complex_id',$reservation->complex_id) !!}
                            <div class="form-group">
                                {!! Form::label('Дата от:','Дата от:',['class'=>'col-sm-2 control-label']) !!}
                                <div class="col-sm-10">
                                    {!! Form::text( 'date_from',$reservation->date_from,['class'=>'form-control datepicker'] ) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!! Form::label('Дата до:','Дата до:',['class'=>'col-sm-2 control-label']) !!}
                                <div class="col-sm-10">
                                    {!! Form::text( 'date_to',$reservation->date_to,['class'=>'form-control datepicker'] ) !!}
                                </div>
                            </div>
                            <div class="room_rows_div">
                                @foreach($reservedRoooms as $oneResRoom)
                                    <div class="row room_{!! $oneResRoom->room_id !!}">
                                        <div class="col-xs-3">
                                            <br>
                                            <h4>{!! $rooms[$oneResRoom->room_id] !!}</h4>
                                        </div>
                                        <div class="col-xs-2">
                                            <div class="form-group">
                                                <label for="label-adult">Възрастни:</label>
                                                <br>
                                                {!! Form::select('data['.$oneResRoom->room_id.']['.$oneResRoom->id.'][adult]',['0'=>'0','1'=>'1','2'=>'2','3'=>'3','4'=>'4'],$oneResRoom->adults,['class'=>'form-control adult-selector']) !!}
                                            </div>

                                        </div>
                                        <div class="col-xs-2">
                                            <div class="form-group">
                                                <label for="label-child">Деца:</label>
                                                <br>
                                                {!! Form::select('data['.$oneResRoom->room_id.']['.$oneResRoom->id.'][child]',['0'=>'0','1'=>'1','2'=>'2','3'=>'3','4'=>'4'],$oneResRoom->children,['class'=>'form-control child-selector']) !!}
                                            </div>
                                        </div>

                                        <div class="col-xs-2">
                                            <div class="form-group">
                                                <br>
                                                <a class="btn btn-danger removeRowButton">remove</a>
                                            </div>
                                        </div>
                                        <div class="col-xs-3">
                                            <div class="form-group">
                                                <br><br>
                                                Свободни стаи:<strong> <span
                                                            style="color: green; font-size: large; "
                                                            class="freeRoomsSpan"></span></strong>
                                                &nbsp;&nbsp; Цена:
                                                <strong>

                                                    <del><span class="roomPriceReal"></span></del>
                                                    <span
                                                            style="color: green; font-size: large; "
                                                            class="roomPriceDiscount"></span> лв.

                                                </strong>

                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <div class="form-group text-center">
                                {!! Form::submit('Промени', ['class'=>'btn btn-success']) !!}
                            </div>
                            {!! Form::close() !!}

                        </div>

                        <div class="row">
                            <div class="col-xs-6 add_room_div">
                                {!! Form::select('rooms',$room_types,null,['class'=>'form-control add_rooms_select']) !!}
                            </div>
                            <div class="col-xs-2">
                                <button class="add_row">Добави стая</button>
                            </div>
                            <div class="col-xs-4">
                                <strong>Крайна цена:</strong><span style="color: green; font-size: large; "
                                                                   class="totalPrice"></span>
                            </div>

                        </div>

                    </div>
                </div>


            </div>
            <div class="text-center">
                <a href="{!! asset('admin/reservations/complex/'.$reservation->complex_id) !!}" class="btn btn-primary">Назад</a>
            </div>

        </section>
    </div>

    <div class="room_row_teplate" style="display: none">
        <div class="row">
            <div class="col-xs-3">
                <br>
                <h4 class="temp_room_name"></h4>
            </div>
            <div class="col-xs-2">
                <div class="form-group">
                    <label for="label-adult">Възрастни:</label>
                    <br>
                    <select class="form-control adult-selector">
                        <option value="0">0</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                    </select>
                </div>

            </div>
            <div class="col-xs-2">
                <div class="form-group">
                    <label for="label-child">Деца:</label>
                    <br>
                    <select class="form-control child-selector">
                        <option value="0">0</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                    </select>
                </div>
            </div>

            <div class="col-xs-2">
                <div class="form-group">
                    <br>
                    <a class="btn btn-danger removeRowButton">remove</a>
                </div>
            </div>
            <div class="col-xs-3">

                <div class="form-group">
                    <br><br>
                    Свободни стаи:<strong> <span
                                style="color: green; font-size: large; "
                                class="freeRoomsSpan"></span></strong>
                    &nbsp;&nbsp; Цена:
                    <strong>

                        <del><span class="roomPriceReal">0 лв.</span></del>
                                                    <span
                                                            style="color: green; font-size: large; "
                                                            class="roomPriceDiscount">0</span> лв.

                    </strong>

                </div>
            </div>
        </div>


    </div>

    <script>

        //        DOCUMENT READY
        $(document).ready(function () {

            setFreeRoomsOnRows();

            setFreeRoomsOnDropDown();

            addNewRow();

            adultChildEventsListener();

            defineDeleteRowButton();

            setPriceOnAllListedRows();

            datePickerEventListener();


            getPricePerRoomByOptionElements();


        });

    </script>

    <script>
        //        DATEPICKER EVENT LISTENER
        function datePickerEventListener() {
            $(document).on('change', '.datepicker', function () { // Change event for datepicker
                setFreeRoomsOnRows();
                setFreeRoomsOnDropDown();
                setPriceOnAllListedRows();
                getPricePerRoomByOptionElements();
            });
        }
    </script>

    <script>
        //ADD NEW ROW
        function addNewRow() {
            $('.add_row').on('click', function (event) {
                        var roomId = $(".add_rooms_select").val();
                        var roomName = $(".add_rooms_select option:selected").text();
                        if (roomName.indexOf("||") >= 0) {
                            var tempName = roomName.split('||');
                            roomName = tempName[0];
                        }

                        var time = $.now();

                        $('.room_row_teplate .row').attr('class', 'row room_' + roomId);
                        $('.room_row_teplate .temp_room_name').text(roomName);
                        $('.room_row_teplate .child-selector').attr('name', 'data[' + roomId + '][' + time + '][child]');
                        $('.room_row_teplate .adult-selector').attr('name', 'data[' + roomId + '][' + time + '][adult]');

                        var tempRoomRow = $('.room_row_teplate').html();
                        $('.room_rows_div').append(tempRoomRow);

//                DEFINE EVENTS FOR NEW ROWS

                        setFreeRoomsOnRows();

                        adultChildEventsListener();

                        setPriceOnAllListedRows();

                        defineDeleteRowButton();

                    }
            )
            ;
        }

    </script>

    <script>
        //        DELETE EVENT
        function defineDeleteRowButton() {
            $('.removeRowButton').on('click', function (event) {
                findAndRemoveSelectedRow($(this));
                setPriceOnAllListedRows();
            });
        }


        function adultChildEventsListener() {
            $('.room_rows_div .child-selector').on('click, change', function (event) {
                setPriceOnAllListedRows();
            });

            $('.room_rows_div .adult-selector').on('click, change', function (event) {
                setPriceOnAllListedRows();
            });
        }


        function findAndRemoveSelectedRow(removeButton) {
            removeButton.parent().closest('.row').remove();
        }

    </script>


    <script>


        function setFreeRoomsOnRows() {
            $('.room_rows_div').children('.row').each(function () {

                var rowClassName = $(this).attr('class');
                var roomId = rowClassName.split("row room_").pop();
                var dateFrom = $('input[name="date_from"]').val();
                var dateTo = $('input[name="date_to"]').val();

                var data = {};
                data['room_id'] = roomId;
                data['date_from'] = dateFrom;
                data['date_to'] = dateTo;

                var elementType = 'row';

                postAjaxFreeRooms(data, $(this), elementType);

            });
        }

        function setFreeRoomsOnDropDown() {
            $('.add_rooms_select').children('option').each(function () {

                var dateFrom = $('input[name="date_from"]').val();
                var dateTo = $('input[name="date_to"]').val();

                var data = {};
                data['room_id'] = $(this).val();
                data['date_from'] = dateFrom;
                data['date_to'] = dateTo;

                var elementType = 'dropDown';

                postAjaxFreeRooms(data, $(this), elementType);
            });
        }


        function postAjaxFreeRooms(objectData, elementForSet, elementType) {

            $.ajaxSetup({headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'}});

            $.ajax({
                url: "/api/reservation/get_free_rooms",
                type: "POST",
                dataType: "json",
                data: objectData
            }).done(function (result) {
//                alert(result['data']['freeRooms']);
                var freeRooms = result['data']['freeRooms'];
                if (elementType == 'row') {
                    putFreeRoomsOnRow(freeRooms, elementForSet);
                } else if (elementType == 'dropDown') {
                    putFreeRoomsOnDropDown(freeRooms, elementForSet);
                }

            }).fail(function (result) {
//                alert('Errors counting free rooms');
            });

        }

        function putFreeRoomsOnRow(freeRooms, row) {
            row.find('.freeRoomsSpan').text(' ' + freeRooms);
        }

        function putFreeRoomsOnDropDown(freeRooms, option) {

            var optionText = option.text();

            if (optionText.indexOf("||") >= 0) {
                var optionTextSplitArr = optionText.split('||');
                var textToAppend = ' || свободни: ' + freeRooms;
                optionText = optionTextSplitArr[0] + textToAppend;
                option.text(optionText);
            } else {
                option.text(optionText + ' || свободни: ' + freeRooms);
            }
        }


        function setPriceOnAllListedRows() {

            var dataToPost = {};
            dataToPost['complex_id'] = $('input[name="complex_id"]').val();
            dataToPost['date_from'] = $('input[name="date_from"]').val();
            dataToPost['date_to'] = $('input[name="date_to"]').val();
            dataToPost['data'] = {};


            $('.room_rows_div').children('.row').each(function () {


                var rowClassName = $(this).attr('class');
                var roomId = rowClassName.split("row room_").pop();


                if (!$.isPlainObject(dataToPost['data'][roomId])) {
                    dataToPost['data'][roomId] = {};
                }


                var rowRawName = $(this).find('.adult-selector').attr('name');
                rowRawName = rowRawName.split('[');
                var rowId = rowRawName[2].replace(']', '');


                if (!$.isPlainObject(dataToPost['data'][roomId][rowId])) {
                    dataToPost['data'][roomId][rowId] = {};
                }

                var adultTempCounter = $(this).find('.adult-selector option:selected').val();
                var childTempCounter = $(this).find('.child-selector option:selected').val();
                dataToPost['data'][roomId][rowId]['adult'] = adultTempCounter;
                dataToPost['data'][roomId][rowId]['child'] = childTempCounter;

            });
//            console.log(JSON.stringify(dataToPost));
            var elementType = 'row';
            postAjaxPricePerRoom(dataToPost, elementType);


        }

        function getPricePerRoomByOptionElements() {

            var dataToPost = {};
            dataToPost['complex_id'] = $('input[name="complex_id"]').val();
            dataToPost['date_from'] = $('input[name="date_from"]').val();
            dataToPost['date_to'] = $('input[name="date_to"]').val();
            dataToPost['data'] = {};

            $('.add_rooms_select').children('option').each(function () {

                var roomId = $(this).val();

                if (!$.isPlainObject(dataToPost['data'][roomId])) {
                    dataToPost['data'][roomId] = {};
                }

                var rowId = $.now();

                if (!$.isPlainObject(dataToPost['data'][roomId][rowId])) {
                    dataToPost['data'][roomId][rowId] = {};
                }

                dataToPost['data'][roomId][rowId]['adult'] = 1;
                dataToPost['data'][roomId][rowId]['child'] = 0;

            });

//            console.log(JSON.stringify(dataToPost));
            var elementType = 'option';
            postAjaxPricePerRoom(dataToPost, elementType);


        }

        function postAjaxPricePerRoom(objectData, elementType) {

            $.ajaxSetup({headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'}});

            $.ajax({
                url: "/api/reservation/get_price_per_room",
                type: "POST",
                dataType: "json",
                data: objectData
            }).done(function (result) {
                if (elementType == 'row') {
                    putPriceToAnyRow(result);
                }
                else if (elementType == 'option') {
                    putPriceToAnyDropDownElement(result);
                }
            }).fail(function (result) {
                alert('Errors');
            });

        }

        function putPriceToAnyRow(data) {
//            console.log(JSON.stringify(data['data']));
            var data = data['data'];
            var totalPrice = '   ' + data['total'] + ' лв.';

            $('.totalPrice').text(totalPrice);

            var rowData = data['rooms_data'];


            $.each(rowData, function (roomId, roomData) {
                $.each(roomData, function (rowId, rowData) {

                    var discountPrice = rowData['early_booking'];
                    var realPrice = rowData['real_price'];

                    var tempAdultSelectorName = 'data[' + roomId + '][' + rowId + '][adult]';

//SET PRICE TO EACH ROW
                    var selector = $("select[name=\"" + tempAdultSelectorName + "\"");

                    var currentRow = selector.parent().closest('.row');

//                    console.log(currentRow);

//                    if (realPrice == 'tooMany') {
                    if (!$.isNumeric(realPrice)) {
                        currentRow.css('background', '#F29488');
                    } else {
                        currentRow.css('background', '');
                        currentRow.find('.roomPriceReal').text(realPrice + ' лв.');
                        currentRow.find('.roomPriceDiscount').text('   ' + discountPrice);
                    }
                });
            });
        }

        function putPriceToAnyDropDownElement(data) {

            var roomsDataArr = data['data']['rooms_data'];

            $('.add_rooms_select').children('option').each(function () {

                var oneRoomType = roomsDataArr[$(this).val()];

                var price = 0;

                $.each(oneRoomType, function (index, value) {
                    price = oneRoomType[index]['early_booking'];

                });


                var priceText = '|| Цена: ' + price;

                var oldText = $(this).text();

                $(this).text(oldText + priceText + ' лв.');

            });

        }

    </script>

@endsection