@extends( 'master' )

@section( 'id' )booking confirm @endsection

@section( 'content' )

    @include('reservation-header')

    <div class="container tables">
        <div class="row">
            <div class="col-sm-12">
                <table class="table table-responsive table-reservation">
                    <thead>
                    <th colspan="6">{!! trans('reservationStep5.reservationInfo') !!}</th>
                    </thead>
                    <tbody>
                    <tr>
                        <td class="row-heading">{!! trans('reservationStep5.complex') !!}</td>
                        <td class="text-uppercase" colspan="5">{!! $getComplexData -> name_bg !!}</td>
                    </tr>
                    <tr>
                        <td class="row-heading">{!! trans('reservationStep5.reservationFrom') !!}</td>
                        <td class="text-uppercase" colspan="5">{!! Session::get('new_reservation.date_from') !!}</td>
                    </tr>
                    <tr>
                        <td class="row-heading">{!! trans('reservationStep5.reservationTo') !!}</td>
                        <td class="text-uppercase" colspan="5">{!! Session::get('new_reservation.date_to') !!}</td>
                    </tr>
                    <!-- the terrible part -->
                    @foreach( Session::get('new_reservation.rooms_data') as $roomId => $roomInfo )
                        @foreach( $roomInfo as $key => $value )
                            <tr>
                                <td class="row-heading">{!! trans('reservationStep5.room') !!}</td>
                                <td>
                                    <table class="table">
                                        <tr>
                                            <td class="text-uppercase"><strong>{!! $value['room_type'] !!}</strong></td>
                                            <td class="row-subheading">{!! trans('reservationStep5.days') !!}:</td>
                                            <td class="row-subheading">{!! trans('reservationStep5.adults') !!}</td>
                                            <td class="row-subheading">{!! trans('reservationStep5.childs') !!}</td>
                                            <td class="row-subheading">{!! trans('reservationStep5.extraBedsPrice') !!}</td>
                                            <td class="row-subheading">{!! trans('reservationStep5.price') !!}</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td class="text-uppercase">{!! Session::get('new_reservation.days') !!}</td>
                                            <td class="text-uppercase"><strong>{!! $value['adult'] !!}</strong></td>
                                            <td class="text-uppercase"><strong>{!! $value['child'] !!}</strong></td>
                                            <td class="text-uppercase"><strong>{!! $value['extra'] !!}
                                                    @if($value['extra']>0)
                                                        ({!! \App\Classes\GlobalFunctions::calculateCurrency($value['extra_bed_price']) !!} @if ($value['is_percentage'] == 0) {!! trans('reservationStep5.currency').')' !!} @else {!! '%)' !!} @endif</strong>
                                                @endif
                                            </td>
                                            <td class="text-uppercase">
                                                <strong>{!! \App\Classes\GlobalFunctions::calculateCurrency($value['price']) !!} {!! trans('reservationStep5.currency') !!}</strong>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        @endforeach
                    @endforeach
                    <!-- whew -->
                    <tr class="row-total">
                        <td class="row-total-heading">{!! trans('reservationStep5.totalPrice') !!}</td>
                        <td>
                            <!-- using inner table so the 'total price' cell width can be set without breaking the layout of the rows above -->
                            <table class="table">
                                @if(!is_array($earlyBooking))
                                    <tr>
                                        <td></td>
                                        <td class="row-total-price">{!! \App\Classes\GlobalFunctions::calculateCurrency($reservationData['total']) !!} {!! trans('reservationStep5.currency') !!}</td>
                                    </tr>
                                @else
                                    <tr>
                                        <td></td>
                                        <td class="row-total-price">{!! \App\Classes\GlobalFunctions::calculateCurrency($reservationData['total']/((1-$earlyBooking['percentage']/100))) !!} {!! trans('reservationStep5.currency') !!}</td>
                                    </tr>
                                @endif
                            </table>
                        </td>

                    </tr>

                    </tbody>
                </table>
            </div>
        </div>


        @if(is_array($earlyBooking))
            <div class="row">
                <div class="col-sm-12">
                    <div class="early-booking lead text-primary">
                        <?php
                        $fromRaw = $earlyBooking['until_date'];

                        if ($fromRaw != 0) {
                            $fromRaw = explode('-', $fromRaw);

                            $year = $fromRaw[0];
                            $month = $fromRaw[1];
                            $dayRaw = $fromRaw[2];

                            $dayRaw = explode(' ', $dayRaw);
                            $day = $dayRaw[0];
                            $from = $day . '/' . $month . '/' . $year;
                        }

                        ?>


                        @if($fromRaw!=0)
                            {!! trans('reservationStep5.bookingBefore1half').$from.': <span class="spaced"><strong>-'.$earlyBooking['percentage'].'%</strong></span>' !!}
                            {!! trans('reservationStep5.newPrice') !!} {!! \App\Classes\GlobalFunctions::calculateCurrency($reservationData['total']) !!}  {!! trans('reservationStep5.currency') !!}
                        @endif
                    </div>
                </div>
            </div>

        @endif

        <div class="row">
            <div class="col-sm-12">
                <table class="table table-responsive table-person">
                    <thead>
                    <th colspan="2">{!! trans('reservationStep5.personalData') !!}</th>
                    </thead>
                    <tbody>
                    <tr>
                        <td class="row-heading">{!! trans('reservationStep5.name') !!}</td>
                        <td class="text-uppercase">{!! $personalData['firstname'] !!}</td>
                    </tr>
                    <tr>
                        <td class="row-heading">{!! trans('reservationStep5.mname') !!}</td>
                        <td class="text-uppercase">{!! $personalData['middle'] !!}</td>
                    </tr>
                    <tr>
                        <td class="row-heading">{!! trans('reservationStep5.lname') !!}</td>
                        <td class="text-uppercase">{!! $personalData['lastname'] !!}</td>
                    </tr>
                    <tr>
                        <td class="row-heading">{!! trans('reservationStep5.address') !!}</td>
                        <td>{!! $personalData['address'] !!}</td>
                    </tr>
                    <tr>
                        <td class="row-heading">{!! trans('reservationStep5.city') !!}</td>
                        <td>{!! $personalData['city'] !!}</td>
                    </tr>
                    <tr>
                        <td class="row-heading">{!! trans('reservationStep5.country') !!}</td>
                        <td>{!! $personalData['country'] !!}</td>
                    </tr>
                    <tr>
                        <td class="row-heading">{!! trans('reservationStep5.phoneCode') !!}</td>
                        <td>{!! $personalData['telephonecode'] !!}</td>
                    </tr>
                    <tr>
                        <td class="row-heading">{!! trans('reservationStep5.primPhone') !!}</td>
                        <td>{!! $personalData['telephone1'] !!}</td>
                    </tr>
                    <tr>
                        <td class="row-heading">{!! trans('reservationStep5.email') !!}</td>
                        <td>{!! $personalData['email'] !!}</td>
                    </tr>
                    <tr>
                        <td class="row-heading">{!! trans('reservationStep5.requarements') !!}</td>
                        <td>{!! $personalData['requirements'] !!}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 text-right">
                {!! Form::open( array('action' => 'ReservationsEntityController@confirmReservation') ) !!}
                {!! Form::submit(trans('reservationStep5.confirm'), array( 'class' => 'btn btn-custom btn-lg btn-warning' )) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>





@endsection

@section( 'view-scripts' )

    @yield('nav-scripts')

@endsection