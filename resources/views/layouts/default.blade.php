<!doctype html>
<html class="no-js" lang="{!! App::getLocale() !!}" xmlns="http://www.w3.org/1999/html">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title></title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="shortcut icon" href="{{ asset('favicon.ico') }}" >
  <link rel="stylesheet" href="{!! asset('owl-carousel/owl.carousel.css') !!}">
  <link rel="stylesheet" href="{!! asset('owl-carousel/owl.theme.css') !!}">
  <link rel="stylesheet" href="{!! asset('owl-carousel/owl.transitions.css') !!}">
  <link rel="stylesheet" href="{!! asset('css/bootstrap.min.css') !!}">
  <link rel="stylesheet" href="{!! asset('css/five-columns.css') !!}">
  <link rel="stylesheet" href="{!! asset('jquery-date-range-picker/daterangepicker.css') !!}">
  <link rel="stylesheet" href="{!! asset('jQuery-YouTube-Carousel/youtubecarousel.css') !!}">
  <link rel="stylesheet" href="{!! asset('css/styles.min.css') !!}">
  <link rel="stylesheet" href="{!! asset('css/animate.css') !!}">

  <script src="{!! asset('js/vendor/modernizr-2.8.3.min.js') !!}"></script>
  <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>
</head>
<body class="@yield('id')">

  <!--[if lt IE 8]>
      <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
  <![endif]-->

  @yield( 'body' )

  <!--<script src="https://code.jquery.com/jquery-2.2.2.min.js"></script>-->
  <script src="{!! asset('js/vendor/jquery-2.2.0.min.js') !!}"></script>
  <script src="{!! asset('jquery-date-range-picker/moment.min.js') !!}"></script>

  <script src="{!! asset('js/vendor/bootstrap.min.js') !!}"></script>
  <script src="{!! asset('js/vendor/jquery-ui.min.js') !!}"></script>
  <script src="{!! asset('js/bootstrap-notify.min.js') !!}"></script>
  <script src="{!! asset('js/retina.min.js') !!}"></script>
  <script src="{!! asset('owl-carousel/owl.carousel.min.js') !!}"></script>
  <script src="{!! asset('jquery-date-range-picker/jquery.daterangepicker.js') !!}"></script>
  <script src="{!! asset('jquery-match-height/dist/jquery.matchHeight-min.js') !!}"></script>
  {{-- <script src="{!! asset('jQuery-YouTube-Carousel/jquery.youtubecarousel.js') !!}"></script> --}}

  <script src="{!! asset('js/plugins.js') !!}"></script>
  <script src="{!! asset('js/main.js') !!}"></script>

  @yield( 'scripts' )

  <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
  <!-- <script>
  (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
    function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
  e=o.createElement(i);r=o.getElementsByTagName(i)[0];
  e.src='//www.google-analytics.com/analytics.js';
  r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
  ga('create','UA-XXXXX-X','auto');ga('send','pageview');
  </script> -->
</body>
</html>
