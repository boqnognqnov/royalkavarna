@extends('master')

@section( 'id' )booking @endsection

@section('content')

    @include('reservation-header')

    <div class="container booking-personal">
        <div class="row">
            <div class="col-sm-12">
                {!! Form::open( array( 'action' => 'ReservationsEntityController@setPersonalData', 'id' => 'step4-form' ) ) !!}
                {{--{!! Form::hidden( 'complex_id', \Session::get('complex_id'), [] ) !!}--}}
                {{--{!! Form::hidden( 'date_from', \Session::get('date_from'), [] ) !!}--}}
                {{--{!! Form::hidden( 'date_to', \Session::get('date_to'), [] ) !!}--}}
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            {!! Form::label('step4-fname', trans('reservationStep4.fname'), array('class' => 'control-label') ) !!}
                            {!! Form::text( 'firstname', @$data['firstname'], array( 'class' => 'form-control', 'id' => 'step4-fname', 'required' => 'required' ) ) !!}
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            {!! Form::label('step4-mname', trans('reservationStep4.mname'), array('class' => 'control-label') ) !!}
                            {!! Form::text( 'middle', @$data['middle'], array( 'class' => 'form-control', 'id' => 'step4-mname' ) ) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('step4-lname', trans('reservationStep4.lname'), array('class' => 'control-label') ) !!}
                            {!! Form::text( 'lastname', @$data['lastname'], array( 'class' => 'form-control', 'id' => 'step4-lname', 'required' => 'required' ) ) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('step4-address', trans('reservationStep4.address'), array('class' => 'control-label') ) !!}
                            {!! Form::text( 'address', @$data['address'], array( 'class' => 'form-control', 'id' => 'step4-address', 'required' => 'required' ) ) !!}
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            {!! Form::label('step4-city', trans('reservationStep4.city'), array('class' => 'control-label') ) !!}
                            {!! Form::text( 'city', @$data['city'], array( 'class' => 'form-control', 'id' => 'step4-city', 'required' => 'required' ) ) !!}
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            {!! Form::label('step4-country', trans('reservationStep4.country'), array('class' => 'control-label') ) !!}
                            {!! Form::text( 'country', @$data['country'], array( 'class' => 'form-control', 'id' => 'step4-country', 'required' => 'required' ) ) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            {!! Form::label('step4-telcode', trans('reservationStep4.phonecode'), array('class' => 'control-label') ) !!}
                            {!! Form::text( 'telephonecode', @$data['telephonecode'], array( 'class' => 'form-control', 'id' => 'step4-telcode', 'aria-describedby' => 'step4-telcode-help', 'required' => 'required' ) ) !!}
                            <span id="step4-telcode-help" class="help-block">{!! trans('reservationStep4.exPhoneCode') !!}</span>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            {!! Form::label('step4-tel1', trans('reservationStep4.primPhone'), array('class' => 'control-label') ) !!}
                            {!! Form::text( 'telephone1', @$data['telephone1'], array( 'class' => 'form-control', 'id' => 'step4-tel1', 'aria-describedby' => 'step4-tel1-help', 'required' => 'required' ) ) !!}
                            <span id="step4-tel1-help" class="help-block">{!! trans('reservationStep4.exPrimPhone') !!}</span>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            {!! Form::label('step4-tel2', trans('reservationStep4.secondPhone'), array('class' => 'control-label') ) !!}
                            {!! Form::text( 'telephone2', @$data['telephone2'], array( 'class' => 'form-control', 'id' => 'step4-tel2' ) ) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p><strong>{!! trans('reservationStep4.parInfo') !!}</strong></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('step4-email', trans('reservationStep4.email'), array('class' => 'control-label') ) !!}
                            {!! Form::text( 'email', @$data['email'], array( 'class' => 'form-control', 'id' => 'step4-email', 'required' => 'required' ) ) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('step4-emailagain', trans('reservationStep4.retypeEmail'), array('class' => 'control-label') ) !!}
                            {!! Form::text( 'retypeemail', @$data['retypeemail'], array( 'class' => 'form-control', 'id' => 'step4-emailagain', 'required' => 'required' ) ) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            {!! Form::label('step4-reqs', trans('reservationStep4.specRequire'), array('class' => 'control-label') ) !!}
                            {!! Form::textarea( 'requirements', @$data['requirements'], array( 'class' => 'form-control', 'id' => 'step4-reqs' ) ) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group clearfix">
                            <noscript>
                                {!! Form::submit(trans('reservationStep4.nextButton'), array('class' => 'btn-next') ) !!}
                            </noscript>
                            <a class="btn-next" onclick="document.forms['step4-form'].submit();">{!! trans('reservationStep4.nextButton') !!} <span
                                        class="arrow">&rarr;</span></a>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    @endsection

    @section('view-scripts')

    @yield('nav-scripts')

            <!-- ERROR HANDLING -->
    <!-- storing error sources and messages in hidden inputs for use in jQuery -->
    @if($errors -> any() )
            <!-- collect the names of elements which threw errors -->
    <!-- holy grail of Laravel error catching - improve if you can -->
    @foreach ($errors->getBag('default')->keys() as $elementName)
        <input type="hidden" class="error-src" value="{{ $elementName }}">
        @endforeach
                <!-- collect the error messages -->
        @foreach ($errors->all() as $error)
            <input type="hidden" class="error-message" value="{{ $error }}">
        @endforeach
        @endif

        <script>
            $(document).ready(function () {
                // loop through hidden fields with error messages
                $('.error-message').each(function () {
                    // show an alert with the message
                    $.notify({
                        // options
                        icon: 'glyphicon glyphicon-remove',
                        message: $(this).attr('value')
                    }, {
                        // settings
                        type: 'danger',
                        delay: 4000,
                        allow_dismiss: true
                    });
                });
                // loop through hidden fields with element names
                $('.error-src').each(function () {
                    // construct the selector to target faulty inputs, eg. [name="firstname"]
                    var selector = '[name="' + $(this).attr('value') + '"]';
                    // find its 'form-group' parent - that's where the validation classes must be applied
                    $(selector).parent('.form-group').addClass('has-error');
                });
            });
        </script>
        <!-- END OF ERROR HANDLING -->

@endsection