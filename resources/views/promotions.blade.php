@extends( 'master' )

@section( 'id' )complex promotions @endsection

@section( 'content' )

    <div class="complex-subnav container-fluid hidden-lg">
        <div class="row">
            <div class="col-sm-12 col-md-8 col-md-offset-2">
                <!-- current subpage name should be here dyniamically -->
                <button class="btn btn-lg btn-block btn-primary text-uppercase inner-pages-nav-trigger collapsed"
                        type="button" data-toggle="collapse" data-target="#inner-pages-nav" aria-expanded="false"
                        aria-controls="inner-pages-nav">
                    {!! trans('NavPromotionPricesGuestBook.promotions') !!} <span class="caret"></span>
                </button>
                <!-- the rest of the subpages should be in the UL -->
                <ul id="inner-pages-nav" class="inner-pages-nav nav nav-pills nav-justified text-uppercase collapse">
                    <li role="presentation"><a
                                href="{!! url('complex/'. $complex->id ) !!}">{!! trans('NavPromotionPricesGuestBook.theComplex') !!}</a>
                    </li>
                    <li role="presentation"><a
                                href="{!! url('complex/'. $complex->id .'/prices') !!}">{!! trans('NavPromotionPricesGuestBook.priceList') !!}</a>
                    </li>
                    <li role="presentation"><a
                                href="{!! url('complex/'. $complex->id ) . '/guestbook'!!}">{!! trans('NavPromotionPricesGuestBook.guestBook') !!}</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container complex-nav">
        <div class="row">
            <div class="col-sm-12">
                <div class="breadcrumbs">
                    <ol class="breadcrumb">
                        <li><a href="#">{!! trans('NavPromotionPricesGuestBook.home') !!}</a></li>
                        <li>
                            <a href="{!! url('complex/'. $complex->id ) !!}">{{ $complex['name_'.App::getLocale()] }}</a>
                        </li>
                        <li class="active">{!! trans('NavPromotionPricesGuestBook.promotions') !!}</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="row visible-lg-block">
            <div class="col-sm-12 col-md-8 col-md-offset-2">
                <ul class="nav nav-pills nav-justified text-uppercase">
                    <li role="presentation"><a
                                href="{!! url('complex/'. $complex->id ) !!}">{!! trans('NavPromotionPricesGuestBook.theComplex') !!}</a>
                    </li>
                    <li role="presentation"><a
                                href="{!! url('complex/'. $complex->id .'/prices') !!}">{!! trans('NavPromotionPricesGuestBook.priceList') !!}</a>
                    </li>
                    <li role="presentation" class="active"><a
                                href="{!! url('complex/'. $complex->id ) . '/promotions'!!}">{!! trans('NavPromotionPricesGuestBook.promotions') !!}</a>
                    </li>
                    <li role="presentation"><a
                                href="{!! url('complex/'. $complex->id ) . '/guestbook'!!}">{!! trans('NavPromotionPricesGuestBook.guestBook') !!}</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container">

        @foreach($promotions as $key=>$promotion)


            @if(($key+1) % 2==0)
                <div class="row promo-item even">
                    <div class="col-sm-12">
                        <div class="wrapper">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-sm-12 col-md-10 col-md-offset-2">
                                        <h2 class="h3 promo-title">{!! $promotion['title_'.App::getLocale()] !!}</h2>
                                        <img src="{!! asset(\App\ComplexPromotions::$imagePath.$promotion['image']) !!}"
                                                alt="promotion image" class="img-responsive">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <p class="promo-desc">
                                            <!-- start slipsum code -->

                                            {!! $promotion['description_'.App::getLocale()] !!}
                                                    <!-- please do not remove this line -->

                                            <!-- end slipsum code -->
                                        </p>

                                        <div class="blue-box">
                                            <h3 class="h5 early-booking">{!! $promotion['mini_title_'.App::getLocale()] !!}</h3>
                                            <hr>
                                            {!! $promotion['mini_description_'.App::getLocale()] !!}
                                            <p> {!! \App\Classes\GlobalFunctions::generateDateTimeToStr($promotion['date_from']) !!}
    {!! trans('promotions.toDate') !!} {!! \App\Classes\GlobalFunctions::generateDateTimeToStr($promotion['date_to']) !!}</p>
                                            @if(isset($promotion['href']) && strlen($promotion['href']) > 0)
                                                <a href="{!! url($promotion['href']) !!}"
                                                        class="btn btn-custom btn-info">{!! $promotion['href_text_'.App::getLocale()] !!}</a>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @elseif(($key+1) % 2!=0)
                <div class="row promo-item odd">
                    <div class="col-sm-12">
                        <div class="wrapper">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-sm-12 col-md-10">
                                        <h2 class="h3 promo-title">{!! $promotion['title_'.App::getLocale()] !!}</h2>
                                        <img src="{!! asset(\App\ComplexPromotions::$imagePath.$promotion['image']) !!}"
                                                alt="promotion image" class="img-responsive">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <p class="promo-desc">
                                            <!-- start slipsum code -->

                                            {!! $promotion['description_'.App::getLocale()] !!}

                                                    <!-- please do not remove this line -->


                                            <!-- end slipsum code -->
                                        </p>

                                        <div class="blue-box">
                                            <h3 class="h5 early-booking">{!! $promotion['mini_title_'.App::getLocale()] !!}</h3>
                                            <hr>
                                            {!! $promotion['mini_description_'.App::getLocale()] !!}
                                            <p>{!! \App\Classes\GlobalFunctions::generateDateTimeToStr($promotion['date_from']) !!}
                                                {!! trans('promotions.toDate') !!} {!! \App\Classes\GlobalFunctions::generateDateTimeToStr($promotion['date_to']) !!}</p>
                                            @if(isset($promotion['href']) && strlen($promotion['href']) > 0)
                                                <a href="{!! url($promotion['href']) !!}"
                                                        class="btn btn-custom btn-info">{!! $promotion['href_text_'.App::getLocale()] !!}</a>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif




        @endforeach


    </div>

@endsection

@section( 'view-scripts' )

    @include('internal-slider-script')

@endsection