@extends( 'master' )

@section( 'id' )room @endsection

@section( 'content' )

    <div class="jumbotron"
         style="background-image: url({!! asset(\App\ComplexRooms::$imagePath.$roomData['main_image']) !!});">
        <div class="container-fluid">
            <!-- don't delete me plz -->
        </div>
    </div>

    <div class="container complex-nav">
        <div class="row">
            <div class="col-sm-12">
                <div class="breadcrumbs">
                    <ol class="breadcrumb">
                        <li><a href="{!! url('/' ) !!}">{!! trans('NavPromotionPricesGuestBook.home') !!}</a></li>
                        <li>
                            <a href="{!! url('complex/'. $complex['id'] ) !!}">{!! $complex['name_'.App::getLocale()] !!}</a>
                        </li>
                        <li>
                            <a href="{!! url('complex/'.$complex['id'] .'/prices') !!}">{!! trans('NavPromotionPricesGuestBook.priceList') !!}</a>
                        </li>
                        <li class="active">{!! $roomData['type_'.App::getLocale()] !!}</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row main-title">
            <div class="col-sm-12">
                <h1 class="h2 title">{!! $roomData['type_'.App::getLocale()] !!}</h1>
            </div>
        </div>
        <div class="row core">
            <div class="col-sm-9 content">
                @include('internal-slider-jumbotron')
                <div class="carousel"></div>
                <div class="filmstrip"></div>

                <div class="features clearfix">
                    @foreach($extras as $extra)
                        <a class="feature text-center pull-left">
                            <img src="{!! asset(\App\Extras::$path.$extra['icon']) !!}" alt="">
                            <span>{!! $extra['name_'.App::getLocale()] !!}</span>
                        </a>
                    @endforeach

                </div>

                <div class="row">
                    <div class="col-sm-3">
                        <h2 class="desc-title text-uppercase">{!! trans('oneRoom.description') !!}</h2>
                    </div>
                    <div class="col-sm-9">
                        <div class="row desc">
                            {!! $roomData['description_'.App::getLocale()] !!}
                        </div>
                        <div class="row desc">
                            {!! $roomData['other_info_'.App::getLocale()] !!}
                        </div>
                        <div class="row desc">
                            {!! $roomData['price_info_'.App::getLocale()] !!}
                        </div>
                        <div class="row prices">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th colspan="2">{!! trans('oneRoom.roomPrices') !!}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($prices as $price=>$priceRanges)
                                    <tr>
                                        <td>
                                            @foreach($priceRanges as $key=>$oneRange)
                                                @if($key==0)
                                                    {!! trans('oneRoom.from') !!}
                                                @else
                                                    /
                                                @endif
                                                {!! $oneRange['from'] !!} {!! trans('oneRoom.to') !!} {!! $oneRange['to'] !!}
                                            @endforeach

                                        </td>
                                        <td>{!! \App\Classes\GlobalFunctions::calculateCurrency($price) !!} {!! trans('oneRoom.currency') !!}</td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-3 aside">
                <div class="promo text-center">
                    <h2 class="promo-title">{!! trans('oneRoom.earlyBooking') !!}</h2>
                    <hr>
                    <ul>
                        @foreach($earlyBookings as $oneBooking)
                            <li>- {!! $oneBooking['percentage'] !!}
                                % {!! trans('oneRoom.until') !!} {!! $oneBooking['until_date'] !!}</li>
                        @endforeach

                    </ul>
                    <div>
                        <!-- start slipsum code -->
                        {!! $complex['early_booking_text_'.App::getLocale()] !!}


                    </div>
                    <a href="{!! url('reservation/complex/'.$complex['id'].'/'.$room_id) !!}"
                       class="btn btn-custom btn-brown">{!! trans('oneRoom.bookRoomButton') !!}</a>
                </div>
            </div>
        </div>
    </div>

@endsection

@section( 'view-scripts' )

    <script>
        $(document).ready(function () {

            $('#carousel').parents('.jumbotron').removeClass('jumbotron').addClass('internal-jumbotron');
            $('#filmstrip').siblings('.filmstrip-custom-nav').remove();

            // Synchronized carousels
            var carousel = $("#carousel");
            var filmstrip = $("#filmstrip");

            carousel.owlCarousel({
                singleItem: true,
                slideSpeed: 1000,
                navigation: false,
                pagination: false,
                mouseDrag: false,
                touchDrag: false,
                afterAction: syncPosition,
                responsiveRefreshRate: 200,
            });

            filmstrip.owlCarousel({
                items: 5,
                //itemsDesktop      : [1199,{!! sizeof($gallery) !!}],
                itemsDesktop: [1199, 8],
                itemsDesktopSmall: [979, 6],
                itemsTablet: [768, 4],
                itemsMobile: [479, 2],
                navigation: false,
                pagination: false,
                responsiveRefreshRate: 100,
                afterInit: function (el) {
                    el.find(".owl-item").eq(0).addClass("synced");
                }
            });

            function syncPosition(el) {
                var current = this.currentItem;
                $("#filmstrip")
                        .find(".owl-item")
                        .removeClass("synced")
                        .eq(current)
                        .addClass("synced")
                if ($("#filmstrip").data("owlCarousel") !== undefined) {
                    center(current)
                }
            }

            $("#filmstrip").on("click", ".owl-item", function (e) {
                e.preventDefault();
                var number = $(this).data("owlItem");
                carousel.trigger("owl.goTo", number);
            });

            function center(number) {
                var filmstripvisible = filmstrip.data("owlCarousel").owl.visibleItems;
                var num = number;
                var found = false;
                for (var i in filmstripvisible) {
                    if (num === filmstripvisible[i]) {
                        var found = true;
                    }
                }

                if (found === false) {
                    if (num > filmstripvisible[filmstripvisible.length - 1]) {
                        filmstrip.trigger("owl.goTo", num - filmstripvisible.length + 2)
                    } else {
                        if (num - 1 === -1) {
                            num = 0;
                        }
                        filmstrip.trigger("owl.goTo", num);
                    }
                } else if (num === filmstripvisible[filmstripvisible.length - 1]) {
                    filmstrip.trigger("owl.goTo", filmstripvisible[1])
                } else if (num === filmstripvisible[0]) {
                    filmstrip.trigger("owl.goTo", num - 1)
                }

            }

            // Custom Navigation Events
            $(".carousel-custom-nav .next").click(function (e) {
                e.preventDefault();
                $("#carousel").trigger('owl.next');
            });
            $(".carousel-custom-nav .prev").click(function (e) {
                e.preventDefault();
                $("#carousel").trigger('owl.prev');
            });
        });
    </script>

    <!-- RESPONSIVE TABS -->
    <script src="{!! asset('js/bootstrap-tabcollapse.js') !!}"></script>

    <script>
        $('.nav-tabs').tabCollapse({
            tabsClass: 'hidden-sm hidden-xs',
            accordionClass: 'visible-sm visible-xs tabs-collapsed'
        });
    </script>

@endsection