@extends( 'master' )

@section( 'id' )presentations @endsection

@section( 'content' )

<?php
  foreach ($presentations as $num => $videoURL) {
    $url = $videoURL['youtube_video'];
    if ($url != '') {
      $urlsubstr = explode('v=', $url);
      if(isset($urlsubstr[1])){
        $urlsubsubstr = explode('&', $urlsubstr[1]);
        $token = $urlsubsubstr[0];
      }
      echo '<input type="hidden" name="video[]" value="'.$token.'">';
    }
  }
?>

<div id="yt_container" class="container"></div>

<style>
	body { background-image: url({!! asset('img/presentations-bg.jpg') !!}); }
</style>

@endsection

@section( 'view-scripts' )

  <script src="{!! asset('jQuery-YouTube-Carousel/jquery.youtubecarousel.js') !!}"></script>

  <script>
    /*List of YouTube videos - you need just the video ID for this (ex:)*/
    var yt_videos = $("input[name='video\\[\\]']").map(function(){return $(this).val();}).get();

    /*Video controls*/
    $('a.prev').click(function() {
        $('.jcarousel').jcarousel('scroll', '-=1');
    });

    $('a.next').click(function() {
        $('.jcarousel').jcarousel('scroll', '+=1');
    });
  </script>

@endsection
