@extends( 'layouts.default' )

@section( 'body' )

<nav class="navbar navbar-inverse navbar-main" role="navigation">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a href="{!! url('/') !!}" class="navbar-brand hidden-lg" title="Royals Kavarna"><img src="{!! asset('img/MobileKavarnaLogo.png') !!}" alt="logo" class=""></a>
    </div> <!-- /.navbar-header -->
    <div id="navbar" class="navbar-collapse collapse clearfix">
      <ul class="nav navbar-nav navbar-left">
        @if(isset($complexes))
          @foreach($complexes as $complex)
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ $complex['name_'.App::getLocale()] }} <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="{!! url('complex/'.$complex->id) !!}">{!! trans('headerAndFooter.theComplex') !!}</a></li>
                <li><a href="{!! url('complex/'.$complex->id.'/prices') !!}">{!! trans('headerAndFooter.priceList') !!}</a></li>
                <li><a href="{!! url('complex/'.$complex->id.'/promotions') !!}">{!! trans('headerAndFooter.promotions') !!}</a></li>
                <li><a href="{!! url('complex/'.$complex->id.'/guestbook') !!}">{!! trans('headerAndFooter.guestBooks') !!}</a></li>
              </ul>
            </li>
          @endforeach
        @endif
      </ul> <!-- /.navbar-left -->

      <ul class="nav navbar-nav navbar-center hidden-xs hidden-sm hidden-md">
        <a href="{!! url('/') !!}" title="Royals Kavarna"><img src="{!! asset('img/logo_dark.png') !!}" alt="logo" class=""></a>
      </ul>

      <ul class="nav navbar-nav navbar-right">
        <li><a href="{!! url('about') !!}">{!! trans('headerAndFooter.abautUs') !!}</a></li>
        <li><a href="{!! url('presentations') !!}">{!! trans('headerAndFooter.presentations') !!}</a></li>
        <li><a href="{!! url('contacts') !!}">{!! trans('headerAndFooter.contacts') !!}</a></li>
        <li class="dropdown lang-menu hidden-xs">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" title="English"><img src="{!! asset('img/flags/'.App::getLocale().'.png') !!}" alt="british flag"> {!! App::getLocale() !!} <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="{!! url('lang/bg') !!}" title="Български"><img src="{!! asset('img/flags/bg.png') !!}" alt="bulgarian flag"> BG</a></li>
            <li><a href="{!! url('lang/ru') !!}" title="Ру́сский"><img src="{!! asset('img/flags/ru.png') !!}" alt="russian flag"> RU</a></li>
            <li><a href="{!! url('lang/en') !!}" title="English"><img src="{!! asset('img/flags/en.png') !!}" alt="british flag"> EN</a></li>
            <li><a href="{!! url('lang/ro') !!}" title="Românește"><img src="{!! asset('img/flags/ro.png') !!}" alt="romanian flag"> RO</a></li>
          </ul>
        </li>
        <li class="lang-menu-xs visible-xs-block">
          <a href="{!! url('lang/en') !!}" title="English"><img src="{!! asset('img/flags/en.png') !!}" alt="british flag"> EN</a>
          <a href="{!! url('lang/bg') !!}" title="Български"><img src="{!! asset('img/flags/bg.png') !!}" alt="bulgarian flag"> BG</a>
          <a href="{!! url('lang/ru') !!}" title="Ру́сский"><img src="{!! asset('img/flags/ru.png') !!}" alt="russian flag"> RU</a>
          <a href="{!! url('lang/ro') !!}" title="Românește"><img src="{!! asset('img/flags/ro.png') !!}" alt="romanian flag"> RO</a>
        </li>
      </ul> <!-- /.navbar-right -->
    </div> <!--/.navbar-collapse -->
  </div> <!-- /.container-fluid -->
</nav>

@yield( 'content' )

<div class="book-now"><button type="button" class="btn btn-lg btn-gold btn-free text-uppercase" onClick="location.href='{!! url('reservation/all_complexes') !!}'"><img src="{!! asset('img/icon-calendar.png') !!}" alt="calendar icon"> <strong>{!! trans('headerAndFooter.bookNowGoldButton') !!}</strong></button></div>

<div id="weather" class="text-center"></div>

@if (Request::path() != '/')
  <footer>
    <div class="footer container-fluid">
      <div class="row">
        <div class="container">
          <div class="row clearfix">
            <div class="footer-block col-sm-6 col-md-3">
              <h5 class="footer-menu-title text-uppercase">{!! trans('headerAndFooter.menu') !!}</h5>
              <ul class="footer-menu">

                @foreach($complexes as $complex)
                  <li><a href="#">{{ $complex['name_'.App::getLocale()] }}</a></li>
                @endforeach

                <li><a href="#">{!! trans('headerAndFooter.abautUs') !!}</a></li>
                <li><a href="#">{!! trans('headerAndFooter.presentations') !!}</a></li>
                <li><a href="#">{!! trans('headerAndFooter.contacts') !!}</a></li>
              </ul>
            </div>
            <div class="footer-block sm-right col-sm-6 col-md-3">
              <h5 class="footer-menu-title text-uppercase">{!! trans('headerAndFooter.usefulLinks') !!}</h5>
              <ul class="footer-menu">
                <li><a href="#">{!! trans('headerAndFooter.bookNow') !!}</a></li>
                <li><a href="#" data-toggle="modal" data-target="#policyModal">{!! trans('headerAndFooter.hotelPolicy') !!}</a></li>
                <li><a href="#">{!! trans('headerAndFooter.siteMap') !!}</a></li>
              </ul>
            </div>
            <div class="clearfix visible-sm-block"></div>
            <div class="footer-block col-sm-6 col-md-3">
              <h5 class="footer-menu-title text-uppercase">{!! trans('headerAndFooter.contactUs') !!}</h5>
              <ul class="footer-menu" class="list-unstyled">
                <li><strong>Bulgaria, Kavarna 9650</strong></li>
                <li><strong>Sea Coast Area</strong></li>
                <li><a href="tel:+35957087070">+359 570 8 70 70</a></li>
                <li><a href="tel:+359886555898">+359 886 555 898</a></li>
                <li><a href="mailto:royalbay@royals-kavarna.bg">royalbay&#64;royals-kavarna.bg</a></li>
                <li><a href="mailto:royalcove@royals-kavarna.bg">royalcove&#64;royals-kavarna.bg</a></li>
              </ul>
            </div>
            <div class="footer-block sm-right col-sm-6 col-md-3">
              <h5 class="footer-menu-title text-uppercase">{!! trans('headerAndFooter.share') !!}</h5>
              <ul class="footer-menu socials">
                <li><a href="#" class="facebook"></a></li>
                <li><a href="#" class="twitter"></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="credits row">
        <div class="col-sm-12 text-center">
          <p class="small">
            2016 &copy; {!! trans('headerAndFooter.rightsReserved') !!}<br>
            {!! trans('headerAndFooter.createdBy') !!}
          </p>
        </div>
      </div>
    </div>
  </footer>
@endif

<!-- Modal -->
<div class="modal fade" id="policyModal" tabindex="-1" role="dialog" aria-labelledby="policyModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="policyModalLabel">Hotel policy</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

@endsection

@section( 'scripts' )

@yield( 'view-scripts' )

@endsection
