<?php
//trans('index.chooseComplexes')
return [

    'chooseComplexes' => 'Vă rugăm alegeţi unul dintre complexele',

    'indexInfo' => 'Marca Royal Hotels uneşte o constelaţie de obiecte, situate în cea mai pitorească
                    част на Северното българско Черноморие - в околностите на Каварна. Уютният Royal Cove 3*, блестящият
                    Royal Bay 4* и аристократичната морска резиденция Villa Valenti 5* са изключителни места за летен
                    отдих',


];
