<?php
//trans('reservationValidation.tooManyPeople')
return [
    'tooManyPeople' => 'Persoanele sunt mai multe decât capacitatea camerei',
    //CLIENT
    'dateIsPased' => 'The first date is pased',
    'firstDateIsBigger' => 'The first date is bigger from the end date',
    'anyDateInputIsEmpty' => 'Unele dintre câmpurile pentru data nu sunt completate ! Vă rugăm completaţi data de la:  şi data până la: .',
    'NoPricesForRooms' => 'În aceasta perioadă nu se oferă cameră în complex. Vă rugăm alegeţi alta perioadă',

    'noRoomsInThisRange' => 'Nu sunt camere libere',

//    ADMIN
    'weCantRemoveAllRoomAndContinue' => 'We can\'t remove all rooms.
    If you want we can to delete all reservation record to previous page',



];
