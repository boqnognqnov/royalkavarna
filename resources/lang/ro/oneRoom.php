<?php
//trans('oneRoom.bookRoomButton')
return [
    'description' => 'Descriere',
    'roomPrices' => 'Preţurile pentru camera:',
    'from' => 'Din',
    'to' => 'până la',
    'currency' => 'lv',
    'earlyBooking' => 'Rezervare anticipată',
    'until' => 'până la',
    'bookRoomButton' => 'Rezervare',
    
];