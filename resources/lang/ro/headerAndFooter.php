<?php
//trans('headerAndFooter.share')
return [
    'theComplex' => 'Complexul',
    'priceList' => 'Lista de preţuri',
    'promotions' => 'Promoţii',
    'guestBooks' => 'Cartea pentru opiniile Dvs',

    'abautUs' => 'Despre noi',
    'presentations' => 'Prezentări',
    'contacts' => 'Contact',

    'bookNow' => 'Rezervare',
    'hotelPolicy' => 'Politica hotelului ',
    'siteMap' => 'Harta site-ului',
    'share' => 'Reţele sociale',
    'rightsReserved' => 'Toate drepturile rezervate',
    'createdBy' => ' Creat de la <a href="http://onecreative.eu">OneCreative</a>',

    'contactUs' => 'Contact',
    'usefulLinks' => 'Link-uri utile',
    'menu' => 'Menu',

    'bookNowGoldButton' => 'Rezervare acum',

];
