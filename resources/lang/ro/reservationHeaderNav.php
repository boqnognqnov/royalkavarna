<?php
//trans('reservationHeaderNav.navStep5')
return [
//HEADER CONTENT
    'bookARoom' => 'Rezervare',

    'step1' => 'Pasul 1',
    'step2' => 'Pasul 2',
    'step3' => 'Pasul 3',
    'step4' => 'Pasul 4',
    'step5' => 'Pasul 5',

    'success' => 'Toate pasurile complete',

//    NAVIGATION STEPS

    'navStep1' => 'Alegere complex',

    'navStep2' => 'Alegere data',

    'navStep3' => 'Alegere camere',

    'navStep4' => 'Informaţii personale',

    'navStep5' => 'Confirmare',


];
