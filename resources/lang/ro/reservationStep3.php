<?php
//trans('reservationStep3.next')
return [

//    Reservation table
    'youAreBooking' => 'Aţi făcut rezervare:',
    'roomType' => 'Camere:',
    'adult' => 'Adulţi:',
    'child' => 'Copii:',
    'extraBed' => 'Paturi suplimentare:',
    'price' => 'Preţ:',
    'totalPrice' => 'Preţ Final:',
    'days' => 'Date rezervate:',
    //OTHER
    'addRoom' => 'Adaugă cameră',
    'viewDetails' => 'Vezi detalii',
    'availableRooms' => 'Libere',
    'next' => 'Înainte',

    'pleaseChooseInfo' => '<p class="">Vă rugăm alegeţi <strong>tipul şi numărul de camere</strong> dorite pentru a fi rezervate. După aceea,
                    определете <strong>броя на възрастните и децата</strong> за всяка стая.</p>',
    'room' => 'Cameră',
    'people' => 'Persoane:',
    'extraBedPriceInfo' => '* Observaţi că preţul creşte cu 20 % pentru pat suplimentar.',

    'currency' => 'EUR',
    'perNight' => 'pe noapte',

    //    TRANS JS
    'roomJS' => 'CAMERĂ',
    'adultsJS' => 'Adulţi:',
    'childsJS' => 'Copii:',

];
