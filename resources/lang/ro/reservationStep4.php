<?php
//trans('reservationStep4.exPrimPhone')
return [
    'parInfoFillData' => 'Vă rugăm completaţi <strong>datele voastre personale.</strong>',
    'titlePersDetails' => 'Date personale:',
//PERSONAL DATA FORM INPUTS
    'fname' => 'Nume *',
    'mname' => 'Prenume',
    'lname' => 'Nume de familie *',

    'address' => 'Adresă *',
    'city' => 'Oraş *',
    'country' => 'Ţară *',

    'phonecode' => 'Cod telefon *',
    'primPhone' => 'Telefon Principal *',
    'secondPhone' => 'Adăugaţi alt telefon (nu este obligatoriu)',


    'parInfo' => 'Vă rugăm completaţi adresa de e-mail, 
    за да получите потвърждение на вашата резервация,
     за да получавате информация и предложения',

    'email' => 'E-mail *',
    'retypeEmail' => 'Repetaţi E-mail *',
    'specRequire' => 'Cerinţe speciale',
    'nextButton' => 'Înainte',

//    EXAMPLES
    'exPhoneCode' => 'Exemplu: +359',
    'exPrimPhone' => 'Exemplu: 895.655.432',

];
