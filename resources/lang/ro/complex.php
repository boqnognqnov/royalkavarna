<?php
//trans('complex.email')
return [

    'welcomeTo' => 'Bine aţi venit în',
    'facilities' => 'Comodităţi',
    'location' => 'Locaţia',
    'locationNearBy' => 'Locaţia',
    'nearAirport' => '- 70 km de la aeroport - Varna',
    'nearVarnaCity' => '- 65 km de la Varna',
    'nearBalckik' => '- 18 km de la Balchik',
    'neatKavarna' => '- 2 km de la Kavarna',

    'phone' => 'Tel./Fax:',
    'mobilePhone' => 'Tel. mobil:',
    'reception' => 'Recepţie:',
    'email' => 'e-mail:'


];
