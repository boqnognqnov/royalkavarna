<?php
//trans('guestBook.position_req')
return [
//PHP VALIDATION
    'success' => 'Aţi trimis cu succes opinia Dvs.',
    'error' => 'Eroare din partea server-ului',

    'name_req' => 'Nu este introdus nume',
    'email_req' => 'Nu este introdus e-mail',
    'email_valid' => 'E-mailul nu este valid',
    'message_req' => 'Câmpul pentru opinia Dvs nu este completat sau este prea scurt',
    'message_valid' => 'Nu sunt permise mai multe simboluri',
    'image_valid' => 'File-il trebuie să fie imagine',
    'position_req' => 'Introduceţi poziţia Dvs.',
//JS VALIDATION
    'chars_left' => 'rămân',
    'tooManyChars' => 'Prea multe simboluri !!!',
    'maxCharLenght' => 'Număr maxim de simboluri.',
    'characters' => 'simboluri.',
//    PAGE ELEMENTS
    'sendOpinion' => 'Trimiteţi opinia Dvs.',

//    PLACEHOLDERS
    'ph_name' => 'Nume *',
    'ph_email' => 'E-mail *',
    'ph_profession' => 'Profesie',
    'ph_photo' => 'Poză',
    'ph_message' => 'Mesaj *',

];
