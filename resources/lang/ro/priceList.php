<?php
//trans('priceList.currency')
return [
    'pricePageTitle' => 'Cazare în Royal Bay',
    'priceInfo' => '* Toate preţurile se referă la cazare fără paturi suplimentare, pe zi, pe apartament şi includ <abbr title="Value Added Tax">T.V.A.</abbr> (9%).
Включва се легло&amp;закуска, застраховка, туристическа такса, безплатно използване на басейна, шезлонзи и чадъри, безплатно използване на фитнес зала. ',
    'startingFrom' => 'Începe de la',
    'perNight' => 'pentru ziua',
    'viewDetails' => 'Detalii',
    'bookNowButton' => 'Rezervare',
    'currency' => 'lv',


];