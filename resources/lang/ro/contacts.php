<?php
//trans('contacts.aboutUs')
return [
    'contactUs' => 'Contact:',
    'name' => 'Nume',
    'message' => 'Mesaj',
    'sendMessButton' => 'Trimiteţi mesaj',
    'mainAddress' => 'Adresa principală',
    'phone_fax' => 'Tel./Fax:',
    'mobile' => 'Mobil:',
    'generalManager' => 'Manager General',
    'aboutUs' => 'Despre noi',

    //    VALIDATION
    'errorException' => 'Eroare din partea server-ului',
    'successMessage' => 'Dvs. aţi lăsat mesajul cu succes',
    'emptyInputs' => 'Vă rugăm completaţi corect toate poziţiile',







];