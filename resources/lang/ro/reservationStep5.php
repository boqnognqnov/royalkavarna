<?php
//trans('reservationStep5.name')
return [

//    RESERVATION DATA
    'reservationInfo' => 'Informaţie despre rezervarea',
    'complex' => 'Complex:',
    'reservationFrom' => 'Rezervare de la:',
    'reservationTo' => 'Rezervare până la:',
    'bookingBefore1half' => 'Rezervare înainte de ',
    'newPrice' => 'PREŢ NOU:',


    'name' => 'Nume:',
    'room' => 'Cameră:',
    'days' => 'zile:',
    'adults' => 'Adulţi:',
    'childs' => 'Copii:',
    'extraBedsPrice' => 'Paturi suplimentare (preţ):',
    'price' => 'Preţ:',
    'totalPrice' => 'Preţ Final:',
    'currency' => 'EUR',

//    PERSONAL DATA
    'personalData' => 'Informaţii personale',
    'fname' => 'Nume:',
    'mname' => 'Prenume',
    'lname' => 'Familie:',
    'address' => 'Adresă: ',

    'city' => 'Oraş:',
    'country' => 'Ţară: ',
    'phoneCode' => 'Cod telefon: ',
    'primPhone' => 'Telefon:',
    'email' => 'Adresă de e-mail:',
    'requarements' => 'Cerinţe:',
    'confirm' => 'Confirmare',

//    CONFIRM RESERVATION
    'successStatus' => 'Rezervaţie reuşită',

    'successSubtitle' => 'Mulţumesc pentru rezervaţia voastră',

    'successMessage' => 'Mulţumim pentru rezervaţia făcută.
    Нашите служители ще проверят информацията и след това ще удобрят резервацията.
    Ако имате нужда от допълнителна информация, посетете',


    'successMessagePart2' => ' pagina "Contact".',


];
