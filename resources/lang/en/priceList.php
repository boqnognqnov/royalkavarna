<?php
//trans('priceList.bookNowButton')
return [
    'pricePageTitle' => 'Accommodation at Royal Bay',
    'priceInfo' => '* All prices refer to accommodation per day for an apartment without extra beds and include <abbr title="Value Added Tax">VAT</abbr> (9%).
Включва се легло&amp;закуска, застраховка, туристическа такса, безплатно използване на басейна, шезлонзи и чадъри, безплатно използване на фитнес зала. ',
    'startingFrom' => 'starts from',
    'perNight' => 'per day',
    'viewDetails' => 'Details',
    'bookNowButton' => 'Book',
    'currency' => 'BGN',


];