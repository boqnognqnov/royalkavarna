<?php
//trans('reservationStep4.exPrimPhone')
return [
    'parInfoFillData' => 'Please fill in <strong>your personal details.</strong>',
    'titlePersDetails' => 'Personal details:',
//PERSONAL DATA FORM INPUTS
    'fname' => 'First name *',
    'mname' => 'Father\'s name',
    'lname' => 'Surname *',

    'address' => 'Address *',
    'city' => 'City *',
    'country' => 'Country *',

    'phonecode' => 'Telephone code *',
    'primPhone' => 'Main phone number *',
    'secondPhone' => 'Add other phone number (optional)',


    'parInfo' => 'Please enter your email,
    за да получите потвърждение на вашата резервация,
     за да получавате информация и предложения',

    'email' => 'E-mail *',
    'retypeEmail' => 'Reenter your E-mail *',
    'specRequire' => 'Special requirements',
    'nextButton' => 'Next',

//    EXAMPLES
    'exPhoneCode' => 'Example: +359',
    'exPrimPhone' => 'Example: 895 655 432',

];
