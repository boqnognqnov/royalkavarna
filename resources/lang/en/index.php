<?php
//trans('index.indexInfo')
return [

    'chooseComplexes' => 'Please select one of the complexes',

    'indexInfo' => 'The Royal Hotels brand brings together in one constellation objects located in the most picturesque
                    част на Северното българско Черноморие - в околностите на Каварна. Уютният Royal Cove 3*, блестящият
                    Royal Bay 4* и аристократичната морска резиденция Villa Valenti 5* са изключителни места за летен
                    отдих',


];
