<?php
//trans('headerAndFooter.bookNowGoldButton')
return [
    'theComplex' => 'The complex',
    'priceList' => 'Price list',
    'promotions' => 'Discounts',
    'guestBooks' => 'Feedback book',

    'abautUs' => 'About us',
    'presentations' => 'Presentations',
    'contacts' => 'Contact',

    'bookNow' => 'Book',
    'hotelPolicy' => 'Policy of the hotel ',
    'siteMap' => 'Site map',
    'share' => 'Social media',
    'rightsReserved' => 'All right reserved',
    'createdBy' => ' Created by <a href="http://onecreative.eu">OneCreative</a>',

    'contactUs' => 'Contacts',
    'usefulLinks' => 'Useful links',
    'menu' => 'Menu',

    'bookNowGoldButton' => 'Book now',

];
