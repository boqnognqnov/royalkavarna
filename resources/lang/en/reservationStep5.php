<?php
//trans('reservationStep5.bookingBefore1half')
return [

//    RESERVATION DATA
    'reservationInfo' => 'Booking information',
    'complex' => 'Complex:',
    'reservationFrom' => 'Booking from:',
    'reservationTo' => 'Booking to:',
    'bookingBefore1half' => 'Booking before ',
    'newPrice' => 'NEW PRICE:',


    'name' => 'Name:',
    'room' => 'Room:',
    'days' => 'days:',
    'adults' => 'Adults:',
    'childs' => 'Children:',
    'extraBedsPrice' => 'Extra beds (price):',
    'price' => 'Price:',
    'totalPrice' => 'Final price:',
    'currency' => 'EUR',

//    PERSONAL DATA
    'personalData' => 'Personal details',
    'fname' => 'First name:',
    'mname' => 'Father\'s name: ',
    'lname' => 'Surname:',
    'address' => 'Address:',

    'city' => 'City:',
    'country' => 'Country:',
    'phoneCode' => 'Telephone code:',
    'primPhone' => 'Telephone number:',
    'email' => 'E-mail:',
    'requarements' => 'Requirements:',
    'confirm' => 'Confirm',

//    CONFIRM RESERVATION
    'successStatus' => 'Successful booking',

    'successSubtitle' => 'Thank you for your booking',

    'successMessage' => 'Thank you for the booking made.
Нашите служители ще проверят информацията и след това ще удобрят резервацията.
Ако имате нужда от допълнителна информация, посетете',


    'successMessagePart2' => ' Contacts page.',


];
