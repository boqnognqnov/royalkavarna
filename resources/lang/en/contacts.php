<?php
//trans('contacts.emptyInputs')
return [
    'contactUs' => 'Contacts:',
    'name' => 'Name',
    'message' => 'Message',
    'sendMessButton' => 'Send a message',
    'mainAddress' => 'Main address',
    'phone_fax' => 'Tel./Fax:',
    'mobile' => 'Mobile:',
    'generalManager' => 'General Manager',
    'aboutUs' => 'About us',

    //    VALIDATION
    'errorException' => 'Server error',
    'successMessage' => 'You have successfully left your message',
    'emptyInputs' => 'Please fill in all fields correctly',







];