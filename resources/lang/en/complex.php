<?php

   //trans('complex.email')
return [

    'welcomeTo' => 'Welcome to',
    'facilities' => 'Facilities',
    'location' => 'Location',
    'locationNearBy' => 'Location',
    'nearAirport' => '- 70 km from airport - Varna',
    'nearVarnaCity' => '- 65 km from Varna',
    'nearBalckik' => '- 18 km from Balchik',
    'neatKavarna' => '- 2 km from Kavarna',

    'phone' => 'Tel./Fax:',
    'mobilePhone' => 'Mobile phone:',
    'reception' => 'Reception desk:',
    'email' => 'E-mail:'

];
