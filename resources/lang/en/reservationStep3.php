<?php
//trans('reservationStep3.currency')
return [

//    Reservation table
    'youAreBooking' => 'You booked:',
    'roomType' => 'Rooms:',
    'adult' => 'Adults:',
    'child' => 'Children:',
    'extraBed' => 'Extra beds:',
    'price' => 'Price:',
    'totalPrice' => 'Final price:',
    'days' => 'booked days:',
    //OTHER
    'addRoom' => 'Add room',
    'viewDetails' => 'See details',
    'availableRooms' => 'Vacant',
    'next' => 'Next',

    'pleaseChooseInfo' => '<p class="">Please select <strong>the type and the number of the rooms</strong> you want to book. Next,
                    определете <strong>броя на възрастните и децата</strong> за всяка стая.</p>',
    'room' => 'Room',
    'people' => 'People:',
    'extraBedPriceInfo' => '* Please note that the price goes up by 20% for an extra bed.',

    'currency' => 'EUR',
    'perNight' => 'per night',

    //    TRANS JS
    'roomJS' => 'ROOM',
    'adultsJS' => 'Adults:',
    'childsJS' => 'Children:',

];
