<?php
//trans('guestBook.ph_message')
return [
//PHP VALIDATION
    'success' => 'You have sent your opinion successfully',
    'error' => 'Server error',

    'name_req' => 'Name is not filled in',
    'email_req' => 'E-mail is not filled in',
    'email_valid' => 'Email is not valid',
    'message_req' => 'The opinion field is empty, or the text is too short',
    'message_valid' => 'That many characters are not allowed',
    'image_valid' => 'The file should be an image',
    'position_req' => 'Enter your position',
//JS VALIDATION
    'chars_left' => 'remain',
    'tooManyChars' => 'Too many characters!!!',
    'maxCharLenght' => 'Maximum number of characters:',
    'characters' => 'characters.',
//    PAGE ELEMENTS
    'sendOpinion' => 'Send your opinion',

//    PLACEHOLDERS
    'ph_name' => 'Name *',
    'ph_email' => 'E-mail *',
    'ph_profession' => 'Profession',
    'ph_photo' => 'Photo',
    'ph_message' => 'Message *',

];
