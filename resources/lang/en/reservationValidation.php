<?php
//trans('reservationValidation.anyDateInputIsEmpty')
return [
    'tooManyPeople' => 'Higher number of persons than the capacity of the room',
    //CLIENT
    'dateIsPased' => 'The first date has passed',
    'firstDateIsBigger' => 'The first date is later than the end date',
    'anyDateInputIsEmpty' => 'Some of the date fields are not filled ! Please fill in date from:  and date to: .',
    'NoPricesForRooms' => 'Rooms in the complex are not offered during this period. Please select another period',

    'noRoomsInThisRange' => 'No vacant rooms',

//    ADMIN
    'weCantRemoveAllRoomAndContinue' => 'We can\'t remove all rooms.
    If you want we can to delete all reservation record to previous page',



];
