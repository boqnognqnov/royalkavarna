<?php
//trans('reservationHeaderNav.navStep5')
return [
//HEADER CONTENT
    'bookARoom' => 'Booking',

    'step1' => 'Step 1',
    'step2' => 'Step 2',
    'step3' => 'Step 3',
    'step4' => 'Step 4',
    'step5' => 'Step 5',

    'success' => 'All steps completed',

//    NAVIGATION STEPS

    'navStep1' => 'Select complex',

    'navStep2' => 'Select date',

    'navStep3' => 'Select rooms',

    'navStep4' => 'Personal details',

    'navStep5' => 'Confirmation',


];
