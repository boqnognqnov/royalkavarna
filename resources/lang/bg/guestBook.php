<?php
//trans('guestBook.position_req')
return [
//PHP VALIDATION
    'success' => 'Изпратихте вашето мнение успешно',
    'error' => 'Грешка от страна на сървъра',

    'name_req' => 'Не е въведено име',
    'email_req' => 'Не е въведен имейл',
    'email_valid' => 'Email адресът не е валиден',
    'message_req' => 'Полето за мнение е празно, или текстът е твърде кратък',
    'message_valid' => 'Не са позволени толкова много символи',
    'image_valid' => 'Файлът трябва да е изображение',
    'position_req' => 'Въведете вашата позиция',
//JS VALIDATION
    'chars_left' => 'остават',
    'tooManyChars' => 'Прекалено много символи !!!',
    'maxCharLenght' => 'Максимален брой символи:',
    'characters' => 'символи.',
//    PAGE ELEMENTS
    'sendOpinion' => 'Изпратете вашето мнение',

//    PLACEHOLDERS
    'ph_name' => 'Име *',
    'ph_email' => 'Е-мейл *',
    'ph_profession' => 'Професия',
    'ph_photo' => 'Снимка',
    'ph_message' => 'Съобщение *',

];
