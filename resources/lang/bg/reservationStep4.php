<?php
//trans('reservationStep4.exPrimPhone')
return [
    'parInfoFillData' => 'Моля попълнете  <strong>вашата лична информация.</strong>',
    'titlePersDetails' => 'Лични данни:',
//PERSONAL DATA FORM INPUTS
    'fname' => 'Име *',
    'mname' => 'Презиме',
    'lname' => 'Фамиля *',

    'address' => 'Адрес *',
    'city' => 'Град *',
    'country' => 'Държава *',

    'phonecode' => 'Телефонен код *',
    'primPhone' => 'Основен телефон *',
    'secondPhone' => 'Добавете друг телефон (Незадължително)',


    'parInfo' => 'Моля попълнете вашия email адрес,
    за да получите потвърждение на вашата резервация,
     за да получавате информация и предложения',

    'email' => 'E-mail *',
    'retypeEmail' => 'Повторете E-mail *',
    'specRequire' => 'Специални изисквания',
    'nextButton' => 'Напред',

//    EXAMPLES
    'exPhoneCode' => 'Пример: +359',
    'exPrimPhone' => 'Пример: 895 655 432',

];
