<?php
//trans('reservationStep5.name')
return [

//    RESERVATION DATA
    'reservationInfo' => 'Резервационна информация',
    'complex' => 'Комплекс:',
    'reservationFrom' => 'Резервация от:',
    'reservationTo' => 'Резервация до:',
    'bookingBefore1half'=>'Резервация преди ',
    'newPrice' => 'НОВА ЦЕНА:',


    'name' => 'Име:',
    'room' => 'Стая:',
    'days' => 'дни:',
    'adults' => 'Възрастни:',
    'childs' => 'Деца:',
    'extraBedsPrice' => 'Допълнителни легла (цена):',
    'price' => 'Цена:',
    'totalPrice' => 'Крайна цена:',
    'currency' => 'лв.',

//    PERSONAL DATA
    'personalData' => 'Лична информация',
    'fname' => 'Име:',
    'mname' => 'Презиме:',
    'lname' => 'Фамилия:',
    'address' => 'Адрес:',

    'city' => 'Град:',
    'country' => 'Държава:',
    'phoneCode' => 'Телефонен код:',
    'primPhone' => 'Телефон:',
    'email' => 'E-mail адрес:',
    'requarements' => 'Изисквания:',
    'confirm' => 'Потвърди',

//    CONFIRM RESERVATION
    'successStatus' => 'Успешна резервация',

    'successSubtitle' => 'Благодарим Ви за вашата резервация',

    'successMessage' => 'Благодарим Ви за направената резервация.
    Нашите служители ще проверят информацията и след това ще одобрят резервацията.
    Ако имате нужда от допълнителна информация, посетете',


    'successMessagePart2' => ' страница "Контакти".',


];
