<?php
//trans('reservationValidation.tooManyPeople')
return [
    'tooManyPeople' => 'Хората са повече от вместимостта на стаята',
    //CLIENT
    'dateIsPased' => 'The first date is pased',
    'firstDateIsBigger' => 'The first date is bigger from the end date',
    'anyDateInputIsEmpty' => 'Някое от полетата за дата не е попълнено ! Моля попълнете дата от:  и дата до: .',
    'NoPricesForRooms' => 'В този период не се предлагат стаи в комплекса. Моля изберете друг период',

    'noRoomsInThisRange'=>'Няма свободни стаи',

//    ADMIN
    'weCantRemoveAllRoomAndContinue' => 'We can\'t remove all rooms.
    If you want we can to delete all reservation record to previous page',



];
