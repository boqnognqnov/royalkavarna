<?php
//trans('complex.email')
return [

    'welcomeTo' => 'Добре дошли в',
    'facilities' => 'Удобства',
    'location' => 'Местоположение',
    'locationNearBy' => 'Местоположение',
    'nearAirport' => '- 70 км от летище - Варна',
    'nearVarnaCity' => '- 65 км от Варна',
    'nearBalckik' => '- 18 км от Балчик',
    'neatKavarna' => '- 2 км от Каварна',

    'phone' => 'Тел./Факс:',
    'mobilePhone' => 'Моб. телефон:',
    'reception' => 'Рецепция:',
    'email' => 'Е-мейл:'


];
