<?php
//trans('reservationHeaderNav.navStep5')
return [
//HEADER CONTENT
    'bookARoom' => 'Резервация',

    'step1' => 'Стъпка 1',
    'step2' => 'Стъпка 2',
    'step3' => 'Стъпка 3',
    'step4' => 'Стъпка 4',
    'step5' => 'Стъпка 5',

    'success' => 'Всички стъпки завършени',

//    NAVIGATION STEPS

    'navStep1' => 'Избор на комплекс',

    'navStep2' => 'Избор на дата',

    'navStep3' => 'Избор на стаи',

    'navStep4' => 'Лична информация',

    'navStep5' => 'Потвърждение',


];
