<?php
//trans('contacts.aboutUs')
return [
    'contactUs' => 'Контакти:',
    'name'=>'Име',
    'message'=>'Съобщение',
    'sendMessButton'=>'Изпратете съобщение',
    'mainAddress'=>'Главен адрес',
    'phone_fax'=>'Тел./Факс:',
    'mobile'=>'Мобилен:',
    'generalManager'=>'Главен менаджър',
    'aboutUs'=>'За нас',

    //    VALIDATION
    'errorException'=>'Грешка от страна на сървъра',
    'successMessage'=>'Вие успешно оставихте вашето съобщение',
    'emptyInputs'=>'Моля попълнете коректно всички полета',







];