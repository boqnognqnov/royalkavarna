<?php
//trans('reservationStep3.next')
return [

//    Reservation table
    'youAreBooking' => 'Вие резервирахте:',
    'roomType' => 'Стаи:',
    'adult' => 'Възрастни:',
    'child' => 'Деца:',
    'extraBed' => 'Допълнителни легла:',
    'price' => 'Цена:',
    'totalPrice' => 'Крайна цена:',
    'days' => 'резервирани дни:',
    //OTHER
    'addRoom' => 'Добави стая',
    'viewDetails' => 'Виж детайли',
    'availableRooms' => 'Свободни',
    'next' => 'Напред',

    'pleaseChooseInfo'=>'<p class="">Моля изберете <strong>типа и броя на стаите</strong> които желаете да резервирате. След това,
                    определете <strong>броя на възрастните и децата</strong> за всяка стая.</p>',
    'room'=>'Стая',
    'people'=>'Хора:',
    'extraBedPriceInfo'=>'* Моля забележете че цената се покачва с 20 % за допълнително легло.',

    'currency'=>'лв',
    'perNight'=>'за вечер',

    //    TRANS JS
    'roomJS' => 'СТАЯ',
    'adultsJS' => 'Възрастни:',
    'childsJS' => 'Деца:',

];
