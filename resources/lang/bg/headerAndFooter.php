<?php
//trans('headerAndFooter.share')
return [
    'theComplex' => 'Комплексът',
    'priceList' => 'Ценова листа',
    'promotions' => 'Промоции',
    'guestBooks' => 'Книга за мнения',

    'abautUs' => 'За нас',
    'presentations' => 'Презентации',
    'contacts' => 'Контакт',

    'bookNow' => 'Резервирай',
    'hotelPolicy' => 'Политика на хотела ',
    'siteMap' => 'Карта на сайта',
    'share' => 'Социални мрежи',
    'rightsReserved' => 'Всички права запазени',
    'createdBy' => ' Създаден от <a href="http://onecreative.eu">OneCreative</a>',

    'contactUs' => 'Контакти',
    'usefulLinks' => 'Полезни линкове',
    'menu' => 'Меню',

    'bookNowGoldButton' => 'Резервирай сега',

];
