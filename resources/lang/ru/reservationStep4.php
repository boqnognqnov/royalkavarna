<?php
//trans('reservationStep4.exPrimPhone')
return [
    'parInfoFillData' => 'Пожалуйста, заполните<strong> вашу личную информацию.</strong>',
    'titlePersDetails' => 'Персональные данные:',
//PERSONAL DATA FORM INPUTS
    'fname' => 'Имя *',
    'mname' => 'Отчество',
    'lname' => 'Фамилия *',

    'address' => 'Адрес *',
    'city' => 'Город *',
    'country' => 'Государство *',

    'phonecode' => 'Телефонный код*',
    'primPhone' => 'Основной телефон *',
    'secondPhone' => 'Добавьте другой телефон (необязательно)',


    'parInfo' => 'Пожалуйста, запишите свой электронный адрес,
    за да получите потвърждение на вашата резервация,
     за да получавате информация и предложения',

    'email' => 'E-mail *',
    'retypeEmail' => 'Повторите E-mail *',
    'specRequire' => 'Специальные требования',
    'nextButton' => 'Вперед',

//    EXAMPLES
    'exPhoneCode' => 'Пример: +359',
    'exPrimPhone' => 'Пример: 895 655 432',

];
