<?php
//trans('reservationHeaderNav.navStep5')
return [
//HEADER CONTENT
    'bookARoom' => 'Бронирование',

    'step1' => 'Шаг 1',
    'step2' => 'Шаг 2',
    'step3' => 'Шаг 3',
    'step4' => 'Шаг 4',
    'step5' => 'Шаг 5',

    'success' => 'Все шаги завершены',

//    NAVIGATION STEPS

    'navStep1' => 'Выбор комплекса',

    'navStep2' => 'Выбор даты',

    'navStep3' => 'Выбор номера',

    'navStep4' => 'Личная информация',

    'navStep5' => 'Подтверждение',


];
