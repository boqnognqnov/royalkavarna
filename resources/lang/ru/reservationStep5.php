<?php
//trans('reservationStep5.name')
return [

//    RESERVATION DATA
    'reservationInfo' => 'Информация о бронировании',
    'complex' => 'Комплекс:',
    'reservationFrom' => 'Бронирование с:',
    'reservationTo' => 'Бронирование до:',
    'bookingBefore1half' => 'Предыдущее бронирование ',
    'newPrice' => 'НОВАЯ ЦЕНА:',


    'name' => 'Имя:',
    'room' => 'Комната:',
    'days' => 'Дни:',
    'adults' => 'Взрослые:',
    'childs' => 'Дети:',
    'extraBedsPrice' => 'Дополнительные кровати(цена):',
    'price' => 'Цена:',
    'totalPrice' => 'Крайная цена:',
    'currency' => 'EUR',

//    PERSONAL DATA
    'personalData' => 'Личная информация',
    'fname' => 'Имя:',
    'mname' => 'Отчество:',
    'lname' => 'Фамилия:',
    'address' => 'Адрес:',

    'city' => 'Город:',
    'country' => 'Государство:',
    'phoneCode' => 'Телефонный код:',
    'primPhone' => 'Телефон:',
    'email' => 'E-mail адрес:',
    'requarements' => 'Требования:',
    'confirm' => 'Потверждение',

//    CONFIRM RESERVATION
    'successStatus' => 'Успешное бронирование',

    'successSubtitle' => 'Благодарим за Ваше бронирование',

    'successMessage' => 'Благодарим Вас за бронирование
    Нашите служители ще проверят информацията и след това ще удобрят резервацията.
    Ако имате нужда от допълнителна информация, посетете',


    'successMessagePart2' => ' страница "Контакты".',


];
