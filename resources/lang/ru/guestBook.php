<?php
//trans('guestBook.position_req')
return [
//PHP VALIDATION
    'success' => 'Вы успешно отправили свое мнение',
    'error' => 'Ошибка на сервере',

    'name_req' => 'Не введено имя',
    'email_req' => 'Не введен е-мейл',
    'email_valid' => 'Адрес электронной почты недействителен',
    'message_req' => 'Поле для мнений  является пустым или текст слишком короткий',
    'message_valid' => 'Не разрешено так много символов',
    'image_valid' => 'Файл должен быть изображением',
    'position_req' => 'Введите вашу позицию',
//JS VALIDATION
    'chars_left' => 'остаются',
    'tooManyChars' => 'Слишком много символов !!!',
    'maxCharLenght' => 'Максимальное количество символов:',
    'characters' => 'символы.',
//    PAGE ELEMENTS
    'sendOpinion' => 'Отправьте свое мнение',

//    PLACEHOLDERS
    'ph_name' => 'Имя *',
    'ph_email' => 'Е-мейл *',
    'ph_profession' => 'Профессия',
    'ph_photo' => 'Фото, изображение',
    'ph_message' => 'Сообщение',

];
