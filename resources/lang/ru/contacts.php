<?php
//trans('contacts.aboutUs')
return [
    'contactUs' => 'Контакты:',
    'name' => 'Имя',
    'message' => 'Сообщение',
    'sendMessButton' => 'Отправьте сообщение',
    'mainAddress' => 'Главный адрес',
    'phone_fax' => 'Тел./Факс:',
    'mobile' => 'Мобильный:',
    'generalManager' => 'Генеральный менеджер',
    'aboutUs' => 'О нас',

    //    VALIDATION
    'errorException' => 'Ошибка на сервере',
    'successMessage' => 'Вы успешно оставили свое сообщение',
    'emptyInputs' => 'Пожалуйста, заполните все поля корректно',
    

];