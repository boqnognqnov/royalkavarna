<?php
//trans('headerAndFooter.share')
return [
    'theComplex' => 'Комплекс',
    'priceList' => 'Прайс-лист',
    'promotions' => 'промо-акция',
    'guestBooks' => 'Книга отзывов',

    'abautUs' => 'О нас',
    'presentations' => 'Презентации',
    'contacts' => 'Контактное лицо',

    'bookNow' => 'Бронируй',
    'hotelPolicy' => 'Политика отеля ',
    'siteMap' => 'Карта сайта',
    'share' => 'Социальные сети',
    'rightsReserved' => 'Все права защищены',
    'createdBy' => ' Создан <a href="http://onecreative.eu">OneCreative</a>',

    'contactUs' => 'Контактное лицо',
    'usefulLinks' => 'Полезные ссылки',
    'menu' => 'Меню',

    'bookNowGoldButton' => 'Бронируй сейчас',

];
