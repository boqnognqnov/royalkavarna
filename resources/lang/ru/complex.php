<?php
//trans('complex.email')
return [

    'welcomeTo' => 'Добро пожаловать в',
    'facilities' => 'Удобства',
    'location' => 'Местоположение',
    'locationNearBy' => 'Местоположение',
    'nearAirport' => '-в 70 км от аэропорта - Варна',
    'nearVarnaCity' => '-в  65 км от Варны',
    'nearBalckik' => '- в 18 км от Балчика',
    'neatKavarna' => '- в 2 км от Каварны',

    'phone' => 'Тел./Факс:',
    'mobilePhone' => 'Моб. телефон:',
    'reception' => 'Рецепция:',
    'email' => 'Электронная почта/ Е-мейл/:'


];
