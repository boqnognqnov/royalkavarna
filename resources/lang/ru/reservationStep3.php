<?php
//trans('reservationStep3.next')
return [

//    Reservation table
    'youAreBooking' => 'Вы бронировали:',
    'roomType' => 'Номера:',
    'adult' => 'Взрослые:',
    'child' => 'Дети:',
    'extraBed' => 'Дополнительные кровати:',
    'price' => 'Цена:',
    'totalPrice' => 'Крайная цена:',
    'days' => 'забронированные дни:',
    //OTHER
    'addRoom' => 'Добавь комнату',
    'viewDetails' => 'Смотри детали',
    'availableRooms' => 'Свободные',
    'next' => 'Вперед',

    'pleaseChooseInfo' => '<p class=""> Пожалуйста, выберите<strong>вид и количество комнат, которые желаете бронировать. После этого,
                    определете <strong>броя на възрастните и децата</strong> за всяка стая.</p>',
    'room' => 'Комната',
    'people' => 'Люди:',
    'extraBedPriceInfo' => 'Пожалуйста, обратите внимание на то, что цена повышается на 20% за дополнительную кровать.',

    'currency' => 'EUR',
    'perNight' => 'за вечер',

    //    TRANS JS
    'roomJS' => 'КОМНАТА',
    'adultsJS' => 'Взрослые:',
    'childsJS' => 'Дети:',

];
