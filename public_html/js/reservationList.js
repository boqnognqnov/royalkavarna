// ROOM DESCRIPTION OVERFLOW HANDLING
$(document).ready(function () {
    $('.sendReservationApi').hide();

    // GET SELECTED ROOM
    $('.selectedCTA').hide();
    var selected_room = $('[name="selected_room"]').val();
    if (selected_room) {
        var targetID = 'selected_room_type' + selected_room;
        // MARK SELECTED ROOM
        $('#' + targetID).addClass('selected').find('.selectedCTA').show();
        // JUMP TO SELECTED ROOM
        var targetPosition = $('#' + targetID).offset();
        window.scrollTo(targetPosition.left, targetPosition.top);
    }

    // ADD ROOM ROW
    clickNewRowButton();

//  FOREACH HTML IF SESSION EXIST

    getSessionApi();


    peopleSelectorEventListener();
});


function peopleSelectorEventListener() {

    $('.people-selector').on('click, change', function (event) {
//                calculatePricePerRow();

        var objectToReturn = collectReservationData();
        getPriceFromApi(objectToReturn);
    });

}


var rowSessionCounter = 1;

function getSessionApi() {
    $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('input[name="tokenJs"]').val()}});
    $.ajax({
        url: "/api/reservation/checkForOpenSession",
        type: "POST",
        dataType: "json"
    }).done(function (result) {
        console.log(JSON.stringify(result['data']));
        if ($('input[name="is_inseparable_js"]').val() == 0) {
            autoFillHotelRows(result);
        } else {
            autoFillHouseRows(result);
        }

        calculatePricePerRow();
    }).fail(function (result) {
        console.log(JSON.stringify(result));
    });
}

function autoFillHotelRows(result) {
    $.each(result['data'], function (roomId, roomData) {

        $.each(roomData, function (rowNum, rowData) {


            var rowTemplate = $('.reservation_inputs_template');

            $('.reservation_inputs_template .span_temp_counter').text(rowSessionCounter);
            rowTemplate.find('.row').attr('id', 'room' + rowSessionCounter);
            rowTemplate.find('.row  .remove_room_row').attr('data-remove-room-id', rowSessionCounter);


            var adultVal = result['data'][roomId][rowNum]['adult'];
            var childVal = result['data'][roomId][rowNum]['child'];

            $('#reservation_inputs_' + roomId).append(rowTemplate.html());

            $('#reservation_inputs_' + roomId).find('#room' + rowSessionCounter + ' .adult-selector').val(adultVal);
            $('#reservation_inputs_' + roomId).find('#room' + rowSessionCounter + ' .child-selector').val(childVal);

            declareRoomRemoveListener();
            adultChildEventsListener();

            rowSessionCounter++;
        });

    });
}

function autoFillHouseRows(result) {
    $.each(result['data'], function (roomId, roomData) {

        $.each(roomData, function (rowNum, rowData) {
            var adultVal = result['data'][roomId][rowNum]['adult'];
            $('#reservation_inputs_' + roomId).find('#room' + rowSessionCounter + ' .adult-selector').val(adultVal);
        });
    });
}

function clickNewRowButton() {
    var temp_counter = 0;
    $('.reserveRoom').on('click', function (event) {
        var $this = $(this);
        var room_id = $this.attr('data-room-id');
        var room_available_counter = $this.attr('data-rooms-available');
        if (rowSessionCounter != 0 && temp_counter == 0) {
            temp_counter = rowSessionCounter;
        }
        temp_counter++;
        $('.reservation_inputs_template .row').attr('id', 'room' + temp_counter);
        $('.reservation_inputs_template .row .remove_room_row').attr('data-remove-room-id', temp_counter);
        var templete_reservation_html = $('.reservation_inputs_template').html();
        var numberOfRoomsOfOneType = countRoomRows($('#reservation_inputs_' + room_id));
        // CHECK AVAILABLE ROOMS MUST BE MORE
        if (room_available_counter >= numberOfRoomsOfOneType) {
            $('#reservation_inputs_' + room_id).append(templete_reservation_html);
            numerateRoomRows($('#reservation_inputs_' + room_id));
            declareRoomRemoveListener();
            adultChildEventsListener();
        }
    });
}
// REMOVE ROOM ROW
function declareRoomRemoveListener() {
    $('.remove_room_row').on('click', function (event) {
        var $this = $(this);
        var room_row_id = $this.attr('data-remove-room-id');
        var parentRoomRowsDiv = $("#room" + room_row_id).parent(".room-rows");
        var room_rows_id = parentRoomRowsDiv.attr("id");
        $("#" + room_rows_id + " #room" + room_row_id).remove();

        $('.final_price_with_booking').empty();

        numerateRoomRows(parentRoomRowsDiv);
        calculatePricePerRow();
    });
}


// NUMBER ROOM ROWS
function numerateRoomRows(parentRoomRowsDiv) {
    var counter = 0;
    $(parentRoomRowsDiv).children('.row').each(function () {
        if ($(this).hasClass("row")) {
            counter++;
            var test = $(this).find('.span_temp_counter').text(counter);
        }
    });
}


// COUNT ROOM ROWS
function countRoomRows(parentRoomRowsDiv) {
    var counter = 0;
    $(parentRoomRowsDiv).children('.row').each(function () {
        if ($(this).hasClass("row")) {
            counter++;
        }
    });
    return counter + 1;
}

// NEXT BUTTON HANDLER
$('.sendReservationApi').on('click', function (event) {
    var objectToReturn = collectReservationData();
    sendReservationApi(objectToReturn);
});

// SUBMIT ALL DATA
function sendReservationApi(objectData) {
    $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('input[name="tokenJs"]').val()}});
    $.ajax({
        url: "/api/reservation/step3/storeSession",
        type: "POST",
        dataType: "json",
        data: objectData
    }).done(function (result) {
        window.location.href = "/reservation/personal_data/organic";
    }).fail(function (result) {
//                $.notify(result.responseJSON['data']);
        $.notify({
            // options
            icon: 'glyphicon glyphicon-remove',
            message: result.responseJSON['data']
        }, {
            // settings
            type: 'danger',
            timer: 0,
            allow_dismiss: true
        });
        // console.log(JSON.stringify(result.responseJSON['data']));
    })
}

// COLLECT ALL DATA
function collectReservationData() {
    var arrayToReturn = {};
    $('.room-rows').each(function () {
        var roomType = $(this);
        var roomTypeID = roomType.attr('id').replace("reservation_inputs_", "");
        arrayToReturn[roomTypeID] = {};
        var rowCounter = 0;
        $(roomType).children('.row').each(function () {
            rowCounter++;
            arrayToReturn[roomTypeID][rowCounter] = {};
            var adultVal = 0;
            if ($(this).find('.adult-selector').val().length != 0) {
                adultVal = $(this).find('.adult-selector').val();
            }
            var childVal = 0;
            if ($(this).find('.adult-selector').val().length != 0) {
                childVal = $(this).find('.child-selector').val();
            }
            arrayToReturn[roomTypeID][rowCounter]['adult'] = adultVal;
            arrayToReturn[roomTypeID][rowCounter]['child'] = childVal;
        });
        rowCounter = 0;
    });
    return arrayToReturn;
}


// INPUT HANDLING
function adultChildEventsListener() {
    $('.room-rows .child-selector').on('click, change', function (event) {
        calculatePricePerRow();
    });
    $('.room-rows .adult-selector').on('click, change', function (event) {
        calculatePricePerRow();
    });
}

// CALCULATE PRICE PER ROW
function calculatePricePerRow() {
    var objectToReturn = collectReservationData();
    getPriceFromApi(objectToReturn);
}

// GET PRICE FROM API
function getPriceFromApi(objectData) {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('input[name="tokenJs"]').val()
        }
    });
    $.ajax({
        url: "/api/reservation/step3/getPrices",
        type: "POST",
        dataType: "json",
        data: objectData
    }).done(function (result) {
        console.log(JSON.stringify(result));
        var totalDiscount = result.data['total'];
        var days = result.data['days'];
        var RoomTypes = result.data['data'];
        var EarlyBooking = result.early_booking;
        var totalReal = totalDiscount / ((100 - EarlyBooking["percentage"]) / 100);
//                totalReal = Math.round(totalReal).toFixed(2);
        var trTemplate = $('.reservation_data_template tbody').html();
        $tableBody = $('#total_data_table').find('tbody');
        $tableBody.empty();
        $tableBody.append(trTemplate);
        var counter = 1;
        var currencyCoef = $('input[name="currencyCoef"]').val();
        $.each(RoomTypes, function (roomId, roomData) {
            $.each(roomData, function (row, rowData) {
                var rowExtraBed = '0';
           

                if (rowData['extra'] > 0) {
                    var isPercentage = '%';
                    var extraBedPrice = rowData['extra_bed_price'];
                    if (rowData['is_percentage'] == false) {
                        isPercentage = $('input[name="currencyName"]').val();
                    }
                    rowExtraBed = rowData['extra'] + ' (+' + (Math.round(currencyCoef * extraBedPrice * 100) / 100).toFixed(2) + ' ' + isPercentage + ')';
                }
                counter++;
                if ($.isNumeric(rowData['real_price'])) {
                    $tableBody.append('<tr class="row-reservation">' + '<td>' + rowData['room_name'] + '</td>' + '<td>' + rowData['adult'] + '</td>' + '<td>' + rowData['child'] + '</td>' + '<td>' + days + '</td>' + '<td>' + rowExtraBed + '</td>' + '<td>' + (Math.round((currencyCoef * rowData['real_price'] * days) * 100) / 100).toFixed(2) + ' ' + $('input[name="currencyName"]').val() + '</td>' + '</tr>');
                } else {
                    $.notify({
                        // options
                        icon: 'glyphicon glyphicon-remove',
                        message: rowData['real_price']
                    }, {
                        // settings
                        type: 'danger',
                        delay: 2000,
                        allow_dismiss: true
                    });


                    $tableBody.append('<tr class="row-reservation row-error">' + '<td>' + rowData['room_name'] + '</td>' + '<td>' + rowData['adult'] + '</td>' + '<td>' + rowData['child'] + '</td>' + '<td>' + days + '</td>' + '<td>' + rowExtraBed + '</td>' + '<td>' + (Math.round(rowData['real_price'] * 100) / 100).toFixed(2) + '</td>' + '</tr>');
                }
            });
        });
        $('.td_rowspan').attr('rowspan', counter);


        if (EarlyBooking['percentage'] > 0) {

            var rawDate = EarlyBooking["until_date"].split(' ');
            rawDate = rawDate[0].split('-');
            var earlyBookingDate = rawDate[2] + '.' + rawDate[1] + '.' + rawDate[0];

            $('.booking-room-group.selected .selectedCTA').hide();
            $tableBody.append('<tr class="row-last"><td colspan="7"><span class="row-label">Total price:</span> ' + '<span class="total-price"><del>' + (Math.round(currencyCoef * totalReal * 100) / 100).toFixed(2) + ' ' + $('input[name="currencyName"]').val() + '</del></span></td></tr>');

            $('.final_price_with_booking').empty().append('Early booking before ' + earlyBookingDate + ': <span class="spaced"><strong>-' + EarlyBooking["percentage"] + '%</strong></span> <span class="text-uppercase"><span class="big">New price:</span> <span class="lead"><strong>' + (Math.round(currencyCoef * totalDiscount * 100) / 100).toFixed(2) + ' ' + $('input[name="currencyName"]').val() + '</strong></span></span>');
            $('.final_price_with_booking').show();
        } else {
            $tableBody.append('<tr class="row-last"><td colspan="7"><span class="row-label">Total price:</span> ' + '<span class="total-price">' + (Math.round(currencyCoef * totalReal * 100) / 100).toFixed(2) + ' ' + $('input[name="currencyName"]').val() + '</span></td></tr>');
            $('.booking-room-group.selected .selectedCTA').show();
        }
        $('.sendReservationApi').show();


    }).fail(function (result) {
        $tableBody = $('#total_data_table').find('tbody');
        $tableBody.empty();
        $('.sendReservationApi').hide();
        $('.final_price_with_booking').hide();
        $('.booking-room-group.selected .selectedCTA').show();
    });
}
