<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComplexPromotionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('complex_promotions', function (Blueprint $table) {
            $table->increments('id');;
            $table->integer('complex_id', false, true);
            $table->foreign('complex_id')->references('id')->on('complexes');
            $table->string('title_bg');
            $table->string('title_en');
            $table->string('title_ru');
            $table->string('title_ro');
            $table->string('image');
            $table->text('description_bg');
            $table->text('description_en');
            $table->text('description_ru');
            $table->text('description_ro');
            $table->string('mini_title_bg');
            $table->string('mini_title_en');
            $table->string('mini_title_ru');
            $table->string('mini_title_ro');
            $table->text('mini_description_bg');
            $table->text('mini_description_en');
            $table->text('mini_description_ru');
            $table->text('mini_description_ro');
            $table->string('href');
            $table->string('href_text_bg');
            $table->string('href_text_en');
            $table->string('href_text_ru');
            $table->string('href_text_ro');
            $table->timestamp('date_from');
            $table->timestamp('date_to');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('complex_promotions');
    }
}
