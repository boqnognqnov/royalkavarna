<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EarlyBooking extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('early_booking', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('complex_id', false, true);
            $table->foreign('complex_id')->references('id')->on('complexes');
            $table->integer('percentage');
            $table->timestamp('until_date');
            $table->boolean('is_active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('early_booking');
    }
}
