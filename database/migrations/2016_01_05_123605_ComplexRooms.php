<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ComplexRooms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('complex_room_types', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('complex_id', false, true);
            $table->foreign('complex_id')->references('id')->on('complexes');
            $table->string('main_image');
            $table->integer('count_rooms', false, true);
            //ROOM TYPE
            $table->string('type_bg');
            $table->string('type_en');
            $table->string('type_ru');
            $table->string('type_ro');
//            SUB TITTLES
            $table->string('type_sub_bg');
            $table->string('type_sub_en');
            $table->string('type_sub_ru');
            $table->string('type_sub_ro');
            //DESCRIPTION
            $table->text('description_bg');
            $table->text('description_en');
            $table->text('description_ru');
            $table->text('description_ro');
            //Other information
            $table->text('other_info_bg');
            $table->text('other_info_en');
            $table->text('other_info_ru');
            $table->text('other_info_ro');
            //Price information
            $table->text('price_info_bg');
            $table->text('price_info_en');
            $table->text('price_info_ru');
            $table->text('price_info_ro');
            $table->timestamps();
        });

        Schema::table('facilities', function (Blueprint $table) {
            $table->foreign('room_id')->references('id')->on('complex_room_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('complex_room_types');
    }
}
