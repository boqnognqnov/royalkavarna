<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddComplexField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('complexes', function (Blueprint $table) {
            $table->string('mini_information_bg')->after('name_ru');
            $table->string('mini_information_en')->after('mini_information_bg');
            $table->string('mini_information_ru')->after('mini_information_en');
            $table->string('mini_information_ro')->after('mini_information_ru');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
