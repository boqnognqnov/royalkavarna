<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ReservationsNewFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reservations', function (Blueprint $table) {
            $table->string('firstname')->after('complex_id');
            $table->string('middle')->after('firstname');
            $table->string('lastname')->after('middle');
            $table->string('address')->after('lastname');
            $table->string('city')->after('address');
            $table->string('country')->after('city');
            $table->string('telephone1')->after('country');
            $table->string('telephonecode')->after('telephone1');
            $table->string('telephone2')->after('telephonecode');
            $table->string('email')->after('telephone2');
            $table->text('requirements')->after('email');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reservations', function (Blueprint $table) {
            //
        });
    }
}
