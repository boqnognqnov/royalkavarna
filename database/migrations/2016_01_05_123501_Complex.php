<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Complex extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('complexes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('image');
            $table->string('logo');
            $table->string('name_bg');
            $table->string('name_en');
            $table->string('name_ru');
            $table->string('name_ro');
            $table->text('about_bg');
            $table->text('about_en');
            $table->text('about_ru');
            $table->text('about_ro');
            $table->text('early_booking_text_bg');
            $table->text('early_booking_text_en');
            $table->text('early_booking_text_ru');
            $table->text('early_booking_text_ro');
            //LOCATION
            $table->string('address_bg');
            $table->string('address_en');
            $table->string('address_ru');
            $table->string('address_ro');
            $table->string('phone');
            $table->string('mobile_phone');
            $table->string('reception_phone');
            $table->string('email');
            $table->string('coordinates');
//            GUEST BOOK
            $table->string('guest_book_color',12);
            $table->string('guest_book_background',255);


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('complexes');
    }
}
