<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddComplexField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('complexes', function (Blueprint $table) {
            $table->string('location_information_bg')->after('mini_information_ro');
            $table->string('location_information_en')->after('location_information_bg');
            $table->string('location_information_ru')->after('location_information_en');
            $table->string('location_information_ro')->after('location_information_ru');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
