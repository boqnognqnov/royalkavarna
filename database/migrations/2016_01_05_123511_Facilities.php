<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Facilities extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facilities', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('complex_id', false, true);
            $table->string('image');
            $table->string('logo');
            $table->string('title_bg');
            $table->string('title_en');
            $table->string('title_ru');
            $table->string('title_ro');
            $table->text('text_bg');
            $table->text('text_en');
            $table->text('text_ru');
            $table->text('text_ro');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('facilities');
    }
}
