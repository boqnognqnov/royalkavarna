<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditTwoTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('complexes', function (Blueprint $table) {
            $table->boolean('is_percentage')->after('logo');
            $table->decimal('extra_bed_price')->after('is_percentage');
        });

        Schema::table('complex_room_types', function (Blueprint $table) {
            $table->integer('beds')->after('count_rooms');
            $table->integer('extra_beds')->after('beds');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
