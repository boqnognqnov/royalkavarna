<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RoomsExtras extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('room_extras', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('room_id', false, true);
            $table->foreign('room_id')->references('id')->on('complex_room_types');
            $table->integer('extra_id', false, true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('room_extras');
    }
}
