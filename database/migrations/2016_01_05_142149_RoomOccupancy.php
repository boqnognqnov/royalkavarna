<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RoomOccupancy extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('room_occupancy', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('room_id', false, true);
            $table->foreign('room_id')->references('id')->on('complex_room_types');
            $table->timestamp('date_from');
            $table->timestamp('date_to');
            $table->integer('count_occupancy_rooms');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('room_occupancy');
    }
}
