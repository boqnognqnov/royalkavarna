<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ComplexGalery extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('complex_gallery', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('complex_id', false, true);
            $table->foreign('complex_id')->references('id')->on('complexes');
            $table->string('image');
            $table->integer('position');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('complex_gallery');
    }
}
