<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AboutInformation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('about', function (Blueprint $table) {
            $table->increments('id');
            $table->string('about_image');
            $table->string('about_logo');
            $table->string('about_slide_image');
            $table->text('about_text_bg');
            $table->text('about_text_en');
            $table->text('about_text_ru');
            $table->text('about_text_ro');
            $table->string('contact_address_bg');
            $table->string('contact_address_en');
            $table->string('contact_address_ru');
            $table->string('contact_address_ro');
            $table->string('contact_telephone');
            $table->string('contact_mobile');
            $table->string('contact_emails');
            $table->string('contact_general_email');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
