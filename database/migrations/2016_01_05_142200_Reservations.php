<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Reservations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('complex_id', false, true);
            $table->foreign('complex_id')->references('id')->on('complexes');
            $table->string('name');
            $table->string('email');
            $table->string('phone');
            $table->timestamp('date_from');
            $table->timestamp('date_to');
            $table->integer('in_early_booking_range', false, true);
            $table->foreign('in_early_booking_range')->references('id')->on('early_booking');
            $table->decimal('price');
            $table->integer('status_id', false, true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reservations');
    }
}
