<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ReservedRooms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reserved_rooms', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('reservation_id', false, true);
            $table->foreign('reservation_id')->references('id')->on('reservations');
            $table->integer('room_id', false, true);
            $table->foreign('room_id')->references('id')->on('complex_room_types');
            $table->integer('adults');
            $table->integer('children');
            $table->integer('extra_bets');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reserved_rooms');
    }
}
