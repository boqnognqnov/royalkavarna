$(document).ready(function () {
    // clear the input fields on page load
    //$('#datepicker-from').val('');
    //$('#datepicker-to').val('');

    // datepicker options
    $('.datepicker').dateRangePicker(
        {
            inline: true,
            container: '#datepicker-target',
            alwaysOpen: true,
            startDate: moment($('input[name="availableFrom"]').val(), 'YYYY-MM-DD HH:mm:ss').add(1, 'days'),
            selectForward: true,
            startOfWeek: 'monday',
            showTopbar: false,
            stickyMonths: true,
            hoveringTooltip: function (days, startTime, hoveringTime) {
                return days >= 2 ? days - 1 + ' overnights' : '';
            },
            beforeShowDay: function (t) {
                var from = $('input[name="availableFrom"]').val();
                var to = $('input[name="availableTo"]').val();
                var fromMoment = moment(from, 'YYYY-MM-DD HH:mm:ss');
                var toMoment = moment(to, 'YYYY-MM-DD HH:mm:ss');
                var today = moment(t);
                var valid = today.isBetween(fromMoment, toMoment);  //today is within range
                var _class = '';
                var _tooltip = valid ? '' : $('input[name="notavailable"]').val();
                return [valid, _class, _tooltip];
            }
        }
        )
        .bind('datepicker-first-date-selected', function (event, obj) {
                /* This event will be triggered when first date is selected */
                $('#datepicker-from').val(moment(obj.date1).format('D-MM-YYYY'));
            }
        )
        .bind('datepicker-change', function (event, obj) {
            /* This event will be triggered when second date is selected */
            $('#datepicker-to').val(moment(obj.date2).format('D-MM-YYYY'));
        });

    // get the 'from' and 'to' input values and set the range in the datepicker
    $('#datepicker-to').blur(function () { // when 'to' input loses focus
        // take raw input values
        var fromField = $('#datepicker-from').val();
        var toField = $('#datepicker-to').val();
        // calculate timestamps for the datepicker function from inputs
        var fromDate = moment(fromField, "D-MM-YYYY").format('YYYY-MM-DD');
        var toDate = moment(toField, "D-MM-YYYY").format('YYYY-MM-DD');
        // if both inputs have data
        if (fromField != '') {
            //console.log('From ' + fromDate + ' to ' + toDate);
            // update the datepicker with input values
            $('.datepicker').data('dateRangePicker').setDateRange(fromDate, toDate);
        }
    });
});

//  DATE PICKER SET DATE
$(document).ready(function () {
    if ($('input[name="old_date_from"]').val() != '' && $('input[name="old_date_to"]').val() != '') {
//  01/27/2016(MM/DD/YYYY) TO D-MM-YYYY

        var datePickerDateFrom = moment($('input[name="old_date_from"]').val(), "MM/DD/YYYY").format('YYYY-MM-D');
        var datePickerDateTo = moment($('input[name="old_date_to"]').val(), "MM/DD/YYYY").format('YYYY-MM-D');

        $('.datepicker').data('dateRangePicker').setDateRange(datePickerDateFrom, datePickerDateTo);


        var dateInputFrom = moment($('input[name="old_date_from"]').val(), "MM/DD/YYYY").format('D-MM-YYYY');
        var dateInputTo = moment($('input[name="old_date_to"]').val(), "MM/DD/YYYY").format('D-MM-YYYY');

        $('#datepicker-from').val(dateInputFrom);
        $('#datepicker-to').val(dateInputTo);
    }
});

// FORM SUBMIT

$('.submitDateLink').on('click', function (event) {
    // take raw input values
    var fromField = $('#datepicker-from').val();
    var toField = $('#datepicker-to').val();
    var complex_id = $('input[name="complex_id"]').val();
    // calculate timestamps for the datepicker function from inputs
    var date_from = moment(fromField, "D-MM-YYYY").format('MM-DD-YYYY');
    var date_to = moment(toField, "D-MM-YYYY").format('MM-DD-YYYY');

    // var selectedRoom = $('input[name="selected_room"]').val();
    //
    // if (selectedRoom != '') {
    //     console.log("/reservation/date/0/" + complex_id + "/" + date_from + "/" + date_to + '#selected_room_type' + selectedRoom);
    //     window.location.href = "/reservation/date/0/" + complex_id + "/" + date_from + "/" + date_to + '#selected_room_type' + selectedRoom;
    // } else {
    window.location.href = "/reservation/date/0/" + complex_id + "/" + date_from + "/" + date_to;
    // }
    event.preventDefault();
});

// JUMP TO DATEPICKERS ON INPUT FOCUS

$('input').on('click', function () {
    var jump = $(this).data('jumpto');
    var new_position = $('#' + jump).offset();
    window.scrollTo(new_position.left, new_position.top);
    return false;
});
