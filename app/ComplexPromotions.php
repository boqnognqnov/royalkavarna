<?php

namespace App;

use App\Classes\GlobalFunctions;
use Illuminate\Database\Eloquent\Model;
/*
*    $table->integer('complex_id', false, true);
    $table->string('title');
    $table->string('image');
    $table->text('description');
    $table->string('mini_title');
    $table->text('mini_description');
    $table->string('href');
    $table->string('href_text');
    $table->timestamp('date_from');
    $table->timestamp('date_to');*/
class ComplexPromotions extends Model
{
    protected $table = "complex_promotions";
    public static $imagePath = 'uploads/promotions/';

    public function getComplex() {
        return $this -> $this->belongsTo( 'App\Complex' );
    }

    public static function getRules() {
        return array(
            'complex_id'            => 'required|exists:complexes,id',
            'title_bg'              => 'required',
            'title_en'              => 'required',
            'title_ru'              => 'required',
            'title_ro'              => 'required',
            'image'                 => 'required|image',
            'description_bg'        => 'required',
            'description_en'        => 'required',
            'description_ru'        => 'required',
            'description_ro'        => 'required',
            'mini_title_bg'         => '',
            'mini_title_en'         => '',
            'mini_title_ru'         => '',
            'mini_title_ro'         => '',
            'date_from'             => 'required',
            'date_to'               => 'required',
        );
    }



    public static function getMessages() {
        return [
            'complex_id.required'           => 'line 66',
            'complex_id.exists'             => 'line 67',
            'title_bg.required'             => 'Изисква се заглавие на Български',
            'title_en.required'             => 'Изисква се заглавие на Английски',
            'title_ru.required'             => 'Изисква се заглавие на Руски',
            'title_ro.required'             => 'Изисква се заглавие на Румънски',

            'image.required'                => 'Изисква се снимка',
            'image.image'                   => 'Файлът трябва да е изображение',

            'description_bg.required'       => 'Изисква се описание на Български',
            'description_en.required'       => 'Изисква се описание на Английски',
            'description_ru.required'       => 'Изисква се описание на Руски',
            'description_ro.required'       => 'Изисква се описание на Румънски',

            'mini_title_bg.alpha'           => 'Изисква се кратко заглавие на Български',
            'mini_title_en.alpha'           => 'Изисква се кратко заглавие на Английски',
            'mini_title_ru.alpha'           => 'Изисква се кратко заглавие на Руски',
            'mini_title_ro.alpha'           => 'Изисква се кратко заглавие на Румънски',

            'mini_description_bg.required'  => 'Изисква се кратко описание на Български',
            'mini_description_en.required'  => 'Изисква се кратко описание на Английски',
            'mini_description_ru.required'  => 'Изисква се кратко описание на Руски',
            'mini_description_ro.required'  => 'Изисква се кратко описание на Румънски',

            'href.required'                 => 'Изисква се линк',

            'href_text_bg.required'         => 'Изисква се линк на Български',
            'href_text_en.required'         => 'Изисква се линк на Английски',
            'href_text_ru.required'         => 'Изисква се линк на Руски',
            'href_text_ro.required'         => 'Изисква се линк на Румънски',

            'date_from.required'            => 'Изисква се дата от:',
            'date_to.required'              => 'Изисква се дата до:',
        ];
    }



    public static function createPromotion( $data ) {

        $validator = \Validator::make( $data, self::getRules(), self::getMessages() );

        if ( $validator -> fails() )
            return array( 'validatorError', $validator );

        $imageName = time();

        $promotionImage = $imageName . '.' . $data['image'] -> getClientOriginalExtension();

        $imagePath = public_path( self::$imagePath . $promotionImage );
        \Image::make( $data['image'] -> getRealPath() ) -> save( $imagePath );

        try {
            $promotion = new ComplexPromotions();
            $promotion -> complex_id = $data['complex_id'];
            $promotion -> title_bg = $data['title_bg'];
            $promotion -> title_en = $data['title_en'];
            $promotion -> title_ru = $data['title_ru'];
            $promotion -> title_ro = $data['title_ro'];
            $promotion -> image = $promotionImage;
            $promotion -> description_bg = $data['description_bg'];
            $promotion -> description_en = $data['description_en'];
            $promotion -> description_ru = $data['description_ru'];
            $promotion -> description_ro = $data['description_ro'];
            if(isset($data['mini_title_bg'])) $promotion -> mini_title_bg = $data['mini_title_bg'];
            if(isset($data['mini_title_en'])) $promotion -> mini_title_en = $data['mini_title_en'];
            if(isset($data['mini_title_ru'])) $promotion -> mini_title_ru = $data['mini_title_ru'];
            if(isset($data['mini_title_ro'])) $promotion -> mini_title_ro = $data['mini_title_ro'];
            if(isset($data['mini_description_bg'])) $promotion -> mini_description_bg = $data['mini_description_bg'];
            if(isset($data['mini_description_en'])) $promotion -> mini_description_en = $data['mini_description_en'];
            if(isset($data['mini_description_ru'])) $promotion -> mini_description_ru = $data['mini_description_ru'];
            if(isset($data['mini_description_ro'])) $promotion -> mini_description_ro = $data['mini_description_ro'];
            if(isset($data['href'])) $promotion -> href = $data['href'];
            if(isset($data['href_text_bg'])) $promotion -> href_text_bg = $data['href_text_bg'];
            if(isset($data['href_text_en'])) $promotion -> href_text_en = $data['href_text_en'];
            if(isset($data['href_text_ru'])) $promotion -> href_text_ru = $data['href_text_ru'];
            if(isset($data['href_text_ro'])) $promotion -> href_text_ro = $data['href_text_ro'];

            $gb=new GlobalFunctions();
            $dateFrom=$gb->changeDateFormat($data['date_from']);
            $dateTo=$gb->changeDateFormat($data['date_to']);
            $promotion -> date_from = $dateFrom;
            $promotion -> date_to = $dateTo;

            $promotion -> save();
        } catch ( \Exception $ex ) {
            return array( 'databaseError', $ex );
        }

        return array( 'successMessage', $promotion );

    }

    public static function updatePromotion( $data ) {

        $messages= self::getMessages();
        $messages['promotion_id.required'] = 'Изисква се ид на промоцията';
        $messages['promotion_id.exists'] = 'Промоцията не съществува';

        $rules=self::getRules();
        $rules['promotion_id'] = 'required|exists:complex_promotions,id';
        $rules['image'] = 'image';

        $validator = \Validator::make( $data, $rules,$messages );

        if ( $validator -> fails() ) {
            return array( 'validatorError', $validator );
        }
        if(isset($data['image'])){
            $imageName = time();
            $promotionImage = $imageName . '.' . $data['image'] -> getClientOriginalExtension();
            $imagePath = public_path( self::$imagePath . $promotionImage );
            \Image::make( $data['image'] -> getRealPath() ) -> save( $imagePath );
        }

        try {
            $promotion = ComplexPromotions::find($data['promotion_id']);
            $promotion -> complex_id = $data['complex_id'];
            $promotion -> title_bg = $data['title_bg'];
            $promotion -> title_en = $data['title_en'];
            $promotion -> title_ru = $data['title_ru'];
            $promotion -> title_ro = $data['title_ro'];
            if(isset($data['image']))$promotion -> image = $promotionImage;
            $promotion -> description_bg = $data['description_bg'];
            $promotion -> description_en = $data['description_en'];
            $promotion -> description_ru = $data['description_ru'];
            $promotion -> description_ro = $data['description_ro'];
            if(isset($data['mini_title_bg'])) $promotion -> mini_title_bg = $data['mini_title_bg'];
            if(isset($data['mini_title_en'])) $promotion -> mini_title_en = $data['mini_title_en'];
            if(isset($data['mini_title_ru'])) $promotion -> mini_title_ru = $data['mini_title_ru'];
            if(isset($data['mini_title_ro'])) $promotion -> mini_title_ro = $data['mini_title_ro'];
            if(isset($data['mini_description_bg'])) $promotion -> mini_description_bg = $data['mini_description_bg'];
            if(isset($data['mini_description_en'])) $promotion -> mini_description_en = $data['mini_description_en'];
            if(isset($data['mini_description_ru'])) $promotion -> mini_description_ru = $data['mini_description_ru'];
            if(isset($data['mini_description_ro'])) $promotion -> mini_description_ro = $data['mini_description_ro'];
            if(isset($data['href'])) $promotion -> href = $data['href'];
            if(isset($data['href_text_bg'])) $promotion -> href_text_bg = $data['href_text_bg'];
            if(isset($data['href_text_en'])) $promotion -> href_text_en = $data['href_text_en'];
            if(isset($data['href_text_ru'])) $promotion -> href_text_ru = $data['href_text_ru'];
            if(isset($data['href_text_ro'])) $promotion -> href_text_ro = $data['href_text_ro'];

            $gb=new GlobalFunctions();
            $dateFrom=$gb->changeDateFormat($data['date_from']);
            $dateTo=$gb->changeDateFormat($data['date_to']);
            $promotion -> date_from = $dateFrom;
            $promotion -> date_to = $dateTo;

            $promotion -> save();
        } catch ( \Exception $ex ) {
            return array( 'databaseError', $ex );
        }

        return array( 'successMessage', $promotion );

    }

    public static function deletePromotion( $id ) {

        try {
            ComplexPromotions::destroy( $id );
        } catch ( \Exception $ex ) {
            return 'databaseError';
        }

        return 'successMessage';
    }
}
