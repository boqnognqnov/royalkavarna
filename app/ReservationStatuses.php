<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ReservationStatuses extends Model
{

    protected $table = 'reservation_statuses';

    public static function getCreateRules()
    {
        return array(
            'status' => 'required'
        );
    }

    public static function getUpdateRules()
    {
        return array(
            'id' => 'required|exists:reservation_statuses,id',
            'status' => 'required'
        );
    }

    public static function getCreateMessages()
    {
        return [
            'status.required' => 'Моля въведете името на статуса.'
        ];
    }

    public static function getUpdateMessages()
    {
        return [
            'id.required' => 'Моля статус който да редактирате.',
            'id.exists' => 'Статусът който се опитвате да редактирате не съществува.',
            'status.required' => 'Моля въведете името на статуса.'
        ];
    }


    public static function createStatus($data)
    {

        $validator = \Validator::make($data, self::getCreateRules(), self::getCreateMessages());

        if ($validator->fails()) {
            return array('validatorError', $validator);
        }

        try {
            $statusEntity = new ReservationStatuses();
            $statusEntity->status = $data['status'];
            $statusEntity->save();
        } catch (\Exception $ex) {
            return array('databaseError', $ex);
        }

        return array('successMessage', $statusEntity);

    }

    public static function updateStatus($data)
    {

        $validator = \Validator::make($data, self::getUpdateRules(), self::getUpdateMessages());

        if ($validator->fails()) {
            return array('validatorError', $validator);
        }

        try {
            $statusEntity = ReservationStatuses::where('id', '=', $data['id'])->first();
            $statusEntity->status = $data['status'];
            $statusEntity->save();
        } catch (\Exception $ex) {
            return array('databaseError', $ex);
        }

        return array('successMessage', $statusEntity);

    }

    public static function deleteStatus($id)
    {

        try {
            ReservationStatuses::destroy($id);
        } catch (\Exception $ex) {
            return 'databaseError';
        }

        return 'successMessage';

    }


}
