<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ComplexRooms extends Model
{
    protected $table = 'complex_room_types';

    public static $imagePath = 'uploads/rooms/mainImages/';

    public function getComplex()
    {
        return $this->$this->belongsTo('App\Complex');
    }

    public static function getCreateRules()
    {
        return array(
            'complex_id' => 'required|exists:complexes,id',
            'main_image' => 'required|image',
            'count_rooms' => 'required',
            'beds' => 'required|min:1',
            'extra_beds' => 'required',
            'type_bg' => 'required',
            'type_en' => 'required',
            'type_ru' => 'required',
            'type_ro' => 'required',
            'description_bg' => 'required',
            'description_en' => 'required',
            'description_ru' => 'required',
            'description_ro' => 'required'
        );
    }

    public static function getUpdateRules()
    {
        $rules = self::getCreateRules();
        $rules['room_id'] = 'required|exists:complex_room_types,id';
        $rules['main_image'] = 'image';

        return $rules;
    }

    public static function getCreateMessages()
    {
        return [
            'complex_id.required' => 'line 39',
            'complex_id.exists' => 'line 40',
            'main_image.required' => 'line 41',
            'main_image.image' => 'line 42',
            'count_rooms.required' => 'line 43',
            'type_bg.required' => 'line 44',
            'type_en.required' => 'line 45',
            'type_ru.required' => 'line 46',
            'type_ro.required' => 'line 47',
            'description_bg.required' => 'line 48',
            'description_en.required' => 'line 49',
            'description_ru.required' => 'line 50',
            'description_ro.required' => 'line 51',
            'beds.required' => 'line 52',
            'beds.min' => 'line 53',
            'extra_beds.required' => 'line 54',
        ];
    }

    public static function getUpdateMessages()
    {
        $messages = self::getCreateMessages();
        $messages['room_id.required'] = 'line 57';
        $messages['room_id.exists'] = 'line 58';

        return $messages;
    }

    public static function createRoom($data)
    {

        $validator = \Validator::make($data, self::getCreateRules(), self::getCreateMessages());

        if ($validator->fails())
            return array('validatorError', $validator);

        $complex = Complex::find($data['complex_id']);

        if ($complex->is_inseparable == true) {
            $complexRooms = ComplexRooms::where('complex_id', $data['complex_id'])->get();
            if (sizeof($complexRooms) > 0)
                return array('RoomAlreadyExist');

        }

        $imageName = time();

        $roomImage = $imageName . '.' . $data['main_image']->getClientOriginalExtension();

        $imagePath = public_path(self::$imagePath . $roomImage);
//        \Image::make( $data['main_image'] -> getRealPath() ) -> save( $imagePath );
        \Image::make($data['main_image']->getRealPath())->resize(555, 400)->save($imagePath);

        try {
            $room = new ComplexRooms();
            $room->complex_id = $data['complex_id'];
            $room->main_image = $roomImage;
            $room->count_rooms = $data['count_rooms'];
            $room->beds = $data['beds'];
            $room->extra_beds = $data['extra_beds'];

            $room->type_bg = $data['type_bg'];
            $room->type_en = $data['type_en'];
            $room->type_ru = $data['type_ru'];
            $room->type_ro = $data['type_ro'];

            $room->type_sub_bg = $data['type_sub_bg'];
            $room->type_sub_en = $data['type_sub_en'];
            $room->type_sub_ru = $data['type_sub_ru'];
            $room->type_sub_ro = $data['type_sub_ro'];

            $room->description_bg = $data['description_bg'];
            $room->description_en = $data['description_en'];
            $room->description_ru = $data['description_ru'];
            $room->description_ro = $data['description_ro'];
            if (isset($data['other_info_bg'])) $room->other_info_bg = $data['other_info_bg'];
            if (isset($data['other_info_en'])) $room->other_info_en = $data['other_info_en'];
            if (isset($data['other_info_ru'])) $room->other_info_ru = $data['other_info_ru'];
            if (isset($data['other_info_ro'])) $room->other_info_ro = $data['other_info_ro'];
            if (isset($data['price_info_bg'])) $room->price_info_bg = $data['price_info_bg'];
            if (isset($data['price_info_en'])) $room->price_info_en = $data['price_info_en'];
            if (isset($data['price_info_ru'])) $room->price_info_ru = $data['price_info_ru'];
            if (isset($data['price_info_ro'])) $room->price_info_ro = $data['price_info_ro'];
            $room->save();
        } catch (\Exception $ex) {
            return array('databaseError', $ex);
        }

        return array('successMessage', $room);

    }

    public static function updateRoom($data)
    {

        $validator = \Validator::make($data, self::getUpdateRules(), self::getUpdateMessages());

        if ($validator->fails()) {
            return array('validatorError', $validator);
        }

        if (isset($data['main_image'])) {
            $imageName = time();
            $roomImage = $imageName . '.' . $data['main_image']->getClientOriginalExtension();
            $imagePath = public_path(self::$imagePath . $roomImage);
//            \Image::make( $data['main_image'] -> getRealPath() ) -> save( $imagePath );
            \Image::make($data['main_image']->getRealPath())->resize(555, 400)->save($imagePath);
        }

        try {
            $room = ComplexRooms::find($data['room_id']);
            $room->complex_id = $data['complex_id'];
            if (isset($data['main_image'])) $room->main_image = $roomImage;
            $room->count_rooms = $data['count_rooms'];
            $room->beds = $data['beds'];
            $room->extra_beds = $data['extra_beds'];
            $room->type_bg = $data['type_bg'];
            $room->type_en = $data['type_en'];
            $room->type_ru = $data['type_ru'];
            $room->type_ro = $data['type_ro'];

            $room->type_sub_bg = $data['type_sub_bg'];
            $room->type_sub_en = $data['type_sub_en'];
            $room->type_sub_ru = $data['type_sub_ru'];
            $room->type_sub_ro = $data['type_sub_ro'];

            $room->description_bg = $data['description_bg'];
            $room->description_en = $data['description_en'];
            $room->description_ru = $data['description_ru'];
            $room->description_ro = $data['description_ro'];
            if (isset($data['other_info_bg'])) $room->other_info_bg = $data['other_info_bg'];
            if (isset($data['other_info_en'])) $room->other_info_en = $data['other_info_en'];
            if (isset($data['other_info_ru'])) $room->other_info_ru = $data['other_info_ru'];
            if (isset($data['other_info_ro'])) $room->other_info_ro = $data['other_info_ro'];
            if (isset($data['price_info_bg'])) $room->price_info_bg = $data['price_info_bg'];
            if (isset($data['price_info_en'])) $room->price_info_en = $data['price_info_en'];
            if (isset($data['price_info_ru'])) $room->price_info_ru = $data['price_info_ru'];
            if (isset($data['price_info_ro'])) $room->price_info_ro = $data['price_info_ro'];
            $room->save();
        } catch (\Exception $ex) {
            return array('databaseError', $ex);
        }

        return array('successMessage', $room);

    }

    public static function deleteRoom($id)
    {

        try {
            ComplexRooms::destroy($id);
        } catch (\Exception $ex) {
            return 'databaseError';
        }

        return 'successMessage';

    }
}
