<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class About extends Model
{
    protected $table = 'about';

    public static $imagePath = 'uploads/images/about/image/';

    public static $sliderImagePath = 'uploads/images/about/sliderImage/';

    public static $logoPath = 'uploads/images/about/logo/';


    public static function getUpdateRules()
    {
        return array(
            'about_image' => 'image',
            'about_logo' => 'image',
            'about_slide_image' => 'image',
            'about_text_bg' => 'required',
            'about_text_en' => 'required',
            'about_text_ru' => 'required',
            'about_text_ro' => 'required',
            'contact_address_bg' => 'required',
            'contact_address_en' => 'required',
            'contact_address_ru' => 'required',
            'contact_address_ro' => 'required',
            'contact_telephone' => 'required',
            'contact_mobile' => 'required',
//            'contact_emails' => 'required',
            'contact_general_email' => 'required|email',
        );
    }


    public static function getUpdateMessages()
    {
        return [
            'about_image.required' => '',
            'about_logo.required' => '',
            'about_slide_image.required' => '',
            'about_image.image' => '',
            'about_logo.image' => '',
            'about_slide_image.image' => '',
            'about_text_bg.required' => '',
            'about_text_en.required' => '',
            'about_text_ru.required' => '',
            'about_text_ro.required' => '',
            'contact_address_bg.required' => '',
            'contact_address_en.required' => '',
            'contact_address_ru.required' => '',
            'contact_address_ro.required' => '',
            'contact_telephone.required' => '',
            'contact_mobile.required' => '',
            'contact_emails.required' => '',
            'contact_general_email.required' => '',
        ];
    }


    public static function updateAboutInformation($data)
    {

        $validator = \Validator::make($data, self::getUpdateRules(), self::getUpdateMessages());

        if ($validator->fails()) {
            return array('validatorError', $validator);
        }

        if (isset($data['about_slide_image'])) {
            $imageName = time();

            $about_slide_image = $imageName . '.' . $data['about_slide_image']->getClientOriginalExtension();

            $aboutSliderImagePath = public_path(self::$sliderImagePath . $about_slide_image);
            \Image::make($data['about_slide_image']->getRealPath())->save($aboutSliderImagePath);
        }

        if (isset($data['about_image'])) {
            $imageName = time();

            $aboutImage = $imageName . '.' . $data['about_image']->getClientOriginalExtension();

            $aboutImagePath = public_path(self::$imagePath . $aboutImage);
            \Image::make($data['about_image']->getRealPath())->save($aboutImagePath);
        }

        if (isset($data['about_logo'])) {
            $imageName = time();

            $aboutLogo = $imageName . '.' . $data['about_logo']->getClientOriginalExtension();

            $aboutLogoPath = public_path(self::$logoPath . $aboutLogo);
            \Image::make($data['about_logo']->getRealPath())->save($aboutLogoPath);
        }

        try {
            $about = About::first();
            if (isset($data['about_image'])) $about->about_image = $aboutImage;
            if (isset($data['about_logo'])) $about->about_logo = $aboutLogo;
            if (isset($data['about_slide_image'])) $about->about_slide_image = $about_slide_image;
            $about->about_text_bg = $data['about_text_bg'];
            $about->about_text_en = $data['about_text_en'];
            $about->about_text_ru = $data['about_text_ru'];
            $about->about_text_ro = $data['about_text_ro'];
            $about->contact_address_bg = $data['contact_address_bg'];
            $about->contact_address_en = $data['contact_address_en'];
            $about->contact_address_ru = $data['contact_address_ru'];
            $about->contact_address_ro = $data['contact_address_ro'];
            $about->contact_telephone = $data['contact_telephone'];
            $about->contact_mobile = $data['contact_mobile'];
//            $about->contact_emails = $data['contact_emails'];
            $about->contact_general_email = $data['contact_general_email'];
            $about->save();
        } catch (\Exception $ex) {
            return array('databaseError', $ex);
        }

        return array('successMessage', $about);

    }
}
