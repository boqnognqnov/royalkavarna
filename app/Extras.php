<?php

namespace App;

use App\Classes\ImageProccessing;
use Illuminate\Database\Eloquent\Model;
use Intervention\Image\ImageManagerStatic as Image;

/*
 * Schema::create('extras', function (Blueprint $table) {
        $table->increments('id');
        $table->string('icon');
        $table->string('name_bg');
        $table->string('name_en');
        $table->string('name_ru');
        $table->string('name_ro');
        $table->timestamps();
    });*/

class Extras extends Model
{
    protected $table = 'extras';


    public static $path = 'uploads/images/icons/';


    public static function addExtraRules()
    {
        return array(
            'icon' => 'required|image',
            'name_bg' => 'required',
            'name_en' => 'required',
            'name_ru' => 'required',
            'name_ro' => 'required',
        );
    }

    public static function addExtraMessages()
    {
        return [
            'name_bg.required' => 'Не е избрано описание на BG',
            'name_en.required' => 'Не е избрано описание на EN',
            'name_ru.required' => 'Не е избрано описание на RU',
            'name_ro.required' => 'Не е избрано описание на RO',

            'icon.required' => 'Моля прикачете изображение',
            'icon.image' => 'Файлът трябва да е изображение',

        ];
    }


    public static function addExtra($data)
    {

        $validator = \Validator::make($data, self::addExtraRules(), self::addExtraMessages());

        if ($validator->fails()) {
            return array('validationError', $validator);
        }


        try {

            $extra = new Extras();

            $fileName = time() . '.' . $data['icon']->getClientOriginalExtension();
            Image::make($data['icon']->getRealPath())->save(self::$path . $fileName);


//            Image::make($file->getRealPath())->resize($widthSize, $heightSize)->save($pathSmall . $fileName);


            $extra->icon = $fileName;
            if (isset($data['name_bg'])) {
                $extra->name_bg = $data['name_bg'];
            }
            if (isset($data['name_en'])) {
                $extra->name_en = $data['name_en'];
            }
            if (isset($data['name_ru'])) {
                $extra->name_ru = $data['name_ru'];
            }
            if (isset($data['name_ro'])) {
                $extra->name_ro = $data['name_ro'];
            }

            $extra->save();

        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('creatingError', $ex);
        }

        return array('successMessage', $extra);

    }


    public static function updateExtra($data)
    {

        $extra = Extras::find($data['extra_id']);
        ImageProccessing::destroyImage($data['extra_id'], Extras::class);


        try {

            if (isset($data['icon'])) {
                $fileName = time() . '.' . $data['icon']->getClientOriginalExtension();
                Image::make($data['icon']->getRealPath())->save(self::$path . $fileName);
//            Image::make($file->getRealPath())->resize($widthSize, $heightSize)->save($pathSmall . $fileName);
                $extra->icon = $fileName;
            }

            if (isset($data['name_bg'])) {
                $extra->name_bg = $data['name_bg'];
            }
            if (isset($data['name_en'])) {
                $extra->name_en = $data['name_en'];
            }
            if (isset($data['name_ru'])) {
                $extra->name_ru = $data['name_ru'];
            }
            if (isset($data['name_ro'])) {
                $extra->name_ro = $data['name_ro'];
            }

            $extra->save();

        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('creatingError', $ex);
        }

        return array('successMessage', $extra);

    }

    public static function destroyExtras($extra_id)
    {
        try {
            $extra = Extras::find($extra_id);

            $allRoomWithSelectedExtra = RoomExtras::where('extra_id', '=', $extra_id)->get();

            foreach ($allRoomWithSelectedExtra as $oneRoom) {
                $oneRoom->delete();
            }

            ImageProccessing::destroyImage($extra_id, Extras::class);

            $extra->delete();
        } catch (\Exception $ex) {
            \Log::error($ex);
            return false;
        }

        return true;


    }


}
