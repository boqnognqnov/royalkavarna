<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\ImageManagerStatic as Image;

class Facilities extends Model
{

    protected $table = 'facilities';

    public static $imagePath = 'uploads/images/facilities/image/';

    public static $logoPath = 'uploads/images/facilities/logo/';

    public static function getCreateRules()
    {
        return array(
            'complex_id' => 'required|exists:complexes,id',
            'image' => 'required|image',
            'logo' => 'required|image',
            'title_bg' => 'required',
            'title_en' => 'required',
            'title_ru' => 'required',
            'title_ro' => 'required',
            'text_bg' => 'required',
            'text_en' => 'required',
            'text_ru' => 'required',
            'text_ro' => 'required',
        );
    }

    public static function getUpdateRules()
    {
        return array(
            'complex_id' => 'required|exists:complexes,id',
            'id' => 'required|exists:facilities,id',
            'image' => 'image',
            'logo' => 'image',
            'title_bg' => 'required',
            'title_en' => 'required',
            'title_ru' => 'required',
            'title_ro' => 'required',
            'text_bg' => 'required',
            'text_en' => 'required',
            'text_ru' => 'required',
            'text_ro' => 'required',
        );
    }

    public static function getCreateMessages()
    {
        return [
            '' => ''
        ];
    }

    public static function getUpdateMessages()
    {
        return [
            '' => ''
        ];
    }

    public static function createFacilities($data)
    {

        $validator = \Validator::make($data, self::getCreateRules(), self::getCreateMessages());

        if ($validator->fails()) {
            return array('validatorError', $validator);
        }

        $imageName = time();

        $facilitiesImage = $imageName . '.' . $data['image']->getClientOriginalExtension();
        $facilitiesLogo = $imageName . '.' . $data['logo']->getClientOriginalExtension();

        $facilitiesImagePath = public_path(self::$imagePath . $facilitiesImage);
        Image::make($data['image']->getRealPath())->resize(490, 370)->save($facilitiesImagePath);

        $facilitiesLogoPath = public_path(self::$logoPath . $facilitiesLogo);
        Image::make($data['logo']->getRealPath())->resize(40, 40)->save($facilitiesLogoPath);

        try {
            $facilitiesEntity = new Facilities();
            $facilitiesEntity->complex_id = $data['complex_id'];
            $facilitiesEntity->image = $facilitiesImage;
            $facilitiesEntity->logo = $facilitiesLogo;
            $facilitiesEntity->title_bg = $data['title_bg'];
            $facilitiesEntity->title_en = $data['title_en'];
            $facilitiesEntity->title_ru = $data['title_ru'];
            $facilitiesEntity->title_ro = $data['title_ro'];
            $facilitiesEntity->text_bg = $data['text_bg'];
            $facilitiesEntity->text_en = $data['text_en'];
            $facilitiesEntity->text_ru = $data['text_ru'];
            $facilitiesEntity->text_ro = $data['text_ro'];
            $facilitiesEntity->save();
        } catch (\Exception $ex) {
            return array('databaseError', $ex);
        }

        return array('successMessage', $facilitiesEntity);

    }

    public static function updateFacilities($data)
    {

        $validator = \Validator::make($data, self::getUpdateRules(), self::getUpdateMessages());

        if ($validator->fails()) {
            return array('validatorError', $validator);
        }

        if (isset($data['image'])) {
            $imageName = time();

            $facilitiesImage = $imageName . '.' . $data['image']->getClientOriginalExtension();

            $facilitiesImagePath = public_path(self::$imagePath . $facilitiesImage);
            Image::make($data['image']->getRealPath())->resize(490, 370)->save($facilitiesImagePath);
        }

        if (isset($data['logo'])) {
            $imageName = time();

            $facilitiesLogo = $imageName . '.' . $data['logo']->getClientOriginalExtension();

            $facilitiesLogoPath = public_path(self::$logoPath . $facilitiesLogo);
            Image::make($data['logo']->getRealPath())->resize(40, 40)->save($facilitiesLogoPath);
        }

        try {
            $facilitiesEntity = Facilities::where('id', '=', $data['id'])->first();
            if (isset($data['image']))
                $facilitiesEntity->image = $facilitiesImage;
            if (isset($data['logo']))
                $facilitiesEntity->logo = $facilitiesLogo;
            $facilitiesEntity->complex_id = $data['complex_id'];
            $facilitiesEntity->title_bg = $data['title_bg'];
            $facilitiesEntity->title_en = $data['title_en'];
            $facilitiesEntity->title_ru = $data['title_ru'];
            $facilitiesEntity->title_ro = $data['title_ro'];
            $facilitiesEntity->text_bg = $data['text_bg'];
            $facilitiesEntity->text_en = $data['text_en'];
            $facilitiesEntity->text_ru = $data['text_ru'];
            $facilitiesEntity->text_ro = $data['text_ro'];
            $facilitiesEntity->save();
        } catch (\Exception $ex) {
            return array('databaseError', $ex);
        }

        return array('successMessage', $facilitiesEntity);

    }

    public static function deleteFacilities($id)
    {

        try {
            Facilities::destroy($id);
        } catch (\Exception $ex) {
            return 'databaseError';
        }

        return 'successMessage';

    }

}
