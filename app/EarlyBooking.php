<?php

namespace App;

use App\Classes\GlobalFunctions;
use Illuminate\Database\Eloquent\Model;

/*
 * Schema::create('early_booking', function (Blueprint $table) {
    $table->increments('id');
    $table->integer('complex_id', false, true);
    $table->foreign('complex_id')->references('id')->on('complexes');
    $table->integer('percentage');
    $table->timestamp('until_date');
    $table->boolean('is_active');
    $table->timestamps();
});*/

class EarlyBooking extends Model
{
    protected $table = 'early_booking';


    public static function addExtraRules()
    {
        return array(
            'complex_id' => 'required',
            'percentage' => 'required',
            'until_date' => 'required',
//            'is_active' => 'required',

        );
    }

    public static function addExtraMessages()
    {
        return [
//            'name_bg.required' => '�� � ������� �������� �� BG',
//            'name_en.required' => '�� � ������� �������� �� EN',
//
        ];
    }


    public static function setPromotion($data)
    {

        $validator = \Validator::make($data, self::addExtraRules(), self::addExtraMessages());

        if ($validator->fails()) {
            return array('validationError', $validator);
        }


        try {
            if (!empty($data['promotion_id'])) {
                $promotion = EarlyBooking::find($data['promotion_id']);
            } else {
                $promotion = new EarlyBooking();
            }

            $promotion->complex_id = $data['complex_id'];
            $promotion->percentage = $data['percentage'];

            $gf = new GlobalFunctions();
            $promotion->until_date = $gf->changeDateFormat($data['until_date']);
            $promotion->is_active = true;
            $promotion->save();

        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('creatingError', $ex);
        }

        return array('successMessage', $promotion);

    }


    public static function destroyPromotion($promotion_id)
    {

        try {
            EarlyBooking::destroy($promotion_id);
        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('destroyError', $ex);
        }

        return array('destroySuccess', true);

    }


}
