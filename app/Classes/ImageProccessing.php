<?php

namespace App\Classes;


use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Config;
use Intervention\Image\ImageManagerStatic as Image;
use File;
use Log;

class ImageProccessing
{

    public static function destroyImage($record_id, $model)
    {
        $fileName = $model::find($record_id)->image;
        try {

            if (File::exists($model::$path . $fileName)) {
                File::delete($model::$path . $fileName);
            }

        } catch
        (\Exception $ex) {
            Log::error($ex);
            return false;
        }
        return true;
    }
}

//public static function moveDown($modelInstance, $current_id)
//{
//    $min = $modelInstance::orderBy('position', 'ASC')->first()->position;
//
//    $currentRecord = $modelInstance::find($current_id);
//    $currentPosition = $currentRecord->position;
//    if ($min < $currentPosition) {
//        $downRecord = $modelInstance::where('position', '<', $currentPosition)->orderBy('position', 'ASC')->first();
//
//        $currentRecord->position = $downRecord->position;
//        $currentRecord->save();
//
//        $downRecord->position = $currentPosition;
//        $downRecord->save();
//    }
//
//}