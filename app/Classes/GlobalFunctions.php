<?php

namespace App\Classes;

use App;
use App\Complex;
use App\ComplexRooms;
use App\EarlyBooking;
use App\Http\Controllers\Api\ReservationsApi;
use App\Reservations;
use App\ReservedRooms;
use App\RoomOccupancy;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;
use Intervention\Image\ImageManagerStatic as Image;
use File;
use Log;

class GlobalFunctions
{

    public function changeDateFormat($date)
    {
        $dateArr = explode('/', $date);
        $dateStr = $dateArr[2] . '-' . $dateArr[0] . '-' . $dateArr[1];
        return new \DateTime($dateStr);
    }

    public static function generateDateTimeToStr($date)
    {
//    EXAMPLE:    2016-01-20 18:28:03 to 01/20/2016

        $dateArr = explode('-', $date);
        $day = explode(' ', $dateArr[2]);
        $day = $day[0];

        $date = $dateArr[1] . '/' . $day . '/' . $dateArr[0];

        return $date;
    }


    public static function getFreeRooms($roomsTypes, $date_from, $date_to)
    {

        $period = new \DatePeriod(
            new \DateTime($date_from),
            new \DateInterval('P1D'),
            new \DateTime($date_to)
        );

        $availableRoomsArr = array();
        $tempArr = array();


        foreach ($roomsTypes as $oneRoom) {

            $count_rooms = ComplexRooms::find($oneRoom->id)->count_rooms;

            foreach ($period as $date) {
                $totalReservedRooms = RoomOccupancy::countReservedRoomsForDate($oneRoom->id, $date->format("Y-m-d"));

                array_push($tempArr, $totalReservedRooms);
            }

            sort($tempArr);
            $availableRoomsArr[$oneRoom->id] = $count_rooms - end($tempArr);
            $tempArr = array();

        }
        return $availableRoomsArr;
    }

    public static function redirectBysession($session, $step)
    {
        $redirectlink = '';

        if ($step == 'complex') {
            if (!isset($session['complex_id'])) {
                $redirectlink = 'reservation/all_complexes';
            }
        }

        if ($step == 'date') {
            if (!isset($session['date_from'])) {
                $redirectlink = 'reservation/complex/' . $session['complex_id'];
            }
        }

        if ($step == 'personalFormular') {
            if (!isset($session['rooms_data'])) {
                $redirectlink = 'reservation/date/' . $session['complex_id'] . '/' . $session['date_from'] . '/' . $session['date_to'];
            }
        }


        return $redirectlink;
    }

    public function calculateReservationPriceByRow($data, $isAdmin = 0, $adminDataSes = null)
    {
        if ($isAdmin == 0) {
            $reservationSession = \Session::get('new_reservation');
        } else {
            $reservationSession = $adminDataSes;
        }

        $complex = Complex::find($reservationSession['complex_id']);
        $dataToReturn = array();
        $dataToReturn['complex_id'] = $complex->id;

        $resApi = New ReservationsApi();

        $requestData = $resApi->checkForEmptyRows($data);
        $priceData = $resApi->getDataFromReservationForm($data, $reservationSession);


        foreach ($requestData as $roomId => $oneRoomData) {

            $locale = 'type_' . \App::getLocale();
            $roomName = ComplexRooms::find($roomId)->$locale;


            foreach ($oneRoomData as $rowId => $rowData) {
                $dataToReturn['data'][$roomId][$rowId]['adult'] = $rowData['adult'];
                $dataToReturn['data'][$roomId][$rowId]['child'] = $rowData['child'];

                $price = $priceData[$roomId][$rowId]['real_price'];
                if ($price === 'tooMany') {
                    $price = trans('reservationValidation.tooManyPeople');
                }
                $dataToReturn['data'][$roomId][$rowId]['real_price'] = $price;


                $dataToReturn['data'][$roomId][$rowId]['early_booking'] = $priceData[$roomId][$rowId]['early_booking'];

                $dataToReturn['data'][$roomId][$rowId]['extra'] = self::getExtraBeds($roomId, $rowData['child'], $rowData['adult']);
                $dataToReturn['data'][$roomId][$rowId]['is_percentage'] = $resApi->isPercentage($roomId);
                $dataToReturn['data'][$roomId][$rowId]['extra_bed_price'] = $complex->extra_bed_price;
                $dataToReturn['data'][$roomId][$rowId]['room_name'] = $roomName;
            }

        }

        $dataFromTotalPrice = $this->calculateTotalPrice($dataToReturn['data'], $reservationSession);
        $dataToReturn['total'] = $dataFromTotalPrice['total'];
        $dataToReturn['days'] = $dataFromTotalPrice['days'];

        return $dataToReturn;

    }

    public function calculateTotalPrice($dataByRoomTypes, $dateRange)
    {
        $totalByOneDay = 0;
        foreach ($dataByRoomTypes as $oneRoom) {
            foreach ($oneRoom as $oneRow) {
                $totalByOneDay = $totalByOneDay + $oneRow['early_booking'];
            }
        }

        $dateFrom = $dateRange['date_from'];
        $dateTo = $dateRange['date_to'];

        $period = new \DatePeriod(
            new \DateTime($dateFrom),
            new \DateInterval('P1D'),
            new \DateTime($dateTo)
        );
        $days = iterator_count($period);

        $dataToReturn = [];
        $dataToReturn['days'] = $days;
        $dataToReturn['total'] = $totalByOneDay * $days;
        return $dataToReturn;


    }


    public static function getExtraBeds($room_id, $childs, $adults)
    {

        $room = ComplexRooms::find($room_id);
        $beds = $room->beds;
        $extraBeds = $room->extra_beds;

        $extraBedsToReturn = 0;

        if ($childs + $adults > $beds) {
            if ($extraBeds >= ($childs + $adults) - $beds) {
                if ($childs != 0) {
                    if ($beds + 1 == $childs + $adults) {
                        $extraBedsToReturn = 0;
                    } else {
//                        if (is_int($childs / 2)) {
//                            $extraBedsToReturn = (($childs / 2) + $adults) - $beds;
//                        } else {
//                            $extraBedsToReturn = ((($childs - 1) / 2) + $adults) - $beds;
//                        }
                        $extraBedsToReturn = (($childs - 1) + $adults) - $beds;
                    }

                } else {
                    $extraBedsToReturn = $adults - $beds;
                }
            } else {
                $extraBedsToReturn = 'tooMany';

            }
        }

        return $extraBedsToReturn;

//        $room = ComplexRooms::find($room_id);
//        $beds = $room->beds;
//        $extraBeds = $room->extra_beds;
//
//        $extraBedsToReturn = 0;
//
//        if ($childs + $adults > $beds) {
//            if ($extraBeds >= ($childs + $adults) - $beds) {
//                $extraBedsToReturn = ($adults + $childs) - $beds;
//            } else {
//                $extraBedsToReturn = 'tooMany';
//            }
//        }
//        return $extraBedsToReturn;

    }


    public static function updateRoomsAndDateReservation($dataToRecord)
    {


        $validationStatus = Reservations::updateDateAndRoomsValidator($dataToRecord);
        if ($validationStatus != 'success') {
            return array('validationError', $validationStatus[1]);
        }

        $roomsArr = $dataToRecord['rooms_data'];
        $reservationData = $dataToRecord;
        unset($reservationData['rooms_data']);

        $reservation = Reservations::find($reservationData['reservation_id']);


        \DB::beginTransaction();
        try {
            $gb = new GlobalFunctions();
            $reservation->date_from = $gb->changeDateFormat($reservationData['date_from']);
            $gb = new GlobalFunctions();
            $reservation->date_to = $gb->changeDateFormat($reservationData['date_to']);

            $reservation->price = $reservationData['total'];
            $reservation->save();

            $oldRoomsArr = ReservedRooms::where('reservation_id', '=', $reservationData['reservation_id'])->get();
            foreach ($oldRoomsArr as $oneOldRoom) {
                $oneOldRoom->delete();
            }

            foreach ($roomsArr as $roomId => $oneRoom) {
                foreach ($oneRoom as $row) {
                    $row['extra_bets'] = $row['extra'];
                    unset($row['extra']);

                    $row['reservation_id'] = $reservationData['reservation_id'];
                    $row['room_id'] = $roomId;
                    ReservedRooms::addReserveRoom($row);
                }
            }

        } catch (\Exception $e) {
            \DB::rollBack();
            throw $e;
            return array('creatingError', 'creatingError');
        }

        \DB::commit();
        return array('successMessage', 'successMessage');
    }

    public static function calculateEarlyBooking($complex_id)
    {
        $promotions = EarlyBooking::where('complex_id', '=', $complex_id)
            ->where('is_active', '=', true)
            ->whereRaw('`until_date` > CURDATE()')
            ->orderBy('until_date', 'ASC')
            ->first();

        $promotionsInRange = ['id' => 0, 'until_date' => 0, 'percentage' => 0];

        if ($promotions != null)
            $promotionsInRange = [
                'id' => $promotions->id,
                'until_date' => $promotions->until_date,
                'percentage' => $promotions->percentage
            ];

        return $promotionsInRange;

    }

    public static function calculatePricePerDay($dateReservationArr, $room_id)
    {

        $dateFrom = $dateReservationArr['date_from'];

        $dateTo = $dateReservationArr['date_to'];


        $period = new \DatePeriod(
            new \DateTime($dateFrom),
            new \DateInterval('P1D'),
            new \DateTime($dateTo)
        );

        $counter = 0;
        $differentPriceArr = array();
        foreach ($period as $oneDay) {
            $counter++;


            $pricePerDay = \DB::table('room_prices')
                ->where('room_id', '=', $room_id)
                ->where('from_date', '<=', $oneDay)
                ->where('to_date', '>=', $oneDay)
                ->select('room_prices.price')
                ->get();

            if (empty($pricePerDay[0]->price)) {
                return 0;
            }


            if (isset($differentPriceArr[$pricePerDay[0]->price])) {
                $differentPriceArr[$pricePerDay[0]->price] = $differentPriceArr[$pricePerDay[0]->price] + 1;
            } else {
                $differentPriceArr[$pricePerDay[0]->price] = 1;
            }
        }


        $tempSum = 0;
        foreach ($differentPriceArr as $onePrice => $duplicatePriceCounter) {
            $tempSum = $tempSum + $onePrice * $duplicatePriceCounter;
        }
        $sum = $tempSum / $counter;

        return $sum;

    }

    public static function getStepsNavInfo($step)
    {
        $session = Session::get('new_reservation');
        switch ($step) {
            case 1:
                $complexName = '';
                if (isset($session['complex_id'])) {
                    $complex = Complex::find($session['complex_id'])->toArray();
                    $complexName = $complex['name_' . App::getLocale()];
                }

                return $complexName;
            case 2:
                $range = [];
                if (isset($session['date_from']) && isset($session['date_to'])) {
                    $range['from'] = $session['date_from'];
                    $range['to'] = $session['date_to'];
                }

                return $range;
            case 3:
                $counter = 0;
                if (isset($session['rooms_data'])) {
                    $roomsData = $session['rooms_data'];
                    foreach ($roomsData as $oneRoomType) {
                        $counter += count($oneRoomType);
                    }
                }
                return $counter;
        }

    }

    public static function calculateCurrency($bgnValue)
    {
//        http://api.fixer.io/latest?base=BGN
        $value = 0;
        if (App::getLocale() != 'bg') {
            $bgnToEur = 0.5113;
            $eur = $bgnToEur * $bgnValue;

            $eur = number_format($eur, 2, '.', '');
            $value = $eur;
        } else {
            $value = $bgnValue;
        }

        return $value;
    }

    public static function getCoef()
    {
//        http://api.fixer.io/latest?base=BGN
        $coef = 0;
        if (App::getLocale() != 'bg') {
            $coef = 0.5113;
        } else {
            $coef = 1;
        }

        return $coef;
    }


}