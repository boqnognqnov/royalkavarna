<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/*Schema::create('room_extras', function (Blueprint $table) {
    $table->increments('id');
    $table->integer('room_id', false, true);
    $table->foreign('room_id')->references('id')->on('complex_room_types');
    $table->string('icon');
    //ROOM TYPE
    $table->string('name_bg');
    $table->string('name_en');
    $table->string('name_ru');
    $table->string('name_ro');
    $table->timestamps();
});*/

class RoomExtras extends Model
{
    protected $table = 'room_extras';
}
