<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;


class Presentation extends Model
{
    protected $table = 'presentations';

    const youtube_type = 'youTubeVideo';
    const video_file_type = 'videoFile';
    const pdf_file_type = 'pdfFile';


    public static $filePath = 'uploads/files/presentations/';


    public static function getCreateRules()
    {
        return array(
            'type' => 'required|in:' . self::youtube_type . ',' . self::video_file_type . ',' . self::pdf_file_type,
            'file' => 'required_if:type,' . self::video_file_type . ',' . self::pdf_file_type,
            'youtube_video' => 'required_if:type,' . self::youtube_type
        );
    }


    public static function getCreateMessages()
    {
        return [
            'type.required' => 'Типа на промоцията е задължителен',
            'type.in' => 'Типа на промоцията трябва да е само от оказаните варианти',
            'file.required_if' => 'Файлът е задължителен, когато сте изпрали, че типа на презентацията е файл',
            'youtube_video.required_if' => 'Данните за YouTube видеото за задължитени, когато сте оказали, че ще качите такъв тип презентация',
        ];
    }


    public static function addPresentation($data)
    {

        $validator = \Validator::make($data, self::getCreateRules(), self::getCreateMessages());

        if ($validator->fails()) {
            return array('validatorError', $validator);
        }

        try {
            $presentation = new Presentation();
            $presentation->type = $data['type'];

            if ($data['type'] == self::youtube_type) {
                $presentation->youtube_video = $data['youtube_video'];
            } else {
                $extension = Input::file('file')->getClientOriginalExtension();
                $name = time() . '.' . $extension;
                Input::file('file')->move(self::$filePath, $name);

                $presentation->file = $name;
            }

            $presentation->save();

        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('databaseError', $ex);
        }

        return array('successMessage', $presentation);

    }

    public static function destroyPresentation($presentation_id)
    {

        try {
            Presentation::destroy($presentation_id);
        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('destroyError', $ex);
        }

        return array('destroySuccess', true);

    }
}
