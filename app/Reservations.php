<?php

namespace App;

use App\Classes\GlobalFunctions;
use Illuminate\Database\Eloquent\Model;

/*
 * Schema::create('reservations', function (Blueprint $table) {
    $table->increments('id');
    $table->integer('complex_id', false, true);
    $table->foreign('complex_id')->references('id')->on('complexes');
    $table->string('name');
    $table->string('email');
    $table->string('phone');
    $table->timestamp('date_from');
    $table->timestamp('date_to');
    $table->integer('in_early_booking_range', false, true);
    $table->foreign('in_early_booking_range')->references('id')->on('early_booking');
    $table->decimal('price');
    $table->integer('status_id', false, true);
    $table->timestamps();
});*/

class Reservations extends Model
{


    protected $table = 'reservations';


    public function getReservedRooms()
    {
        return $this->hasOne('App\ReservedRooms');
    }

    public function getStatus()
    {
        return $this->hasOne('App\ReservationStatuses', 'id', 'status_id');
    }

    public static function newReservationRules()
    {
        return array(
            'firstname' => 'required|alpha',
            'middle' => 'alpha',
            'lastname' => 'required|alpha',
            'address' => 'required|regex:/^([A-Za-z0-9\p{Cyrillic}\s\-\_\.\,\'\"\#\/\№])+$/u',
            'city' => 'required|regex:/^([A-Za-z\p{Cyrillic}\s]{3,50})+$/u',
            'country' => 'required|regex:/^([A-Za-z\p{Cyrillic}\s]{3,50})+$/u',
            'telephone1' => 'required|regex:/^([0-9\s]{5,16})$/',
            'telephonecode' => 'required|regex:/^([0-9\+]{2,6})$/',
            'telephone2' => 'regex:/^([0-9\s]{5,16})$/',
            'email' => 'required|email',
            'retypeemail' => 'required|same:email',
            'requirements' => 'regex:/^([A-Za-z0-9\p{Cyrillic}\s\-\_\.\,\!\?])+$/u'
        );
    }

    public static function newReservationMessages()
    {
        return [
            'firstname.required' => 'Не е въведено име.',
            'firstname.alpha' => 'Само букви.',
            'middle.alpha' => 'Само букви.',
            'lastname.required' => 'Не е въведена фамилия.',
            'lastname.alpha' => 'Само букви.',
            'address.required' => 'Не е въведен адрес',
            'address.regex' => 'Не позволени символи.',
            'city.required' => 'Не е въведена град.',
            'city.regex' => 'Не позволени символи.',
            'country.required' => 'Не е въведена държава.',
            'country.regex' => 'Не позволени символи.',
            'telephone1.required' => 'Не е въведен телефон',
            'telephone1.regex' => 'Не позволени символи.',
            'telephonecode.required' => 'Не е въведен код на телефона',
            'telephonecode.regex' => 'Не позволени символи.',
            'telephone2.regex' => 'Не позволени символи.',
            'email.required' => 'Не е въведен email',
            'email.email' => 'Невалиден email',
            'retypeemail.required' => 'Не е въведен email',
            'retypeemail.same' => 'Двата E-mails не съвпадат.',
            'requirements.alpha_numeric' => 'Не позволени символи.',
            'requirements.regex' => 'Не позволени символи.'
        ];
    }

    public static function updatePersonalRules()
    {
        return array(
            'firstname' => 'required|alpha',
            'middle' => 'alpha',
            'lastname' => 'required|alpha',
            'address' => 'required|regex:/^([A-Za-z0-9\p{Cyrillic}\s\-\_\.\,\'\"\#\/\№])+$/u',
            'city' => 'required|regex:/^([A-Za-z\p{Cyrillic}\s]{3,50})+$/u',
            'country' => 'required|regex:/^([A-Za-z\p{Cyrillic}\s]{3,50})+$/u',
            'telephone1' => 'required|regex:/^([0-9]{5,16})$/',
            'telephonecode' => 'required|regex:/^([0-9\+]{2,6})$/',
            'telephone2' => 'regex:/^([0-9]{5,16})$/',
            'email' => 'required|email',
        );
    }

    public static function updatePersonalMessages()
    {
        return [
            'firstname.required' => 'Не е въведено име.',
            'firstname.alpha' => 'Само букви.',
            'middle.alpha' => 'Само букви.',
            'lastname.required' => 'Не е въведена фамилия.',
            'lastname.alpha' => 'Само букви.',
            'address.required' => 'Не е въведен адрес',
            'address.regex' => 'Не позволени символи.',
            'city.required' => 'Не е въведена град.',
            'city.regex' => 'Не позволени символи.',
            'country.required' => 'Не е въведена държава.',
            'country.regex' => 'Не позволени символи.',
            'telephone1.required' => 'Не е въведен телефон',
            'telephone1.regex' => 'Не позволени символи.',
            'telephonecode.required' => 'Не е въведен код на телефона',
            'telephonecode.regex' => 'Не позволени символи.',
            'telephone2.regex' => 'Не позволени символи.',
            'email.required' => 'Не е въведен email',
            'email.email' => 'Невалиден email',
        ];
    }


    public static function dateAndRoomsRules()
    {
        return array(
            'date_from' => 'required',
            'date_to' => 'required',
            'rooms_data' => 'required',
        );
    }

    public static function dateAndRoomsMessages()
    {
        return [
            'date_from.required' => 'Не сте избрали начална дата',
            'date_to.required' => 'Не сте избрали крайна дата',
            'rooms_data.required' => 'Възможно е да няма избрани стаи',
        ];
    }

    public static function validateNewReservation($data)
    {
        $validator = \Validator::make($data, self::newReservationRules(), self::newReservationMessages());

        if ($validator->fails()) {
            return array('validationError', $validator);
        }
        return 'success';
    }

    public static function updateDateAndRoomsValidator($data)
    {
        $validator = \Validator::make($data, self::dateAndRoomsRules(), self::dateAndRoomsMessages());

        if ($validator->fails()) {
            return array('validationError', $validator);
        }
        return 'success';
    }

    public static function newReservation($data)
    {

        try {

            $reservation = new Reservations();

            $gb = new GlobalFunctions();
            $reservation->date_from = $gb->changeDateFormat($data['date_from']);

            $gb = new GlobalFunctions();
            $reservation->date_to = $gb->changeDateFormat($data['date_to']);

            $reservation->price = $data['total'];

            $reservation->complex_id = $data['complex_id'];
            $reservation->firstname = $data['firstname'];
            $reservation->middle = $data['middle'];
            $reservation->lastname = $data['lastname'];
            $reservation->address = $data['address'];
            $reservation->city = $data['city'];
            $reservation->country = $data['country'];
            $reservation->telephone1 = $data['telephone1'];
            $reservation->telephonecode = $data['telephonecode'];
            $reservation->telephone2 = $data['telephone2'];
            $reservation->email = $data['email'];
            $reservation->requirements = $data['requirements'];


            $reservation->status_id = 1;

            $reservation->save();

        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('creatingError', $ex);
        }

        return array('successMessage', $reservation);

    }

    public static function destroyReservation($reservation_id)
    {
        try {

            $reservation = Reservations::find($reservation_id);
            $reservedRooms = ReservedRooms::where('reservation_id', '=', $reservation_id)->get();

            foreach ($reservedRooms as $room) {
                $room->delete();
            }
            $reservation->delete();

        } catch (\Exception $ex) {
            \Log::error($ex);
            return 'destroyError';
        }
        return 'success';


    }

    public static function updatePersonalData($data)
    {

        $validator = \Validator::make($data, self::updatePersonalRules(), self::updatePersonalMessages());

        if ($validator->fails()) {
            return array('validationError', $validator);
        }


        try {
            $reservation = Reservations::find($data['id']);
            $reservation->firstname = $data['firstname'];
            $reservation->middle = $data['middle'];
            $reservation->address = $data['address'];
            $reservation->city = $data['city'];
            $reservation->country = $data['country'];
            $reservation->telephonecode = $data['telephonecode'];
            $reservation->telephone1 = $data['telephone1'];
            $reservation->telephone2 = $data['telephone2'];
            $reservation->firstname = $data['firstname'];
            $reservation->email = $data['email'];
            $reservation->save();

        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('creatingError', $ex);
        }
        return array('successMessage', $reservation);


    }

}
