<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Classes\GlobalFunctions;

class RoomPrices extends Model
{

    protected $table = 'room_prices';

    public static function getCreateRules()
    {
        return array(
            'room_id' => 'required|exists:complex_room_types,id',
            'from_date' => 'required',
            'to_date' => 'required',
            'price' => 'required'
        );
    }

    public static function getUpdateRules()
    {
        return array(
            'id' => 'required|exists:room_prices,id',
            'room_id' => 'required|exists:complex_room_types,id',
            'from_date' => 'required',
            'to_date' => 'required',
            'price' => 'required'
        );
    }

    public static function getCreateMessages()
    {
        return [
            'room_id.required' => 'Моля изберете стая на която да поставите цената.',
            'room_id.exists' => 'Стаята на която се опитвате да поставите цена на сеществува.',
            'from_date.required' => 'Моля изберете начална дата.',
            'to_date.required' => 'Моля изберете крайна дата.',
            'price.required' => 'Моля въведете цена.'
        ];
    }

    public static function getUpdateMessages()
    {
        return [
            'id.required' => 'Моля изберете цена която да редактирате.',
            'id.exists' => 'Цената която се опитвате да редактирате не съществува.',
            'room_id.required' => 'Моля изберете стая на която да поставите цената.',
            'room_id.exists' => 'Стаята на която се опитвате да поставите цена на сеществува.',
            'from_date.required' => 'Моля изберете начална дата.',
            'to_date.required' => 'Моля изберете крайна дата.',
            'price.required' => 'Моля въведете цена.'
        ];
    }


    public static function createPrice($data)
    {

        $validator = \Validator::make($data, self::getCreateRules(), self::getCreateMessages());

        if ($validator->fails()) {
            return array('validatorError', $validator);
        }

        try {
            $priceEntity = new RoomPrices();
            $priceEntity->room_id = $data['room_id'];
            $priceEntity->from_date = date("Y-m-d", strtotime($data['from_date']));
            $priceEntity->to_date = date("Y-m-d", strtotime($data['to_date']));
            $priceEntity->price = $data['price'];
            $priceEntity->save();
        } catch (\Exception $ex) {
            return array('databaseError', $ex);
        }

        return array('successMessage', $priceEntity);

    }

    public static function updatePrice($data)
    {

        $validator = \Validator::make($data, self::getUpdateRules(), self::getUpdateMessages());

        if ($validator->fails()) {
            return array('validatorError', $validator);
        }

        try {
            $priceEntity = RoomPrices::where('id', '=', $data['id'])->first();
            $priceEntity->room_id = $data['room_id'];
            $priceEntity->from_date = date("Y-m-d", strtotime($data['from_date']));
            $priceEntity->to_date = date("Y-m-d", strtotime($data['to_date']));
            $priceEntity->price = $data['price'];
            $priceEntity->save();
        } catch (\Exception $ex) {
            return array('databaseError', $ex);
        }

        return array('successMessage', $priceEntity);

    }

    public static function deletePrice($id)
    {

        try {
            RoomPrices::destroy($id);
        } catch (\Exception $ex) {
            return 'databaseError';
        }

        return 'successMessage';

    }

}
