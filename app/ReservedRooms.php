<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ReservedRooms extends Model
{
    protected $table = 'reserved_rooms';


    public static function addReserveRoom($data)
    {


//        try {

        $room = new ReservedRooms();

        $room->reservation_id = $data['reservation_id'];

        $room->room_id = $data['room_id'];
        $room->adults = $data['adult'];
        $room->children = $data['child'];
        $room->extra_bets = $data['extra_bets'];
        $room->save();

//        } catch (\Exception $ex) {
//            \Log::error($ex);
//            return array('status', 'error');
//        }

        return array('status', 'success');
    }

    public static function updateReserveRoom($data)
    {


        try {

            $room = ReservedRooms::find($data['reserved_room_id']);

            $room->reservation_id = $data['reservation_id'];

            $room->room_id = $data['room_id'];
            $room->adults = $data['adults'];
            $room->children = $data['children'];
            $room->extra_bets = $data['extra_bets'];
            $room->save();

        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('status', 'error');
        }

        return array('status', 'success');
    }

}
