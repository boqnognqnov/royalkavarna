<?php namespace App;

use Illuminate\Database\Eloquent\Model;
//use Intervention\Image\ImageManagerStatic as Image;
use Intervention\Image\ImageManagerStatic as Image;
use File;

class Slider extends Model
{

    protected $table = 'sliders';

    public static $imagePath = 'uploads/sliders/';
    public static $imagePathEsc = 'uploads/sliders/';

    public static function getRules()
    {
        return array(
            'image' => 'required|image',
            'text_bg' => 'required',
            'text_en' => 'required',
            'text_ru' => 'required',
            'text_ro' => 'required',
//            'href' => 'required',

        );
    }


    public static function getMessages()
    {
        return [
            'image.required' => 'Моля изберете снимка',
            'image.image' => 'Файлът трябва да е изображение',
            'text_bg.required' => 'Изисква се текст на BG',
            'text_en.required' => 'Изисква се текст на EN',
            'text_ru.required' => 'Изисква се текст на RU',
            'text_ro.required' => 'Изисква се текст на RO',
            'href.required' => '',


        ];
    }


    public static function createSlide($data)
    {

        $validator = \Validator::make($data, self::getRules(), self::getMessages());

        if ($validator->fails()) {
            return array('validatorError', $validator);
        }

        $fileName = time() . '.' . $data['image']->getClientOriginalExtension();
//        Image::make($data['image']->getRealPath())->resize(126, 126)->save(self::$path . $fileName);
        Image::make($data['image']->getRealPath())->save(self::$imagePath . $fileName);


        if (!isset($data['is_active'])) {
            $data['is_active'] = false;
        }

        try {
            $sliderEntity = new Slider();
            $sliderEntity->image = $fileName;

            $sliderEntity->text_bg = $data['text_bg'];
            $sliderEntity->text_en = $data['text_en'];
            $sliderEntity->text_ru = $data['text_ru'];
            $sliderEntity->text_ro = $data['text_ro'];


            $sliderEntity->href = $data['href'];
            $sliderEntity->is_active = $data['is_active'];
            $sliderEntity->save();
        } catch (\Exception $ex) {
            return array('databaseError', $ex);
        }

        return array('successMessage', $sliderEntity);

    }

    public static function updateSlide($data)
    {
        $rules = self::getRules();
        $rules['image'] = '';

        $validator = \Validator::make($data, $rules, self::getMessages());


        if ($validator->fails()) {
            return array('validatorError', $validator);
        }

        $sliderEntity = Slider::find($data['id']);

        if (isset($data['image'])) {
            $oldImage = $sliderEntity->image;

            File::delete(self::$imagePath . $oldImage);

            $fileName = time() . '.' . $data['image']->getClientOriginalExtension();
//        Image::make($data['image']->getRealPath())->resize(126, 126)->save(self::$path . $fileName);
            Image::make($data['image']->getRealPath())->save(self::$imagePath . $fileName);
        }

        if (!isset($data['is_active'])) {
            $data['is_active'] = false;
        }

        try {

            if (isset($data['image'])) {
                $sliderEntity->image = $fileName;
            }


            $sliderEntity->text_bg = $data['text_bg'];
            $sliderEntity->text_en = $data['text_en'];
            $sliderEntity->text_ru = $data['text_ru'];
            $sliderEntity->text_ro = $data['text_ro'];

            $sliderEntity->href = $data['href'];
            $sliderEntity->is_active = $data['is_active'];
            $sliderEntity->save();
        } catch (\Exception $ex) {
            return array('databaseError', $ex);
        }

        return array('successMessage', $sliderEntity);

    }

    public static function deleteSlide($slider_id)
    {

        try {

            $slider = Slider::find($slider_id);
            $iamge = $slider->image;
            File::delete(self::$imagePath . $iamge);

            $slider->delete();
        } catch (\Exception $ex) {
            return 'databaseError';
        }

        return 'successMessage';

    }

    public static function setActivity($slider_id)
    {
        try {
            $slider = Slider::find($slider_id);
            if ($slider->is_active == true) {
                $slider->is_active = false;
            } else {
                $slider->is_active = true;
            }

            $slider->save();

        } catch (\Exception $ex) {
            return 'error';
        }
        return 'success';

    }

}
