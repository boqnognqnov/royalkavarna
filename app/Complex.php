<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\ImageManagerStatic as Image;

class Complex extends Model
{

    protected $table = 'complexes';

    public static $imagePath = 'uploads/images/complexes/image/';

    public static $logoPath = 'uploads/images/complexes/logo/';

    public static $bgPath = 'uploads/images/complexes/background/';

    public static $bookPath = 'uploads/images/complexes/book/';


//    public function rooms()
//    {
//        return $this->hasMany('App\ComplexRooms', 'complex_id');
//    }

    public static function getCreateRules()
    {
        return array(
            'guest_book_background' => 'required|image',
            'image' => 'required|image',
            'logo' => 'required|image',
            'is_percentage' => 'boolean',
            'is_inseparable' => 'boolean',
            'extra_bed_price' => 'required|numeric',
            'name_bg' => 'required',
            'name_en' => 'required',
            'name_ru' => 'required',
            'name_ro' => 'required',
            'mini_information_bg' => 'required',
            'mini_information_en' => 'required',
            'mini_information_ru' => 'required',
            'mini_information_ro' => 'required',
            'location_information_bg' => 'required',
            'location_information_en' => 'required',
            'location_information_ru' => 'required',
            'location_information_ro' => 'required',
            'about_bg' => 'required',
            'about_en' => 'required',
            'about_ru' => 'required',
            'about_ro' => 'required',
            'early_booking_text_bg' => 'required',
            'early_booking_text_en' => 'required',
            'early_booking_text_ru' => 'required',
            'early_booking_text_ro' => 'required',
            'address_bg' => 'required',
            'address_en' => 'required',
            'address_ru' => 'required',
            'address_ro' => 'required',
            'phone' => 'required',
            'mobile_phone' => 'required',
            'reception_phone' => 'required',
            'email' => 'required|email',
            'coordinates' => 'required',
            'guest_book_color' => 'required|image'
        );
    }

    public static function getUpdateRules()
    {
        $rules = self::getCreateRules();
        $rules['id'] = 'required|exists:complexes,id';
        $rules['image'] = 'image';
        $rules['logo'] = 'image';
        $rules['guest_book_background'] = 'image';
        $rules['guest_book_color'] = 'image';

        return $rules;
    }

    public static function getCreateMessages()
    {
        return [
            'guest_book_background.required' => 'Снимката за guest book background е задължителна',
            'guest_book_background.image' => 'Снимката за guest book background е в грешен формат',
            'guest_book_color.required' => 'Цвета за книгата в guest book е задължителен',
            'guest_book_color.image' => 'Снимката за книгата е в грешен формат',
            'image.required' => 'Снимката е задължителна',
            'image.image' => 'Снимката е в грешен формат',
            'logo.required' => 'Логото е задължително',
            'logo.image' => 'Логото е в грешен формат',
            'is_percentage.boolean' => 'Чекбокса за начина на изчисляване на доп. легла е грешно подаден',
            'is_inseparable.boolean' => 'Чекбокса за това дали комплекса се предлага само целия е грешно подаден',
            'extra_bed_price.required' => 'Стойността за изчисляване на доп. легла е задължителна',
            'extra_bed_price.numeric' => 'Стойността за изчисляване на доп. легла е в грешен формат',
            'name_bg.required' => '(BG) Името на комплекса не е попълнено',
            'name_en.required' => '(EN) Името на комплекса не е попълнено',
            'name_ru.required' => '(RU) Името на комплекса не е попълнено',
            'name_ro.required' => '(RO) Името на комплекса не е попълнено',
            'mini_information_bg.required' => '(BG) Мини информацията е задължителна за Home страницата',
            'mini_information_en.required' => '(EN) Мини информацията е задължителна за Home страницата',
            'mini_information_ru.required' => '(RU) Мини информацията е задължителна за Home страницата',
            'mini_information_ro.required' => '(RO) Мини информацията е задължителна за Home страницата',
            'location_information_bg' => '(BG) Location информацията е задължителна за картата на комплекса',
            'location_information_en' => '(EN) Location информацията е задължителна за картата на комплекса',
            'location_information_ru' => '(RU) Location информацията е задължителна за картата на комплекса',
            'location_information_ro' => '(RO) Location информацията е задължителна за картата на комплекса',
            'about_bg.required' => '(BG) Информацията за комплекса не е попълнена',
            'about_en.required' => '(EN) Информацията за комплекса не е попълнена',
            'about_ru.required' => '(RU) Информацията за комплекса не е попълнена',
            'about_ro.required' => '(RO) Информацията за комплекса не е попълнена',
            'early_booking_text_bg.required' => '(BG) Текста за ранните резервации не е попълнен',
            'early_booking_text_en.required' => '(EN) Текста за ранните резервации не е попълнен',
            'early_booking_text_ru.required' => '(RU) Текста за ранните резервации не е попълнен',
            'early_booking_text_ro.required' => '(RO) Текста за ранните резервации не е попълнен',
            'address_bg.required' => '(BG) Адреса на комплекса не е попълнен',
            'address_en.required' => '(EN) Адреса на комплекса не е попълнен',
            'address_ru.required' => '(RU) Адреса на комплекса не е попълнен',
            'address_ro.required' => '(RO) Адреса на комплекса не е попълнен',
            'phone.required' => 'Телефонът на коплекса е задължителен',
            'mobile_phone.required' => 'Мобилният телефон на комплекса е задължителен',
            'reception_phone.required' => 'Телефонт на рецепцията е задължителен',
            'email.required' => 'Емайл-ът е задължителен',
            'email.email' => 'Емайл-ът е в грешен формат',
            'coordinates.required' => 'Координатите на комплекса за задължителни за картата'
        ];
    }

    public static function getUpdateMessages()
    {
        $messages = self::getCreateMessages();
        $messages['id.required'] = trans('complex.required_complex_id');
        $messages['id.exists'] = trans('complex.complex_id_dont_exist');

        return $messages;
    }

    public static function createComplex($data)
    {

        $validator = \Validator::make($data, self::getCreateRules(), self::getCreateMessages());

        if ($validator->fails()) {
            return array('validatorError', $validator);
        }

        $imageName = time();

        $complexImage = $imageName . '.' . $data['image']->getClientOriginalExtension();
        $complexLogo = $imageName . '.' . $data['logo']->getClientOriginalExtension();
        $complexBG = $imageName . '.' . $data['guest_book_background']->getClientOriginalExtension();
        $complexGuesBook = $imageName . '.' . $data['guest_book_color']->getClientOriginalExtension();

        $complexImagePath = public_path(self::$imagePath . $complexImage);
        Image::make($data['image']->getRealPath())->save($complexImagePath);

        $complexBgPath = public_path(self::$bgPath . $complexBG);
        Image::make($data['guest_book_background']->getRealPath())->save($complexBgPath);

        $complexLogoPath = public_path(self::$logoPath . $complexLogo);
        Image::make($data['logo']->getRealPath())->save($complexLogoPath);

        $complexGuesBookPath = public_path(self::$bookPath . $complexGuesBook);
        Image::make($data['guest_book_color']->getRealPath())->save($complexGuesBookPath);

        try {
            $complexEntity = new Complex();
            $complexEntity->image = $complexImage;
            $complexEntity->logo = $complexLogo;
            $complexEntity->is_percentage = $data['is_percentage'];
            $complexEntity->is_inseparable = $data['is_inseparable'];
            $complexEntity->extra_bed_price = $data['extra_bed_price'];
            $complexEntity->name_bg = $data['name_bg'];
            $complexEntity->name_en = $data['name_en'];
            $complexEntity->name_ru = $data['name_ru'];
            $complexEntity->name_ro = $data['name_ro'];
            $complexEntity->mini_information_bg = $data['mini_information_bg'];
            $complexEntity->mini_information_en = $data['mini_information_en'];
            $complexEntity->mini_information_ru = $data['mini_information_ru'];
            $complexEntity->mini_information_ro = $data['mini_information_ro'];
            $complexEntity->location_information_bg = $data['location_information_bg'];
            $complexEntity->location_information_en = $data['location_information_en'];
            $complexEntity->location_information_ru = $data['location_information_ru'];
            $complexEntity->location_information_ro = $data['location_information_ro'];
            $complexEntity->about_bg = $data['about_bg'];
            $complexEntity->about_en = $data['about_en'];
            $complexEntity->about_ru = $data['about_ru'];
            $complexEntity->about_ro = $data['about_ro'];
            $complexEntity->early_booking_text_bg = $data['early_booking_text_bg'];
            $complexEntity->early_booking_text_en = $data['early_booking_text_en'];
            $complexEntity->early_booking_text_ru = $data['early_booking_text_ru'];
            $complexEntity->early_booking_text_ro = $data['early_booking_text_ro'];
            $complexEntity->address_bg = $data['address_bg'];
            $complexEntity->address_en = $data['address_en'];
            $complexEntity->address_ru = $data['address_ru'];
            $complexEntity->address_ro = $data['address_ro'];
            $complexEntity->phone = $data['phone'];
            $complexEntity->mobile_phone = $data['mobile_phone'];
            $complexEntity->reception_phone = $data['reception_phone'];
            $complexEntity->email = $data['email'];
            $complexEntity->coordinates = $data['coordinates'];
            $complexEntity->guest_book_background = $complexBG;
            $complexEntity->guest_book_color = $complexGuesBook;
            $complexEntity->save();
        } catch (\Exception $ex) {
            return array('databaseError', $ex);
        }

        return array('successMessage', $complexEntity);

    }

    public static function updateComplex($data)
    {

        $validator = \Validator::make($data, self::getUpdateRules(), self::getUpdateMessages());

        if ($validator->fails()) {
            return array('validatorError', $validator);
        }

        if (isset($data['guest_book_background'])) {
            $imageName = time();

            $complexBG = $imageName . '.' . $data['guest_book_background']->getClientOriginalExtension();

            $complexBGPath = public_path(self::$bgPath . $complexBG);
            Image::make($data['guest_book_background']->getRealPath())->save($complexBGPath);
        }

        if (isset($data['image'])) {
            $imageName = time();

            $complexImage = $imageName . '.' . $data['image']->getClientOriginalExtension();

            $complexImagePath = public_path(self::$imagePath . $complexImage);
            Image::make($data['image']->getRealPath())->save($complexImagePath);
        }

        if (isset($data['logo'])) {
            $imageName = time();

            $complexLogo = $imageName . '.' . $data['logo']->getClientOriginalExtension();

            $complexLogoPath = public_path(self::$logoPath . $complexLogo);
            Image::make($data['logo']->getRealPath())->save($complexLogoPath);
        }

        if (isset($data['guest_book_color'])) {
            $imageName = time();

            $complexGuesBook = $imageName . '.' . $data['guest_book_color']->getClientOriginalExtension();

            $complexGuesBookPath = public_path(self::$bookPath . $complexGuesBook);
            Image::make($data['guest_book_color']->getRealPath())->save($complexGuesBookPath);
        }

        try {
            $complexEntity = Complex::where('id', '=', $data['id'])->first();
            if (isset($data['image']))
                $complexEntity->image = $complexImage;
            if (isset($data['logo']))
                $complexEntity->logo = $complexLogo;
            $complexEntity->is_percentage = $data['is_percentage'];
            $complexEntity->is_inseparable = $data['is_inseparable'];
            $complexEntity->extra_bed_price = $data['extra_bed_price'];
            $complexEntity->name_bg = $data['name_bg'];
            $complexEntity->name_en = $data['name_en'];
            $complexEntity->name_ru = $data['name_ru'];
            $complexEntity->name_ro = $data['name_ro'];
            $complexEntity->mini_information_bg = $data['mini_information_bg'];
            $complexEntity->mini_information_en = $data['mini_information_en'];
            $complexEntity->mini_information_ru = $data['mini_information_ru'];
            $complexEntity->mini_information_ro = $data['mini_information_ro'];
            $complexEntity->location_information_bg = $data['location_information_bg'];
            $complexEntity->location_information_en = $data['location_information_en'];
            $complexEntity->location_information_ru = $data['location_information_ru'];
            $complexEntity->location_information_ro = $data['location_information_ro'];
            $complexEntity->about_bg = $data['about_bg'];
            $complexEntity->about_en = $data['about_en'];
            $complexEntity->about_ru = $data['about_ru'];
            $complexEntity->about_ro = $data['about_ro'];
            $complexEntity->early_booking_text_bg = $data['early_booking_text_bg'];
            $complexEntity->early_booking_text_en = $data['early_booking_text_en'];
            $complexEntity->early_booking_text_ru = $data['early_booking_text_ru'];
            $complexEntity->early_booking_text_ro = $data['early_booking_text_ro'];
            $complexEntity->address_bg = $data['address_bg'];
            $complexEntity->address_en = $data['address_en'];
            $complexEntity->address_ru = $data['address_ru'];
            $complexEntity->address_ro = $data['address_ro'];
            $complexEntity->phone = $data['phone'];
            $complexEntity->mobile_phone = $data['mobile_phone'];
            $complexEntity->reception_phone = $data['reception_phone'];
            $complexEntity->email = $data['email'];
            $complexEntity->coordinates = $data['coordinates'];
            if (isset($data['guest_book_background'])) $complexEntity->guest_book_background = $complexBG;
            if (isset($data['guest_book_color'])) $complexEntity->guest_book_color = $complexGuesBook;
            $complexEntity->save();
        } catch (\Exception $ex) {
            return array('databaseError', $ex);
        }

        return array('successMessage', $complexEntity);

    }

    public static function deleteComplex($id)
    {

        try {
            //////////////
            $earlyBookings = EarlyBooking::where('complex_id', '=', $id)->get();

            foreach ($earlyBookings as $oneBooking) {
                $oneBooking->delete();
                $reservations = Reservations::where('complex_id', '=', $id)->get();
                foreach ($reservations as $oneReservation) {
                    $reservedRooms = ReservedRooms::where('reservation_id', '=', $oneReservation->id)->get();
                    foreach ($reservedRooms as $oneRooms) {
                        $oneRooms->delete();
                    }
                    $oneReservation->delete();
                }
            }
            //////////////
            $gallery = ComplexGalery::where('complex_id', '=', $id)->get();
            foreach ($gallery as $oneRecord) {
                $oneRecord->delete();
            }

            Complex::destroy($id);
        } catch (\Exception $ex) {
            return 'databaseError';
        }

        return 'successMessage';

    }

}
