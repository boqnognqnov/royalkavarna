<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\ImageManagerStatic as Image;
use App\Classes\ImageProccessing;
use File;

/*Schema::create('complex_gallery', function (Blueprint $table) {
    $table->increments('id');
    $table->integer('complex_id', false, true);
    $table->foreign('complex_id')->references('id')->on('complexes');
    $table->string('image');
    $table->timestamps();
});*/

class RoomGalery extends Model
{
    protected $table = 'room_gallery';

    public static $path = 'uploads/rooms/gallery/';
    public static $pathSmall = 'uploads/rooms/gallery/small/';


    public static function galleryRules()
    {
        return array(
            'room_id' => 'required',
            'image' => 'required|image',
        );
    }

    public static function galleryMessages()
    {
        return [
            'room_id.required' => 'Не е избрана  стая  редакция',
            'image.required' => 'Моля прикачете изображение',
            'image.image' => 'Файлът трябва да е изображение',

        ];
    }

    public static function setImage($data)
    {


        $validator = \Validator::make($data, self::galleryRules(), self::galleryMessages());

        if ($validator->fails()) {
            return array('validationError', $validator);
        }

        $lastPosition = 0;
        $lastRecord = RoomGalery::orderBy('position', 'desc')->first();
        if ($lastRecord) {
            $lastPosition = $lastRecord->position;
        }

        $image = new RoomGalery();


        try {
            $time = time();
            $fileName = $time . '.' . $data['image']->getClientOriginalExtension();
//            Image::make($data['image']->getRealPath())->save(self::$path . $fileName);

            Image::make($data['image']->getRealPath())->resize(848, 500)->save(self::$path . $fileName);
            Image::make($data['image']->getRealPath())->resize(165, 97)->save(self::$pathSmall . $fileName);


            $image->room_id = $data['room_id'];
            $image->image = $fileName;
            $image->position = $lastPosition + 1;

            $image->save();

        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('creatingError', $ex);
        }

        return array('successMessage', $image);

    }

    public static function destroyImage($image_id)
    {
//        $status = ImageProccessing::destroyImage($image_id, ComplexGalery::class);
        $status = true;

        $fileName = RoomGalery::find($image_id)->image;

        try {
            File::delete(RoomGalery::$pathSmall . $fileName);
            File::delete(RoomGalery::$path . $fileName);

            RoomGalery::destroy($image_id);
        } catch (\Exception $ex) {
            \Log::error($ex);
            $status = false;
        }


        if ($status == true) {
            return array('status' => 'success');
        } else {
            return array('status' => 'fail');
        }


    }

}
