<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomOccupancy extends Model
{
    protected $table = 'room_occupancy';

    public static function getCreateRules()
    {
        return array(
            'room_id' => 'required|exists:complex_room_types,id',
            'date_from' => 'required',
            'date_to' => 'required',
            'count_occupancy_rooms' => 'required|min:1'
        );
    }

    public static function getUpdateRules()
    {
        $rules = self::getCreateRules();
        $rules['room_occupancy_id'] = 'required|exists:room_occupancy,id';

        return $rules;
    }

    public static function getCreateMessages()
    {
        return [
            'room_id.required' => 'Id-то на стаята е задължително',
            'room_id.exists' => 'Id-то на стаята не съществува',
            'date_from.required' => 'Началната дата е задължителна',
            'date_to.required' => 'Крайната дата е задължителна',
            'count_occupancy_rooms.required' => 'Броя заети стаи е задължителен',
            'count_occupancy_rooms.min' => 'Броя заети стаи трябва да е минимум 1'
        ];
    }

    public static function getUpdateMessages()
    {
        $messages = self::getCreateMessages();
        $messages['room_occupancy_id.required'] = 'Липсва ID';
        $messages['room_occupancy_id.exists'] = 'ID не съществува';

        return $messages;
    }

    public static function countReservedRoomsForDate($room_type_id, $date)
    {
        //Occupancy rooms without site reservation
        $countOccupancyRooms = \DB::select("SELECT SUM(count_occupancy_rooms) as countRooms FROM
            (SELECT count_occupancy_rooms
            FROM royals.room_occupancy WHERE
            DATE(?) >= DATE(date_from) AND
            DATE(?) < DATE(date_to) AND
            room_id = ?) as selected", [
            $date,//'2016-01-14'
            $date,//'2016-01-14'
            $room_type_id//1
        ]);

        //Occupancy rooms WITH site reservation
        $countReservedRooms = \DB::select("SELECT count(1) as countRooms FROM reservations as r
                join reserved_rooms as rr on rr.reservation_id = r.id
                where DATE(?) >= DATE(r.date_from) AND
                    DATE(?) < DATE(r.date_to) AND rr.room_id = ?;", [
            $date,//'2016-01-14'
            $date,//'2016-01-14'
            $room_type_id//1
        ]);

        return $countOccupancyRooms[0]->countRooms + $countReservedRooms[0]->countRooms;
    }

    public static function checkOccupancy($room_type_id, $date_from, $date_to, $count_occ_rooms, $occupancy_id = null)
    {
        //Get count rooms in the hodel
        $count_rooms = ComplexRooms::where('id', '=', $room_type_id)->first()->get(['count_rooms']);

        $roomOccupancy = null;
        //get exist occupancy
        if ($occupancy_id != null)
            $roomOccupancy = RoomOccupancy::find($occupancy_id);

        //Get all dates from start date to end date of occupancy
        $period = new \DatePeriod(
            new \DateTime($date_from),
            new \DateInterval('P1D'),
            new \DateTime($date_to)
        );

        //check every single date from start date to end date
        foreach ($period as $date) {

            $totalReservedRooms = self::countReservedRoomsForDate($room_type_id, $date->format("Y-m-d"));

            if ($roomOccupancy)
                $totalReservedRooms -= $roomOccupancy->count_occupancy_rooms;

            //Check is the day have enough room count
            if ((intval($count_rooms[0]->count_rooms) - $totalReservedRooms) < $count_occ_rooms)
                return ['status' => 'notEnoughRooms', 'date' => $date->format("d:m:Y")];
        }

        return ['status' => 'success'];
    }

    public static function createRoomOccupancy($data)
    {

        $validator = \Validator::make($data, self::getCreateRules(), self::getCreateMessages());

        if ($validator->fails())
            return array('validatorError', $validator);

//        $date_from = date("Y-m-d H:i:s", strtotime($data['date_from']));
        $dateArr = explode('/', $data['date_from']);
        $dateStr = $dateArr[2] . '-' . $dateArr[0] . '-' . $dateArr[1];
        $date_from = new \DateTime($dateStr);

//        $date_to = date("Y-m-d H:i:s", strtotime($data['date_to']));

        $dateArr = explode('/', $data['date_to']);
        $dateStr = $dateArr[2] . '-' . $dateArr[0] . '-' . $dateArr[1];
        $date_to = new \DateTime($dateStr);


        if ($date_from >= $date_to)
            return array('dateError', 'Началната дата е по-голяма или равна на крайната !');

        $canOccupancyRooms = self::checkOccupancy($data['room_id'], $data['date_from'], $data['date_to'], $data['count_occupancy_rooms']);

        if ($canOccupancyRooms['status'] != 'success')
            return ['notEnoughRooms', $canOccupancyRooms['date']];


        try {
            $roomOccupancy = new RoomOccupancy();
            $roomOccupancy->room_id = $data['room_id'];
            $roomOccupancy->date_from = $date_from;
            $roomOccupancy->date_to = $date_to;
            $roomOccupancy->count_occupancy_rooms = $data['count_occupancy_rooms'];
            $roomOccupancy->save();
        } catch (\Exception $ex) {
            return array('databaseError', $ex);
        }

        return array('successMessage', $roomOccupancy);

    }

    public static function updateRoomOccupancy($data)
    {

        $validator = \Validator::make($data, self::getUpdateRules(), self::getUpdateMessages());

        if ($validator->fails()) {
            return array('validatorError', $validator);
        }

//        $date_from = date("Y-m-d H:i:s", strtotime($data['date_from']));
        $dateArr = explode('/', $data['date_from']);
        $dateStr = $dateArr[2] . '-' . $dateArr[0] . '-' . $dateArr[1];
        $date_from = new \DateTime($dateStr);

//        $date_to = date("Y-m-d H:i:s", strtotime($data['date_to']));
        $dateArr = explode('/', $data['date_to']);
        $dateStr = $dateArr[2] . '-' . $dateArr[0] . '-' . $dateArr[1];
        $date_to = new \DateTime($dateStr);

        if ($date_from >= $date_to)
            return array('dateError', 'Началната дата е по-голяма или равна на крайната !');

        $canOccupancyRooms = self::checkOccupancy($data['room_id'], $data['date_from'], $data['date_to'], $data['count_occupancy_rooms'], $data['room_occupancy_id']);

        if ($canOccupancyRooms['status'] != 'success')
            return ['notEnoughRooms', $canOccupancyRooms['date']];

        try {
            $roomOccupancy = RoomOccupancy::find($data['room_occupancy_id']);
            $roomOccupancy->room_id = $data['room_id'];
            $roomOccupancy->date_from = $date_from;
            $roomOccupancy->date_to = $date_to;
            $roomOccupancy->count_occupancy_rooms = $data['count_occupancy_rooms'];
            $roomOccupancy->save();
        } catch (\Exception $ex) {
            return array('databaseError', $ex);
        }

        return array('successMessage', $roomOccupancy);
    }

    public static function deleteRoomOccupancy($id)
    {

        try {
            RoomOccupancy::destroy($id);
        } catch (\Exception $ex) {
            return 'databaseError';
        }

        return 'successMessage';

    }
}
