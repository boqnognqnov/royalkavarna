<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\ImageManagerStatic as Image;
use App\Classes\ImageProccessing;
use File;

/*Schema::create('complex_gallery', function (Blueprint $table) {
    $table->increments('id');
    $table->integer('complex_id', false, true);
    $table->foreign('complex_id')->references('id')->on('complexes');
    $table->string('image');
    $table->timestamps();
});*/

class ComplexGalery extends Model
{
    protected $table = 'complex_gallery';


//    public static $path = 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'complexes' . DIRECTORY_SEPARATOR . 'gallery' . DIRECTORY_SEPARATOR;
    public static $path = 'uploads/images/complexes/gallery/';
    public static $pathSmall = 'uploads/images/complexes/gallery/small/';


    public static function galleryRules()
    {
        return array(
            'complex_id' => 'required',
            'image' => 'required|image',
        );
    }

    public static function galleryMessages()
    {
        return [
            'complex_id.required' => 'Не е избран комплекс за редакция',
            'image.required' => 'Моля прикачете изображение',
            'image.image' => 'Файлът трябва да е изображение',

        ];
    }

    public static function setImage($data)
    {


        $validator = \Validator::make($data, self::galleryRules(), self::galleryMessages());

        if ($validator->fails()) {
            return array('validationError', $validator);
        }

        $lastPosition = 0;
        $lastRecord = ComplexGalery::orderBy('position', 'desc')->first();
        if ($lastRecord) {
            $lastPosition = $lastRecord->position;
        }

        $image = new ComplexGalery();


        try {
            $time = time();
            $fileName = $time . '.' . $data['image']->getClientOriginalExtension();
//            Image::make($data['image']->getRealPath())->save(self::$path . $fileName);

            Image::make($data['image']->getRealPath())->save(self::$path . $fileName);
            Image::make($data['image']->getRealPath())->save(self::$pathSmall . $fileName);

//            Image::make($data['image']->getRealPath())->resize(1920, 600)->save(self::$path . $fileName);
//            Image::make($data['image']->getRealPath())->resize(864, 270)->save(self::$pathSmall . $fileName);


            $image->complex_id = $data['complex_id'];
            $image->image = $fileName;
            $image->position = $lastPosition + 1;

            $image->save();

        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('creatingError', $ex);
        }

        return array('successMessage', $image);

    }

    public static function destroyImage($image_id)
    {
//        $status = ImageProccessing::destroyImage($image_id, ComplexGalery::class);
        $status = true;

        $fileName = ComplexGalery::find($image_id)->image;

        try {
            File::delete(ComplexGalery::$pathSmall . $fileName);
            File::delete(ComplexGalery::$path . $fileName);

            ComplexGalery::destroy($image_id);
        } catch (\Exception $ex) {
            \Log::error($ex);
            $status = false;
        }


        if ($status == true) {
            return array('status' => 'success');
        } else {
            return array('status' => 'fail');
        }


    }

}
