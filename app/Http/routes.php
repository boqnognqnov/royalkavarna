<?php

Route::group(['middleware' => 'web'], function () {

    Route::auth();

    Route::get('lang/{locale?}', 'LangController@chaingeLang');

    Route::get('/', 'HomePageController@index');

    Route::get('room/{room_id}', 'RoomsEntityController@index');

    Route::get('home', 'HomePageController@index');
    Route::get('about', 'AboutController@about');

    Route::get('contacts', 'AboutController@contact');
    Route::post('contacts/send_mail', 'AboutController@contactSendMessage');

    Route::get('complex/{id}', 'ComplexesEntityController@show');
    Route::get('complex/{id}/prices', 'ComplexesEntityController@pricelist');
    Route::get('complex/{id}/promotions', 'ComplexesEntityController@promotions');
    Route::get('complex/{id}/guestbook', 'ComplexesEntityController@guestbook');
    Route::post('guestbook/add', 'GuestBooksController@addPosts');
    Route::get('presentations', 'PresentationController@presentations');
    Route::get('test', function () {
        return view('theme-test');
    });

    Route::get('session', function () {
        \Session::forget('new_reservation');
        return redirect()->back();
    });
    Route::get('reservation/step_3/add_row_event/{room_id}', 'ReservationsEntityController@step3addRowEventEvent');

    Route::get('reservation/all_complexes', 'ReservationsEntityController@getAllComplexes');
    Route::get('reservation/complex/{complex_id?}/{room_id?}', 'ReservationsEntityController@selectComplex');
    Route::get('reservation/date/{isOnNav?}/{complex_id?}/{date_from?}/{date_to?}', 'ReservationsEntityController@setDateRange');
    Route::get('reservation/personal_data/{typeOfRedirect?}', 'ReservationsEntityController@getPersonalData');
    Route::post('reservation/personal_data', 'ReservationsEntityController@setPersonalData');
    Route::get('reservation/confirm', 'ReservationsEntityController@showReservation');
    Route::post('reservation/confirm', 'ReservationsEntityController@confirmReservation');

//    API
    Route::post('/api/reservation/step3/getPrices', 'Api\ReservationsApi@sendPricePerRow');
    Route::post('/api/reservation/step3/storeSession', 'Api\ReservationsApi@roomsReservation');
    Route::post('/api/reservation/get_free_rooms', 'Api\GetFreeRoomsApi@sendFreeRooms');
    Route::post('/api/reservation/get_price_per_room', 'Api\ReservationsApi@adminGetPricePerRow');
    Route::post('/api/reservation/checkForOpenSession', 'Api\GetSessionStep3@getSessionStep3IfExist');


    Route::group(['middleware' => 'auth'], function () {

        Route::post('/api/getRoomExtras', 'Api\GetRoomExtras@getExtras');
        Route::post('setRoomExtras', 'ComplexRoomController@updateRoomExtras');


        Route::get('admin', 'HomeEntityController@index');
        Route::get('admin/about', 'AboutController@indexAdmin');
        Route::post('admin/about', 'AboutController@updateAbout');
        Route::get('admin/home', 'HomeEntityController@index');

        Route::get('admin/rooms', 'RoomsEntityController@index');
        Route::get('admin/complexes/room/{room_id}/gallery', 'RoomsEntityController@indexAdmin');
        Route::post('admin/complexes/room/setImage', 'RoomsEntityController@setImageToGallery');

        Route::post('admin/complexes/room/gallery/destroy', 'RoomsEntityController@destroyImage');
        Route::get('admin/complexes/room/gallery/{room_id}/left/{image_id}', 'RoomsEntityController@moveImageLeft');
        Route::get('admin/complexes/room/gallery/{room_id}/right/{image_id}', 'RoomsEntityController@moveImageRight');


        Route::get('admin/reservations', 'ReservationsEntityController@indexAdminListComplex');
        Route::get('admin/reservations/complex/{complex_id?}', 'ReservationsEntityController@indexAdminByComplex');
        Route::get('admin/reservations/complex/{complex_id?}/archive', 'ReservationsEntityController@oldReservations');
        Route::post('admin/reservations/statusAndMessage', 'ReservationsEntityController@statusAndMessage');
        Route::post('admin/reservations/delete', 'ReservationsEntityController@deleteReservation');
        Route::get('admin/reservations/update/{reservation_id}', 'ReservationsEntityController@adminShowUpdateForm');
        Route::post('admin/reservations/update_personal_data', 'ReservationsEntityController@updatePersonalData');
        Route::post('admin/reservations/update_rooms_and_date', 'ReservationsEntityController@updateRoomsAndDate');


        Route::get('admin/extras', 'ExtrasEntityController@indexAdmin');
        Route::post('/admin/extras/store', 'ExtrasEntityController@setExtras');
        Route::post('/admin/extras/update', 'ExtrasEntityController@updateExtra');
        Route::post('/admin/extras/destroy', 'ExtrasEntityController@destroyExtras');

        Route::get('admin/complexes', 'ComplexesEntityController@index');
        Route::get('admin/complexes/create', 'ComplexesEntityController@create');
        Route::post('/admin/complexes/create', 'ComplexesEntityController@store');
        Route::get('admin/complexes/edit/{id}', 'ComplexesEntityController@edit');
        Route::post('admin/complexes/edit', 'ComplexesEntityController@update');
        Route::post('admin/complexes/delete', 'ComplexesEntityController@destroy');

//        COMPLEX GALLERY
        Route::get('admin/complexes/gallery/{complex_id}', 'ComplexesEntityController@complexGallery');
        Route::post('admin/complexes/gallery/store', 'ComplexesEntityController@setImageToGallery');
        Route::post('admin/complexes/gallery/destroy', 'ComplexesEntityController@destroyImage');

        Route::get('admin/complexes/gallery/{complex_id}/left/{image_id}', 'ComplexesEntityController@moveImageLeft');
        Route::get('admin/complexes/gallery/{complex_id}/right/{image_id}', 'ComplexesEntityController@moveImageRight');


        //COMPLEX ROOMS
        Route::get('/admin/complexes/{complex_id}/rooms', 'ComplexRoomController@index');
        Route::get('/admin/complexes/{complex_id}/rooms/create', 'ComplexRoomController@create');
        Route::post('/admin/complexes/{complex_id}/rooms/create', 'ComplexRoomController@store');
        Route::get('/admin/complexes/{complex_id}/rooms/{room_id}/edit', 'ComplexRoomController@edit');
        Route::post('/admin/complexes/{complex_id}/rooms/{room_id}/edit', 'ComplexRoomController@update');
        Route::post('/admin/complexes/{complex_id}/rooms/{room_id}/destroy', 'ComplexRoomController@destroy');

        Route::get('admin/facilities', 'FacilitiesEntityController@index');
        Route::get('admin/facilities/create', 'FacilitiesEntityController@create');
        Route::post('/admin/facilities/create', 'FacilitiesEntityController@store');
        Route::get('admin/facilities/edit/{id}', 'FacilitiesEntityController@edit');
        Route::post('/admin/facilities/edit', 'FacilitiesEntityController@update');
        Route::post('/admin/facilities/delete/{id}', 'FacilitiesEntityController@destroy');

//        EARLY BOOKING
        Route::get('/admin/complexes/promotions/{complex_id}', 'ComplexPromotionsController@indexAdmin');
        Route::post('/admin/complexes/promotions/store', 'ComplexPromotionsController@setPromotion');
        Route::post('/admin/complexes/promotions/setActivity', 'ComplexPromotionsController@changeActivityPromotion');
        Route::post('/admin/complexes/promotions/destroy', 'ComplexPromotionsController@deletePromotion');

        Route::get('admin/statuses', 'ReservationsStatusesEntityController@index');
        Route::get('admin/statuses/create', 'ReservationsStatusesEntityController@create');
        Route::post('/admin/statuses/create', 'ReservationsStatusesEntityController@store');
        Route::get('admin/statuses/edit/{id}', 'ReservationsStatusesEntityController@edit');
        Route::post('admin/statuses/edit', 'ReservationsStatusesEntityController@update');
        Route::get('admin/statuses/delete/{id}', 'ReservationsStatusesEntityController@destroy');

        //ROOMS PRICES
        Route::get('/admin/complexes/{complex_id}/rooms/{room_id}/prices', 'RoomsPricesEntityController@index');
        Route::get('admin/prices/create', 'RoomsPricesEntityController@create');
        Route::post('/admin/prices/create', 'RoomsPricesEntityController@store');
        Route::get('admin/prices/edit/{price_id}', 'RoomsPricesEntityController@edit');
        Route::post('admin/prices/edit', 'RoomsPricesEntityController@update');
        Route::get('admin/prices/delete/{id}', 'RoomsPricesEntityController@destroy');


//        GUEST BOOKS
        Route::get('admin/guest_books', 'GuestBooksController@indexAdmin');

        Route::post('admin/guest_books/approve', 'GuestBooksController@approvePost');
        Route::post('admin/guest_books/delete', 'GuestBooksController@deletePost');

        //ROOM OCCUPANCIES
        Route::get('/admin/complexes/{complex_id}/rooms/{room_id}/occupancies', 'RoomOccupancyController@index');
        Route::get('/admin/complexes/{complex_id}/rooms/{room_id}/occupancies/archive', 'RoomOccupancyController@archive');
        Route::post('/admin/reserve/room', 'RoomOccupancyController@store');
        Route::post('/admin/reserve/room/update', 'RoomOccupancyController@update');
        Route::post('/admin/reserve/room/update/destroy', 'RoomOccupancyController@destroy');

        //Complex Promotions
        Route::get('/admin/complexes/{complex_id}/promotions', 'PromotionsController@index');
        Route::get('/admin/complexes/{complex_id}/promotions/create', 'PromotionsController@create');
        Route::post('/admin/complexes/{complex_id}/promotions/create', 'PromotionsController@store');
        Route::get('/admin/complexes/{complex_id}/promotions/{promotion_id}/update', 'PromotionsController@edit');
        Route::post('/admin/complexes/{complex_id}/promotions/{promotion_id}/update', 'PromotionsController@update');
        Route::post('/admin/complexes/{complex_id}/promotions/{promotion_id}/destroy', 'PromotionsController@destroy');

        //Presentations
        Route::get('/admin/presentations', 'PresentationController@indexAdmin');
        Route::post('/admin/presentations/store', 'PresentationController@addPresentation');
        Route::get('/admin/presentations/{id}/destroy', 'PresentationController@destroyPresentation');
        Route::post('/admin/presentations/active/{id}', 'PresentationController@changeStatus');
        //SLIDERS
        Route::get('/admin/sliders', 'SlidersEntityController@indexAdmin');
        Route::get('/admin/sliders/create', 'SlidersEntityController@createSlider');
        Route::get('/admin/sliders/edit/{slider_id}', 'SlidersEntityController@editSlider');
        Route::post('/admin/sliders/update', 'SlidersEntityController@update');
        Route::post('/admin/sliders/store', 'SlidersEntityController@store');
        Route::post('/admin/sliders/delete', 'SlidersEntityController@destroy');
        Route::post('/admin/sliders/setActivity', 'SlidersEntityController@setActivity');
    });
});
