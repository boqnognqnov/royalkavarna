<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Session;
use App;
use Config;

class Locale
{

    public function handle($request, Closure $next)
    {
        $language = Session::get('locale', Config::get('app.locale'));

        App::setLocale($language);

        return $next($request);
    }
}
