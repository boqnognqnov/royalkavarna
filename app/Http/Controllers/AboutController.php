<?php

namespace App\Http\Controllers;

use App\About;
use App\Complex;
use App\ComplexGalery;
use App\Extras;
use Illuminate\Support\Facades\Input;
//use App\Http\Requests;
use Illuminate\Http\Request;

class AboutController extends Controller
{


    public function indexAdmin()
    {
        $about = About::first();

        return view('admin.list.about', ['about' => $about]);
    }

    public function about()
    {
        return view('about');
    }

    public function contact()
    {
        return view('contacts');
    }

    public function contactSendMessage(Request $request)
    {

        $data['email'] = $request->get('email');
        $data['text'] = $request->get('text_message');
        $data['name'] = $request->get('name');

        if (empty($data['name']) || empty($data['email']) || empty($data['text'])) {
            return back()->withInput()->withErrors('error', trans('contacts.emptyInputs'));
        }


        $emails['general'] = About::first()->contact_general_email;
        $emails['byComplexes'] = [];

        $allEmailsbyComplexes = Complex::orderBy('id')
            ->select('complexes.email')
            ->get()
            ->toArray();

        foreach ($allEmailsbyComplexes as $key => $oneEmail) {
            $emails['byComplexes'][$key] = $oneEmail['email'];
        }

        try {
            if (!empty($data['email'])) {
                \Mail::send('emails.requestMessage', $data, function ($message) use ($data, $emails) {
                    $message->from($data['email'], $data['name']);
                    $message->to($emails['general'], 'Info')->subject('Оставено съобщение от "контакт формата" на Royal Estate');
                    foreach ($emails['byComplexes'] as $oneEmail) {
//                        dump($oneEmail);
                        $message->bcc($oneEmail, 'Info')->subject('Оставено съобщение от "контакт формата" на Royal Estate');
                    }

//                    $message->to('boqnognqnov@gmail.com', 'Info')->subject('Оставено съобщение от "контакт формата" на Royal Estate');
//                    $message->bcc('bozhidarovboyan@gmail.com', 'Info')->subject('Оставено съобщение от "контакт формата" на Royal Estate');
//                    $message->bcc('alarma7a@gmail.com', 'Info')->subject('Оставено съобщение от контакт формата на Royal Estate');
                });
            }
        } catch (\Exception $ex) {
            return back()->withErrors(trans('contacts.errorException'));
        }
        return redirect()->back()->with('successMessage', trans('contacts.successMessage'));


    }


    public function updateAbout(Request $request)
    {
        $data = $request->all();
        $status = About::updateAboutInformation($data);


        switch ($status[0]) {
            case 'validatorError':
                return back()->withInput()->withErrors($status[1]);
            case 'creatingError':
                return back()->withInput()->withErrors('Грешка при запис в базата');
            case 'imageFailed':
                return back()->withInput()->withErrors('Грешка при запис на изображението');
            case 'successMessage':
                return redirect()->back()->with('successMessage', 'Успешен запис');
            default:
                return back()->withInput()->withErrors('RunTime Error');
        }
    }

}
