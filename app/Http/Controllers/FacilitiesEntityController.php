<?php namespace App\Http\Controllers;

    use App\Complex;
    use App\ComplexRooms;
    use App\Facilities;
    use Illuminate\Http\Request;
    use App\Http\Requests;

    class FacilitiesEntityController extends Controller {

        public function __construct() {
            $this -> middleware('auth');
        }

        public function index() {

            $facilitiesData = Facilities::join('complexes', 'complexes.id', '=', 'facilities.complex_id')
                ->orderBy('complex_id')->get(['facilities.*', 'complexes.name_bg as complex_name']);

            return view( 'admin.facilities', [
                'facilitiesData' => $facilitiesData
            ] );

        }

        public function create() {

            $complexes      = Complex::all();
            $complexesSelect = [];
            foreach( $complexes as $complex ) {
                $complexesSelect[ $complex -> id ]    = $complex -> name_bg;
            }

            return view( 'admin.create.facilities', [
                'complexesSelect' => $complexesSelect
            ] );
        }

        public function store( Request $request ) {

            $inputData = $request -> all();

            $tryCreateFacilities = Facilities::createFacilities( $inputData );

            switch( $tryCreateFacilities[0] ) {
                case 'validatorError':
                    return back() -> withInput() -> withErrors( $tryCreateFacilities[1] );
                case 'databaseError':
                    return back() -> withInput() -> withErrors( 'Данните не бяха добавени успешно, моля опитайте пак.' );
                case 'successMessage':
                    return redirect('/admin/facilities') -> with( 'successMessage', 'Данните са добавени успешно.' );
                default:
                    return back() -> withInput() -> withErrors( 'Проблем, моля опитай те по късно!' );
            }

        }

        public function edit( $id ) {

            $facilitiesData = Facilities::where( 'id', '=', $id ) -> first();
            $complexes      = Complex::all();
            $complexesSelect = [];
            foreach( $complexes as $complex ) {
                $complexesSelect[ $complex -> id ]    = $complex -> name_bg;
            }

            return view( 'admin.update.facilities', [
                'facilitiesData'    => $facilitiesData,
                'complexesSelect'       => $complexesSelect
            ] );

        }

        public function update( Request $request ) {

            $inputData = $request -> all();

            $tryCreateFacilities = Facilities::updateFacilities( $inputData );

            switch( $tryCreateFacilities[0] ) {
                case 'validatorError':
                    return back() -> withInput() -> withErrors( $tryCreateFacilities[1] );
                case 'databaseError':
                    return back() -> withInput() -> withErrors( 'Данните не бяха редактирани успешно, моля опитайте пак.' );
                case 'successMessage':
                    return redirect('/admin/facilities')-> with( 'successMessage', 'Данните са редактирани успешно.' );
                default:
                    return back() -> withInput() -> withErrors( 'Проблем, моля опитай те по късно!' );
            }

        }

        public function destroy( Request $request ) {

            $inputData = $request -> get('id');

            $tryDeleteFacilities = Facilities::deleteFacilities( $inputData );

            switch ( $tryDeleteFacilities ) {
                case 'databaseError':
                    return back() -> withInput() -> withErrors( 'Проблем, моля опитай те по късно!' );
                case 'successMessage':
                    return redirect() -> back() -> with( 'successMessage', 'Данните са изтрити успешно!' );
                default:
                    return back() -> withInput() -> withErrors( 'Проблем, моля опитай те по късно!' );
            }

        }

    }
