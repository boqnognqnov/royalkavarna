<?php

namespace App\Http\Controllers;

use App\ComplexGalery;
use App\Extras;
use Illuminate\Support\Facades\Input;
//use App\Http\Requests;
use Illuminate\Http\Request;

class ExtrasEntityController extends Controller
{


    public function indexAdmin()
    {
        $extras = Extras::all();

        return view('admin.list.extras', ['extras' => $extras]);
    }

    public function setExtras(Request $request)
    {
        $data = $request->all();
        $status = Extras::addExtra($data);
        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'creatingError':
                return back()->withInput()->withErrors('Грешка при запис в базата');
            case 'imageFailed':
                return back()->withInput()->withErrors('Грешка при запис на изображението');
            case 'successMessage':
                return redirect()->back()->with('successMessage', 'Успешен запис');

        }
    }


    public function updateExtra(Request $request)
    {
        $data = $request->all();
        $status = Extras::updateExtra($data);

        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'creatingError':
                return back()->withInput()->withErrors('Грешка при запис в базата');
            case 'imageFailed':
                return back()->withInput()->withErrors('Грешка при запис на изображението');
            case 'successMessage':
                return redirect()->back()->with('successMessage', 'Успешен запис');

        }
    }

    public function destroyExtras(Request $request)
    {
        $extra_id = $request->get('extra_id');
        $status = Extras::destroyExtras($extra_id);
        if ($status == true) {
            return redirect()->back()->with('successMessage', 'Успешно изтрихте записа');
        }
        return back()->withErrors('Грешка при изтриване на записа');
    }

}
