<?php namespace App\Http\Controllers;

use App\Classes\GlobalFunctions;
use App\Complex;
use App\ComplexPromotions;
use App\ComplexRooms;
use App\RoomOccupancy;
use Illuminate\Http\Request;
use App\Slider;

class PromotionsController extends Controller
{


    // Route::get('/admin/complexes/{complex_id}/promotions', 'PromotionsController@index');
    public function index($complex_id)
//        ADMIN
    {
        $promotions = ComplexPromotions::where('complex_id', '=', $complex_id)->get();
        $complex = Complex::find($complex_id);


        return view('admin.list.complexpromotions', [
            'complex' => $complex,
            'promotions' => $promotions
        ]);
    }

    // Route::get('/admin/complexes/{complex_id}/promotions/create', 'PromotionsController@create');
    public function create($complex_id)
    {
        $complex = Complex::find($complex_id);

        return view('admin.create.promotions', [
            'complex' => $complex
        ]);
    }

    // Route::post('/admin/complexes/{complex_id}/promotions/create', 'PromotionsController@store');
    public function store(Request $requests)
    {
        $inputData = $requests->all();

        $tryAddComplexPromotion = ComplexPromotions::createPromotion($inputData);

        switch ($tryAddComplexPromotion[0]) {
            case 'validatorError':
                return back()->withInput()->withErrors($tryAddComplexPromotion[1]);
            case 'databaseError':
                return back()->withInput()->withErrors('Данните не бяха добавени успешно, моля опитайте пак.');
            case 'dateError':
                return back()->withInput()->withErrors([$tryAddComplexPromotion[1]]);
            case 'successMessage':
                return redirect('/admin/complexes/' . $tryAddComplexPromotion[1]->complex_id . '/promotions')->with('successMessage', 'Данните са добавени успешно.');
            case 'notEnoughRooms':
                return back()->withInput()->withErrors('Няма толкова свободни стаи за този период от време');
            default:
                return back()->withInput()->withErrors('Проблем, моля опитайте по късно!');
        }
    }

    // Route::get('/admin/complexes/{complex_id}/promotions/{promotion_id}/update', 'PromotionsController@edit');
    public function edit($complex_id, $promotion_id)
    {
        $complex = Complex::find($complex_id);
        $complexPromotion = ComplexPromotions::find($promotion_id);

        $complexPromotion->date_from = GlobalFunctions::generateDateTimeToStr($complexPromotion->date_from);
        $complexPromotion->date_to = GlobalFunctions::generateDateTimeToStr($complexPromotion->date_to);

//        return dump($complexPromotion);

        return view('admin.update.promotions', [
            'complex' => $complex,
            'complexPromotion' => $complexPromotion
        ]);
    }

    // Route::post('/admin/complexes/{complex_id}/promotions/{promotion_id}/update', 'PromotionsController@update');
    public function update(Request $requests)
    {
        $inputData = $requests->all();

        $tryUpdateComplexPromotion = ComplexPromotions::updatePromotion($inputData);

        switch ($tryUpdateComplexPromotion[0]) {
            case 'validatorError':
                return back()->withInput()->withErrors($tryUpdateComplexPromotion[1]);
            case 'databaseError':
                return back()->withInput()->withErrors('Данните не бяха редактирани успешно, моля опитайте пак.');
            case 'dateError':
                return back()->withInput()->withErrors([$tryUpdateComplexPromotion[1]]);
            case 'successMessage':
                return redirect('/admin/complexes/' . $tryUpdateComplexPromotion[1]->complex_id . '/promotions')->with('successMessage', 'Данните са редактирани успешно.');
            case 'notEnoughRooms':
                return back()->withInput()->withErrors('Няма толкова свободни стаи за този период от време');
            default:
                return back()->withInput()->withErrors('Проблем, моля опитай те по късно!');
        }
    }


    // Route::post('/admin/complexes/{complex_id}/promotions/{promotion_id}/destroy', 'PromotionsController@destroy');
    public function destroy(Request $request)
    {
        $id = $request->get('promotion_id');

        $tryDeleteRoomOccupancy = ComplexPromotions::deletePromotion($id);

        switch ($tryDeleteRoomOccupancy) {
            case 'databaseError':
                return back()->withInput()->withErrors('Проблем, моля опитай те по късно! DB error : 95');
            case 'successMessage':
                return redirect()->back()->with('successMessage', 'Данните са изтрити успешно!');
            default:
                return back()->withInput()->withErrors('Проблем, моля опитай те по късно!');
        }
    }
}