<?php

namespace App\Http\Controllers;

use App\About;
use App\ComplexGalery;
use App\Extras;
use App\Presentation;
use Illuminate\Support\Facades\Input;
//use App\Http\Requests;
use Illuminate\Http\Request;

class PresentationController extends Controller
{


    public function indexAdmin()
    {
        $presentations = Presentation::all();

        return view('admin.list.presentations', ['presentations' => $presentations]);
    }

    public function presentations()
    {
        $presentations = Presentation::where('is_active','=',1)->get()->toArray();
        return view('presentations', ['presentations' => $presentations]);
    }

    public function changeStatus($id){

        $presentation = Presentation::find($id);
        if ($presentation == null)
            return back()->withErrors('Не е намерена такава презентация');

        $presentation->is_active = ! $presentation->is_active;
        $presentation->save();

        return redirect()->back()->with('successMessage', 'Успешно променихте статуса');
    }

    public function addPresentation(Request $request)
    {
        $data = $request->all();
        $status = Presentation::addPresentation($data);

        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'creatingError':
                return back()->withInput()->withErrors('Грешка при запис в базата');
            case 'imageFailed':
                return back()->withInput()->withErrors('Грешка при запис на изображението');
            case 'successMessage':
                return redirect()->back()->with('successMessage', 'Успешен запис');
            default:
                return back()->withInput()->withErrors('RunTime Error');
        }
    }

    public function destroyPresentation($id)
    {
        $status = Presentation::destroyPresentation($id);
        if ($status[0] == 'destroySuccess') {
            return redirect()->back()->with('successMessage', 'Успешно изтрихте записа');
        }
        return back()->withErrors('Грешка при изтриване на записа');
    }
}
