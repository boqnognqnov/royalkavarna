<?php

namespace App\Http\Controllers;

use App\EarlyBooking;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ComplexPromotionsController extends Controller
{
    public function indexAdmin($complex_id)
    {
        $promotions = EarlyBooking::where('complex_id', '=', $complex_id)->get();
//        return dump($promotions);
        return view('admin.list.promotions', ['promotions' => $promotions, 'complex_id' => $complex_id]);
    }


    public function setPromotion(Request $request)
    {
        $data = $request->all();


//        return dump($d);
        $status = EarlyBooking::setPromotion($data);
//        return dump($status);
        return redirect()->back();
    }

    public function deletePromotion(Request $request)
    {
        $id = $request->get('promotion_id');
        $status = EarlyBooking::destroyPromotion($id);
//        return dump($status);
        return redirect()->back();

    }

    public function changeActivityPromotion(Request $request)

    {
        $promotion = EarlyBooking::find($request->get('promotion_id'));
        $promotion->is_active = ! $promotion->is_active;
        $promotion->save();

        return redirect()->back();

    }
}
