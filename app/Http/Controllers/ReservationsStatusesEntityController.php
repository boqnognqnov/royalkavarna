<?php

namespace App\Http\Controllers;

use App\ReservationStatuses;
use Illuminate\Http\Request;
use App\Http\Requests;

class ReservationsStatusesEntityController extends Controller {

//    public function __construct() {
//        $this -> middleware('auth');
//    }

    public function index() {

        $statusData = ReservationStatuses::all();

        return view( 'admin.statuses', [
            'statusData' => $statusData
        ] );

    }

    public function create() {
        return view( 'admin.create.statuses' );
    }

    public function store( Request $request ) {

        $inputData = $request -> all();

        $tryCreateStatus = ReservationStatuses::createStatus( $inputData );

        switch( $tryCreateStatus[0] ) {
            case 'validatorError':
                return back() -> withInput() -> withErrors( $tryCreateStatus[1] );
            case 'databaseError':
                return back() -> withInput() -> withErrors( 'Данните не бяха добавени успешно, моля опитайте пак.' );
            case 'successMessage':
                return redirect() -> back() -> with( 'successMessage', 'Данните са добавени успешно.' );
            default:
                return back() -> withInput() -> withErrors( 'Проблем, моля опитай те по късно!' );
        }

    }

    public function edit( $id ) {

        $statusData = ReservationStatuses::where( 'id', '=', $id ) -> first();

        return view( 'admin.update.statuses', [
            'statusData' => $statusData
        ] );

    }

    public function update( Request $request ) {

        $inputData = $request -> all();

        $tryCreateStatus = ReservationStatuses::updateStatus( $inputData );

        switch( $tryCreateStatus[0] ) {
            case 'validatorError':
                return back() -> withInput() -> withErrors( $tryCreateStatus[1] );
            case 'databaseError':
                return back() -> withInput() -> withErrors( 'Данните не бяха редактирани успешно, моля опитайте пак.' );
            case 'successMessage':
                return redirect() -> back() -> with( 'successMessage', 'Данните са редактирани успешно.' );
            default:
                return back() -> withInput() -> withErrors( 'Проблем, моля опитай те по късно!' );
        }

    }

    public function destroy( $id ) {

        $tryDeleteStatus = ReservationStatuses::deleteStatus( $id );

        switch ( $tryDeleteStatus ) {
            case 'databaseError':
                return back() -> withInput() -> withErrors( 'Проблем, моля опитай те по късно!' );
            case 'successMessage':
                return redirect() -> back() -> with( 'successMessage', 'Данните са изтрити успешно!' );
            default:
                return back() -> withInput() -> withErrors( 'Проблем, моля опитай те по късно!' );
        }

    }

}
