<?php namespace App\Http\Controllers\Api;

use App\Classes\GlobalFunctions;
use App\Complex;
use App\ComplexRooms;
use App\EarlyBooking;
use Illuminate\Http\Request;

use App\Reservations;
use App\ReservedRooms;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class GetSessionStep3 extends Controller
{

    public function getSessionStep3IfExist()
    {
        $session = \Session::get('new_reservation');
        if (isset($session['rooms_data'])) {

            $roomsData = $session['rooms_data'];

            return \Response::json(array(
                'status' => 'success',
                'data' => $roomsData
            ), 200);
        } else {
            return \Response::json(array(
                'status' => 'noSession'), 400);

        }


    }


}
