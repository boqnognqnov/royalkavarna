<?php namespace App\Http\Controllers\Api;

use App\Classes\GlobalFunctions;
use App\Complex;
use App\ComplexRooms;
use App\EarlyBooking;
use App\RoomOccupancy;
use Illuminate\Http\Request;

use App\Reservations;
use App\ReservedRooms;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class GetFreeRoomsApi extends Controller
{
    public function sendFreeRooms()
    {
        $data = Input::all();

        $roomId = $data['room_id'];
        $dateFrom = $data['date_from'];
        $dateTo = $data['date_to'];


        $period = new \DatePeriod(
            new \DateTime($dateFrom),
            new \DateInterval('P1D'),
            new \DateTime($dateTo)
        );

        $availableRoomsArr = array();
        $tempArr = array();


        $count_rooms = ComplexRooms::find($roomId)->count_rooms;

        foreach ($period as $date) {
            $totalReservedRooms = RoomOccupancy::countReservedRoomsForDate($roomId, $date->format("Y-m-d"));

            array_push($tempArr, $totalReservedRooms);
        }

        sort($tempArr);
        $availableRoomsArr['freeRooms'] = $count_rooms - end($tempArr);


        $dataToReturn = ['sadsa' => 'asdsadsad', 'asdsa' => 'asdsad'];
//        return dump($dataToReturn);
        return \Response::json(array(
            'status' => 'success',
            'data' => $availableRoomsArr
        ), 200);


    }


}
