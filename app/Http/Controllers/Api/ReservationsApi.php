<?php namespace App\Http\Controllers\Api;

use App\Classes\GlobalFunctions;
use App\Complex;
use App\ComplexRooms;
use App\EarlyBooking;
use Illuminate\Http\Request;

use App\Reservations;
use App\ReservedRooms;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class ReservationsApi extends Controller
{

    public function makeReservation()
    {

    }


    public function sendPricePerRow()
    {
        $data = Input::all();


        $gb = new GlobalFunctions();
        $dataToReturn = $gb->calculateReservationPriceByRow($data);

        $early_booking = GlobalFunctions::calculateEarlyBooking($dataToReturn['complex_id']);

//        return dump($dataToReturn);
        return \Response::json(array(
            'status' => 'success',
            'data' => $dataToReturn,
            'early_booking' => $early_booking
        ), 200);


    }


    public function isPercentage($room_id)
    {
        $complexId = ComplexRooms::find($room_id)->complex_id;
        $isPercentage = Complex::find($complexId)->is_percentage;
        return $isPercentage;
    }


    public function getDataFromReservationForm($data, $reservationSession)
    {
        $price = array();

        foreach ($data as $room_id => $oneRoomType) {
            foreach ($oneRoomType as $rowKey => $oneRow) {
                $childs = $oneRow['child'];
                $adults = $oneRow['adult'];


                $dateReservationArr['date_from'] = $reservationSession['date_from'];
                $dateReservationArr['date_to'] = $reservationSession['date_to'];

                $priceForOneRow = $this->calculatePriceByRow($reservationSession['complex_id'], $room_id, $childs, $adults, $dateReservationArr);

                $price[$room_id][$rowKey]['real_price'] = $priceForOneRow;

                $earlyBooking = GlobalFunctions::calculateEarlyBooking($reservationSession['complex_id']);

                $percentEarlyBooking = $earlyBooking['percentage'];

                $price[$room_id][$rowKey]['early_booking'] = $priceForOneRow * ((100 - $percentEarlyBooking) / 100);


            }
        }
        return $price;
    }

    public function adminGetPricePerRow()
    {
        $dataArr = Input::all();
//        return dump($dataArr);
        $data = $dataArr['data'];
        unset($dataArr['data'], $dataArr['_token']);

        $gb = new GlobalFunctions();
        $dataToReturn = $gb->calculateReservationPriceByRow($data, true, $dataArr);

        $dataToRecord['complex_id'] = $dataArr['complex_id'];
        $dataToRecord['date_from'] = $dataArr['date_from'];
        $dataToRecord['date_to'] = $dataArr['date_to'];
        $dataToRecord['rooms_data'] = $dataToReturn['data'];
        $dataToRecord['total'] = $dataToReturn['total'];

//        return dump($dataToRecord);


        return \Response::json(array(
            'status' => 'success',
            'data' => $dataToRecord
        ), 200);

    }


    public function roomsReservation(Request $request)
    {

        $data = Input::all();


        $data = $this->checkForEmptyRows($data);


        $reservationSession = \Session::get('new_reservation');
//        return dump($reservationSession);
        $complex = Complex::find($reservationSession['complex_id']);

        $prices = $this->getDataFromReservationForm($data, $reservationSession);


        $RoomType = 'type_' . \App::getLocale();
        foreach ($data as $roomId => $oneRoomType) {
            $roomName = ComplexRooms::find($roomId)->$RoomType;
            foreach ($oneRoomType as $keyRow => $row) {
                $data[$roomId][$keyRow]['extra'] = GlobalFunctions::getExtraBeds($roomId, $row['child'], $row['adult']);
                $data[$roomId][$keyRow]['is_percentage'] = $this->isPercentage($roomId);
                $data[$roomId][$keyRow]['extra_bed_price'] = $complex->extra_bed_price;
                $data[$roomId][$keyRow]['price'] = $prices[$roomId][$keyRow]['early_booking'];
                $data[$roomId][$keyRow]['room_type'] = $roomName;

            }
        }

        $reservationSession['rooms_data'] = $data;
        $reservationSession['early_booking'] = GlobalFunctions::calculateEarlyBooking($reservationSession['complex_id']);
//        $reservationSession['price_by_room_type'] = $prices;
        $gb = new GlobalFunctions();


        $dataFromTotalPrice = $gb->calculateTotalPrice($prices, $reservationSession);
        $reservationSession['total'] = $dataFromTotalPrice['total'];
        $reservationSession['days'] = $dataFromTotalPrice['days'];

//        return dump($prices);
        $status = $this->checkForToManyHumans($prices);


        if ($status == false) {
            $msg = trans('reservationValidation.tooManyPeople');


            return \Response::json(array(
                'status' => 'fail',
                'data' => $msg,
            ), 400);
        }
        \Session::put('new_reservation', $reservationSession);

        return \Response::json(array(
            'status' => 'success',
//            'data' => $data
        ), 200);

    }

    private function checkForToManyHumans($reservationData)
    {
        $status = true;
        foreach ($reservationData as $oneRoom) {
            foreach ($oneRoom as $oneRow) {
                if ($oneRow['real_price'] === 'tooMany') {
                    $status = false;
                }
            }
        }
        return $status;
    }

    private function calculatePriceByRow($complex_id, $room_id, $childs, $adults, $dateReservationArr)
    {

        $pricePerDay = GlobalFunctions::calculatePricePerDay($dateReservationArr, $room_id);

        $priceToReturn = 0;

        $complex = Complex::find($complex_id);
        $room = ComplexRooms::find($room_id);

        $extraBedValue = $complex->extra_bed_price;

        $beds = $room->beds;
        $extra_beds = $room->extra_beds;

        if ($beds >= $childs + $adults || $complex->is_inseparable) {
//            NO EXTRA BEDS

            $priceToReturn = $pricePerDay;
        } else if ($extra_beds >= (($childs + $adults) - $beds)) {

            if ($childs == 0) {
//                ONLY ADULTS

                $extraAdults = ($adults - $beds);

                $priceToReturn = $pricePerDay + $this->calculateAdults($extraAdults, $complex->is_percentage, $pricePerDay, $extraBedValue);

            } else if ($adults == 0) {
//                ONLY CHILDS

                $tempExtraChilds = ($childs - $beds);

                $priceToReturn = $pricePerDay + $this->calculateChildDiscount($tempExtraChilds, $complex->is_percentage, $pricePerDay, $extraBedValue);

            } else {

                if ($beds - $adults > 0) {
                    $freeBeds = $beds - $adults;

                    $extraChild = $childs - $freeBeds;

                    $priceToReturn = $pricePerDay + $this->calculateChildDiscount($extraChild, $complex->is_percentage, $pricePerDay, $extraBedValue);
                } else if ($beds - $adults < 0) {
//                    ONE EXTRA ADULT + CHILDS
                    $extraAdults = $adults - $beds;

                    $priceExtraAdults = $this->calculateAdults($extraAdults, $complex->is_percentage, $pricePerDay, $extraBedValue);

                    $priceExtraChild = $this->calculateChildDiscount($childs, $complex->is_percentage, $pricePerDay, $extraBedValue);

                    $priceToReturn = $pricePerDay + $priceExtraAdults + $priceExtraChild;
                } else {
//                    BEDS - ADULTS == 0
                    $priceToReturn = $pricePerDay + $this->calculateChildDiscount($childs, $complex->is_percentage, $pricePerDay, $extraBedValue);
                }

            }


        } else {
            return 'tooMany';
        }


        return $priceToReturn;
    }

    private function calculateChildDiscount($childs, $is_percentage, $pricePerRoom, $extraBedValue)
    {

        if ($childs == 1) {
            return $priceToReturn = 0;
        }
        $countChildDiscount = $childs / 2;

        if ($is_percentage == false) {
            $priceToReturn = $countChildDiscount * $extraBedValue;
        } else {
            $priceToReturn = (($extraBedValue / 100 * $pricePerRoom) * $countChildDiscount);
        }
        return $priceToReturn;
    }

    private function calculateAdults($adults, $is_percentage, $pricePerRoom, $extraBedValue)
    {

        if ($is_percentage == false) {
            $priceToReturn = $adults * $extraBedValue;
        } else {
            $priceToReturn = (($extraBedValue / 100 * $pricePerRoom) * $adults);
        }
        return $priceToReturn;
    }


    public function checkForEmptyRows($data)
    {
        foreach ($data as $room_id => $oneRoomType) {
            foreach ($oneRoomType as $rowKey => $oneRow) {
                $childs = $oneRow['child'];
                $adults = $oneRow['adult'];

                if ($childs == 0 && $adults == 0) {
                    unset($data[$room_id][$rowKey]);
                    if (count($data[$room_id]) == 0) {
                        unset($data[$room_id]);
                    }
                    continue;
                }

            }
        }

        return $data;
    }


}
