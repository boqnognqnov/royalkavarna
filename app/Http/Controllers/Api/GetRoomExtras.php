<?php namespace App\Http\Controllers\Api;

use App\Classes\GlobalFunctions;
use App\Complex;
use App\ComplexRooms;
use App\EarlyBooking;
use App\Extras;
use App\RoomExtras;
use App\RoomOccupancy;
use Illuminate\Http\Request;

use App\Reservations;
use App\ReservedRooms;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class GetRoomExtras extends Controller
{
    public function getExtras()
    {
        $data = Input::all();

        $roomId = $data['id'];

        $extrasByRoom = [];
        $allExtras = Extras::all()->toArray();

//        $extrasByRoom[$roomId] = $this->initioalCheckBoxes($roomId, $allExtras);
        $extrasByRoom = $this->initioalCheckBoxes($roomId, $allExtras);


        return \Response::json(array(
            'status' => 'success',
            'data' => $extrasByRoom
        ), 200);


    }

    private function initioalCheckBoxes($roomId, $allExtras)
    {
        $extraArrToReturn = [];
        foreach ($allExtras as $oneExtra) {
            $tempExtra = RoomExtras::where('room_id', '=', $roomId)->where('extra_id', '=', $oneExtra['id'])->get()->toArray();

            $extraArrToReturn[$oneExtra['id']]['label'] = $oneExtra['name_bg'];
            if (count($tempExtra) > 0) {
                $extraArrToReturn[$oneExtra['id']]['checked'] = true;
            } else {
                $extraArrToReturn[$oneExtra['id']]['checked'] = false;
            }
        }
        return $extraArrToReturn;
    }


}
