<?php namespace App\Http\Controllers;

use App\Complex;
use App\ComplexRooms;
use App\Extras;
use App\RoomExtras;
use App\RoomOccupancy;
use Illuminate\Http\Request;
use App\Slider;

class ComplexRoomController extends Controller
{


    // Route::get('/admin/complexes/{complex_id}/rooms', 'ComplexRoomController@index');
    public function index($complex_id)
    {
        $complexRooms = ComplexRooms::where('complex_id', '=', $complex_id)->orderBy('id', 'desc')->get();
        $complex = Complex::find($complex_id);

        $reserved_rooms = [];

        foreach ($complexRooms as $complexRoom) {
            $reserved_rooms[$complexRoom->id] = RoomOccupancy::countReservedRoomsForDate($complexRoom->id, date("Y-m-d", strtotime('now')));

        }
//        return dump($extrasByRoom);


        return view('admin.list.complexRooms', [
            'complexRooms' => $complexRooms,
            'complex' => $complex,
            'reserved_rooms' => $reserved_rooms
        ]);
    }


    // Route::get('/admin/complexes/{complex_id}/rooms/create', 'ComplexRoomController@create');
    public function create($complex_id)
    {
        $complex = Complex::find($complex_id);

        return view('admin.create.room', ['complex' => $complex]);
    }

    // Route::post('/admin/complexes/{complex_id}/rooms/create', 'ComplexRoomController@store');
    public function store(Request $requests)
    {
        $inputData = $requests->all();

        $tryCreateRoom = ComplexRooms::createRoom($inputData);

        switch ($tryCreateRoom[0]) {
            case 'validatorError':
                return back()->withInput()->withErrors($tryCreateRoom[1]);
            case 'databaseError':
                return back()->withInput()->withErrors('Данните не бяха добавени успешно, моля опитайте пак.');
            case 'successMessage':
                return redirect("/admin/complexes/" . $tryCreateRoom[1]->complex_id . "/rooms")->with('successMessage', 'Данните са добавени успешно.');
            default:
                return back()->withInput()->withErrors('Проблем, моля опитайте по късно!');
        }
    }

    // Route::get('/admin/complexes/{complex_id}/rooms/{room_id}/edit', 'ComplexRoomController@edit');
    public function edit($complex_id, $room_id)
    {
        $room = ComplexRooms::find($room_id);
        $complex = Complex::find($complex_id);

        return view('admin.update.room', [
            'room' => $room,
            'complex' => $complex
        ]);
    }

    // Route::post('/admin/complexes/{complex_id}/rooms/{room_id}/edit', 'ComplexRoomController@edit');
    public function update(Request $requests)
    {
        $inputData = $requests->all();

        $tryCreateRoom = ComplexRooms::updateRoom($inputData);

        switch ($tryCreateRoom[0]) {
            case 'validatorError':
                return back()->withInput()->withErrors($tryCreateRoom[1]);
            case 'databaseError':
                return back()->withInput()->withErrors('Данните не бяха редактирани успешно, моля опитайте пак.');
            case 'successMessage':
                return redirect('/admin/complexes/' . $tryCreateRoom[1]->complex_id . '/rooms')->with('successMessage', 'Данните са редактирани успешно.');
            default:
                return back()->withInput()->withErrors('Проблем, моля опитай те по късно!');
        }
    }


    // Route::post('/admin/complexes/{complex_id}/rooms/{room_id}/destroy', 'ComplexRoomController@edit');
    public function destroy(Request $request)
    {
        $inputData = $request->get('room_id');

        $tryDeleteRoom = ComplexRooms::deleteRoom($inputData);

        switch ($tryDeleteRoom) {
            case 'databaseError':
                return back()->withInput()->withErrors('Проблем, моля опитай те по късно! DB error : 93');
            case 'successMessage':
                return redirect()->back()->with('successMessage', 'Данните са изтрити успешно!');
            default:
                return back()->withInput()->withErrors('Проблем, моля опитай те по късно!');
        }
    }

    public function updateRoomExtras(Request $request)
    {
        $data = $request->all();
        $roomId = $data['room_id'];

        try {
            $oldRoomExtras = RoomExtras::where('room_id', '=', $roomId)->get();
            foreach ($oldRoomExtras as $oneExtra) {
                $oneExtra->delete();
            }

            if (isset($data['check_boxes'])) {
                $checkBoxesArr = $data['check_boxes'];

                foreach ($checkBoxesArr as $oneCheck) {
                    $tempExtra = new RoomExtras();
                    $tempExtra->room_id = $roomId;
                    $tempExtra->extra_id = $oneCheck;

                    $tempExtra->save();
                }
            }

        } catch (\Exception $ex) {
            return array('databaseError', $ex);
        }
        return redirect()->back()->with('successSetExtras', $roomId);
    }
}
