<?php namespace App\Http\Controllers;

    use App\ComplexRooms;
    use App\RoomPrices;
    use Illuminate\Http\Request;
    use App\Http\Requests;

    class RoomsPricesEntityController extends Controller {



        public function index($complex_id, $room_id) {

            $pricesData = RoomPrices::Where('room_id', $room_id)
                ->orderBy('from_date', 'ASC')
                ->get([
                    'room_prices.*'
                ]);

            $room = ComplexRooms::find($room_id);
            $complex = ComplexRooms::find($complex_id);

            return view( 'admin.list.prices', [
                'pricesData' => $pricesData,
                'room' => $room,
                'complex' => $complex
            ] );

        }

        public function create() {
            return view( 'admin.create.prices' );
        }

        public function store( Request $request ) {

            $inputData = $request -> all();

            $tryCreatePrice = RoomPrices::createPrice( $inputData );

            switch( $tryCreatePrice[0] ) {
                case 'validatorError':
                    return back() -> withInput() -> withErrors( $tryCreatePrice[1] );
                case 'databaseError':
                    return back() -> withInput() -> withErrors( 'Данните не бяха добавени успешно, моля опитайте пак.' );
                case 'successMessage':
                    return redirect() -> back() -> with( 'successMessage', 'Данните са добавени успешно.' );
                default:
                    return back() -> withInput() -> withErrors( 'Проблем, моля опитайте по късно!' );
            }

        }

        public function edit( $priceId ) {


            $pricesData = RoomPrices::find($priceId);
            $complexId=ComplexRooms::find($pricesData->room_id)->complex_id;


            return view( 'admin.update.prices', [
                'pricesData' => $pricesData,
                'complexId'=>$complexId
            ] );

        }

        public function update( Request $request ) {

            $inputData = $request -> all();

            $tryCreatePrice = RoomPrices::updatePrice( $inputData );

            switch( $tryCreatePrice[0] ) {
                case 'validatorError':
                    return back() -> withInput() -> withErrors( $tryCreatePrice[1] );
                case 'databaseError':
                    return back() -> withInput() -> withErrors( 'Данните не бяха редактирани успешно, моля опитайте пак.' );
                case 'successMessage':
                    $room = ComplexRooms::find($tryCreatePrice[1]->room_id);
                    return redirect("/admin/complexes/$room->complex_id/rooms/$room->id/prices") -> with( 'successMessage', 'Данните са редактирани успешно.' );
                default:
                    return back() -> withInput() -> withErrors( 'Проблем, моля опитай те по късно!' );
            }

        }

        public function destroy( $id ) {

            $inputData = $id;

            $tryDeletePrice = RoomPrices::deletePrice( $inputData );

            switch ( $tryDeletePrice ) {
                case 'databaseError':
                    return back() -> withInput() -> withErrors( 'Проблем, моля опитай те по късно!' );
                case 'successMessage':
                    return redirect() -> back() -> with( 'successMessage', 'Данните са изтрити успешно!' );
                default:
                    return back() -> withInput() -> withErrors( 'Проблем, моля опитайте по късно!' );
            }

        }

    }
