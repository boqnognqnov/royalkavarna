<?php

namespace App\Http\Controllers;

use App\Http\Requests;

class HomeEntityController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        return view('admin.index');
    }

}
