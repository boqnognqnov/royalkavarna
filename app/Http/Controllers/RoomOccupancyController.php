<?php namespace App\Http\Controllers;

use App\Complex;
use App\ComplexRooms;
use App\RoomOccupancy;
use Illuminate\Http\Request;
use App\Slider;

class RoomOccupancyController extends Controller {

    public function __construct()
    {
        $this -> middleware( 'auth' );
    }

    // Route::get('/admin/complexes/{complex_id}/rooms/{room_id}/occupancies', 'RoomOccupancyController@index');
    public function index($complex_id, $room_id)
    {
        $complexRoom = ComplexRooms::find($room_id);
        $complex = Complex::find($complexRoom->complex_id);

        $occupancies = RoomOccupancy::where('room_id', '=', $room_id)
            ->whereRaw('date_to > CURDATE()')->orderBy('date_from')->get();

        return view( 'admin.list.reservations', [
            'complexRoom' => $complexRoom,
            'complex' => $complex,
            'occupancies' => $occupancies,
            'isEditable' => true
        ]);
    }
    // Route::get('/admin/complexes/{complex_id}/rooms/{room_id}/occupancies/archive', 'RoomOccupancyController@index');
    public function archive($complex_id, $room_id)
    {
        $complexRoom = ComplexRooms::find($room_id);
        $complex = Complex::find($complexRoom->complex_id);

        $occupancies = RoomOccupancy::where('room_id', '=', $room_id)
            ->whereRaw('date_to <= CURDATE()')->orderBy('date_from')->get();

        return view( 'admin.list.reservations', [
            'complexRoom' => $complexRoom,
            'complex' => $complex,
            'occupancies' => $occupancies,
            'isEditable' => false
        ]);
    }

    // Modal at /admin/complexes/{complex_id}/rooms
    public function create($complex_id)
    {
        //
    }

    // Route::post('/admin/reserve/room', 'RoomOccupancyController@store');
    public function store( Request  $requests )
    {
        $inputData = $requests -> all();


        $tryAddRoomOccupancy = RoomOccupancy::createRoomOccupancy( $inputData );

        switch( $tryAddRoomOccupancy[0] ) {
            case 'validatorError':
                return back() -> withInput() -> withErrors( $tryAddRoomOccupancy[1] );
            case 'databaseError':
                return back() -> withInput() -> withErrors( 'Данните не бяха добавени успешно, моля опитайте пак.' );
            case 'dateError':
                return back() -> withInput() -> withErrors( [$tryAddRoomOccupancy[1]] );
            case 'successMessage':
                return redirect()-> back() -> with( 'successMessage', 'Данните са добавени успешно.' );
            case 'notEnoughRooms':
                return back() -> withInput() -> withErrors( 'Няма толкова свободни стаи за този период от време' );
            default:
                return back() -> withInput() -> withErrors( 'Проблем, моля опитайте по късно!' );
        }
    }

    // Modal at /admin/complexes/{complex_id}/rooms/{room_id}/occupancies
    public function edit( $complex_id, $room_id )
    {
        //
    }

    // Route::post('/admin/reserve/room/update', 'RoomOccupancyController@update');
    public function update( Request  $requests )
    {
        $inputData = $requests -> all();

        $tryUpdateRoomOccupancy = RoomOccupancy::updateRoomOccupancy( $inputData );

        switch( $tryUpdateRoomOccupancy[0] ) {
            case 'validatorError':
                return back() -> withInput() -> withErrors( $tryUpdateRoomOccupancy[1] );
            case 'databaseError':
                return back() -> withInput() -> withErrors( 'Данните не бяха редактирани успешно, моля опитайте пак.' );
            case 'dateError':
                return back() -> withInput() -> withErrors( [$tryUpdateRoomOccupancy[1]] );
            case 'successMessage':
                return redirect() -> back() -> with( 'successMessage', 'Данните са редактирани успешно.' );
            case 'notEnoughRooms':
                return back() -> withInput() -> withErrors( 'Няма толкова свободни стаи за този период от време' );
            default:
                return back() -> withInput() -> withErrors( 'Проблем, моля опитай те по късно!' );
        }
    }


    // Route::post('/admin/reserve/room/update/destroy', 'RoomOccupancyController@destroy');
    public function destroy( Request $request )
    {
        $id = $request -> get('occupancy_room_id');

        $tryDeleteRoomOccupancy = RoomOccupancy::deleteRoomOccupancy( $id );

        switch ( $tryDeleteRoomOccupancy ) {
            case 'databaseError':
                return back() -> withInput() -> withErrors( 'Проблем, моля опитай те по късно! DB error : 95' );
            case 'successMessage':
                return redirect() -> back() -> with( 'successMessage', 'Данните са изтрити успешно!' );
            default:
                return back() -> withInput() -> withErrors( 'Проблем, моля опитай те по късно!' );
        }
    }
}