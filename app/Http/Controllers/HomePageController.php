<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Slider;

class HomePageController extends Controller
{

    public function index()
    {
        $sliders = Slider::where('is_active', '=', 1)->get()->toArray();

        return view('index', ['sliders' => $sliders]);
    }

    public function about()
    {

        return view('about');
    }

}
