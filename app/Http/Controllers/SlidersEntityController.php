<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Slider;
use Illuminate\Http\Request;

class SlidersEntityController extends Controller
{


    public function indexAdmin()
    {

        $sliders = Slider::orderBy('id', 'desc')->get();
        return view('admin.list.sliders', [
            'sliders' => $sliders
        ]);
    }

    public function createSlider()
    {
        return view('admin.create.sliders');
    }

    public function editSlider($slider_id)
    {
//        return dump($slider_id);
        $slider = Slider::find($slider_id);
        return view('admin.update.sliders', ['slider' => $slider]);
    }


    public function store(Request $requests)
    {

        $data = $requests->all();
//        return dump($data);

        $status = Slider::createSlide($data);

        switch ($status[0]) {
            case 'validatorError':
                return back()->withInput()->withErrors($status[1]);
            case 'databaseError':
                return back()->withInput()->withErrors('Грешка от страна на сървъра, моля опитай те по късно!');
            case 'successMessage':
                return redirect('admin/sliders')->with('successMessage', 'Данните са добавени успешно.');
            default:
                return back()->withInput()->withErrors('Грешка от страна на сървъра, моля опитай те по късно!');
        }

    }


    public function update(Request $requests)
    {

        $data = $requests->all();

        $status = Slider::updateSlide($data);
//        return dump($status);

        switch ($status[0]) {
            case 'validatorError':
                return back()->withInput()->withErrors($status[1]);
            case 'databaseError':
                return back()->withInput()->withErrors('Грешка от страна на сървъра, моля опитай те по късно!');
            case 'successMessage':
                return redirect('admin/sliders')->with('successMessage', 'Данните са променени успешно.');
            default:
                return back()->withInput()->withErrors('Грешка от страна на сървъра, моля опитай те по късно!');
        }

    }

    public function destroy(Request $request)
    {


        $id = $request->get('slider_id');


        $status = Slider::deleteSlide($id);

        switch ($status) {
            case 'databaseError':
                return back()->withInput()->withErrors('Грешка от страна на сървъра, моля опитайте по късно!');
            case 'successMessage':
                return redirect()->back()->with('successMessage', 'Данните са изтрити успешно!');
            default:
                return back()->withInput()->withErrors('Грешка от страна на сървъра, моля опитайте по късно!');
        }

    }

    public function setActivity(Request $request)
    {
        $slider_id = $request->get('slider_id');

        $status = Slider::setActivity($slider_id);
        switch ($status) {
            case 'error':
                return back()->withInput()->withErrors('Грешка от страна на сървъра, моля опитайте по късно!');
            default:
                return redirect()->back();
        }


    }

}
