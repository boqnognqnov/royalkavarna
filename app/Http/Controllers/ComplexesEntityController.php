<?php namespace App\Http\Controllers;

use App\ComplexGuestBook;
use App\ComplexPromotions;
use App\ComplexRooms;
use App\ComplexRoomTypeEntity;
use App\Facilities;
use Illuminate\Http\Request;
use App\Complex;
use App\ComplexGalery;
use App\Http\Requests;

class ComplexesEntityController extends Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        //ADMIN !!! ADMIN !!!
        $complexData = Complex::all();

        return view('admin.complexes', [
            'complexData' => $complexData
        ]);
    }

    public function pricelist($id)
    {
        $complex = Complex::find($id);
        if ($complex == null)
            return view('errors.404');

        $gallery = ComplexGalery::Where('complex_id', $id)->orderBy('position', 'ASC')->get();

//        $roomTypes = \DB::table('complex_room_types')
        $roomTypes = ComplexRoomTypeEntity::join('complexes', 'complexes.id', '=', 'complex_room_types.complex_id')
            ->leftjoin('room_prices', 'complex_room_types.id', '=', 'room_prices.room_id')
            ->where('complexes.id', $id)
            ->select(['complex_room_types.*', 'room_prices.price'])
            ->groupBy('complex_room_types.id')
            ->orderBy('room_prices.price', 'ASC')
            ->get()->toArray();

//        return dump($roomTypes);

        return view('prices', [
            'complex' => $complex,
            'gallery' => $gallery,
            'roomTypes' => $roomTypes]);
    }

    public function guestbook($id)
    {

        $complex = Complex::find($id);
        if ($complex == null)
            return view('errors.404');

        $gallery = ComplexGalery::Where('complex_id', $id)->get();

        $posts = ComplexGuestBook::where('complex_id', '=', $id)->where('is_approved', '=', 1)->orderBy('id', 'DESC')->get();

        $validationLang['chars_left'] = trans('guestBook.chars_left');
        $validationLang['tooManyChars'] = trans('guestBook.tooManyChars');


        return view('guestbook', [
            'posts' => $posts,
            'complex' => $complex,
            'gallery' => $gallery,
            'validations' => $validationLang

        ]);
    }

    public function promotions($id)
    {

        $complex = Complex::find($id);
        if ($complex == null)
            return view('errors.404');

        $gallery = ComplexGalery::Where('complex_id', $id)->get();

        $promotions = ComplexPromotions::where('complex_id', '=', $id)->get()->toArray();

//        return dump($promotions);


        return view('promotions', [
            'promotions' => $promotions,
            'complex' => $complex,
            'gallery' => $gallery]);


    }


    public function create()
    {
        return view('admin.create.complexes');
    }

    public function store(Request $requests)
    {

        $inputData = $requests->all();

        $tryCreateComplex = Complex::createComplex($inputData);

        switch ($tryCreateComplex[0]) {
            case 'validatorError':
                return back()->withInput()->withErrors($tryCreateComplex[1]);
            case 'databaseError':
                return back()->withInput()->withErrors('Данните не бяха добавени успешно, моля опитайте пак.');
            case 'successMessage':
                return redirect('/admin/complexes')->with('successMessage', 'Данните са добавени успешно.');
            default:
                return back()->withInput()->withErrors('Проблем, моля опитай те по късно!');
        }

    }

    public function show($id)
    {
        $complex = Complex::find($id);
        if ($complex == null)
            return view('errors.404');

        $gallery = ComplexGalery::Where('complex_id', $id)->orderBy('position', 'ASC')->get();
        $facilities = Facilities::Where('complex_id', $id)->get();
//        return dump($facilities);

        return view('complex', [
            'complex' => $complex,
            'gallery' => $gallery,
            'facilities' => $facilities]);
    }

    public function edit($id)
    {

        $complexData = Complex::where('id', '=', $id)->first();

        if (!$complexData) {
            return view('errors.404');
        }

        return view('admin.update.complexes', [
            'complexData' => $complexData
        ]);

    }

    public function update(Request $request)
    {

        $inputData = $request->all();

        $tryUpdateComplex = Complex::updateComplex($inputData);

        switch ($tryUpdateComplex[0]) {
            case 'validatorError':
                return back()->withInput()->withErrors($tryUpdateComplex[1]);
            case 'databaseError':
                return back()->withInput()->withErrors('Данните не бяха редактирани успешно, моля опитайте пак.');
            case 'successMessage':
                return redirect('/admin/complexes')->with('successMessage', 'Данните са редактирани успешно.');
            default:
                return back()->withInput()->withErrors('Проблем, моля опитай те по късно!');
        }

    }

    public function destroy(Request $request)
    {

        $inputData = $request->get('id');

        $tryDeleteComplex = Complex::deleteComplex($inputData);

        switch ($tryDeleteComplex) {
            case 'databaseError':
                return back()->withInput()->withErrors('Проблем, моля опитай те по късно!');
            case 'successMessage':
                return redirect()->back()->with('successMessage', 'Данните са изтрити успешно!');
            default:
                return back()->withInput()->withErrors('Проблем, моля опитай те по късно!');
        }

    }

    public function complexGallery($complex_id)
    {

        $galleryData = ComplexGalery::where('complex_id', '=', $complex_id)->orderBy('position', 'asc')->get();
        return view('admin.list.complexes_gallery', [
            'galleryData' => $galleryData, 'complex_id' => $complex_id
        ]);

    }

    public function setImageToGallery(Request $request)
    {

        $data = $request->all();
        $status = ComplexGalery::setImage($data);
//        return dump($status);
        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'creatingError':
                return back()->withInput()->withErrors('Грешка в записа на сървъра');
            case 'imageFailed':
                return back()->withInput()->withErrors('Грешка в записа на изображението');
            case 'successMessage':
                return redirect()->back()->with('successMessage', 'Информацията записана успешно');

        }

    }

    public function destroyImage(Request $request)
    {
        $status = ComplexGalery::destroyImage($request->get('image_id'));

        if ($status['status'] == 'success') {
            return redirect()->back()->with('successMessage', 'Информацията изтрита успешно');
        }
        return back()->withErrors('Грешка при изтриване на изображението');
    }

    public function moveImageRight($complexId, $imageId)
    {

        $max = ComplexGalery::orderBy('position', 'DESC')->get()->first()->position;

        $currentRecord = ComplexGalery::find($imageId);
        $currentPosition = $currentRecord->position;
        if ($max > $currentPosition) {
            $downRecord = null;
            $counter = $currentPosition + 1;
            do {

                $downRecord = ComplexGalery::where('complex_id', '=', $complexId)->where('position', '=', $counter)->get()->first();
                if ($downRecord == null) {
                    $counter++;
                }

            } while ($downRecord == null);

            $currentRecord->position = $counter;
            $currentRecord->save();
            $downRecord->position = $currentPosition;
            $downRecord->save();
        }

        return redirect()->back();
    }

    public function moveImageLeft($complexId, $imageId)
    {

        $min = ComplexGalery::orderBy('position', 'ASC')->get()->first()->position;

        $currentRecord = ComplexGalery::find($imageId);
        $currentPosition = $currentRecord->position;
        if ($min < $currentPosition) {
            $downRecord = null;
            $counter = $currentPosition - 1;
            do {

                $downRecord = ComplexGalery::where('complex_id', '=', $complexId)->where('position', '=', $counter)->get()->first();
                if ($downRecord == null) {
                    $counter--;
                }

            } while ($downRecord == null);

            $currentRecord->position = $counter;
            $currentRecord->save();
            $downRecord->position = $currentPosition;
            $downRecord->save();
        }

        return redirect()->back();
    }

}
