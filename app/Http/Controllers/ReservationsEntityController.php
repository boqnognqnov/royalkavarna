<?php

namespace App\Http\Controllers;

use App\About;
use App\ComplexRooms;
use App\Reservations;
use App\ReservationStatuses;
use App\ReservedRooms;
use App\RoomPrices;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Complex;

use App\Classes\GlobalFunctions;

class ReservationsEntityController extends Controller
{


    public function indexAdminListComplex()
    {
        \Session::put('new_reservation');
        $complexes = Complex::all();
        return view('admin.list.client-reservation-list-complexes', ['complexes' => $complexes]);

    }

    public function indexAdminByComplex($complexId = 0)
    {
        $statusList = ReservationStatuses::all();
        $statuses = [];
        foreach ($statusList as $status) {
            $statuses[$status->id] = $status->status;
        }

        $complex = null;
        $reservations = Reservations::orderBy('date_from', 'ASC')
            ->whereRaw('`date_to` >= CURDATE()');

        if ($complexId != 0) {
            $reservations = $reservations->where('complex_id', '=', $complexId);
            $complex = Complex::find($complexId);
        }

        $reservations = $reservations->get();

        return view('admin.list.client-reservations-one-complex', [
            'reservations' => $reservations,
            'statuses' => $statuses,
            'complex' => $complex,
            'isEditable' => true
        ]);
    }

    public function oldReservations($complexId = 0)
    {
        $statusList = ReservationStatuses::all();
        $statuses = [];
        foreach ($statusList as $status) {
            $statuses[$status->id] = $status->status;
        }

        $complex = null;
        $reservations = Reservations::orderBy('date_from', 'ASC')
            ->whereRaw('`date_to` < CURDATE()');

        if ($complexId != 0) {
            $reservations = $reservations->where('complex_id', '=', $complexId);
            $complex = Complex::find($complexId);
        }

        $reservations = $reservations->get();

        return view('admin.list.client-reservations-one-complex', [
            'reservations' => $reservations,
            'statuses' => $statuses,
            'complex' => $complex,
            'isEditable' => false
        ]);
    }

    public function getAllComplexes()
    {
        $complexes = Complex::all();
        return view('reservation-list-complexes', ['complexes' => $complexes]);
    }


    public function selectComplex($complex_id, $roomId = 0)
    {
        if ($roomId > 0) {
            \Session::put('selected_room', $roomId);

            $this->step3addRowEventEvent($roomId);
        } else {
            \Session::forget('selected_room');
        }


        $session = \Session::get('new_reservation');

        if (isset($session['complex_id'])) {
            if ($session['complex_id'] != $complex_id) {
                unset($session['date_from'], $session['date_to'], $session['rooms_data']);
            }
        }

        $oldDateRange = [];
        if (isset($session['date_from']) && isset($session['date_to'])) {
            $oldDateRange['date_from'] = $session['date_from'];
            $oldDateRange['date_to'] = $session['date_to'];
        }

        $session['complex_id'] = $complex_id;

        \Session::put('new_reservation', $session);

        /***********GET RANGE WITH PROVIDED SERVICES*********/
        $availableDateRange = RoomPrices::join('complex_room_types', 'complex_room_types.id', '=', 'room_prices.room_id')
            ->Where('complex_room_types.complex_id', $complex_id)
            ->orderBy('from_date', 'ASC')->take(1)
            ->select([
                \DB::raw('from_date - interval 1 day as from_date'),
                \DB::raw('to_date + interval 2 day as to_date')
            ])->unionAll(RoomPrices::join('complex_room_types', 'complex_room_types.id', '=', 'room_prices.room_id')
                ->Where('complex_room_types.complex_id', $complex_id)
                ->orderBy('from_date', 'DESC')->take(1)
                ->select([
                    \DB::raw('from_date - interval 1 day as from_date'),
                    \DB::raw('to_date + interval 2 day as to_date')
                ])->getQuery())
            ->get();

        $startDate = $availableDateRange[0]->from_date;
        $endDate = $availableDateRange[1]->to_date;
        /***********GET RANGE WITH PROVIDED SERVICES*********/


        return view('reservation-select-date', [
            'complex_id' => $complex_id,
            'oldDateData' => $oldDateRange,
            'startDate' => $startDate,
            'endDate' => $endDate]);
    }


    public function setDateRange($redirectOnNav, $complex_id, $date_from, $date_to)
    {

        $session = \Session::get('new_reservation');


        $data['date_from'] = str_replace('-', '/', $date_from);
        $data['date_to'] = str_replace('-', '/', $date_to);
        $data['complex_id'] = $complex_id;

        if ($redirectOnNav == 0) {
            if (isset($session['date_from']) && isset($session['date_to'])) {
                if (($session['date_from'] != $data['date_from']) || ($session['date_to'] != $data['date_to'])) {
                    unset($session['rooms_data']);
                }
            }
        }


//        DATE VALIDATION

        if (!preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $data['date_from'])) {
            return redirect()->back()->withErrors(trans('reservationValidation.anyDateInputIsEmpty'));
        }

        if (!preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $data['date_to'])) {
            return redirect()->back()->withErrors(trans('reservationValidation.anyDateInputIsEmpty'));
        }


        $gb = new GlobalFunctions();
        $date_from = $gb->changeDateFormat($data['date_from']);
        $date_to = $gb->changeDateFormat($data['date_to']);

        $date_now = date('d-m-Y', strtotime('now'));
        $date_now = new \DateTime($date_now);

        if ($date_now > $date_from) {
            return redirect()->back()->withErrors(trans('reservationValidation.dateIsPased'));
        }

        if ($date_from >= $date_to) {
            return redirect()->back()->withErrors(trans('reservationValidation.firstDateIsBigger'));
        }

        $session['complex_id'] = $data['complex_id'];
        $session['date_from'] = $data['date_from'];
        $session['date_to'] = $data['date_to'];

        \Session::put('new_reservation', $session);


        $roomTypes = ComplexRooms::leftjoin('room_prices', 'room_prices.room_id', '=', 'complex_room_types.id')
            ->where('complex_room_types.complex_id', $data['complex_id'])
            ->whereRaw("room_prices.from_date <= STR_TO_DATE('" . $data['date_from'] . "', '%m/%d/%Y')")
            ->whereRaw("room_prices.to_date >= STR_TO_DATE('" . $data['date_to'] . "', '%m/%d/%Y') - interval 1 day")
            ->select(['complex_room_types.*', 'room_prices.price'])
            ->groupBy('complex_room_types.id')
            ->get();


        if (sizeof($roomTypes) == 0)
            return redirect()->back()->withErrors(trans('reservationValidation.NoPricesForRooms'));

        $availableRooms = GlobalFunctions::getFreeRooms($roomTypes, $data['date_from'], $data['date_to']);

        $complexInseparable = Complex::find($session['complex_id'])->is_inseparable;
        $data['is_inseparable'] = $complexInseparable;
        $roomTypes = $roomTypes->toArray();
        return view('reservation-list-rooms', ['reservationData' => $data, 'roomTypes' => $roomTypes, 'availableRooms' => $availableRooms]);

    }

    public function getPersonalData($redirectType = '')
    {
        $session = \Session::get('new_reservation');


        if ($redirectType == 'organic') {
            if (empty($session['rooms_data'])) {
                return back()->withErrors(trans('reservationValidation.weCantRemoveAllRoomAndContinue'));
            }
        }

        $status = GlobalFunctions::redirectBysession($session, 'personalFormular');
        if ($status != '') {
            return redirect($status);
        }

        $formularSession = [];

        if (isset($session['personalData'])) {
            $formularSession = $session['personalData'];
        }


        return view('reservation-personal-data', ['data' => $formularSession]);
    }

    public function setPersonalData(Request $request)
    {
        $data = $request->all();


        $reservationData = \Session::get('new_reservation');
        $reservationData['personalData'] = $data;


        $validationStatus = Reservations::validateNewReservation($reservationData['personalData']);

        if ($validationStatus != 'success') {
            return back()->withInput()->withErrors($validationStatus[1]);
        }


        \Session::put('new_reservation', $reservationData);


        return redirect('reservation/confirm');


    }

    public function showReservation(Request $request)
    {
        $hasEarlyBooking = '';
        $reservationData = $request->session()->get('new_reservation');
        $personalData = $request->session()->get('new_reservation.personalData');
        $getComplexData = Complex::where('id', '=', $reservationData['complex_id'])->first();

        if (isset($reservationData['early_booking'])) {
            $hasEarlyBooking = $reservationData['early_booking'];
        }

        return view('reservation-confirm', [
            'reservationData' => $reservationData,
            'personalData' => $personalData,
            'getComplexData' => $getComplexData,
            'earlyBooking' => $hasEarlyBooking
//            'roomData' => $roomData,
//            'roomInfo' => $roomInfo,
//            'roomid' => $roomid
        ]);

    }


    public function confirmReservation()
    {
        $data = \Session::get('new_reservation');
//        return dump($data);

        if (empty($data)) {
            return redirect('/');
        }

        $dataToReservationModel = $data;
        unset($dataToReservationModel['rooms_data'], $dataToReservationModel['personalData']);
        foreach ($data['personalData'] as $propKey => $onePersonalProperty) {
            $dataToReservationModel[$propKey] = $onePersonalProperty;
        }

        \DB::beginTransaction();
        try {
            $reservationStatus = Reservations::newReservation($dataToReservationModel);
            if ($reservationStatus[0] == 'creatingError') {
                return back()->withErrors('error', 'error');
            }
            $reservationId = $reservationStatus[1]->id;
            foreach ($data['rooms_data'] as $roomId => $oneRoom) {
                foreach ($oneRoom as $row) {
                    $row['extra_bets'] = $row['extra'];
                    unset($row['extra']);

                    $row['reservation_id'] = $reservationId;
                    $row['room_id'] = $roomId;
                    ReservedRooms::addReserveRoom($row);
                }
            }

        } catch (\Exception $e) {
            \DB::rollBack();

            throw $e;
        }

        \DB::commit();

        $this->sendMailByNewReservation($data);
        \Session::forget('new_reservation');

        return view('reservation-success');
    }

    private function sendMailByNewReservation($data)
    {
        try {

            $tempDate = explode('/', $data['date_from']);
            $data['date_from'] = $tempDate[1] . '/' . $tempDate[0] . '/' . $tempDate[2];
            $tempDate = explode('/', $data['date_to']);
            $data['date_to'] = $tempDate[1] . '/' . $tempDate[0] . '/' . $tempDate[2];

            $complex = Complex::find($data['complex_id']);

            $data['complex_name'] = $complex->name_bg;


            $emails['general'] = About::first()->contact_general_email;
            $emails['complexEmail'] = $complex->email;


            \Mail::send('emails.completeReservation', $data, function ($message) use ($data, $emails) {
                $message->from($data['personalData']['email'], $data['personalData']['firstname'] . ' ' . $data['personalData']['lastname']);
                $message->to($emails['general'], 'Info')->subject('Нова резервация от "резервационната форма" на Royal Estate');
                $message->bcc($emails['complexEmail'], 'Info')->subject('Нова резервация от "резервационната форма" на Royal Estate');

            });

        } catch (\Exception $ex) {

        }
    }

    public function statusAndMessage(Request $request)
    {
        $data = $request->all();

        $msg = '';

        $reservation_id = $data['reservation_id'];
        $reservation = Reservations::find($reservation_id);

        $status = $data['status'];
        if ($reservation->status_id != $status) {
            $msg .= 'Успешна промяна на статуса';
        }

        $reservation->status_id = $status;
        $reservation->save();

        try {
            if (!empty($data['text'] && isset($data['send_true']))) {
                $msg .= '; Успешно изпратено съобщение до клиента.';

                $data['email'] = $reservation->email;

                if (!empty($data['email'])) {
                    \Mail::send('emails.reservationFeedback', $data, function ($message) use ($data) {
                        $message->from($data['email'], 'Royal Estate');
                        $message->to('boqnognqnov@gmail.com', 'Info')->subject('Оставено съобщение от RoyalEstate');
                        $message->bcc('bozhidarovboyan@gmail.com', 'Info')->subject('Оставено съобщение от RoyalEstate');
                        $message->bcc('alarma7a@gmail.com', 'Info')->subject('Оставено съобщение от RoyalEstate');
                    });
                }
            }

        } catch (\Exception $ex) {
            \Log::error($ex);
            return back()->withErrors('Проблем, моля опитайте по късно!');
        }
        if ($msg == '') {
            $msg = 'Нямаше данни за промяна';
        }
        return redirect()->back()->with('successMessage', $msg);
    }

    public function deleteReservation(Request $request)
    {
        $reservation_id = $request->get('reservation_id');
        $status = Reservations::destroyReservation($reservation_id);
        switch ($status) {
            case 'destroyError':
                return back()->withErrors('Грешка при изтриването!');
            case 'success':
                return redirect()->back()->with('successMessage', 'Данните са изтрити успешно!');
            default:
                return back()->withInput()->withErrors('Грешка при изтриването!');
        }

    }

    public function adminShowUpdateForm($reservation_id)
    {
        $reservation = Reservations::find($reservation_id);
        $rooms = [];
        $reservedRoooms = ReservedRooms::where('reservation_id', '=', $reservation_id)->get();
        foreach ($reservedRoooms as $oneRoom) {
            $rooms[$oneRoom->room_id] = ComplexRooms::find($oneRoom->room_id)->type_bg;
        }
        $reservation->date_from = GlobalFunctions::generateDateTimeToStr($reservation->date_from);
        $reservation->date_to = GlobalFunctions::generateDateTimeToStr($reservation->date_to);
        $roomTypes = ComplexRooms::where('complex_id', '=', $reservation->complex_id)->get();
        $roomTypesArr = [];
        foreach ($roomTypes as $oneRoom) {
            $roomTypesArr[$oneRoom->id] = $oneRoom->type_bg . $oneRoom->type_sub_bg;
        }

        return view('admin.update.reservation', ['reservation' => $reservation,
            'reservedRoooms' => $reservedRoooms,
            'rooms' => $rooms,
            'room_types' => $roomTypesArr
        ]);

    }

    public function updatePersonalData(Request $request)
    {
        $data = $request->all();
        $status = Reservations::updatePersonalData($data);
        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'creatingError':
                return back()->withInput()->withErrors('Грешка !');
            case 'successMessage':
                return redirect()->back()->with('successMessage', 'Данните са променени успешно!');
            default:
                return back()->withInput()->withErrors('Грешка !');
        }
    }

    public function updateRoomsAndDate(Request $request)
    {
        $dataArr = $request->all();

//        return dump($dataArr);

        if (!isset($dataArr['data'])) {
            return back()->withInput()->withErrors('Не може да премахнете всички стаи.
              Ако желаете, може да изтриете цялата резервация чрез предходната стъпка');
        }

        foreach ($dataArr['data'] as $roomId => $oneRoomType) {
            foreach ($oneRoomType as $key => $oneRow) {
                $extraBedCounter = GlobalFunctions::getExtraBeds($roomId, $oneRow['child'], $oneRow['adult']);
                if ($extraBedCounter === 'tooMany') {
                    return back()->withInput()->withErrors('Надхвърляте капацитета на някоя от стаите');
                }
            }
        }

        $data = $dataArr['data'];
        unset($dataArr['data'], $dataArr['_token']);

        $gb = new GlobalFunctions();
        $dataToReturn = $gb->calculateReservationPriceByRow($data, true, $dataArr);

        $dataToRecord['reservation_id'] = $dataArr['reservation_id'];
        $dataToRecord['complex_id'] = $dataArr['complex_id'];
        $dataToRecord['date_from'] = $dataArr['date_from'];
        $dataToRecord['date_to'] = $dataArr['date_to'];
        $dataToRecord['rooms_data'] = $dataToReturn['data'];
        $dataToRecord['total'] = $dataToReturn['total'];


        $status = GlobalFunctions::updateRoomsAndDateReservation($dataToRecord);

        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'creatingError':
                return back()->withInput()->withErrors('Грешка от страна на сървъра');
            case 'successMessage':
//                return redirect('admin/reservations/complex/' . $dataToRecord['complex_id'])
//                    ->with('successMessage', 'Данните са променени успешно!');
                return redirect('admin/reservations/update/' . $dataArr['reservation_id'])
                    ->with('successMessage', 'Данните са променени успешно!');


            default:
                return back()->withInput()->withErrors('Грешка !');
        }


    }

    public function step3addRowEventEvent($roomId)
    {
        $data = \Session::get('new_reservation');
        if (isset($data['rooms_data'][$roomId])) {
            \Session::put('reservation_inputs_ankers', 'reservation_inputs_' . $roomId);
            return back();
        } else {
            $data['rooms_data'][$roomId][1]['adult'] = 1;
            \Session::put('new_reservation', $data);
            \Session::put('reservation_inputs_ankers', 'reservation_inputs_' . $roomId);
            return back();
        }

    }
}
