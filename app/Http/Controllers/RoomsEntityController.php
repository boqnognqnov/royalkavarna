<?php

namespace App\Http\Controllers;

use App\EarlyBooking;
use App\Http\Requests;
use App\RoomExtras;
use App\RoomGalery;
use App\ComplexRooms;
use App\RoomPrices;
use Illuminate\Http\Request;
use App\Complex;
use App\Classes\GlobalFunctions;


class RoomsEntityController extends Controller
{
    public function index($room_id)
    {
        $room = ComplexRooms::find($room_id)->toArray();
        $gallery = RoomGalery::where('room_id', '=', $room_id)->get();

        $extras = RoomExtras::where('room_id', '=', $room_id)->join('extras', 'extras.id', '=', 'room_extras.extra_id')->get()->toArray();
        $complex = Complex::find($room['complex_id'])->toArray();

        $prices = RoomPrices::where('room_id', '=', $room_id)->get()->toArray();
        $prices = $this->convertPricesData($prices);

        $now = new \DateTime();


        $rawEarlyBookings = EarlyBooking::where('complex_id', '=', $complex['id'])
            ->where('until_date', '>', $now)
            ->where('is_active', '=', 1)
            ->get()->toArray();


        $earlyBookings = [];
        foreach ($rawEarlyBookings as $key => $oneBooking) {
            $earlyBookings[$key]['percentage'] = $oneBooking['percentage'];
            $earlyBookings[$key]['until_date'] = GlobalFunctions::generateDateTimeToStr($oneBooking['until_date']);
        }

        return view('room', ['roomData' => $room, 'gallery' => $gallery,
            'extras' => $extras, 'complex' => $complex,
            'isRoomSlider' => true, 'prices' => $prices,
            'earlyBookings' => $earlyBookings,
            'room_id' => $room_id
        ]);

    }

    private function convertPricesData($prices)
    {
        $dateRange = [];
        foreach ($prices as $key => $onePrice) {

            if (isset($dateRange[$onePrice['price']])) {
                $arrayCount = count($dateRange[$onePrice['price']]);
            } else {
                $arrayCount = 0;
            }

            $dateRange[$onePrice['price']][$arrayCount]['from'] = GlobalFunctions::generateDateTimeToStr($onePrice['from_date']);
            $dateRange[$onePrice['price']][$arrayCount]['to'] = GlobalFunctions::generateDateTimeToStr($onePrice['to_date']);

        }

        return $dateRange;
    }


    public function indexAdmin($roomId)
    {
        $gallery = RoomGalery::where('room_id', '=', $roomId)->orderBy('position', 'ASC')->get()->toArray();

        $complexId = ComplexRooms::find($roomId)->complex_id;

        return view('admin.list.room_gallery', ['gallery' => $gallery, 'roomId' => $roomId, 'complexId' => $complexId]);
    }


    public function setImageToGallery(Request $request)
    {

        $data = $request->all();
        $status = RoomGalery::setImage($data);
//        return dump($status);
        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'creatingError':
                return back()->withInput()->withErrors('Грешка в записа на сървъра');
            case 'imageFailed':
                return back()->withInput()->withErrors('Грешка в записа на изображението');
            case 'successMessage':
                return redirect()->back()->with('successMessage', 'Информацията записана успешно');

        }

    }

    public function destroyImage(Request $request)
    {
        $status = RoomGalery::destroyImage($request->get('image_id'));

        if ($status['status'] == 'success') {
            return redirect()->back()->with('successMessage', 'Информацията изтрита успешно');
        }
        return back()->withErrors('Грешка при изтриване на изображението');
    }


    public function moveImageRight($roomId, $imageId)
    {

        $max = RoomGalery::orderBy('position', 'DESC')->get()->first()->position;

        $currentRecord = RoomGalery::find($imageId);
        $currentPosition = $currentRecord->position;
        if ($max > $currentPosition) {

            $downRecord = RoomGalery::where('room_id', '=', $roomId)->where('position', '>', $currentPosition)->orderBy('position', 'ASC')->first();

            if ($downRecord) {
                $currentRecord->position = $downRecord->position;
                $currentRecord->save();
                $downRecord->position = $currentPosition;
                $downRecord->save();
            }
        }

        return redirect()->back();
    }

    public function moveImageLeft($roomId, $imageId)
    {
        $min = RoomGalery::orderBy('position', 'ASC')->get()->first()->position;

        $currentRecord = RoomGalery::find($imageId);
        $currentPosition = $currentRecord->position;
        if ($min < $currentPosition) {

            $downRecord = RoomGalery::where('room_id', '=', $roomId)->where('position', '<', $currentPosition)->orderBy('position', 'DESC')->first();

            if ($downRecord) {
                $currentRecord->position = $downRecord->position;
                $currentRecord->save();
                $downRecord->position = $currentPosition;
                $downRecord->save();
            }

        }

        return redirect()->back();
    }


}
