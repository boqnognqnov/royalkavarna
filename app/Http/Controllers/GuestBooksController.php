<?php

namespace App\Http\Controllers;

use App\Complex;
use App\ComplexGuestBook;
use App\EarlyBooking;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class GuestBooksController extends Controller
{
    public function indexAdmin()
    {

        $complexes = Complex::all();

        $arrToReturn = [];
        foreach ($complexes as $oneComplex) {
            $arrToReturn[$oneComplex->id]['name'] = $oneComplex->name_bg;
            $arrToReturn[$oneComplex->id]['posts'] = ComplexGuestBook::where('complex_id', '=', $oneComplex->id)->orderBy('id','DESC')->get()->toArray();
        }


        return view('admin.list.guest-book', ['posts' => $arrToReturn]);

    }

    public function addPosts(Request $requests)
    {
        $data = $requests->all();
        $status = ComplexGuestBook::addPost($data);

//        return dump($status);

        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'creatingError':
                return back()->withErrors(trans('guestBook.error'));
            case 'successMessage':
                return redirect()->back()->with('successMessage', trans('guestBook.success'));
        }

    }

    public function approvePost(Request $requests)
    {
        $post_id = $requests->get('post_id');
        $status = ComplexGuestBook::approvePost($post_id);
//        return dump($status);

        switch ($status['status']) {
            case 'error':
                return back()->withErrors('Грешка от страна на сървъра');
            case 'success':
                return redirect()->back()->with('successMessage', 'Успешно променихте статуса на съобщението');
        }

    }

    public function deletePost(Request $requests)
    {
        $post_id = $requests->get('post_id');
        $status = ComplexGuestBook::deletePost($post_id);
        switch ($status['status']) {
            case 'error':
                return back()->withErrors('Грешка от страна на сървъра');
            case 'success':
                return redirect()->back()->with('successMessage', 'Успешно изтрихте мнението');
        }
    }


}
