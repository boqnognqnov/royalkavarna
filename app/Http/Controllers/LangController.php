<?php

namespace App\Http\Controllers;

use App\About;
use App\ComplexGalery;
use App\Extras;
use Illuminate\Support\Facades\Input;
//use App\Http\Requests;
use Illuminate\Http\Request;

class LangController extends Controller
{
    public function chaingeLang($locale)
    {

        \Session::put('locale', $locale);
        return redirect()->back();
    }

}
