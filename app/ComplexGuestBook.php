<?php

namespace App;

use App\Classes\ImageProccessing;
use Illuminate\Database\Eloquent\Model;
use Intervention\Image\ImageManagerStatic as Image;

/*
 * Schema::create('complex_guest_books', function (Blueprint $table) {
    $table->increments('id');
    $table->string('name');
    $table->string('email');
    $table->text('message');
    $table->string('image');
    $table->string('position');
    $table->boolean('is_approved');
    $table->timestamps();
});*/

class ComplexGuestBook extends Model
{
    protected $table = 'complex_guest_books';

    public function getComplex()
    {
        return $this->hasOne('App\Complex', 'id', 'complex_id');
    }


    public static $path = 'uploads/images/guest_books/';


    public static function addGuestBookPostsRules()
    {
        return array(
            'name' => 'required',
            'email' => 'required|email',
            'message' => 'required|max:500',
            'image' => 'image',
            'position' => 'required',
        );
    }

    public static function addGuestBookPostsMessages()
    {
        return [
            'name.required' => trans('guestBook.name_req'),
            'email.required' => trans('guestBook.email_req'),
            'email.email' => trans('guestBook.email_valid'),
            'message.required' => trans('guestBook.message_req'),
            'message.max' => trans('guestBook.message_valid'),
            'image.image' => trans('guestBook.image_valid'),
            'position.required' => trans('guestBook.position_req'),

        ];
    }

    public static function addPost($data)
    {
        $validator = \Validator::make($data, self::addGuestBookPostsRules(), self::addGuestBookPostsMessages());

        if ($validator->fails()) {
            return array('validationError', $validator);
        }


        try {

            $post = new ComplexGuestBook();
            if (isset($data['image'])) {
                $fileName = time() . '.' . $data['image']->getClientOriginalExtension();
                Image::make($data['image']->getRealPath())->resize(126, 126)->save(self::$path . $fileName);

//            Image::make($file->getRealPath())->resize($widthSize, $heightSize)->save($pathSmall . $fileName);
                $post->image = $fileName;
            }


            $post->name = $data['name'];
            $post->email = $data['email'];
            $post->message = $data['message'];
            $post->position = $data['position'];

//NEW UNTESTED ROW
            $post->complex_id = $data['complex_id'];
//NEW UNTESTED ROW

            $post->is_approved = false;

            $post->save();

        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('creatingError', $ex);
        }

        return array('successMessage', $post);
    }

    public static function approvePost($post_id)
    {

        try {
            $post = ComplexGuestBook::find($post_id);

            if ($post->is_approved == false) {
                $post->is_approved = true;
            } else {
                $post->is_approved = false;
            }

            $post->save();

        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('status' => 'error');
        }
        return array('status' => 'success');
    }

    public static function deletePost($post_id)
    {

        try {

            ImageProccessing::destroyImage($post_id, ComplexGuestBook::class);
            ComplexGuestBook::destroy($post_id);

        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('status' => 'error');
        }
        return array('status' => 'success');
    }
}
